<?PHP
require_once("./include/membersite_config.php");
$fgmembersite->block_login_page();
$emailsent = false;
if(isset($_POST['submitted']))
{ 
   if($fgmembersite->EmailResetPasswordLink())
   {
        $fgmembersite->RedirectToURL("reset-pwd-link-sent.php");
        exit;
   }
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US">
<head>
      <meta http-equiv='Content-Type' content='text/html; charset=utf-8'/>
      <title>Reset Password Request</title>
      <!--<link rel="STYLESHEET" type="text/css" href="style/fg_membersite.css" />-->
      <script type='text/javascript' src='scripts/gen_validatorv31.js'></script>
	    <link href="assets/css/style.css" rel="stylesheet" type="text/css" />
		<link href="css/custom.css" rel="stylesheet" type="text/css" />
</head>
<body class="widescreen">
<!-- Form Code Start -->
<div id='fg_membersite'>
<form id='resetreq' action='<?php echo $fgmembersite->GetSelfScript(); ?>' method='post' accept-charset='UTF-8'>
<div class="wrapper-page" >
<div class='account-bg'>
<div class="card-box m-b-0" >
<div class="text-xs-center m-t-20">
	<img src="../images/logo.png">
</div>
<div class="m-t-30 m-b-20">
<div class="col-xs-12 text-xs-center">
                            <h6 class="text-muted text-uppercase m-b-0 m-t-0">Reset Password</h6>
                        </div>
<input type='hidden' name='submitted' id='submitted' value='1'/>

<div class='short_explanation' style='float:right'><small></small></div>

<div style="text-align:center;"><small class='error'><?php echo $fgmembersite->GetErrorMessage(); ?></small></div>
<div class='container'>
  <br/>
    <input type='text' name='email' id='email' value='<?php echo $fgmembersite->SafeDisplay('email') ?>' maxlength="50" class="form-control" placeholder="Your email" /><br/>
    <div class="col-xs-12 text-xs-center"><small id='resetreq_email_errorloc' class='error'></small></div>
</div>
<div class='short_explanation' style='text-align:center;'><small>A link to reset your password will be sent to the email address</small></div>
<br>
<div class='container'><button type='button'  class='btn'  style="float:left"  onclick="window.location.href='shuru.php'" ><i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp;Back</button>
    <input type='submit' name='Submit' value='Submit' class='btn' /><br>
</div>
</div>
</div>
</div>
</div>
</form>
<!-- client-side Form Validations:
Uses the excellent form validation script from JavaScript-coder.com-->

<script type='text/javascript'>
// <![CDATA[

    var frmvalidator  = new Validator("resetreq");
    frmvalidator.EnableOnPageErrorDisplay();
    frmvalidator.EnableMsgsTogether();

    frmvalidator.addValidation("email","req","Please provide the email address used to sign-up");
    frmvalidator.addValidation("email","email","Please provide the email address used to sign-up");

// ]]>
</script>
<style>
#resetreq_email_errorloc{
	color:red;
}	
.error{
	color:red;
}
.resetreq_email_errorloc{
	font-size:80%;
	font-weight:normal;
}
.widescreen{
	   background-color: #2b3d51;
}
.btn{
	position: relative;
    cursor: pointer;
    display: inline-block;
    overflow: hidden;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    -webkit-tap-highlight-color: transparent;
    vertical-align: middle;
    z-index: 1;
    will-change: opacity, transform;
    -webkit-transition: all 0.3s ease-out;
    -moz-transition: all 0.3s ease-out;
    -o-transition: all 0.3s ease-out;
    -ms-transition: all 0.3s ease-out;
    transition: all 0.3s ease-out;
    color: #fff;
    background-color: #ff5d48;
    border-color: #ff5d48;
    font-weight: normal;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    cursor: pointer;
    user-select: none;
    border: 1px solid transparent;
    padding: 0.375rem 1rem;
    font-size: 1rem;
    line-height: 1.5;
    border-radius: 0.25rem;
	float:right;
}
</style>
</div>
<!--
Form Code End (see html-form-guide.com for more info.)
-->

</body>
</html>