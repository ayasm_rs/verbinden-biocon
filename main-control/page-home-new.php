<?PHP
require_once("./include/membersite_config.php");
include("./include/mysqli_connect.php");
include("./include/html_codes.php");
include("./include/function.php");
$common_function = new common_function($connect);
$page_list= $common_function->page_list();
$option='<option value=""> Select page link</option>';
for($i=0; $i < count($page_list); $i++)
{
	$option .='<option value="'.$page_list[$i]["id"].'">'.$page_list[$i]["page_name"].'</option>';
}


admin_way_top();

admin_top_bar();


admin_left_menu();

//After login check for page access

$id_user=$fgmembersite->idUser();
$admin = 0;

$select_event="SELECT * FROM `page_title_des` WHERE page_name='global scale manufacturing' ORDER BY id DESC LIMIT 1";
$run_product=mysqli_query($connect,$select_event);
while($row_event=mysqli_fetch_array($run_product)){
	$metadescription=$row_event["metadescription"];
	$page_title=$row_event["page_title"];
	$page_description=$row_event["page_description"];
	$eid=$row_event["id"];
}

if(isset($_POST['create_page'])){
	
	$fcount=count($_POST['pimage']);
	$f_name=array();

	if($fcount!=0)
	{

		for($i=0;$i<$fcount;$i++)
		{
			$file=htmlspecialchars(mysqli_real_escape_string($connect, $_POST['pimage'][$i]),ENT_QUOTES, 'UTF-8');
			$name='global quality'.date('ymdis');
			$file_name = str_replace(" ","-",$name).".jpg";
			//echo $file;exit;
			list($type, $file) = explode(';', $file);
			list(, $file)      = explode(',', $file);
			file_put_contents("user_images/".$file_name , base64_decode($file));
			$f_name[] = $file_name;
		}
	}
	
	
	$cmp_metadescription = htmlspecialchars(mysqli_real_escape_string($connect, $_POST['cmp_metadescription']),ENT_QUOTES, 'UTF-8');
	$page_title = htmlspecialchars(mysqli_real_escape_string($connect, $_POST['page_title']),ENT_QUOTES, 'UTF-8');
	$p_description = htmlspecialchars(mysqli_real_escape_string($connect, $_POST['p_description']),ENT_QUOTES, 'UTF-8');
	
	$cmp_metadescription = htmlspecialchars(mysqli_real_escape_string($connect, $_POST['cmp_metadescription']),ENT_QUOTES, 'UTF-8');
	$page_title = htmlspecialchars(mysqli_real_escape_string($connect, $_POST['page_title']),ENT_QUOTES, 'UTF-8');
	$p_description = htmlspecialchars(mysqli_real_escape_string($connect, $_POST['p_description']),ENT_QUOTES, 'UTF-8');
	$eid = htmlspecialchars(mysqli_real_escape_string($connect, $_POST['eid']),ENT_QUOTES, 'UTF-8');
	if($eid!=''){
		$sql="UPDATE `page_title_des` SET `metadescription`='$cmp_metadescription', `page_title`='$page_title', page_description='$p_description' WHERE id='$eid' ";
		mysqli_query($connect, $sql);
		$id=$eid;
	}else{
		$sql="INSERT INTO `page_title_des` (`metadescription`, `page_title`, `page_description`, `page_name`) VALUES ('".$cmp_metadescription."', '".$page_title."', '".$p_description."' ,'global quality')";
		$res=mysqli_query($connect, $sql);
		$id=mysqli_insert_id($connect);	
	}
	
	$count=count($_POST['pimage']);
	for($i=0;$i<$count;$i++)
	{
		$description = htmlspecialchars(mysqli_real_escape_string($connect, $_POST['description'][$i]),ENT_QUOTES, 'UTF-8');
		$d_priority = htmlspecialchars(mysqli_real_escape_string($connect, $_POST['d_priority'][$i]),ENT_QUOTES, 'UTF-8');
		$fN=$f_name[$i];
    $date=date('Y-m-d');
		$sql="INSERT INTO `global`(`image_name`, `description`, `page_type`, `created_date`, `update_date`, `added_by`, `page_title_des_id`) VALUES ('".$fN."', '".$description."', 'global quality', '".$date."', '".$date."','".$id_user."','".$id."')";
		mysqli_query($connect, $sql);
		$page_id = mysqli_insert_id($connect);    
    $date_time=date('Y-m-d H:i:s');
	mysqli_query($connect,"INSERT INTO `latest_updates`(`temp_page_id`, `edited_by`, `edited_type`, `updated_date`, `approved_by`, `page_id`, `table_name`, `updated_date_time`) VALUES ('','$id_user','added','$date','','$page_id','global quality','$date_time')");
	}
	
	
}


//redirect code


    $id_user=$fgmembersite->idUser();
	$approver = 0;$admin=0;
	if($fgmembersite->UserRole()=="admin"){
		$admin=1;
	}
	if($fgmembersite->UserRole()!="admin"){
		echo "<script>window.location.href='dashboard.php'</script>";
	}

//mysqli_query($connect, "TRUNCATE table temp_data");
?>
									
<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<style>
.cr_img img{max-width:500px !important;height:auto !important;}
</style>
<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">
			
			<div class="row">
				<div class="col-xs-12">
					<div class="page-title-box">
						<h4 class="page-title">Create Global Quality</h4>
						<!--button class="btn btn-primary"  type="button" name="create_page" onclick="$('#sub_btn').click()" style="float:right">Create Page</button-->
								
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
			<!-- end row -->
                        <form method="POST" name="xxfrm" id="xxfrm" enctype="multipart/form-data">
				<div class="row">
					<div class="col-xs-12 col-lg-12 col-xl-12">
						<div class="card-box">
							<h4 class="header-title m-t-0 m-b-30">Page Details</h4>
							<section>
								<p class="text-muted m-b-20 font-13 pull-right">
									(* All fields are mandatory)
								</p>
								<div class="clearfix"></div>
								<div class="row page_title">
										
										<div class="col-lg-6 form-group">
											<label class="control-label " for="page_title">Page Title <span style=" font-weight: normal; color: #039cfd; " id="page_title_len">[ 60 character remaining ]</span></label>
                                            <input id="page_title" name="page_title" value="<?php echo $page_title; ?>"  type="text" class=" form-control" maxlength="255">
											<input type="hidden" name="eid" value="<?php echo $eid; ?>" >
										</div>
										<div class="col-lg-6 form-group">
											<label class="control-label " for="cmp_metadescription">Meta Description</label>
                                            <input id="cmp_metadescription" name="cmp_metadescription" value="<?php echo $metadescription; ?>"  type="text" class=" form-control" maxlength="255">
										</div>
								</div>
					  <!------- header image, title---------->								
								<h4 class="header-title m-t-0 m-b-30">Banner Image</h4>
								  <div class="form-group clearfix">
									 <div class="col-md-10 col-md-offset-1">
									 <div class="banner_error" id="banner_error" style="color:red;width: 600px;text-align: center;display:none">Please upload banner image!</div>
										<div class="block1_add_menu_toggle row simple-cropper-images" >
											<div class="cropme" style="width: 573px; height: 149px;"></div>
											<div style="width:600px"> <center><br>Click on the icon to upload the banner image!</center></div>
											 <textarea id="banner_image_txt" name="banner_image" style="display:none"></textarea>
										</div>
										<br>
										
									 </div>
								  </div>
								  <div class="form-group clearfix">
								  </div>
							   
					  <!------- header image, title---------->
					  <div class="clearfix"></div>
								<div class="row page_title">
										
										<div class="col-lg-6 form-group">
											<label class="control-label " for="page_heading">Page Title </label>
                                            <input id="page_heading" name="page_heading" value="<?php echo $page_heading; ?>"  type="text" class=" form-control" maxlength="255">
											<input type="hidden" name="eid" value="<?php echo $eid; ?>" >
										</div>
										<div class="col-lg-6 form-group">
											<label class="control-label " for="short_metadescription">Short Description</label>
                                            <input id="short_metadescription" name="short_metadescription" value="<?php echo $short_metadescription; ?>"  type="text" class=" form-control" maxlength="255">
										</div>
								</div>
					  <!-- start our value drivers div  -->
						 <h5 class="heading_cust">Our Value Drivers</h5>
							<!--img src="assets/images/users/block07.jpg" data-toggle="modal" data-target="#myModal"  data-title="Block 6" class="block-img">
							
							<span>(Preview Image)</span-->
						 <hr>
						 <section class="row">
							<div class="col-xs-12 col-lg-12 col-xl-12">
							   <section>
								  <div class="row">
									 <div class="col-xs-12 col-lg-6 col-xl-6">
										<div class="form-group clearfix">
										   <label class="col-lg-2 control-label " for="" >Title
										   </label>
										   <div class="col-lg-10">
											  <input type="text" name="title_value" id="title_value" class="form-control"    />
											  <br />
										   </div>
										</div>
									 </div>
									 <div class="col-xs-12 col-lg-6 col-xl-6">
										<div class="form-group clearfix">
										   <label class="col-lg-2 control-label " for="" >Short Description
										   </label>
										   <div class="col-lg-10">
											  <input type="text" name="value_description" id="value_description" class="form-control"/>
											  <br />
										   </div>
										</div>
									 </div>
								  </div>
								  <div class="form-group clearfix">
									 <div class="col-lg-10">
										<div class="block7_add_menu_toggle row " >
										</div>
										<br>
										+ &nbsp;<a class="btn btn-primary waves-effect waves-light m-r-5 m-b-10" id="block7_add_menu">Add Item</a>
									 </div>
								  </div>
								  <div class="form-group clearfix">
								  </div>
							   </section>
							</div>
							<!-- end col-->
						 </section>
					  <!-- end our value drivers div  -->
					  
					  <!-- start our value drivers div  -->
						 <h5 class="heading_cust">Our Growth Drivers</h5>
							<!--img src="assets/images/users/block07.jpg" data-toggle="modal" data-target="#myModal"  data-title="Block 6" class="block-img">
							
							<span>(Preview Image)</span-->
						 <hr>
						 <section class="row">
							<div class="col-xs-12 col-lg-12 col-xl-12">
							   <section>
								  <div class="row">
									 <div class="col-xs-12 col-lg-6 col-xl-6">
										<div class="form-group clearfix">
										   <label class="col-lg-2 control-label " for="" >Title
										   </label>
										   <div class="col-lg-10">
											  <input type="text" name="title_drivers" id="title_drivers" class="form-control"    />
											  <br />
										   </div>
										</div>
									 </div>
									 <div class="col-xs-12 col-lg-6 col-xl-6">
										<div class="form-group clearfix">
										   <label class="col-lg-2 control-label " for="" >Short Description
										   </label>
										   <div class="col-lg-10">
											  <input type="text" name="value_drivers" id="value_drivers" class="form-control"/>
											  <br />
										   </div>
										</div>
									 </div>
								  </div>
								  <div class="form-group clearfix">
									 <div class="col-lg-10">
										<div class="block2_1_add_menu_toggle row " >
										</div>
										<br>
										+ &nbsp;<a class="btn btn-primary waves-effect waves-light m-r-5 m-b-10" id="block2_1_add_menu">Add Item</a>
									 </div>
								  </div>
								  <div class="form-group clearfix">
								  </div>
							   </section>
							</div>
							<!-- end col-->
						 </section>
					  <!-- end our value drivers div  -->
					  <!-- start our value drivers div  -->
						 <h5 class="heading_cust">Our Innovation Path</h5>
							<!--img src="assets/images/users/block07.jpg" data-toggle="modal" data-target="#myModal"  data-title="Block 6" class="block-img">
							
							<span>(Preview Image)</span-->
						 <hr>
						 <section class="row">
							<div class="col-xs-12 col-lg-12 col-xl-12">
							   <section>
								  <div class="row">
									 <div class="col-xs-12 col-lg-6 col-xl-6">
										<div class="form-group clearfix">
										   <label class="col-lg-2 control-label " for="" >Title
										   </label>
										   <div class="col-lg-10">
											  <input type="text" name="title_value" id="title_value" class="form-control"    />
											  <br />
										   </div>
										</div>
									 </div>
									 <div class="col-xs-12 col-lg-6 col-xl-6">
										<div class="form-group clearfix">
										   <label class="col-lg-2 control-label " for="" >Short Description
										   </label>
										   <div class="col-lg-10">
											  <input type="text" name="value_description" id="value_description" class="form-control"/>
											  <br />
										   </div>
										</div>
									 </div>
								  </div>
								  <div class="form-group clearfix">
									 <div class="col-lg-10">
										<div class="block3_add_menu_toggle row " >
										</div>
										<br>
										+ &nbsp;<a class="btn btn-primary waves-effect waves-light m-r-5 m-b-10" id="block3_add_menu">Add Item</a>
									 </div>
								  </div>
								  <div class="form-group clearfix">
								  </div>
							   </section>
							</div>
							<!-- end col-->
						 </section>
					  <!-- end our value drivers div  -->
					  
								<h4 class="header-title m-t-0 m-b-30">Global Quality</h4>
								<div class="row">
									<div class="col-lg-12 form-group">
											<label class="control-label " for="meta_description">Page Overview*</label>
                                            <textarea id="p_description" name="p_description" class=" form-control" ></textarea>
											<span id="p_descriptionerror" class="errorcls"></span>
										</div>
									</div>
									<div class="row" id="add_more" >
										<div class="col-lg-6 form-group">
											<label class="control-label " for="meta_description">Image 1*</label><br/>
                                            <input type="hidden" class=" form-control pimage" name="pimage[]" id="pimage_1" class="imgs" data-id="PImg" >
											<input type="hidden" name="prf_image[]" id="prf_image" >
											<img src="" id="PImg" class="imsrc" width="150px"/>
											<span class="errorcls pimageerror" id="pimageerror"></span>
										</div>
										<div class="col-lg-12 form-group">
											<label class="control-label " for="meta_description">Description 1*</label>
                                            <textarea id="description1" name="description[]" class=" form-control description" ></textarea>
											<span class="errorcls descriptionerror" id="descriptionerror"></span>
										</div>
									</div>&nbsp;
									<div class="row" >
										<div class="col-lg-12 form-group">
											<div class="input_fields_wrap">
												<button class="btn btn-primary add_field_button">Add More Fields</button>
											</div>
										</div>
										<div class="clearfix"></div>
									</div>
									
							</section>
						</div>
					</div>
					<div class="col-lg-12 form-group clearfix">
						<div class="card-box">
							<div class="col-lg-10 text-xs-center">
							<input type="hidden" name="create_page" id="create_page" value="submit" />
							<button class="btn btn-primary"  type="button" name="sub_btn" id="sub_btn">Create Page</button>
								
							</div>
						</div>
					</div>
				</div>
				</form>
			<button class="btn btn-custom waves-effect waves-light btn-sm page_create_succcess"  style="display:none;">Click me</button>
		</div> <!-- container -->

	</div> <!-- content -->

</div>



<!--center>
<div class="container-narrow">
  <div>
    <h1>jQuery jQuery Awesome Cropper Demo</h1>
    
    <form role="form">
      <input id="sample_input" type="hidden" name="test[image]">
    </form>
  </div>
  <hr>
  
</div></center-->

<!-- End content-page -->




<!----------- Banner crop plugin  ------------------> 
	
		<link rel="stylesheet" type="text/css" href="simple_cropper/css/style.css" />
		<link rel="stylesheet" type="text/css" href="simple_cropper/css/style-example.css" />
		<link rel="stylesheet" type="text/css" href="simple_cropper/css/jquery.Jcrop.css" />

		<!-- Js files-->
		<script type="text/javascript" src="simple_cropper/scripts/jquery-1.10.2.min.js"></script>
		<script type="text/javascript" src="simple_cropper/scripts/jquery.Jcrop.js"></script>
		<script type="text/javascript" src="simple_cropper/scripts/jquery.SimpleCropper.js"></script>
		<script>
		 $('.cropme').simpleCropper();
		</script>
		
<!---------------- croper  --------------------->
<link rel="stylesheet" href="assets/css/choosenJs/prism.css">
	<link rel="stylesheet" href="assets/css/choosenJs/chosen.css">
	<link rel="stylesheet" href="assets/css/custom.css">
		<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/cropper/2.3.4/cropper.min.css'>
			
	
    <script src="assets/js/choosenJs/chosen.jquery.js" type="text/javascript">
    </script>
    <script src="assets/js/choosenJs/prism.js" type="text/javascript" charset="utf-8">
    </script>
    <script src="assets/js/choosenJs/init.js" type="text/javascript" charset="utf-8">
    </script>
		<script src='https://cdnjs.cloudflare.com/ajax/libs/cropperjs/0.8.1/cropper.min.js'></script>
		
		
            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->

<!-- Custom box css -->
<link href="assets/plugins/custombox/css/custombox.min.css" rel="stylesheet">

 <!-- Modal-Effect -->
<script src="assets/plugins/custombox/js/custombox.min.js"></script>
<script src="assets/plugins/custombox/js/legacy.min.js"></script>
			
<!-- Sweet Alert css -->
<link href="assets/plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css" />
<!-- Sweet Alert js -->
<script src="assets/plugins/bootstrap-sweetalert/sweet-alert.min.js"></script>
<script src="assets/pages/jquery.sweet-alert.init.js"></script>

<!-- Editor -->
<script src="//code.jquery.com/jquery-1.12.0.min.js"></script>


<script type="text/javascript">

var validExt = ".png, .jpeg, .jpg";
var validExt1 = ".png";
function fileExtValidate(fdata,id) { 
 var filePath = fdata.value;
 var getFileExt = filePath.substring(filePath.lastIndexOf('.') + 1).toLowerCase();
 if(id.includes("PImg") || id.includes("mPimg"))
 var pos = validExt1.indexOf(getFileExt);
else
var pos = validExt.indexOf(getFileExt);
	
 if(pos < 0) {
 	alert("This file is not allowed, please upload valid file.");
 	return false;
  } else {
  	return true;
  }
}
$(document).ready(function(){

$("#add_more").on("change",".d_priority",function(){
	var len_priority=$(".d_priority").length;
	var priority_value=$(this).val();
	var this_pri=$(this);
	var n_priority=$(".d_priority").index(this);
	for(i=0;i<len_priority;i++){
		//$(".d_priority:nth-child(1)").val();
		if(priority_value==$(".d_priority:eq( "+i+" )").val() && n_priority!=i){
		//$(this).removeClass("form-control");		
        this_pri.css("border","1px solid #fb0505");
		alert("Change the priority");
		}else{
		this_pri.css("border","1px solid #ccc");
		}
		}
	//alert($(".d_priority").length);
	
});

/**************************ADD MORE FIELDS**************************************/
var max_fields      = 20; //maximum input boxes allowed
    var wrapper         = $("#add_more"); //Fields wrapper
    var add_button      = $(".add_field_button"); //Add button ID
   
    var x = 2; //initlal text box count
    $(add_button).click(function(e){ //on add input button click
	//debugger;
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
$(wrapper).append('<div class="col-lg-12 form-group remove_div_'+x+'"> <hr> </div><div class="col-lg-6 form-group remove_div_'+x+'"><label class="control-label " for="meta_description">Image '+x+'*</label><br/><input type="hidden" class=" form-control pimage" name="pimage[]" id="pimage_'+x+'" class="imgs" data-id="mPimg_'+x+'" ><br/><div class="container"><div class="row"><div class="panel panel-body"><div class="span4 cropme landscape" id="landscape"></div></div></div></div><img src="" id="mPimg_'+x+'" class="imsrc" width="150px"/><span class="errorcls pimageerror" id="pimageerror"></span></div><div class="col-lg-12 form-group remove_div_'+x+'"><label class="control-label " for="meta_description">Description '+x+'*</label><textarea id="description'+x+'" name="description[]" class=" form-control description"  ></textarea><span class="errorcls descriptionerror" id="descriptionerror"></span><br><a href="#" onclick="$(\'.remove_div_'+x+'\').remove();return false;" class="btn btn-primary remove_field">Remove</a><br></div>'); //add input box
CKEDITOR.replace( 'description'+x+'' );
$('#pimage_'+x).awesomeCropper(
        { width: 1165, height: 500, debug: true }
        );
x++; //text box increment

        }
    });
   
    /* $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('section').remove(); x--;
    }) */
/*******************************************************************************/
$(wrapper).on("change",".imgs", function(e) {
var id=$(this).attr("data-id");
var filename=$(this);
if(fileExtValidate(this,id)) {
var fr = new FileReader;
var ext = this.files[0].type.split('/').pop().toLowerCase();
if($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {
    alert('invalid extension!');
}
else{
	var size = parseFloat(this.files[0].size / 1024).toFixed(2);
    fr.onload = function(e) { // file is loaded
     	var img = new Image;
		img.onload = function() {
			
			 var imgwidth = this.width;
			var imgheight = this.height;
			var flag=0;
			if(id.includes("PImg") || id.includes("mPimg"))
			{
				if(imgwidth>550 || imgheight>520)
				{
				flag=1;
				alert("Max image dimesion 550*460");
				filename.val("");
				}
			}
			else
			{
				if(imgwidth<725 || imgheight<239)
				{
				flag=1;
				alert("Min image dimesion 725*239");
				filename.val("");
				}
			}
			if(flag==0)
			{
            $('#'+id).attr('src', e.target.result);
			}
			
			};
		img.src = fr.result; // is the data URL because called with readAsDataURL
    };
    fr.readAsDataURL(this.files[0]); // I'm using a <input type="file"> for demonstrating
}	
	}
	else
		$(this).val("");
		
});
	
/******************************************************************************/
$(".imgs").on('change', function () {
var id=$(this).attr("data-id");
var filename=$(this);
if(fileExtValidate(this,id)) {
var fr = new FileReader;
var ext = this.files[0].type.split('/').pop().toLowerCase();
if($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {
    alert('invalid extension!');
}
else{
	var size = parseFloat(this.files[0].size / 1024).toFixed(2);
    fr.onload = function(e) { // file is loaded
     	
		var img = new Image;
		
		img.onload = function() {
			
            var imgwidth = this.width;
			var imgheight = this.height;
			var flag=0;
			if(id.includes("PImg") || id.includes("mPimg"))
			{
				if(imgwidth>550 || imgheight>520)
				{
				flag=1;
				filename.val("");
				alert("Max image dimesion 550*460");
				
				}
			}
			else
			{
				if(imgwidth<725 || imgheight<239)
				{
				flag=1;
				filename.val("");
				alert("Min image dimesion 725*239");
				}
			}
			if(flag==0)
			{
            $('#'+id).attr('src', e.target.result);
			}
					
			};
		img.src = fr.result; // is the data URL because called with readAsDataURL
    };
    fr.readAsDataURL(this.files[0]); // I'm using a <input type="file"> for demonstrating
}	
	}
	else
		$(this).val("");
		
});	
	$('.page_create_succcess' ).click(function () {
		swal("Status!", "New global quality created successfully.", "success");
	});	
	<?php  if(isset($_POST['create_page'])){ ?>
	$('.page_create_succcess').click();	
	<?php } ?>
	


	
});


$(document).on('click',"#sub_btn",function(){
				var xxfrm = $("#xxfrm");			
                var p_description       = $("#p_description");
                var p_descriptionerror  = $("#p_descriptionerror");
			if(validatedescription() & validatepimage() & validatep_description())
                {
                    xxfrm.submit();
                } 
                
				                                           
                function validatep_description()
                {
                  if(CKEDITOR.instances['p_description'].getData().length<2)
                   {

                     p_description.removeClass("form-control");		
                     p_description.addClass("form-control errortxtbox");
                     p_descriptionerror.text("Please enter overview");

                     return false;	
                   }
                   else
                   {
                     p_description.removeClass("form-control errortxtbox");	
                     p_description.addClass("form-control");		
                     p_descriptionerror.text("");	

                     return true;
                   }    
                   
                } ///////////////  
				
				function validatedescription()
                {
				var description;
				var description_id;
                var descriptionerror;
				var error_states=true;
				for(i=0;i<$('input[name="pimage[]"]').length;i++){
				description_id=$(".description:eq("+i+")").attr("id");
				description=$(".description:eq("+i+")");
				//alert(CKEDITOR.instances[description_id].getData().length);
				descriptionerror=$(".descriptionerror:eq("+i+")");
                   if(CKEDITOR.instances[description_id].getData().length<2)
                   {
                     description.removeClass("form-control");		
                     description.addClass("form-control errortxtbox");
                     descriptionerror.text("Please enter description"); 

                     error_states=false;	
                   }
                   else
                   {
                     description.removeClass("form-control errortxtbox");	
                     description.addClass("form-control");		
                     descriptionerror.text(""); 
                     //return true;
                   }   
				}
				if(error_states==false){return false;}else{return true;}
                } ///////////////  
				
				function validatepimage()
                {

				var pimage;
                var pimageerror;
				var error_states=true;
				for(i=0;i<$('input[name="pimage[]"]').length;i++){
				//debugger;
				pimage=$(".pimage:eq("+i+")");
				pimageerror=$(".pimageerror:eq("+i+")");
                  if(pimage.val().length<1)
                   {
						//alert("error");
                     pimage.removeClass("form-control");		
                     pimage.addClass("form-control errortxtbox");
                     pimageerror.text("Please select image"); 

                     error_states=false;	
                   }
                   else
                   {
                     pimage.removeClass("form-control errortxtbox");	
                     pimage.addClass("form-control");		
                     pimageerror.text(""); 	
					//alert("success");
                     //return true;
                   }    
				}
				if(error_states==false){return false;}else{return true;}
                } ///////////////  
				
                                          
                
                
		});

$(document).on("click", ".confirm",function(){
	window.location.href = "page-existing.php";
});
$("#page_title").keyup(function(){
	var leng=$("#page_title").val().length;
	$("#page_title_len").text(" [ "+(60-leng)+" character remaining ]");
});

</script>



<style>
.radio, .checkbox{
	display:inline-block;
}
.all_widgets{
    margin-left: 39px;	
	border-radius: 75%;
    width: 63px;
    text-align: center;
    padding: 10px;
    cursor: pointer;
    color: black;
    border: solid 1px #64b0f2;
	background-color:#64b0f2;
}

</style>

<!-- custom scroll bar --->
<!--script src="js/jquery.mCustomScrollbar.concat.js"></script-->
<link rel="stylesheet" type="text/css" href="css/jquery.mCustomScrollbar.css" />
<!--<script src="js/page-create-boardofdirectory.js"></script>-->


<!-- Css files siva -->
<link href="components/imgareaselect/css/imgareaselect-default.css" rel="stylesheet" media="screen">
<link rel="stylesheet" href="css/jquery.awesome-cropper.css">


<?php
admin_right_bar();

admin_footer();
?>
<script>
	$("#page-create").attr("class","active");
</script>

<!-- /container by siva --> 

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> 
<script src="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script> 
<script src="components/imgareaselect/scripts/jquery.imgareaselect.js"></script> 
<script src="build/jquery.awesome-cropper.js"></script> 
<script>
    $(document).ready(function () {
        $('#pimage_1').awesomeCropper(
        { width: 1165, height: 500, debug: true }
        );
    });
</script> 
<style>
.awesome-cropper>img{
	width:150px;
}	
canvas {
    max-width: 150px;
}
#add_more .form-group {
    margin-bottom: 0;
}
.page_title .form-group {
    margin-bottom: 30px;
}
.cropme img{
	width:100%;
	height:100%;
}
</style>

<script>
   // Replace the <textarea id="editor1"> with a CKEditor
   // instance, using default configuration.
   CKEDITOR.replace( 'description1' );
    CKEDITOR.replace( 'p_description' );
	
	
	<!----------- Add menu button click code for 7th Block  ------------------>  
	var n=0;
	$("#block7_add_menu").click(function(){
		n++;
		$(".block7_add_menu_toggle").append('<div id="div_block7_'+n+'" class="col-sm-6" ><div class="row"><div style="padding:10px;margin:10px;border: 1px solid #ccc;float: left;width: 100%;" class="row"><div class="col-md-6" style="float:left;width:100%;"><main class="page"><div class="box"><label>New Image Upload</label><br><input type="file" id="file-input_block7_'+n+'" class="file-input" ><textarea class="hide" name="block7_menu_file[]" id="block7_menu_file_block7_'+n+'" ></textarea></div><div class="box-2" id="box-2_block7_'+n+'"><div class="result_block7_'+n+'"></div></div><div class="box-2 img-result_block7_'+n+'"><img class="cropped_block7_'+n+'" src="" alt=""></div><div class="box"><div class="options_block7_'+n+' "><input type="number" min="0" class="hide img-w_block7_'+n+'" value="150" min="100" max="1200" /></div><a class="btn-success btn-sm btn save_block7_'+n+' " onclick=$(".cropped_block7_'+n+'").show();$(this).hide();$("#box-2_block7_'+n+'").hide() style="display:none;width: calc(100%/2 - 2em);padding: 0.5em;">Crop</a></div></main></div><div class="col-md-12" ><label>Title*</label><input type="text" class="form-control" name="block7_menu_title[]"  /></div><div class="col-md-12" ><label>Short Description*</label><textarea class="form-control" name="block7_menu_count[]"  ></textarea></div><div class="col-md-6" ><label>Select Link*</label><select class="form-control" name="select_block7[]"  ><?php echo $option; ?></select></div><div class="col-md-6" ><label>background Color*</label><input type="color" class="" name="block7_menu_color[]"  /></div><div class="col-md-12" ><a class="btn btn-danger btn-sm " onclick=$("#div_block7_'+n+'").remove(); style="float:right">Remove</a></div></div></div></div>');
		
		$('body').append('<script>document.querySelector("#file-input_block7_'+n+'").addEventListener("change",function(e){var t=document.querySelector(".result_block7_'+n+'"),r=(document.querySelector(".img-result_block7_'+n+'"),document.querySelector(".img-w_block7_'+n+'"),document.querySelector(".img-h_block7_'+n+'"),document.querySelector(".options_block7_'+n+'"));document.querySelector(".save_block7_'+n+'"),document.querySelector(".cropped_block7_'+n+'"),document.querySelector("#file-input_block7_'+n+'");if(e.target.files.length){var o=new FileReader;o.onload=function(e){e.target.result&&($(".save_block7_'+n+'").show(),$("#box-2_block7_'+n+'").show(),$(".cropped_block7_'+n+'").hide(),img=document.createElement("img"),img.id="image",img.src=e.target.result,t.innerHTML="",t.appendChild(img),r.classList.remove("hide"),cropper_block7_'+n+' = new Cropper(img))},o.readAsDataURL(e.target.files[0])}}),document.querySelector(".save_block7_'+n+'").addEventListener("click",function(e){document.querySelector(".result_block7_'+n+'");var t=document.querySelector(".img-result_block7_'+n+'"),r=document.querySelector(".img-w_block7_'+n+'"),o=(document.querySelector(".img-h_block7_'+n+'"),document.querySelector(".options_block7_'+n+'"),document.querySelector(".save_block7_'+n+'"),document.querySelector(".cropped_block7_'+n+'"));document.querySelector("#file-input_block7_'+n+'");e.preventDefault();var c=cropper_block7_'+n+'.getCroppedCanvas({width:r.value}).toDataURL();o.classList.remove("hide"),t.classList.remove("hide"),o.src=c;document.querySelector("#block7_menu_file_block7_'+n+'").value=c;});<\/script>');
		
	});

<!----------- Add menu button click code for 2th Block  ------------------>  
	var a=0;
	$("#block2_1_add_menu").click(function(){
		a++;
		$(".block2_1_add_menu_toggle").append('<div id="div_block2_1_'+a+'" class="col-sm-6" ><div class="row"><div style="padding:10px;margin:10px;border: 1px solid #ccc;float: left;width: 100%;" class="row"><div class="col-md-6" style="float:left;width:100%;"><main class="page"><div class="box"><label>New Image Upload</label><br><input type="file" id="file-input_block2_1_'+a+'" class="file-input" ><textarea class="hide" name="block2_1_menu_file[]" id="block2_1_menu_file_block2_1_'+a+'" ></textarea></div><div class="box-2" id="box-2_block2_1_'+a+'"><div class="result_block2_1_'+a+'"></div></div><div class="box-2 img-result_block2_1_'+a+'"><img class="cropped_block2_1_'+a+'" src="" alt=""></div><div class="box"><div class="options_block2_1_'+a+' "><input type="number" min="0" class="hide img-w_block2_1_'+a+'" value="150" min="100" max="1200" /></div><a class="btn-success btn-sm btn save_block2_1_'+a+' " onclick=$(".cropped_block2_1_'+a+'").show();$(this).hide();$("#box-2_block2_1_'+a+'").hide() style="display:none;width: calc(100%/2 - 2em);padding: 0.5em;">Crop</a></div></main></div><div class="col-md-12" ><label>Title*</label><input type="text" class="form-control" name="block2_1_menu_title[]"  /></div><div class="col-md-12" ><label>Short Description*</label><textarea class="form-control" name="block2_1_menu_count[]"  ></textarea></div><div class="col-md-6" ><label>Select Link*</label><select class="form-control" name="select_block2_1[]"  ><?php echo $option; ?></select></div><div class="col-md-6" ><label>background Color*</label><input type="color" class="" name="block2_1_menu_color[]"  /></div><div class="col-md-6" style="float:left;width:100%;"><main class="page"><div class="box"><label>New Image Upload</label><br><input type="file" id="file-input_block2_2_'+a+'" class="file-input" ><textarea class="hide" name="block2_2_menu_file[]" id="block2_2_menu_file_block2_2_'+a+'" ></textarea></div><div class="box-2" id="box-2_block2_2_'+a+'"><div class="result_block2_2_'+a+'"></div></div><div class="box-2 img-result_block2_2_'+a+'"><img class="cropped_block2_2_'+a+'" src="" alt=""></div><div class="box"><div class="options_block2_2_'+a+' "><input type="number" min="0" class="hide img-w_block2_2_'+a+'" value="150" min="100" max="1200" /></div><a class="btn-success btn-sm btn save_block2_2_'+a+' " onclick=$(".cropped_block2_2_'+a+'").show();$(this).hide();$("#box-2_block2_2_'+a+'").hide() style="display:none;width: calc(100%/2 - 2em);padding: 0.5em;">Crop</a></div></main></div><div class="col-md-12" ><a class="btn btn-danger btn-sm " onclick=$("#div_block2_1_'+a+'").remove(); style="float:right">Remove</a></div></div></div></div>');
		
		$('body').append('<script>document.querySelector("#file-input_block2_1_'+a+'").addEventListener("change",function(e){var t=document.querySelector(".result_block2_1_'+a+'"),r=(document.querySelector(".img-result_block2_1_'+a+'"),document.querySelector(".img-w_block2_1_'+a+'"),document.querySelector(".img-h_block2_1_'+a+'"),document.querySelector(".options_block2_1_'+a+'"));document.querySelector(".save_block2_1_'+a+'"),document.querySelector(".cropped_block2_1_'+a+'"),document.querySelector("#file-input_block2_1_'+a+'");if(e.target.files.length){var o=new FileReader;o.onload=function(e){e.target.result&&($(".save_block2_1_'+a+'").show(),$("#box-2_block2_1_'+a+'").show(),$(".cropped_block2_1_'+a+'").hide(),img=document.createElement("img"),img.id="image",img.src=e.target.result,t.innerHTML="",t.appendChild(img),r.classList.remove("hide"),cropper_block2_1_'+a+' = new Cropper(img))},o.readAsDataURL(e.target.files[0])}}),document.querySelector(".save_block2_1_'+a+'").addEventListener("click",function(e){document.querySelector(".result_block2_1_'+a+'");var t=document.querySelector(".img-result_block2_1_'+a+'"),r=document.querySelector(".img-w_block2_1_'+a+'"),o=(document.querySelector(".img-h_block2_1_'+a+'"),document.querySelector(".options_block2_1_'+a+'"),document.querySelector(".save_block2_1_'+a+'"),document.querySelector(".cropped_block2_1_'+a+'"));document.querySelector("#file-input_block2_1_'+a+'");e.preventDefault();var c=cropper_block2_1_'+a+'.getCroppedCanvas({width:r.value}).toDataURL();o.classList.remove("hide"),t.classList.remove("hide"),o.src=c;document.querySelector("#block2_1_menu_file_block2_1_'+a+'").value=c;});document.querySelector("#file-input_block2_2_'+a+'").addEventListener("change",function(e){var t=document.querySelector(".result_block2_2_'+a+'"),r=(document.querySelector(".img-result_block2_2_'+a+'"),document.querySelector(".img-w_block2_2_'+a+'"),document.querySelector(".img-h_block2_2_'+a+'"),document.querySelector(".options_block2_2_'+a+'"));document.querySelector(".save_block2_2_'+a+'"),document.querySelector(".cropped_block2_2_'+a+'"),document.querySelector("#file-input_block2_2_'+a+'");if(e.target.files.length){var o=new FileReader;o.onload=function(e){e.target.result&&($(".save_block2_2_'+a+'").show(),$("#box-2_block2_2_'+a+'").show(),$(".cropped_block2_2_'+a+'").hide(),img=document.createElement("img"),img.id="image",img.src=e.target.result,t.innerHTML="",t.appendChild(img),r.classList.remove("hide"),cropper_block2_2_'+a+' = new Cropper(img,{aspectRatio: 393 / 413}))},o.readAsDataURL(e.target.files[0])}}),document.querySelector(".save_block2_2_'+a+'").addEventListener("click",function(e){document.querySelector(".result_block2_2_'+a+'");var t=document.querySelector(".img-result_block2_2_'+a+'"),r=document.querySelector(".img-w_block2_2_'+a+'"),o=(document.querySelector(".img-h_block2_2_'+a+'"),document.querySelector(".options_block2_2_'+a+'"),document.querySelector(".save_block2_2_'+a+'"),document.querySelector(".cropped_block2_2_'+a+'"));document.querySelector("#file-input_block2_2_'+a+'");e.preventDefault();var c=cropper_block2_2_'+a+'.getCroppedCanvas({width:r.value}).toDataURL();o.classList.remove("hide"),t.classList.remove("hide"),o.src=c;document.querySelector("#block2_2_menu_file_block2_2_'+a+'").value=c;});<\/script>');
		
	});
<!----------- Add menu button click code for 3th Block  ------------------>  
	var b=0;
	$("#block3_add_menu").click(function(){
		b++;
		$(".block3_add_menu_toggle").append('<div id="div_block3_'+b+'" class="col-sm-6" ><div class="row"><div style="padding:10px;margin:10px;border: 1px solid #ccc;float: left;width: 100%;" class="row"><div class="col-md-6" style="float:left;width:100%;"><main class="page"><div class="box"><label>New Image Upload</label><br><input type="file" id="file-input_block3_'+b+'" class="file-input" ><textarea class="hide" name="block3_menu_file[]" id="block3_menu_file_block3_'+b+'" ></textarea></div><div class="box-2" id="box-2_block3_'+b+'"><div class="result_block3_'+b+'"></div></div><div class="box-2 img-result_block3_'+b+'"><img class="cropped_block3_'+b+'" src="" alt=""></div><div class="box"><div class="options_block3_'+b+' "><input type="number" min="0" class="hide img-w_block3_'+b+'" value="150" min="100" max="1200" /></div><a class="btn-success btn-sm btn save_block3_'+b+' " onclick=$(".cropped_block3_'+b+'").show();$(this).hide();$("#box-2_block3_'+b+'").hide() style="display:none;width: calc(100%/2 - 2em);padding: 0.5em;">Crop</a></div></main></div><div class="col-md-12" ><label>Title*</label><input type="text" class="form-control" name="block3_menu_title[]"  /></div><div class="col-md-12" ><label>Short Description*</label><textarea class="form-control" name="block3_menu_count[]"  ></textarea></div><div class="col-md-6" ><label>Select Link*</label><select class="form-control" name="select_block3[]"  ><?php echo $option; ?></select></div><div class="col-md-6" ><label>background Color*</label><input type="color" class="" name="block3_menu_color[]"  /></div><div class="col-md-12" ><a class="btn btn-danger btn-sm " onclick=$("#div_block3_'+b+'").remove(); style="float:right">Remove</a></div></div></div></div>');
		
		$('body').append('<script>document.querySelector("#file-input_block3_'+b+'").addEventListener("change",function(e){var t=document.querySelector(".result_block3_'+b+'"),r=(document.querySelector(".img-result_block3_'+b+'"),document.querySelector(".img-w_block3_'+b+'"),document.querySelector(".img-h_block3_'+b+'"),document.querySelector(".options_block3_'+b+'"));document.querySelector(".save_block3_'+b+'"),document.querySelector(".cropped_block3_'+b+'"),document.querySelector("#file-input_block3_'+b+'");if(e.target.files.length){var o=new FileReader;o.onload=function(e){e.target.result&&($(".save_block3_'+b+'").show(),$("#box-2_block3_'+b+'").show(),$(".cropped_block3_'+b+'").hide(),img=document.createElement("img"),img.id="image",img.src=e.target.result,t.innerHTML="",t.appendChild(img),r.classList.remove("hide"),cropper_block3_'+b+' = new Cropper(img))},o.readAsDataURL(e.target.files[0])}}),document.querySelector(".save_block3_'+b+'").addEventListener("click",function(e){document.querySelector(".result_block3_'+b+'");var t=document.querySelector(".img-result_block3_'+b+'"),r=document.querySelector(".img-w_block3_'+b+'"),o=(document.querySelector(".img-h_block3_'+b+'"),document.querySelector(".options_block3_'+b+'"),document.querySelector(".save_block3_'+b+'"),document.querySelector(".cropped_block3_'+b+'"));document.querySelector("#file-input_block3_'+b+'");e.preventDefault();var c=cropper_block3_'+b+'.getCroppedCanvas({width:r.value}).toDataURL();o.classList.remove("hide"),t.classList.remove("hide"),o.src=c;document.querySelector("#block3_menu_file_block3_'+b+'").value=c;});<\/script>');
		
	});

</script>