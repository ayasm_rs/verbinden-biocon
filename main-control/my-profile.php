<?PHP
require_once("./include/membersite_config.php");
include("./include/mysqli_connect.php");
include("./include/html_codes.php");
if(!session_start()){
	session_start();
}
$id = $_SESSION['id_user']; //substr($_GET['id'],0,35);

// ---- *** 
$sess_datas_data = sess_datas();
if(!is_numeric($id))
{
    $fgmembersite->page_404_errorpage();
}
else if($sess_datas_data["id_user"]!=trim($id))
{
    $fgmembersite->page_404_errorpage();
}
/* else if(count($_GET)==0 or count($_GET)>2)
{
    $fgmembersite->page_404_errorpage();
} */
// ---- *** 

admin_way_top();

admin_top_bar();

admin_left_menu();

$allowedTags='<p><strong><em><u><h1><h2><h3><h4><h5><h6>';
$allowedTags.='<li><ol><ul><span><div><br><ins><del><a><table><style><tr><th><td><img>';



$sql = mysqli_query($connect,"SELECT *, t1.ref_id as usruniqueid FROM biocon_users t1 WHERE t1.id_user=".$id);
$no_of_rows = mysqli_num_rows($sql);
if($no_of_rows==0 or $no_of_rows=="")
{
	$id_usr_chck = 0;
}
else
{
	$userRow = mysqli_fetch_array($sql);
	$id_usr_chck = $userRow["ref_id"];
}



if(isset($_POST['profile_update'])){
	
	$faculty_type = "";
	$name = htmlspecialchars(mysqli_real_escape_string($connect, $_POST['person_name']),ENT_QUOTES, 'UTF-8');
	$email = htmlspecialchars(mysqli_real_escape_string($connect, $_POST['email']),ENT_QUOTES, 'UTF-8');
	$phone_number = htmlspecialchars(mysqli_real_escape_string($connect, $_POST['phone_number']),ENT_QUOTES, 'UTF-8');
	$username = htmlspecialchars(mysqli_real_escape_string($connect, $_POST['username']),ENT_QUOTES, 'UTF-8');
	$password = mysqli_real_escape_string($connect, $_POST['password']);
	$educationQualifications = htmlspecialchars(mysqli_real_escape_string($connect, $_POST['educational_qualifications']),ENT_QUOTES, 'UTF-8');
	if(isset($_POST['faculty_type'])){
		$faculty_type = htmlspecialchars(mysqli_real_escape_string($connect, $_POST['faculty_type']),ENT_QUOTES, 'UTF-8');
	}
	$honors = htmlspecialchars(mysqli_real_escape_string($connect, $_POST['honors']),ENT_QUOTES, 'UTF-8');
	$research = htmlspecialchars(mysqli_real_escape_string($connect, $_POST['research']),ENT_QUOTES, 'UTF-8');
	$subjects = htmlspecialchars(mysqli_real_escape_string($connect, $_POST['subjects']),ENT_QUOTES, 'UTF-8');
	$designation = htmlspecialchars(mysqli_real_escape_string($connect, $_POST['designation']),ENT_QUOTES, 'UTF-8');
	
        $description = mysqli_real_escape_string($connect, $_POST['description']);        
        $description = strip_tags($description,$allowedTags);
        
	$selected_publications = mysqli_real_escape_string($connect, $_POST['selected_publications']);        
        $selected_publications   = strip_tags($selected_publications,$allowedTags);
        
	$role = htmlspecialchars(mysqli_real_escape_string($connect, $_POST['role']),ENT_QUOTES, 'UTF-8');
	$password_original = htmlspecialchars(mysqli_real_escape_string($connect, $_POST['password_original']),ENT_QUOTES, 'UTF-8');
	$highDesignation =implode("~", $_POST['highDesignation']);
	$orderhighDesignation = htmlspecialchars(mysqli_real_escape_string($connect, $_POST['orderhighDesignation']),ENT_QUOTES, 'UTF-8');
	$profile_pic = $_POST['profile_pic'];
	$page_catregory_access_arr = $_POST['select_category'];
	$page_catregory_access = implode("~",$page_catregory_access_arr);
	$page_access_urls_arr = $_POST['page_access_ids'];
	$page_access_urls = implode("~",$page_access_urls_arr);
	
        $other_information = mysqli_real_escape_string($connect, $_POST['other_information']);        
        $other_information = strip_tags($other_information,$allowedTags);
        
	$research_conslting = mysqli_real_escape_string($connect, $_POST['research_conslting']);        
        $research_conslting = strip_tags($research_conslting,$allowedTags);
        
	$teaching =mysqli_real_escape_string($connect, $_POST['teaching']);        
	$teaching = strip_tags($teaching,$allowedTags);
        
        $edit_type = htmlspecialchars(mysqli_real_escape_string($connect, $_POST['edit_type']),ENT_QUOTES, 'UTF-8');
   
	if(strlen($password)==60){$password = "";}
		if(trim($password)!="")
         {
             $password = password_hash($password, PASSWORD_DEFAULT, ['cost' => 11]);
         }    
         else
         {
             $password = "";
         }    
	
	if (!isset($_FILES['image']['tmp_name'])) {
		$profile_pic = $_POST['profile_pic'];
	}
	else{
		$file=$_FILES['image']['tmp_name'];
		
		if($file){
			$profile_pic = "";
			$image= addslashes(file_get_contents($_FILES['image']['tmp_name']));
			$image_name= addslashes($_FILES['image']['name']);
			$extension = pathinfo($image_name, PATHINFO_EXTENSION);
			$profileImage = time().".".$extension;
			$profile_pic = $profileImage;	
			move_uploaded_file($_FILES["image"]["tmp_name"],"../main-control/user_images/" . $profileImage);
			//mysqli_query($connect, "UPDATE biocon_users set profile_pic='$profile_pic' where id_user=".$id);
		}
	}
        
	mysqli_query($connect, "UPDATE temp_biocon_users SET name = '$name', phone_number = '$phone_number', profile_pic ='$profile_pic',email = '$email', confirmcode = 'y', role = '$role', education = '$educationQualifications', description = '$description', honors_awards = '$honors', selected_publications = '$selected_publications', research_interests = '$research', subjects = '$subjects', faculty_type = '$faculty_type', username = '$username', admin_status = 0 WHERE id_user = ".$id_usr_chck);  
	
	mysqli_query($connect, "UPDATE `biocon_users` SET `name`='$name',`email`='$email',`phone_number`='$phone_number',`username`='$username' WHERE ref_id = ".$id_usr_chck);
	
	if($password!="")
    {    
        mysqli_query($connect, "UPDATE temp_biocon_users SET password = '$password' where id_user = ".$id_usr_chck);
        mysqli_query($connect, "UPDATE biocon_users SET password = '$password' where ref_id = ".$id_usr_chck);
    }    
	
	$last_updated_id = $id_usr_chck;																																																																					
	
	
	
	
	$date=date('Y-m-d');
    $date_time=date('Y-m-d H:i:s');
	mysqli_query($connect,"INSERT INTO `latest_updates`(`temp_page_id`, `edited_by`, `edited_type`, `updated_date`, `approved_by`, `page_id`, `table_name`, `updated_date_time`) VALUES ('','$id_usr_chck','added','$date','','$id_usr_chck','My Profile','$date_time')");
	
	
	
} 



if($fgmembersite->UserRole()!="admin"){
	if($id!=$fgmembersite->idUser()){
		$id_user=$fgmembersite->idUser();
		echo "<script>window.location.href='page-edit-faculty.php?id=".$id_user."'</script>";
	}
}

?>
									
            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
<?php
$sql = mysqli_query($connect,"SELECT * FROM temp_biocon_users WHERE id_user=".$id_usr_chck); 
while($userRow=mysqli_fetch_array($sql)){
	$id =  $userRow['id_user'];
	$profile_pic =   $userRow['profile_pic'];
	$name =$userRow['name'];
	$email = $userRow['email'];
	$phone_number =  $userRow['phone_number'];
	$username = $userRow['username'];
	$password = $userRow['password'];
	$educationQualifications =  $userRow['education'];
	$faculty_type = $userRow['faculty_type'];
	$honors = $userRow['honors_awards'];
	$research = $userRow['research_interests'];
	$subjects = $userRow['subjects'];
	$designation = $userRow['designation'];
	$highDesignation = $userRow['highDesignation'];
	$description = stripcslashes($userRow['description']);
	$role = $userRow['role'];
	$selected_publications = stripcslashes($userRow['selected_publications']);
	$orderhighDesignation = $userRow['orderhighDesignation'];
	
	$other_information = stripcslashes($userRow['other_information']);
	$research_conslting = stripcslashes($userRow['research_conslting']);
	$teaching = stripcslashes($userRow['teaching']);
	
	$faculty_type=str_replace(" ","_",$faculty_type);
	
	$edit_type = $userRow['edit_type'];
    $page_access_urls =  explode("~",$userRow['page_access_ids']);
    $page_catregory_access_array =  explode("~",$userRow['page_catregory_access']);
}
$admin = "1";
if($fgmembersite->UserRole()=="admin"){
	$admin ="0";
}
else { $admin ="1"; }


?>

<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">
			
			<div class="row">
				<div class="col-xs-12">
					<div class="page-title-box">
						<h4 class="page-title">Update Profile</h4>
						<button type="button" name="profile_update" class="btn btn-primary" onclick="$('#sub_btn').click()" style="float:right">Update Details</button>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
			<!-- end row -->
			<form action="" method="POST" enctype="multipart/form-data" name="xxfrm" id="xxfrm">
			<input type="hidden" value="<?php echo $profile_pic; ?>" name="profile_pic" />
			<div class="row">
				<div class="col-xs-12 col-lg-12 col-xl-12">
					<div class="card-box">
						<h4 class="header-title m-t-0 m-b-30">Personal details</h4>
						<section>
							<p class="text-muted m-b-20 font-13 pull-right">
								(* All fields are mandatory)
							</p>
							<div class="clearfix"></div>
								
							
								<div class="row">
									<div class="col-lg-4 form-group">
										<label class="control-label " for="name">Name*</label>
										<input class="form-control" id="name" value="<?php echo $name; ?>" <?php if($admin == "1"){ echo "readonly"; } ?> name="person_name" type="text" maxlength="35">
										<span id="nameerror" class="errorcls"></span>
									</div>
									<div class="col-lg-4 form-group">
										<label class="control-label " for="email">Email address</label>
										<input id="email" name="email" type="text" value="<?php echo $email; ?>" <?php if($admin == "1"){ echo "readonly"; } ?>  class=" form-control" maxlength="50">
										 <span id="emailerror" class="errorcls"></span>
										<span id="email_message" style="display:none;"><p class="text-danger">Email-address already exists. Please try with different email address.</p></span>
									</div>
									<div class="col-lg-4 form-group">
										<label class="control-label " for="phone_number">Phone number</label>
										<input id="phone_number" name="phone_number" value="<?php echo $phone_number; ?>"  type="text" class=" form-control" maxlength="15">
									</div>
									<div class="clearfix"></div>
								</div>
								<!--div class="row">
									<div class="col-lg-4 form-group">
										<label class="control-label " for="designation">Designation</label>
										<?php
										$designation_value = '';
										$designation_fetch = mysqli_query($connect, "SELECT designation FROM biocon_users GROUP BY designation");
										while($row = mysqli_fetch_array($designation_fetch)){
											$designation_value .= $row['designation']."~";
										}
										$designation_array = '';
										$designation_explode = explode("~", $designation_value);
										foreach($designation_explode as $designation_exploded){
											$designation_array .= '"'.$designation_exploded.'"~';
										}
										//$designation_array = rtrim($designation_array, ', "",').'"';
										$designation_array = implode(",", array_unique(explode("~", $designation_array)));
										?>
										
										<script>
										var j_noconflict = $.noConflict();
											j_noconflict(function(){
												var sampleTags = [<?php echo $designation_array; ?>];
												j_noconflict('#designation').tagit({
													availableTags: sampleTags,
													allowSpaces: true
												});
											});
										</script>
										
										<input class="form-control " <?php if($admin == "0"){echo 'id="designation"'; } ?>
										 <?php if($admin == "1"){ echo "disabled"; } ?>  value="<?php echo $designation; ?>"  name="designation" type="text">
										
										<small class="text-muted">Type the destination and press enter key to insert another destination</small>
									</div>
									<div class="col-lg-4 form-group">
										<label class="control-label " for="honors">Honors and Awards</label>
										<?php
										$honor_value = '';
										$honor_fetch = mysqli_query($connect, "SELECT honors_awards FROM biocon_users GROUP BY honors_awards");
										while($row = mysqli_fetch_array($honor_fetch)){
											$honor_value .= $row['honors_awards'].'~';
										}
										$honor_array = '';
										$honor_explode = explode('~', $honor_value);
										foreach($honor_explode as $honor_exploded){
											$honor_array .= '"'.$honor_exploded.'"~';
										}
										//$honor_array = rtrim($honor_array, ', "",').'"';
										$honor_array = implode(",", array_unique(explode("~", $honor_array)));
										?>
										<script>
											j_noconflict(function(){
												var sampleTags = [<?php echo $honor_array; ?>];
												j_noconflict('#honors').tagit({
													availableTags: sampleTags,
													allowSpaces: true
												});
											});
										</script>
										<input id="honors" name="honors" type="text" value="<?php echo $honors; ?>"  class="form-control">
										<small class="text-muted">Type the honors and awards and press enter key to insert another honors and awards</small>
									</div>
									<div class="col-lg-4 form-group">
										<label class="control-label " for="research">Research Interests</label>
										<?php
										$research_value = '';
										$research_fetch = mysqli_query($connect, "SELECT research_interests FROM biocon_users GROUP BY research_interests");
										while($row = mysqli_fetch_array($research_fetch)){
											$research_value .= $row['research_interests'].'~';
										}
										$research_array = '';
										$research_explode = explode('~', $research_value);
										foreach($research_explode as $research_exploded){
											$research_array .= '"'.$research_exploded.'"~';
										}
										//$research_array = rtrim($research_array, ', "",').'"';
										$research_array = implode(",", array_unique(explode("~", $research_array)));
										?>
										<script>
										j_noconflict(function(){
												var sampleTags = [<?php echo $research_array; ?>];
												j_noconflict('#research').tagit({
													availableTags: sampleTags,
													allowSpaces: true
												});
											});
										</script>
										<input id="research" name="research" value="<?php echo $research; ?>"  type="text" class="form-control">
										<small class="text-muted">Type the research intrests and press enter key to insert another research intrests</small>
									</div>
									<div class="clearfix"></div>
								</div-->
								<!--div class="row">
									<div class="col-lg-4 form-group">
									<label class="control-label " for="educational_qualifications">Educational Qualifications</label>
									<?php
									$qualifications_value = '';
									$qualifications_fetch = mysqli_query($connect, "SELECT education FROM biocon_users GROUP BY education");
									while($row = mysqli_fetch_array($qualifications_fetch)){
										$qualifications_value .= $row['education'].'~';
									}
									$qualifications_array = '';
									$qualifications_explode = explode('~', $qualifications_value);
									foreach($qualifications_explode as $qualifications_exploded){
										$qualifications_array .= '"'.$qualifications_exploded.'"~';
									}
									//$qualifications_array = rtrim($qualifications_array, ', "",').'"';
									$qualifications_array = implode(",", array_unique(explode("~", $qualifications_array)));
									?>
									<script>
										j_noconflict(function(){
											var sampleTags = [<?php echo $qualifications_array; ?>];

											j_noconflict('#educational_qualifications').tagit({
												availableTags: sampleTags,
												allowSpaces: true
											});
											
										});
									</script>
										<input  <?php if($admin == "0"){echo 'id="educational_qualifications"'; } ?>
										 <?php if($admin == "1"){ echo "disabled"; } ?> <?php if($admin == "1"){ echo "readonly"; } ?> value="<?php echo $educationQualifications; ?>" name="educational_qualifications" type="text"  class="form-control">
										<small class="text-muted">Type the qualification and press enter key to insert another qualification</small>
									</div>
									<div class="col-lg-4 form-group">
										<label class="control-label " for="subjects">Subjects</label>
										<?php
										$subjects_value = '';
										$subjects_fetch = mysqli_query($connect, "SELECT subjects FROM biocon_users GROUP BY subjects");
										while($row = mysqli_fetch_array($subjects_fetch)){
											$subjects_value .= $row['subjects'].'~';
										}
										$subjects_array = '';
										$subjects_explode = explode('~', $subjects_value);
										foreach($subjects_explode as $subjects_exploded){
											$subjects_array .= '"'.$subjects_exploded.'"~';
										}
										//$subjects_array = rtrim($subjects_array, ', "",').'"';
										$subjects_array = implode(",", array_unique(explode("~", $subjects_array)));
										?>
									<script>
										j_noconflict(function(){
											var sampleTags1 = [<?php echo $subjects_array; ?>];
											j_noconflict('#subjects').tagit({
												availableTags: sampleTags1,
												allowSpaces: true
											});
										});
									</script>
									
										<input  <?php if($admin == "0"){echo 'id="subjects"'; } ?>
										 <?php if($admin == "1"){ echo "disabled"; } ?> name="subjects" type="text"  class="form-control" value="<?php echo $subjects; ?>">
										<small class="text-muted">Type the subject and press enter key to insert another subject</small>
									</div>
									<div class="col-lg-4 form-group">
											<label class="control-label " for="highDesignation">Other Roles</label>
											<?php
											
											$highDesignation_explode = explode('~', $highDesignation); 
											
											
										
											?>
											<select class="form-control" id="highDesignation" name="highDesignation[]" multiple <?php if($admin == "1"){ echo "disabled"; } ?>  >
												<?php
													if(in_array("Administration",$highDesignation_explode)){ 
														echo '<option value="Administration" selected>Administration</option>';
													}
													else{
														echo '<option value="Administration" >Administration</option>';
													}
													if(in_array("faculty",$highDesignation_explode)){ 
														echo '<option value="faculty" selected>Faculty</option>';
													}
													else{ 
														echo '<option value="faculty" >Faculty</option>';
													}
													if(in_array("Governing Body",$highDesignation_explode)){ 
														echo '<option value="Governing Body" selected>Governing Body</option>';
													}else
													{ 
														echo '<option value="Governing Body" >Governing Body</option>';
													}
													if(in_array("staff",$highDesignation_explode)){ 
														echo '<option value="staff" selected>Staff</option>';
													}else{
														echo '<option value="staff" >Staff</option>';
													}
													
												?>
											</select>
								
										</div>
										<div class="col-lg-4 form-group">
											<label class="control-label " for="orderhighDesignation">Role Index</label>
															
										
											<input id="orderhighDesignation" name="orderhighDesignation" type="text" <?php if($admin == "1"){ echo "readonly"; } ?> class="form-control" value="<?php echo $orderhighDesignation; ?>" maxlength="15">
											<small class="text-muted">Type the order number separated with comma.</small>
										</div>
								</div-->
								<!--div class="row">
									<div class="col-lg-6 form-group">
										<label class="control-label " for="selected_publications">Selected Publications</label>
										<textarea rows="5" class="form-control" id="selected_publications" placeholder="Paragraph Text" name="selected_publications"><?php echo $selected_publications; ?></textarea>
									</div>
									<div class="col-lg-6 form-group">
										<label class="control-label " for="description">Description</label>
										<textarea rows="5" class="form-control" placeholder="Content" spellcheck="false"  id="description" name="description" ><?php echo $description; ?></textarea>
									</div>
									<div class="col-lg-6 form-group">
											<label class="control-label " for="teaching">Teaching</label>
											<textarea rows="5" class="form-control" id="teaching" placeholder="Paragraph Text" name="teaching"><?php echo $teaching; ?></textarea>
										</div>
										<div class="col-lg-6 form-group">
											<label class="control-label " for="research_conslting">Research & Consulting</label>
											<textarea rows="5" class="form-control" id="research_conslting" placeholder="Paragraph Text" name="research_conslting"><?php echo $research_conslting; ?></textarea>
										</div>
										<div class="col-lg-6 form-group">
											<label class="control-label " for="other_information">Other Information</label>
											<textarea rows="5" class="form-control" id="other_information" placeholder="Paragraph Text" name="other_information"><?php echo $other_information; ?></textarea>
										</div>
								</div-->
						</section>
					</div>
				</div>
				<div class="col-xs-12 col-lg-6 col-xl-6">
					<div class="card-box">
						<h4 class="header-title m-t-0 m-b-30">Profile photo</h4>
						<div class="text-xs-center m-b-20">
							<img data-toggle="modal" data-target=".bs-example-modal-lg" src="../main-control/user_images/<?php echo $profile_pic; ?>" id="uploaded_image" >
						</div>
						<div class="col-lg-12 text-xs-center" >
							<label>Edit image*</label>
						</div>
							<div class="col-lg-12 text-xs-center">
								<label class="file">
									<input type="file" name="image" id="file"  onchange="imagePreview(this);" value="" >
									<span class="file-custom"></span><br><br>
									<span class="errorcls" id="fileerror"></span>
								</label>
							</div>
					</div>
				</div>
				<div class="col-xs-12 col-lg-6 col-xl-6">
					<div class="card-box">
						<h4 class="header-title m-t-0 m-b-30">Login details</h4>
						<section>
							<div class="form-group clearfix">
									<label class="col-lg-2 control-label " for="username">Username</label>
									<div class="col-lg-10">
										<input id="username" name="username"  type="text"  class="form-control" value="<?php echo $username; ?>" maxlength="35">
										<span id="usernameerror" class="errorcls"></span>
									</div>
								</div>
								<div class="form-group clearfix">
									<label class="col-lg-2 control-label " for="password" >Password</label>
									<div class="col-lg-10">
										<input id="password" name="password" type="password" class="form-control" value="<?php echo $password; ?>" maxlength="35">
										<input id="password_original" name="password_original" type="hidden" value="<?php echo $password; ?>">
										<span id="passworderror" class="errorcls"></span>
									</div>
								</div>
								<div class="form-group clearfix">
									<label class="col-lg-2 control-label " for="password" >Confirm password</label>
									<div class="col-lg-10">
										<input id="password1" name="password1" type="password" class="form-control" value="<?php echo $password; ?>" maxlength="35">
										<span id="confirmpassworderror" class="errorcls"></span><br>
										<small id="confirmpasswordhint" class="password-error">Password should contain (8 to 15 characters which should include at least one lowercase, one uppercase, one numeric digit and one special character)</small>
									</div>
								</div>
								<div class="form-group clearfix" id="passwordError" style="display:none">
									<label class="col-lg-2 " for="password" ></label>
									<div class="col-lg-10">
									<span style="color:red;">Main password and confirm password does not match. </span>
									</div>
								</div>
								<div class="form-group clearfix" id="passwordError1" style="display:none">
									<label class="col-lg-2 " for="password" ></label>
									<div class="col-lg-10">
									<span style="color:red;">Not a valid password. </span>
									</div>
								</div>
						</section>
					</div>
					<!--div class="card-box">
						<h4 class="header-title m-t-0 m-b-30">Roles</h4>
						<section>
							<div class="form-group clearfix">
								<label class="col-lg-2 control-label " for="role">Role</label>
								<div class="col-lg-10">
									<select class="form-control" id="role" name="role" <?php if($admin == "1"){ echo "disabled"; } ?>>
											<option value="admin" id="role_admin" <?php if($role=="admin"){ echo 'selected'; }?> >admin</option>
											<option value="faculty" id="role_faculty" <?php if($role=="faculty"){ echo 'selected'; }?> >faculty</option>
											<option value="others" <?php if($role=="others"){ echo 'selected'; }?> >Others</option>
									</select>
								</div>
							</div>
						
							<div class="form-group clearfix" id="faculty_type_div" >
								<label class="col-lg-2 control-label " for="role">Type</label>
								<div class="col-lg-10">
									<select class="form-control" id="faculty_type" <?php if($admin == "1"){ echo "disabled"; } ?> name="faculty_type">
									    <option value="" id="" ></option>
										<option value="Regular Faculty" id="Regular_Faculty" <?php if($faculty_type=="Regular_Faculty"){ echo 'selected'; }?> >Regular Faculty</option>
										<option value="Adjunct Faculty" id="Adjunt_Faculty"  <?php if($faculty_type=="Adjunct_Faculty"){ echo 'selected'; }?> >Adjunct Faculty</option>
										<option value="Advisory Faculty" id="Advisory_Faculty"  <?php if($faculty_type=="Advisory_Faculty"){ echo 'selected'; }?> >Advisory Faculty</option>
										<option value="Visiting Faculty" id="Visiting_Faculty"  <?php if($faculty_type=="Visiting_Faculty"){ echo 'selected'; }?> >Visiting Faculty</option>
									</select>
								</div>
							</div>
							<?php $style = "style='display:block;'"; if($admin == "1"){ 
							$style = "style='display:none;'"; 
							} ?>
							 <div class="form-group clearfix" id="cat_div" <?php echo $style; ?>>
							     
									<div class="col-lg-10">
											<label class="control-label " for="select_category" >Select Page category</label><br>
											<select class='form-contol 3col active'  name='select_category[]'  id='select_category' multiple='multiple' >
												<?php 
													echo '<option value="A" ';
													if(in_array("A",$page_catregory_access_array)){
														echo "selected";
													}
													echo'>About Us Pages</option>';
													
													echo '<option value="Ad" ';
													if(in_array("Ad",$page_catregory_access_array)){
														echo "selected";
													}
													echo'>Admissions Pages</option>';
													
													echo '<option value="C" ';
													if(in_array("C",$page_catregory_access_array)){
														echo "selected";
													}
													echo'>Careers Pages</option>';
												
													echo '<option value="D" ';
													if(in_array("D",$page_catregory_access_array)){
														echo "selected";
													}
													echo'>Donate Pages</option>';
													
													echo '<option value="F" ';
													if(in_array("F",$page_catregory_access_array)){
														echo "selected";
													}
													echo'>Faculty Pages</option>';
													echo '<option value="fullwidth" ';
													if(in_array("fullwidth",$page_catregory_access_array)){
														echo "selected";
													}
													echo'>Fullwidth Pages</option>';
													echo '<option value="P" ';
													if(in_array("P",$page_catregory_access_array)){
														echo "selected";
													}
													echo'>Programs Pages</option>';
												
													echo '<option value="R" ';
													if(in_array("R",$page_catregory_access_array)){
														echo "selected";
													}
													echo'>Research Pages</option>';
													echo '<option value="people" ';
													if(in_array("people",$page_catregory_access_array)){
														echo "selected";
													}
													echo'>People Pages</option>';
													echo '<option value="otherpages" ';
													if(in_array("otherpages",$page_catregory_access_array)){
														echo "selected";
													}
													echo'>Other Pages</option>';
													
													
													
													
												?>
											</select>
										</div>
									</div>
								<div class="form-group clearfix" id="faculty_type_div" <?php echo $style; ?> >
									<label class="col-lg-2 control-label " for="role">Page Access Control</label>
									<div class="col-lg-10">

										<select class='form-contol 3col active access_pages'  name='page_access_ids[]'  multiple='multiple' >
										 <?php
											$pages = mysqli_query($connect,"SELECT * FROM pages WHERE page_nav_name='A' ");
											echo '<optgroup label="About Us Pages">';
											while($page = mysqli_fetch_array($pages)){
												$page_url = $page['id'];
												if(!empty($page['pagetitle'])){
													if(in_array($page_url,$page_access_urls)){
														echo "<option  value='".$page_url."' selected class='A'> ".$page['pagetitle']."</option>";
													}else{
														echo "<option  value='".$page_url."' class='A' > ".$page['pagetitle']."</option>";
													}
													
												}
											}
											echo '</optgroup>';
										  ?>
										   <?php
											$pages = mysqli_query($connect,"SELECT * FROM pages WHERE page_nav_name='Ad' ");
											echo '<optgroup label="Admissions Pages">';
											while($page = mysqli_fetch_array($pages)){
												$page_url = $page['id'];
												if(!empty($page['pagetitle'])){
													if(in_array($page_url,$page_access_urls)){
														echo "<option  value='".$page_url."' selected class='Ad' onclick='$(\'li .A\').click();'> ".$page['pagetitle']."</option>";
													}else{
														echo "<option  value='".$page_url."' class='Ad' onclick='$(\'li .A\').click();'> ".$page['pagetitle']."</option>";
													}
													
												}
											}
											echo '</optgroup>';
										  ?>
										   <?php
											$pages = mysqli_query($connect,"SELECT * FROM pages WHERE page_nav_name='C' ");
											echo '<optgroup label="Careers Pages">';
											while($page = mysqli_fetch_array($pages)){
												$page_url = $page['id'];
												if(!empty($page['pagetitle'])){
													if(in_array($page_url,$page_access_urls)){
														echo "<option  value='".$page_url."' selected class='C'> ".$page['pagetitle']."</option>";
													}else{
														echo "<option  value='".$page_url."' class='C'> ".$page['pagetitle']."</option>";
													}
													
												}
											}
											echo '</optgroup>';
										  ?>
										   <?php
											$pages = mysqli_query($connect,"SELECT * FROM pages WHERE page_nav_name='D' ");
											echo '<optgroup label="Donate Pages">';
											while($page = mysqli_fetch_array($pages)){
												$page_url = $page['id'];
												if(!empty($page['pagetitle'])){
													if(in_array($page_url,$page_access_urls)){
														echo "<option  value='".$page_url."' selected class='donate'> ".$page['pagetitle']."</option>";
													}else{
														echo "<option  value='".$page_url."' class='donate'> ".$page['pagetitle']."</option>";
													}
													
												}
											}
											echo '</optgroup>';
										  ?>
										   <?php
											$pages = mysqli_query($connect,"SELECT * FROM pages WHERE page_nav_name='' ");
											echo '<optgroup label="Fullwidth Pages">';
											while($page = mysqli_fetch_array($pages)){
												$page_url = $page['id'];
												if(!empty($page['pagetitle'])){
													echo "<option  value='".$page_url."' class='fullwidth'> ".$page['pagetitle']."</option>";
												}
											}
											echo '</optgroup>';
										  ?>
										  <?php
											$pages = mysqli_query($connect,"SELECT * FROM pages WHERE page_nav_name='P' ");
											echo '<optgroup label="Programs Pages">';
											while($page = mysqli_fetch_array($pages)){
												$page_url = $page['id'];
												if(!empty($page['pagetitle'])){
													if(in_array($page_url,$page_access_urls)){
														echo "<option  value='".$page_url."' selected class='P'> ".$page['pagetitle']."</option>";
													}else{
														echo "<option  value='".$page_url."' class='P'> ".$page['pagetitle']."</option>";
													}
													
												}
											}
											echo '</optgroup>';
										  ?>
										   <?php
											$pages = mysqli_query($connect,"SELECT * FROM pages WHERE page_nav_name='R' ");
											echo '<optgroup label="Research Pages">';
											while($page = mysqli_fetch_array($pages)){
												$page_url = $page['id'];
												if(!empty($page['pagetitle'])){
													if(in_array($page_url,$page_access_urls)){
														echo "<option  value='".$page_url."' selected class='R'> ".$page['pagetitle']."</option>";
													}else{
														echo "<option  value='".$page_url."' class='R'> ".$page['pagetitle']."</option>";
													}
													
												}
											}
											echo '</optgroup>';
										  ?>
										   <?php 
											  echo '<optgroup label="People Pages">';
											  echo '<option value="administration" ';
											   if(in_array("administration",$page_access_urls)){
											  echo 'selected';
											  }
											  echo ' class="people">Administration People Page</option>';
											  echo '<option value="faculty" ';
											   if(in_array("faculty",$page_access_urls)){
											  echo 'selected';
											  } 
											  echo' class="people">Faculty People Page</option>';
											  echo '<option value="governing_body" ';
											   if(in_array("governing_body",$page_access_urls)){
											  echo 'selected';
											  } 
											  echo' class="people">Governing Body People Page</option>';
											  echo '<option value="placement_team" ';
											   if(in_array("placement_team",$page_access_urls)){
											  echo 'selected';
											  } 
											  echo' class="people">placement People Page</option>';
											  echo '<option value="research_people" ';
											   if(in_array("research_people",$page_access_urls)){
											  echo 'selected';
											  } 
											  echo' class="people" >Research People Page</option>';
											  echo '<option value="staff"  ';
											   if(in_array("staff",$page_access_urls)){
											  echo 'selected';
											  } 
											  echo' class="people" >Staff People Page</option>';
											  echo '</optgroup>';




											  echo '<optgroup label="Other Pages">';
											 echo '<option value="alumni_speak" ';
											   if(in_array("alumni_speak",$page_access_urls)){ 
											   	echo 'selected';
											   }
											  echo ' class="otherpages">Alumni Speak Page</option>';
											  echo '<option value="list_of_holidays" ';
											   if(in_array("list_of_holidays",$page_access_urls)){ 
											   	echo 'selected';
											   }
											  echo ' class="otherpages">Calendar Page</option>';
											  echo '<option value="media" ';
											   if(in_array("media",$page_access_urls)){ 
											   	echo 'selected';
											   }
											  echo ' class="otherpages">Media Page</option>';
											  
											  echo '<option value="recruiting_companies" ';
											   if(in_array("recruiting_companies",$page_access_urls)){ 
											   	echo 'selected';
											   }
											  echo ' class="otherpages">Recruting Companies Page</option>';
											  
											  echo '<option value="testimonials" ';
											   if(in_array("testimonials",$page_access_urls)){ 
											   	echo 'selected';
											   }

											  echo ' class="otherpages">Testimonials</option>';
											  
											 
											  echo '<option value="tickers" ';
											   if(in_array("tickers",$page_access_urls)){ 
											   	echo 'selected';
											   }

											  echo ' class="otherpages">Tickers</option>';
											  echo '</optgroup>';
										  ?>
										
										  </select>
									</div>
								</div>
								
								<div class="form-group clearfix" id="faculty_type_div" <?php echo $style; ?>>
									
									<div class="col-lg-10">
										<label class="control-label " for="edit_type">Page Access User Type</label><br>
											<select class="form-control " id="edit_type" name="edit_type">
											    <option value="">Select access type</option>
												<option value="editor" <?php if($edit_type=="editor"){  echo "selected";  }?>>Editor</option>
												<option value="approver" <?php if($edit_type=="approver"){  echo "selected";  }?>>Approver</option>
											</select>
									</div>
								</div>
							
						</section>
					</div-->
				</div><!-- end col-->
				<div class="col-lg-12 form-group clearfix">
					<div class="card-box">
						<div class="col-lg-10 text-xs-center">
						<input type="hidden" name="profile_update" id="profile_update" value="submit" />    
						<button type="button" name="sub_btn" class="btn btn-primary" id="sub_btn" value="submit">Update Details</button>
						<button class="btn btn-custom waves-effect waves-light btn-sm profile_update_succcess"  style="display:none;">Click me</button>
						<input type="hidden" name="hidEditId" id="hidEditId" value="<?php echo $id_usr_chck;?>" />
						</div>
					</div>
				</div>
			</div>
			</form>
			 <div id="custom-modal" class="modal-demo">
				<button type="button" class="close" onclick="Custombox.close();">
					<span>&times;</span><span class="sr-only">Close</span>
				</button>
				<h4 class="custom-modal-title">Status</h4>
				<div class="custom-modal-text">
				  Page Updated Successfully.
				</div>
			</div>
		</div> <!-- container -->

	</div> <!-- content -->



</div>
<!-- End content-page -->


            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->

			
<!-- Sweet Alert css -->
<link href="assets/plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css" />
<!-- Sweet Alert js -->
<script src="assets/plugins/bootstrap-sweetalert/sweet-alert.min.js"></script>
<script src="assets/pages/jquery.sweet-alert.init.js"></script>

<!-- Editor -->
<script src="//code.jquery.com/jquery-1.12.0.min.js"></script>
<!-- editor old
<script src="//code.jquery.com/jquery-1.12.0.min.js"></script>
<script src="js/jquery.syntaxhighlighter.js"></script>
<script src="assets/plugins/elrte/js/jquery-ui-1.8.13.custom.min.js" type="text/javascript" charset="utf-8"></script>
<link rel="stylesheet" href="assets/plugins/elrte/css/smoothness/jquery-ui-1.8.13.custom.css" type="text/css" media="screen" charset="utf-8">
<script src="assets/plugins/elrte/js/elrte.min.js" type="text/javascript" charset="utf-8"></script>
<link rel="stylesheet" href="assets/plugins/elrte/css/elrte.min.css" type="text/css" media="screen" charset="utf-8">
<script src="assets/plugins/elrte/js/i18n/elrte.ru.js" type="text/javascript" charset="utf-8"></script>
 -->
<script>
function CheckPassword(inputtxt)   
{   
	var paswd=  /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{7,15}$/;  
	if(inputtxt.match(paswd))   
	{   
	console.log('Correct, try another...')  
	return true;  
	}  
	else  
	{   
	console.log('Wrong...!')  
	return false;  
	}  
}       
var a=1;var b =1;var c=1;var d=1;var p=1;var r=1;var people=1;var other=1;

$(document).on('click','span',function() { 
   var x = $(this).next().text(); 
   
   if(x=="About Us Pages"){
		
			if(a==1){
			$("li .A").click();
			
			a=0;
			}else{
				a=1;
				$("li .A").click();
			}
			
		
	}
	if(x=="Admissions Pages"){
		if(b==1){
			$("li .Ad").click();
			b=0;
		}else{
			b=1;
			$("li .Ad").click();
		}
		
	}
	if(x=="Careers Pages"){
		if(c==1){
			$("li .C").click();
			c=0;
		}else{
			c=1;
			$("li .C").click();
		}
	}
	if(x=="Donate Pages"){
		if(d==1){
			$("li .donate").click();
			d=0;
		}else{
			d=1;
			$("li .donate").click();
		}
	}
	if(x=="Programs Pages"){
		if(p==1){
		 $("li .P").click();
		 $p=0;
		}else{
			$p=1;
			$("li .P").click();
		}
	}
	if(x=="Research Pages"){
		if(r==1){
			$("li .R").click();
			r=0;
		}else{
			r=1;
			$("li .R").click();
		}
	}
	if(x=="People Pages"){
		if(people==1){
			$("li .people").click();
		}else{
			people=1;
			$("li .people").click();
		}
	}
	if(x=="Other Pages"){
		
			if(other==1){
				$("li .otherpages").click();
				other=0;
			}else{
				other=1;
				$("li .otherpages").click();
			}
		
	}
 });
 
 function imagePreview(input) {
    
        var fileerror		= $("#fileerror");
        
        if($('#file').val()=="")
        {
            fileerror.text("Please upload profile image");
            return false;
        }
        else 
        {
            var fileExtension = ['jpeg', 'jpg', 'png', 'gif'];
            if ($.inArray($('#file').val().split('.').pop().toLowerCase(), fileExtension) == -1) {				
               fileerror.text("Only the following formats are allowed : jpeg, jpg, png, gif");
               return false; 
            }
            else
            {
                
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $('#uploaded_image').attr('src', e.target.result);
                        fileerror.text("");
                    };
                       reader.readAsDataURL(input.files[0]);
                    }
            }    
            
        } // if($('#file').val()=="")   
        
	
}
 
$(document).on('click','label',function() {
	var x =$(this).text();
	if(x=="About Us Pages"){
		
		if(a==1){
			$("li .A").click();
			a=0;
		}else{
			a=1;
			$("li .A").click();
		}
		console.log(a);
	}
	if(x=="Admissions Pages"){
		if(b==1){
			$("li .Ad").click();
			b=0;
		}else{
			b=1;
			$("li .Ad").click();
		}
		
	}
	if(x=="Careers Pages"){
		if(c==1){
			$("li .C").click();
			c=0;
		}else{
			c=1;
			$("li .C").click();
		}
	}
	if(x=="Donate Pages"){
		if(d==1){
			$("li .donate").click();
			d=0;
		}else{
			d=1;
			$("li .donate").click();
		}
	}
	if(x=="Programs Pages"){
		if(p==1){
		 $("li .P").click();
		 $p=0;
		}else{
			$p=1;
			$("li .P").click();
		}
	}
	if(x=="Research Pages"){
		if(r==1){
			$("li .R").click();
			r=0;
		}else{
			r=1;
			$("li .R").click();
		}
	}
	if(x=="People Pages"){
		if(people==1){
			$("li .people").click();
		}else{
			people=1;
			$("li .people").click();
		}
	}
	if(x=="Other Pages"){
		if(other==1){
			$("li .otherpages").click();
			other=0;
		}else{
			other=1;
			$("li .otherpages").click();
		}
	}
	if(x=="Fullwidth Pages"){
		if(other==1){
			$("li .fullwidth").click();
			other=0;
		}else{
			other=1;
			$("li .fullwidth").click();
		}
	}
});
function alertmsg()
{
	swal("Status!", "Page updated successfully.", "success");
	return false;
}  
$(document).ready(function(){
	
	<?php  if(isset($_POST['profile_update'])){ ?>
	alertmsg();

	<?php } ?>
	
	$("#page-existing").attr("class","active");
	
	
	$("#role_<?php echo $role; ?>").attr("selected","selected");
	$("#<?php echo $faculty_type; ?>").attr("selected","selected");
	
	
	$(document).on("click", ".confirm",function(){
		//window.location.reload();
	});

     

});
</script>

<script src="js/my-profile.js"></script>

<style>
#subjects{ text-transform: lowercase;}
#uploaded_image{ width:285px;cursor: pointer;border-radius: 6px; }
.SumoSelect{
	width:100% !important;
}
</style>


<!--Sumo select dropdown--->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<script src="https://hemantnegi.github.io/jquery.sumoselect/javascripts/jquery.sumoselect.js"></script>
<link href="https://hemantnegi.github.io/jquery.sumoselect/stylesheets/sumoselect.css" rel="stylesheet" />
<script type="text/javascript">
var j_dropdown = $.noConflict();
j_dropdown(document).ready(function () {

j_dropdown('.testselect2').SumoSelect(); 
j_dropdown('#highDesignation').SumoSelect(); 
j_dropdown('.access_pages').SumoSelect(); 

j_dropdown('#select_category').SumoSelect(); });
</script>

<?php
admin_right_bar();

admin_footer();
?>

<script>

   // Replace the <textarea id="editor1"> with a CKEditor
   // instance, using default configuration.
   CKEDITOR.replace( 'selected_publications' );
   CKEDITOR.replace( 'description' );
   CKEDITOR.replace( 'teaching' );
   CKEDITOR.replace( 'research_conslting' );
   CKEDITOR.replace( 'other_information' );
</script>