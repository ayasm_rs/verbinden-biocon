<?php
		@header("HTTP/1.0 403 Forbidden");
        echo '<!DOCTYPE html><html><head><title>Access forbidden!</title><meta charset="utf-8">';
        echo '</head><body>';
        echo "<h1>Access forbidden!</h1>";
        echo "<p>You don't have permission to access the requested directory. There is either no index document or the directory is read-protected.</p>";
		echo "<p>If you think this is a server error, please contact the webmaster.</p>";
        echo '</body></html>';
        exit();     
?>		