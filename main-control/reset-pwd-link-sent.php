<?php
require_once("./include/membersite_config.php");
include("./include/mysqli_connect.php");
include("./include/html_codes.php");
$fgmembersite->block_login_page();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US">
<head>
      <meta http-equiv='Content-Type' content='text/html; charset=utf-8'/>
      <title>Thank you!</title>
      <link href="assets/css/style.css" rel="stylesheet" type="text/css" />
        <link href="css/custom.css" rel="stylesheet" type="text/css" />
        
        <script type='text/javascript' src='js/gen_validatorv31.js'></script>

        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <script src="assets/js/modernizr.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
</head>
<body class="widescreen">
<div id='fg_membersite_content'>
<div class="wrapper-page" >
<div class='account-bg'>
<div class="card-box m-b-0" >
<div class="text-xs-center m-t-20">
	<img src="../<?php echo $logo; ?>">
</div>
<div class="m-t-30 m-b-20">
<h2>Reset password link sent</h2>
An email is sent to your email address that contains the link to reset the password.
</div>
</div>
</div>
</div>
<style>
.resetreq_email_errorloc{
	font-size:80%;
	font-weight:normal;
}
.widescreen{
	   background-color: #2b3d51;
}
.btn{
	position: relative;
    cursor: pointer;
    display: inline-block;
    overflow: hidden;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    -webkit-tap-highlight-color: transparent;
    vertical-align: middle;
    z-index: 1;
    will-change: opacity, transform;
    -webkit-transition: all 0.3s ease-out;
    -moz-transition: all 0.3s ease-out;
    -o-transition: all 0.3s ease-out;
    -ms-transition: all 0.3s ease-out;
    transition: all 0.3s ease-out;
    color: #fff;
    background-color: #ff5d48;
    border-color: #ff5d48;
    font-weight: normal;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    cursor: pointer;
    user-select: none;
    border: 1px solid transparent;
    padding: 0.375rem 1rem;
    font-size: 1rem;
    line-height: 1.5;
    border-radius: 0.25rem;
	float:right;
}
</style>
</body>
</html>
