<?PHP
require_once("./include/membersite_config.php");
include("./include/mysqli_connect.php");
include("./include/html_codes.php");
include("./include/function.php");
$common_function = new common_function($connect);
$page_list= $common_function->page_list();
$option='<option value=""> Select page link</option>';
for($i=0; $i < count($page_list); $i++)
{
	$option .='<option value="'.$page_list[$i]["id"].'">'.$page_list[$i]["page_name"].'</option>';
}
$temp_list= $common_function->temp_list();
$temp='<option value=""> Select template link</option>';
for($i=0; $i < count($temp_list); $i++)
{
	$temp .='<option value="'.$temp_list[$i]["id"].'">'.$temp_list[$i]["template_name"].'</option>';
}


admin_way_top();

admin_top_bar();


admin_left_menu();

//After login check for page access

$id_user=$fgmembersite->idUser();
$admin = 0;
$pid=mysqli_real_escape_string($connect, $_GET['pid']);
$select_page="SELECT * FROM `page` WHERE id=".$pid;
$run=mysqli_query($connect,$select_page);
while($row=mysqli_fetch_array($run)){
	//$id = $row["id"];
	$page_title = $row["page_title"];
	$meta_desc = $row["meta_desc"];
	$page_name = $row["page_name"];
	$page_img = $row["page_img"];
	$page_heading = $row["page_heading"];
	$page_description = $row["page_description"];
	$temp_ids = $row["temp_ids"];
	$created_by = $row["created_by"];
}

if(isset($_POST['create_page'])){
	
	$page_name = htmlspecialchars(mysqli_real_escape_string($connect, $_POST['page_name']),ENT_QUOTES, 'UTF-8');
	$file=htmlspecialchars(mysqli_real_escape_string($connect, $_POST['banner_image']),ENT_QUOTES, 'UTF-8');
	$name=date('ymdis');
	if($_POST['banner_image']['name']!=''){
	$file_name = str_replace(" ","-",$page_name.$name).".jpg";
	//echo $file;exit;
	list($type, $file) = explode(';', $file);
	list(, $file)      = explode(',', $file);
	file_put_contents("../images/".$file_name , base64_decode($file));
	}else{
	$file_name=mysqli_real_escape_string($connect, $_POST['bg_image']);
	}
	/* $f_name[] = $file_name; */
	
	$page_title = htmlspecialchars(mysqli_real_escape_string($connect, $_POST['page_title']),ENT_QUOTES, 'UTF-8');
	$cmp_metadescription = htmlspecialchars(mysqli_real_escape_string($connect, $_POST['cmp_metadescription']),ENT_QUOTES, 'UTF-8');
	$page_heading = htmlspecialchars(mysqli_real_escape_string($connect, $_POST['page_heading']),ENT_QUOTES, 'UTF-8');
	
	$short_metadescription = htmlspecialchars(mysqli_real_escape_string($connect, $_POST['short_metadescription']),ENT_QUOTES, 'UTF-8');
	$template_id = mysqli_real_escape_string($connect, implode("~",$_POST['template_id']));
		$sql="UPDATE `page` SET `page_title`='".$page_title."', `meta_desc`='".$cmp_metadescription."', `page_name`='".$page_name."', `page_img`='".$file_name."', `page_heading`='".$page_heading."', `page_description`='".$short_metadescription."', `temp_ids`='".$template_id."', `created_by`='1' WHERE id=".$pid;
		$res=mysqli_query($connect, $sql);
		//$id=mysqli_insert_id($connect);
//echo "template_id";		
		$temp_id=$_POST['template_id'];
		$temp_pos_id=$_POST['temp_pos_id'];
		foreach($temp_id as $key => $val){
			mysqli_query($connect,"DELETE FROM `template_title_desc` WHERE `page_id_td`='".$pid."' AND `temp_pos_id`='".$temp_pos_id[$key]."' AND `temp_id`!='".$temp_id[$key]."'");
			mysqli_query($connect,"DELETE FROM `template_details` WHERE `page_id_td`='".$pid."' AND `temp_pos_id`='".$temp_pos_id[$key]."' AND `temp_id`!='".$temp_id[$key]."'");
			//echo $temp_id[$key].'ssss'.$temp_pos_id[$key];
		}
}


//redirect code


    $id_user=$fgmembersite->idUser();
	$approver = 0;$admin=0;
	if($fgmembersite->UserRole()=="admin"){
		$admin=1;
	}
	if($fgmembersite->UserRole()!="admin"){
		echo "<script>window.location.href='dashboard.php'</script>";
	}

//mysqli_query($connect, "TRUNCATE table temp_data");
?>
									
<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<style>
.cr_img img{max-width:500px !important;height:auto !important;}
.error_msg{
	color:red;
}
</style>
<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">
			
			<div class="row">
				<div class="col-xs-12">
					<div class="page-title-box">
						<h4 class="page-title">Edit Page</h4>
						<!--button class="btn btn-primary"  type="button" name="create_page" onclick="$('#sub_btn').click()" style="float:right">Create Page</button-->
								
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
			<!-- end row -->
                        <form method="POST" name="xxfrm" id="xxfrm" enctype="multipart/form-data">
				<div class="row">
					<div class="col-xs-12 col-lg-12 col-xl-12">
						<div class="card-box">
							<h4 class="header-title m-t-0 m-b-30">Page Details</h4>
							<section>
								<p class="text-muted m-b-20 font-13 pull-right">
									(* All fields are mandatory)
								</p>
								<div class="clearfix"></div>
								<div class="row page_title">
										
										<div class="col-lg-6 form-group">
											<label class="control-label " for="page_name">Page URL* </label>
                                            <input id="page_name" name="page_name" value="<?php echo $page_name; ?>"  type="text" class=" form-control" maxlength="255">
											<span class="error_msg" id="page_nameerror"></span>
										</div>
										<div class="col-lg-6 form-group">
										</div>
										<div class="col-lg-6 form-group">
											<label class="control-label " for="page_title">Page Title* </label>
                                            <input id="page_title" name="page_title" value="<?php echo $page_title; ?>"  type="text" class=" form-control" maxlength="255">
											<span class="error_msg" id="page_titleerror"></span>
										</div>
										<div class="col-lg-6 form-group">
											<label class="control-label " for="cmp_metadescription">Meta Description</label>
                                            <input id="cmp_metadescription" name="cmp_metadescription" value="<?php echo $meta_desc; ?>"  type="text" class=" form-control" maxlength="255">
										</div>
								</div>
					  <!------- header image, title---------->								
								<h4 class="header-title m-t-0 m-b-30">Banner Image*</h4>
								  <div class="form-group clearfix">
									 <div class="col-md-10 col-md-offset-1">
									 <div class="banner_error" id="banner_error" style="color:red;width: 600px;text-align: center;display:none">Please upload banner image!</div>
										<div class="block1_add_menu_toggle row simple-cropper-images" >
											<div class="cropme" style="width: 573px; height: 149px;"><img src="../images/<?php echo $page_img; ?>" /><center><br>Click on the icon to upload the banner image!</center></div>
											 <textarea id="banner_image_txt" name="banner_image" style="display:none"></textarea>
											 <span class="error_msg" id="banner_imageerror"></span>
											 <input type="hidden" name="bg_image" value="<?php echo $page_img; ?>" />
										</div>
										<br>
										
									 </div>
								  </div>
								  <div class="form-group clearfix">
								  </div>
							   
					  <!------- header image, title---------->
					  <div class="clearfix"></div>
								<div class="row page_title">
										
										<div class="col-lg-6 form-group">
											<label class="control-label " for="page_heading">Page Heading* </label>
                                            <input id="page_heading" name="page_heading" value="<?php echo $page_heading; ?>"  type="text" class=" form-control" maxlength="255">
											<span class="error_msg" id="page_headingerror"></span>
										</div>
										<div class="col-lg-12 form-group">
											<label class="control-label " for="short_metadescription">Short Description</label>
                                            <textarea id="short_metadescription" name="short_metadescription"  type="text" class=" form-control" maxlength="255"><?php echo $page_description; ?></textarea>
											<span class="error_msg" id="short_metadescriptionerror"></span>
										</div>
								</div>
								<?php 
								$i=1;
								$remove="";
								$tmp_array=explode("~",$temp_ids);
								//$tmp_ids=str_replace("~",",",$temp_ids);
								//var_dump($tmp_ids);exit;
								foreach($tmp_array as $key){
								$select_tmp="SELECT * FROM `templates` WHERE id ='$key'";
								//echo $select_tmp;exit;
								$run_tmp=mysqli_query($connect,$select_tmp);
								while($row_tmp=mysqli_fetch_array($run_tmp)){ ?>
								<div class="col-lg-12 form-group" id="remove_div_<?php echo $i; ?>"><div id="show_id_<?php echo $i; ?>"><a href="<?php echo $row_tmp['template_link'].'?pid='.$pid.'&tid='.$row_tmp['id'].'&tpos='.$i;?>" target="_blank" ><img src="images/<?php echo $row_tmp['template_icon']; ?>" id="img_id_<?php echo $i; ?>" style="width:100%"></a><br></div><input type="hidden" name="temp_pos_id[]" id="temp_pos_id_<?php echo $i; ?>" value="<?php echo $i; ?>" /><select name="template_id[]" id="template_id_<?php echo $i; ?>" onchange="select_temp(<?php echo $i; ?>);"><?php 
								
								$temps='<option value=""> Select template link</option>';
								for($j=0; $j < count($temp_list); $j++)
								{
									$selected="";
									if($temp_list[$j]["id"]==$row_tmp['id'])$selected="selected";
									$temps .='<option value="'.$temp_list[$j]["id"].'" '.$selected.'>'.$temp_list[$j]["template_name"].'</option>';
								}
								echo $temps; 
								?></select><?php echo $remove; ?></div>
								<?php 
								$i++;
								$remove='<span onclick=$("\#remove_div_'.$i.'").remove();>Remove</span>';
								}
								}					
								?>
							</section>
								
								<div id="add_menu_item">
										<br>
										+ &nbsp;<a class="btn btn-primary waves-effect waves-light m-r-5 m-b-10" id="block7_add_menu">Add Templates</a>
								</div>
						</div>
					</div>
					<div class="col-lg-12 form-group clearfix">
						<div class="card-box">
							<div class="col-lg-10 text-xs-center">
							<input type="hidden" name="create_page" id="create_page" value="submit" />
							<button class="btn btn-primary"  type="button" name="sub_btn" id="sub_btn">Update Page</button>
								
							</div>
						</div>
					</div>
				</div>
				</form>
			<button class="btn btn-custom waves-effect waves-light btn-sm page_create_succcess"  style="display:none;">Click me</button>
		</div> <!-- container -->

	</div> <!-- content -->

</div>



<!----------- Banner crop plugin  ------------------> 
	
		<link rel="stylesheet" type="text/css" href="simple_cropper/css/style.css" />
		<link rel="stylesheet" type="text/css" href="simple_cropper/css/style-example.css" />
		<link rel="stylesheet" type="text/css" href="simple_cropper/css/jquery.Jcrop.css" />

		<!-- Js files-->
		<script type="text/javascript" src="simple_cropper/scripts/jquery-1.10.2.min.js"></script>
		<script type="text/javascript" src="simple_cropper/scripts/jquery.Jcrop.js"></script>
		<script type="text/javascript" src="simple_cropper/scripts/jquery.SimpleCropper.js"></script>
		<script>
		 $('.cropme').simpleCropper();
		</script>
		
<!---------------- croper  --------------------->
<link rel="stylesheet" href="assets/css/choosenJs/prism.css">
	<link rel="stylesheet" href="assets/css/choosenJs/chosen.css">
	<link rel="stylesheet" href="assets/css/custom.css">
		<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/cropper/2.3.4/cropper.min.css'>
			
	
    <script src="assets/js/choosenJs/chosen.jquery.js" type="text/javascript">
    </script>
    <script src="assets/js/choosenJs/prism.js" type="text/javascript" charset="utf-8">
    </script>
    <script src="assets/js/choosenJs/init.js" type="text/javascript" charset="utf-8">
    </script>
		<script src='https://cdnjs.cloudflare.com/ajax/libs/cropperjs/0.8.1/cropper.min.js'></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>		
<script>
$(function() {
    // Multiple images preview in browser
    var imagesPreview = function(input, placeToInsertImagePreview) {

        if (input.files) {
            var filesAmount = input.files.length;

            for (i = 0; i < filesAmount; i++) {
                var reader = new FileReader();

                reader.onload = function(event) {
                    $($.parseHTML('<img>')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                }

                reader.readAsDataURL(input.files[i]);
            }
        }

    };

    $('#gallery-photo-add').on('change', function() {
        imagesPreview(this, 'div.gallery');
    });
});
</script>
            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->

<!-- Custom box css -->
<link href="assets/plugins/custombox/css/custombox.min.css" rel="stylesheet">

 <!-- Modal-Effect -->
<script src="assets/plugins/custombox/js/custombox.min.js"></script>
<script src="assets/plugins/custombox/js/legacy.min.js"></script>
			
<!-- Sweet Alert css -->
<link href="assets/plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css" />
<!-- Sweet Alert js -->
<script src="assets/plugins/bootstrap-sweetalert/sweet-alert.min.js"></script>
<script src="assets/pages/jquery.sweet-alert.init.js"></script>

<!-- Editor -->
<script src="//code.jquery.com/jquery-1.12.0.min.js"></script>


<script type="text/javascript">

var validExt = ".png, .jpeg, .jpg";
var validExt1 = ".png";
function fileExtValidate(fdata,id) { 
 var filePath = fdata.value;
 var getFileExt = filePath.substring(filePath.lastIndexOf('.') + 1).toLowerCase();
 if(id.includes("PImg") || id.includes("mPimg"))
 var pos = validExt1.indexOf(getFileExt);
else
var pos = validExt.indexOf(getFileExt);
	
 if(pos < 0) {
 	alert("This file is not allowed, please upload valid file.");
 	return false;
  } else {
  	return true;
  }
}

var slug = function(str) {
    str = str.substr(0, 35);
    var $slug = '';
    var trimmed = $.trim(str);
    $slug = trimmed.replace(/[^a-z0-9-]/gi, '-').
    replace(/-+/g, '-');
    return $slug.toLowerCase();
}

$(document).ready(function(){
$('#page_name').on('input', function() {
			$("#page_name").val(slug($("#page_name").val()));
	});

$("#add_more").on("change",".d_priority",function(){
	var len_priority=$(".d_priority").length;
	var priority_value=$(this).val();
	var this_pri=$(this);
	var n_priority=$(".d_priority").index(this);
	for(i=0;i<len_priority;i++){
		//$(".d_priority:nth-child(1)").val();
		if(priority_value==$(".d_priority:eq( "+i+" )").val() && n_priority!=i){
		//$(this).removeClass("form-control");		
        this_pri.css("border","1px solid #fb0505");
		alert("Change the priority");
		}else{
		this_pri.css("border","1px solid #ccc");
		}
		}
	//alert($(".d_priority").length);
	
});

/**************************ADD MORE FIELDS**************************************/
var max_fields      = 20; //maximum input boxes allowed
    var wrapper         = $("#add_more"); //Fields wrapper
    var add_button      = $(".add_field_button"); //Add button ID
   
    var x = 2; //initlal text box count
    $(add_button).click(function(e){ //on add input button click
	//debugger;
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
$(wrapper).append('<div class="col-lg-12 form-group remove_div_'+x+'"> <hr> </div><div class="col-lg-6 form-group remove_div_'+x+'"><label class="control-label " for="meta_description">Image '+x+'*</label><br/><input type="hidden" class=" form-control pimage" name="pimage[]" id="pimage_'+x+'" class="imgs" data-id="mPimg_'+x+'" ><br/><div class="container"><div class="row"><div class="panel panel-body"><div class="span4 cropme landscape" id="landscape"></div></div></div></div><img src="" id="mPimg_'+x+'" class="imsrc" width="150px"/><span class="errorcls pimageerror" id="pimageerror"></span></div><div class="col-lg-12 form-group remove_div_'+x+'"><label class="control-label " for="meta_description">Description '+x+'*</label><textarea id="description'+x+'" name="description[]" class=" form-control description"  ></textarea><span class="errorcls descriptionerror" id="descriptionerror"></span><br><a href="#" onclick="$(\'.remove_div_'+x+'\').remove();return false;" class="btn btn-primary remove_field">Remove</a><br></div>'); //add input box
CKEDITOR.replace( 'description'+x+'' );
$('#pimage_'+x).awesomeCropper(
        { width: 1165, height: 500, debug: true }
        );
x++; //text box increment

        }
    });
   
    /* $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('section').remove(); x--;
    }) */
/*******************************************************************************/
$(wrapper).on("change",".imgs", function(e) {
var id=$(this).attr("data-id");
var filename=$(this);
if(fileExtValidate(this,id)) {
var fr = new FileReader;
var ext = this.files[0].type.split('/').pop().toLowerCase();
if($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {
    alert('invalid extension!');
}
else{
	var size = parseFloat(this.files[0].size / 1024).toFixed(2);
    fr.onload = function(e) { // file is loaded
     	var img = new Image;
		img.onload = function() {
			
			 var imgwidth = this.width;
			var imgheight = this.height;
			var flag=0;
			if(id.includes("PImg") || id.includes("mPimg"))
			{
				if(imgwidth>550 || imgheight>520)
				{
				flag=1;
				alert("Max image dimesion 550*460");
				filename.val("");
				}
			}
			else
			{
				if(imgwidth<725 || imgheight<239)
				{
				flag=1;
				alert("Min image dimesion 725*239");
				filename.val("");
				}
			}
			if(flag==0)
			{
            $('#'+id).attr('src', e.target.result);
			}
			
			};
		img.src = fr.result; // is the data URL because called with readAsDataURL
    };
    fr.readAsDataURL(this.files[0]); // I'm using a <input type="file"> for demonstrating
}	
	}
	else
		$(this).val("");
		
});
	
/******************************************************************************/
$(".imgs").on('change', function () {
var id=$(this).attr("data-id");
var filename=$(this);
if(fileExtValidate(this,id)) {
var fr = new FileReader;
var ext = this.files[0].type.split('/').pop().toLowerCase();
if($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {
    alert('invalid extension!');
}
else{
	var size = parseFloat(this.files[0].size / 1024).toFixed(2);
    fr.onload = function(e) { // file is loaded
     	
		var img = new Image;
		
		img.onload = function() {
			
            var imgwidth = this.width;
			var imgheight = this.height;
			var flag=0;
			if(id.includes("PImg") || id.includes("mPimg"))
			{
				if(imgwidth>550 || imgheight>520)
				{
				flag=1;
				filename.val("");
				alert("Max image dimesion 550*460");
				
				}
			}
			else
			{
				if(imgwidth<725 || imgheight<239)
				{
				flag=1;
				filename.val("");
				alert("Min image dimesion 725*239");
				}
			}
			if(flag==0)
			{
            $('#'+id).attr('src', e.target.result);
			}
					
			};
		img.src = fr.result; // is the data URL because called with readAsDataURL
    };
    fr.readAsDataURL(this.files[0]); // I'm using a <input type="file"> for demonstrating
}	
	}
	else
		$(this).val("");
		
});	
	$('.page_create_succcess' ).click(function () {
		swal("Status!", "Page edited successfully.", "success");
	});	
	<?php  if(isset($_POST['create_page'])){ ?>
	$('.page_create_succcess').click();	
	<?php } ?>
	


	
});


$(document).on('click',"#sub_btn",function(){
				var xxfrm = $("#xxfrm");
				var page_name = $("#page_name");
				var page_nameerror = $("#page_nameerror");
				var page_title = $("#page_title");
				var page_titleerror = $("#page_titleerror");
				var page_heading = $("#page_heading");
				var page_headingerror = $("#page_headingerror");
				var short_metadescription = $("#short_metadescription");
				var short_metadescriptionerror = $("#short_metadescriptionerror");
				
			if(validatepage_name(page_name,page_nameerror,"Please enter page name") & validatepage_name(page_title,page_titleerror,"Please enter page title") & validatepage_name(page_heading,page_headingerror,"Please enter page heading"))
                {
                    xxfrm.submit();
                } 
                
				function validatepage_name(input,error,message)
                {
					//alert(page_name.val().length);
                  if(input.val().length<1)
                   {
                     error.text(message);
                     return false;	
                   }
                   else
                   {
                     error.text("");	
                     return true;
                   }    
                   
                } ///////////////   
                
		});

$(document).on("click", ".confirm",function(){
	window.location.href = "page-existing.php";
});

</script>



<style>
.radio, .checkbox{
	display:inline-block;
}
.all_widgets{
    margin-left: 39px;	
	border-radius: 75%;
    width: 63px;
    text-align: center;
    padding: 10px;
    cursor: pointer;
    color: black;
    border: solid 1px #64b0f2;
	background-color:#64b0f2;
}

</style>

<!-- custom scroll bar --->
<!--script src="js/jquery.mCustomScrollbar.concat.js"></script-->
<link rel="stylesheet" type="text/css" href="css/jquery.mCustomScrollbar.css" />
<!--<script src="js/page-create-boardofdirectory.js"></script>-->


<!-- Css files siva -->
<link href="components/imgareaselect/css/imgareaselect-default.css" rel="stylesheet" media="screen">
<link rel="stylesheet" href="css/jquery.awesome-cropper.css">


<?php
admin_right_bar();

admin_footer();
?>
<script>
	$("#page-create").attr("class","active");
</script>

<!-- /container by siva --> 

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> 
<script src="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script> 
<script src="components/imgareaselect/scripts/jquery.imgareaselect.js"></script> 
<script src="build/jquery.awesome-cropper.js"></script> 
<script>
    $(document).ready(function () {
        $('#pimage_1').awesomeCropper(
        { width: 1165, height: 500, debug: true }
        );
    });
</script> 
<style>
.awesome-cropper>img{
	width:150px;
}	
canvas {
    max-width: 150px;
}
#add_more .form-group {
    margin-bottom: 0;
}
.page_title .form-group {
    margin-bottom: 30px;
}
.cropme img{
	width:100%;
	height:100%;
}
</style>

<script>
   // Replace the <textarea id="editor1"> with a CKEditor
   // instance, using default configuration.
   
	var n=<?php echo --$i; ?>;
	$("#block7_add_menu").click(function(){
		n++;
		$("section").append('<div class="col-lg-12" id="remove_div_'+n+'"><div id="show_id_'+n+'"></div><select name="template_id[]" id="template_id_'+n+'" onchange="select_temp('+n+');"><?php echo $temp; ?></select><input type="hidden" name="temp_pos_id[]" id="temp_pos_id_'+n+'" value="'+n+'" /><span onclick=$("#remove_div_'+n+'").remove();>Remove</span><\/div>');
	});

function select_temp(id){
	var selected_value=document.getElementById("template_id_"+id).value;
	var temp_pos_id=document.getElementById("temp_pos_id_"+id).value;
	var page_id=<?php echo $pid; ?>;
	$.ajax({
			url: "ajaxCalls.php", 
			async:true,
			method:"post",
			data:{"action":"selectTemp","id":selected_value,"page_id":page_id,"temp_pos":temp_pos_id},
			success: function(result){
				$("#show_id_"+id).html(result);
			}
		});
}
	
	
	
/* 	$("#block7_add_menu").click(function(){
		n++;
		$("section").append('<div class="col-lg-12 form-group" id="remove_div_'+n+'"><img src="" id="img_id_'+n+'" style="width:100%"><br><select name="template_id[]" id="template_id_'+n+'" onchange="select_temp('+n+');"><?php echo $temp; ?></select><span onclick=$("#remove_div_'+n+'").remove();>Remove</span><\/div>');
	});

function select_temp(id){
	var selected_value=document.getElementById("template_id_"+id).value;
	var img_src=document.getElementById("img_id_"+id);
	img_src.src="images/template"+selected_value+".png";
	//alert(selected_value);
} */
</script>