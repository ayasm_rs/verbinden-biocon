<?PHP
require_once("./include/membersite_config.php");
include("./include/mysqli_connect.php");
include("./include/html_codes.php");

admin_way_top();

admin_top_bar();


admin_left_menu();

//After login check for page access

$id_user=$fgmembersite->idUser();
$admin = 0;



if(isset($_POST['create_page'])){
	$cmp_metadescription = htmlspecialchars(mysqli_real_escape_string($connect, $_POST['cmp_metadescription']),ENT_QUOTES, 'UTF-8');
	$page_title = htmlspecialchars(mysqli_real_escape_string($connect, $_POST['page_title']),ENT_QUOTES, 'UTF-8');
	$p_description = htmlspecialchars(mysqli_real_escape_string($connect, $_POST['p_description']),ENT_QUOTES, 'UTF-8');
	$eid = htmlspecialchars(mysqli_real_escape_string($connect, $_POST['eid']),ENT_QUOTES, 'UTF-8');
	$banner_image = htmlspecialchars(mysqli_real_escape_string($connect, $_POST['banner_image']),ENT_QUOTES, 'UTF-8');
	$cmp_shortdescription = htmlspecialchars(mysqli_real_escape_string($connect, $_POST['cmp_shortdescription']),ENT_QUOTES, 'UTF-8');
	
	$pps=strpos($banner_image,"data:image");
	

	if($pps>=0){
		$exist_img=htmlspecialchars(mysqli_real_escape_string($connect, $_POST['exist_img']),ENT_QUOTES, 'UTF-8');
		$block1_banner_image = convertAndSaveImages($banner_image,"About Us",$exist_img);
	}
	
	if($eid!=''){
		$str="";
		
		if(trim($block1_banner_image)!='')
			$str.=", `image_path`='".$block1_banner_image."'";
		
		$sql="UPDATE `page_title_des` SET `metadescription`='$cmp_metadescription', `page_title`='$page_title',`page_short_description`='$cmp_shortdescription', page_description='$p_description' ".$str." WHERE id='$eid' ";
		mysqli_query($connect, $sql);
		$id=$eid;
		
	}else{
		$sql="INSERT INTO `page_title_des` (`metadescription`, `page_title`, `page_description`, `page_name`, `image_path`, `page_short_description`) VALUES ('".$cmp_metadescription."', '".$page_title."', '".$p_description."' ,'about us','".$block1_banner_image."','".$cmp_shortdescription."')";
		$res=mysqli_query($connect, $sql);
		$id=mysqli_insert_id($connect);	
	}
	$date=date('Y-m-d');
		  
    $date_time=date('Y-m-d H:i:s');
	mysqli_query($connect,"INSERT INTO `latest_updates`(`temp_page_id`, `edited_by`, `edited_type`, `updated_date`, `approved_by`, `page_id`, `table_name`, `updated_date_time`) VALUES ('','$id_user','added','$date','','$id','about us','$date_time')");
	
}


//redirect code


    $id_user=$fgmembersite->idUser();
	$approver = 0;$admin=0;
	if($fgmembersite->UserRole()=="admin"){
		$admin=1;
	}
	if($fgmembersite->UserRole()!="admin"){
		echo "<script>window.location.href='dashboard.php'</script>";
	}
/*************************/
$select_event="SELECT * FROM `page_title_des` WHERE page_name='about us' ORDER BY id DESC LIMIT 1";
$run_product=mysqli_query($connect,$select_event);
while($row_event=mysqli_fetch_array($run_product)){
	$metadescription=$row_event["metadescription"];
	$page_title=$row_event["page_title"];
	$page_description=$row_event["page_description"];
	$eid=$row_event["id"];
	$imgFile=$row_event["image_path"];
	$page_short_description=$row_event["page_short_description"];
	
	
}
/*************************/
	
	
//mysqli_query($connect, "TRUNCATE table temp_data");
?>
<style>
.cropme img{
width:100%;
height:100%;
}
.preview_error {
    /* background-color: red; */
  
    color: red;
    font-size: 12px;
    float: none !important;
    display: block;
    width: 100%;
}
</style>									
<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">
			
			<div class="row">
				<div class="col-xs-12">
					<div class="page-title-box">
						<h4 class="page-title">About Us Overview</h4>
						<!--button class="btn btn-primary"  type="button" name="create_page" onclick="$('#sub_btn').click()" style="float:right">Create Page</button-->
								
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
			<!-- end row -->
                        <form method="POST" name="xxfrm" id="xxfrm" enctype="multipart/form-data">
				<div class="row">
					<div class="col-xs-12 col-lg-12 col-xl-12">
						<div class="card-box">
							<h4 class="header-title m-t-0 m-b-30">Page Details</h4>
							<section>
								<p class="text-muted m-b-20 font-13 pull-right">
									(* All fields are mandatory)
								</p>
								<div class="clearfix"></div>
								<div class="row">
										
										<div class="col-lg-6 form-group">
											<label class="control-label " for="page_title">Page Title <span style=" font-weight: normal; color: #039cfd; " id="page_title_len">[ 60 character remaining ]</span></label>
                                            <input id="page_title" name="page_title" value="<?php echo $page_title; ?>"  type="text" class=" form-control" maxlength="255">
											<input type="hidden" name="eid" value="<?php echo $eid; ?>" >
											<?php if(trim($imgFile)!=''){?>
											<input type="hidden"  name="exist_img" value="<?php echo trim($imgFile); ?>" />
											<?php } ?>
										</div>
										<div class="col-lg-6 form-group">
											<label class="control-label " for="cmp_metadescription">Meta Description</label>
                                            <input id="cmp_metadescription" name="cmp_metadescription" value="<?php echo $metadescription; ?>"  type="text" class=" form-control" maxlength="255">
										</div>
								</div>
								<div class="row">
										
										<div class="col-lg-12 form-group">
											<label class="control-label " for="banner_image">Upload Image *</label>
											<div class="block1_add_menu_toggle row simple-cropper-images" >
											<div class="cropme" style="width: 573px; height: 149px;">
											<?php if(trim($imgFile)!=''){?>
											<img src="<?php echo "../images/banner-images/".trim($imgFile); ?>">
											<?php } ?>
											</div>
											<div > <center><br>Click on the icon to upload the banner image!</center></div>
											 <textarea id="banner_image_txt" name="banner_image" style="display:none">
											 </textarea>
											 <span id="p_img_re" class="preview_error"></span>
											</div>
										</div>
								</div>
								<h4 class="header-title m-t-0 m-b-30">About Us</h4>
								<div class="row">
								<div class="col-lg-6 form-group">
								<label class="control-label " for="meta_description">Short Description*</label>
								<input id="cmp_shortdescription" name="cmp_shortdescription" value="<?php echo $page_short_description; ?>"  type="text" class=" form-control" maxlength="255">
								</div>
									<div class="col-lg-12 form-group">
											<label class="control-label " for="meta_description">Page Overview*</label>
                                            <textarea id="p_description" name="p_description" class=" form-control" ><?php echo $page_description; ?></textarea>
											<span id="p_descriptionerror" class="errorcls"></span>
										</div>
									</div>
								</div>
									
							</section>
						</div>
					</div>
					<div class="col-lg-12 form-group clearfix">
						<div class="card-box">
							<div class="col-lg-10 text-xs-center">
							<input type="hidden" name="create_page" id="create_page" value="submit" />
							<button class="btn btn-primary"  type="button" name="sub_btn" id="sub_btn">Create Page</button>
								
							</div>
						</div>
					</div>
				</div>
				</form>
			<button class="btn btn-custom waves-effect waves-light btn-sm page_create_succcess"  style="display:none;">Click me</button>
		</div> <!-- container -->

	</div> <!-- content -->

</div>



<!--center>
<div class="container-narrow">
  <div>
    <h1>jQuery jQuery Awesome Cropper Demo</h1>
    
    <form role="form">
      <input id="sample_input" type="hidden" name="test[image]">
    </form>
  </div>
  <hr>
  
</div></center-->

<!-- End content-page -->

            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->
			
<!-- Custom box css -->
<link href="assets/plugins/custombox/css/custombox.min.css" rel="stylesheet">

 <!-- Modal-Effect -->
<script src="assets/plugins/custombox/js/custombox.min.js"></script>
<script src="assets/plugins/custombox/js/legacy.min.js"></script>
			
<!-- Sweet Alert css -->
<link href="assets/plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css" />
<!-- Sweet Alert js -->
<script src="assets/plugins/bootstrap-sweetalert/sweet-alert.min.js"></script>
<script src="assets/pages/jquery.sweet-alert.init.js"></script>

<!-- Editor -->
<script src="//code.jquery.com/jquery-1.12.0.min.js"></script>


<script type="text/javascript">

var validExt = ".png, .jpeg, .jpg";
var validExt1 = ".png";
function fileExtValidate(fdata,id) { 
 var filePath = fdata.value;
 var getFileExt = filePath.substring(filePath.lastIndexOf('.') + 1).toLowerCase();
 if(id.includes("PImg") || id.includes("mPimg"))
 var pos = validExt1.indexOf(getFileExt);
else
var pos = validExt.indexOf(getFileExt);
	
 if(pos < 0) {
 	alert("This file is not allowed, please upload valid file.");
 	return false;
  } else {
  	return true;
  }
}
$(document).ready(function(){

	$('.page_create_succcess' ).click(function () {
		swal("Status!", "About us successfully updated.", "success");
	});	
	<?php  if(isset($_POST['create_page'])){ ?>
	$('.page_create_succcess').click();	
	<?php } ?>
	


	
});


$(document).on('click',"#sub_btn",function(){
				var xxfrm = $("#xxfrm");			
                var p_description       = $("#p_description");
                var p_descriptionerror  = $("#p_descriptionerror");
                var p_img_re  = $(".preview_error");
				
				
			
			if(validatep_description() && validateimg() )
                {
                    xxfrm.submit();
                } 
                
				                                           
                function validatep_description()
                {
                  if(CKEDITOR.instances['p_description'].getData().length<2)
                   {

                     p_description.removeClass("form-control");		
                     p_description.addClass("form-control errortxtbox");
                     p_descriptionerror.text("Please enter page overview");

                     return false;	
                   }
                   else
                   {
                     p_description.removeClass("form-control errortxtbox");	
                     p_description.addClass("form-control");		
                     p_descriptionerror.text("");	

                     return true;
                   }    
                   
                } ///////////////  
				
				function validateimg()
				{
					
					var div = $(".cropme").find('img');
					legt=div.length;
					if(legt>0)
					{
						p_img_re.text("");
						return true;
					}
					else
					{
						p_img_re.text("Please browse the image");
						return false;
					} 
				}
				
		});

$(document).on("click", ".confirm",function(){
	window.location.href = "page-existing.php";
});
$("#page_title").keyup(function(){
	var leng=$("#page_title").val().length;
	$("#page_title_len").text(" [ "+(60-leng)+" character remaining ]");
});



</script>



<style>
.radio, .checkbox{
	display:inline-block;
}
.all_widgets{
    margin-left: 39px;	
	border-radius: 75%;
    width: 63px;
    text-align: center;
    padding: 10px;
    cursor: pointer;
    color: black;
    border: solid 1px #64b0f2;
	background-color:#64b0f2;
}

</style>

<!-- custom scroll bar --->
<!--script src="js/jquery.mCustomScrollbar.concat.js"></script-->
<link rel="stylesheet" type="text/css" href="css/jquery.mCustomScrollbar.css" />
<!--<script src="js/page-create-boardofdirectory.js"></script>-->


<!-- Css files siva -->
<link href="components/imgareaselect/css/imgareaselect-default.css" rel="stylesheet" media="screen">
<link rel="stylesheet" href="css/jquery.awesome-cropper.css">


<?php
admin_right_bar();

admin_footer();
?>
<script>
	$("#page-existing").attr("class","active");
</script>

<!-- /container by siva --> 

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> 
<script src="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script> 
<script src="components/imgareaselect/scripts/jquery.imgareaselect.js"></script> 
<script>
     CKEDITOR.replace( 'p_description' );
</script>
<?php admin_image_cropper(); ?>
