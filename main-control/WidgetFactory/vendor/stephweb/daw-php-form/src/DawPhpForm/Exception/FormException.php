<?php

namespace DawPhpForm\Exception;

use Exception;

/**
 * @link     https://github.com/stephweb/daw-php-form
 * @author   Stephen Damian <contact@devandweb.fr>
 * @license  MIT License
 */
class FormException extends Exception
{

}
