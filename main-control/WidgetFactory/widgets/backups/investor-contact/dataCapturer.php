<?php
/**
 * Created by PhpStorm.
 * User: ays29
 * Date: 6/17/19
 * Time: 8:36 AM
 */

$fh=$this->formHandler;

$data=$_POST;
$data['alert_types']=implode(',',$data['alert_types']);
$data['referrer_pid']=$this->context['pid'];

$validator = new Zend\Validator\Db\NoRecordExists([
    'table'   => 'mailing_list',
    'field'   => 'email',
    'adapter' => $this->dbAdapter,
]);

if ($validator->isValid($data['email'])) {
    $insert=$this->sql->insert('mailing_list');
    $insert->values($data);
    $str=$this->sql->buildSqlString($insert);
    $statement=$this->dbAdapter->query($str);
    $statement->execute();

    //send the mail    
    $mailbody=$this->render('mail.twig',['data'=>$data]);
    $response=$this->mail('sendgrid','vinay@iverbinden.com','brumsfum@mailinator.com','A new subscriber was added to the site',$mailbody);
    $output['mailstatus']=$response->body();

    $output['status'] = true;
    $output['messages'] = "Subscriber added successfully!";
} else {
    $output['status'] = false;
    $output['messages'] = $validator->getMessages();
}

echo json_encode($output);

