<?php
/**
 * Created by PhpStorm.
 * User: ays29
 * Date: 6/17/19
 * Time: 7:41 AM
 */
$opts=$this->getWidgetOption();
$contactData=$opts['data'];
$output= $this->render('display.twig',[
    'wid'=>$this->context['tid'],
    'pid'=>$this->context['pid'],
    'tpos'=>$this->context['tpos'],
    'attach_uri'=>$this->get_uploads_uri(),
    'data'=>$contactData
]);