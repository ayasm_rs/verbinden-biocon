<?php
/**
 * Created by PhpStorm.
 * User: Ayas
 * Date: 6/17/19
 * Time: 7:37 AM
 */
$isDashboard=true;
if($this->context['pid']!=0){

    $isDashboard=false;
}
$data=$this->getWidgetOption()['data'];

$formOut=$this->render('customform.twig',[
    'data'=>$data,
    'attach_uri'=>$this->get_uploads_uri(),
    'endpoint'=>$actionURL,
    'import_btn'=>!$isDashboard
],false);