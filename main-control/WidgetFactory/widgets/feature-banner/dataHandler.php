<?php

/**
 * Created by PhpStorm.
 * User: Ayas
 * Date: 6/22/19
 * Time: 8:06 PM
 */
$fh = $this->formHandler;

//require all fields
$fh->required = "link_title,link_dest";
// set our upload directory
$fh->upload_dir = $this->get_uploads_dir();

// rename our upload with a 32-character hash(php7 only)
$fh->upload_rename = 'hash';

// only allow PDFs to be uploaded
$fh->upload_accepted_types = 'images,pdf';



$proceed = isset($_GET['isAjax']);
if ($proceed)
    $proceed = $_GET['isAjax'];

if ($proceed) {

    $ajax = (array) json_decode(file_get_contents('php://input'));
    try {
        switch ($fh->post('action')) {
            case 'insert':
                error_reporting(0);
                $banner_file = $fh->post('banner_img');
                $banner_pdf = $fh->post('link_dest');
                error_reporting(E_ALL);
                $data = $this->getWidgetOption()['data'];
                
                if ($banner_file == '') {                    
                    $fh->info_message('Banner image unchanged.');
                } else {

                    $data['banner_img'] = $banner_file['name'];
                
                    
                    $fh->info_message('Banner image changed.') ;
                }
                $data['link_title'] = $fh->post('link_title');                
                if($banner_pdf==''){
                    $fh->info_message('Banner PDF unchanged.');
                }else{
                    $data['link_dest'] = $banner_pdf['name'];
                    $fh->info_message('Banner PDF changed.');
                }




                $output['data'] = $data;


                $this->addWidgetOption($data);

                break;
            case 'import':
                $opts = $this->getWidgetDefault();
                $toImport = $opts['data'];
                $this->addWidgetOption($toImport);
                $fh->success_message('Data Imported Successfuly!');
                break;


            default:
                throw new Exception('Not Quite My Action.');
        }
        $output['status'] = !$fh->errors();
        $output['message'] = $fh->messages();
    } catch (Exception $e) {
        $output['status'] = false;
        $output['message'] = $e->getMessage();
    } finally {
        if (isset($output)) {
            header('Content-Type: application/json');
            echo (json_encode($output));
        }
    }
} else {
    die('Invalid Access');
}
