<?php
/**
 * Created by PhpStorm.
 * User: Ayas
 * Date: 6/22/19
 * Time: 8:06 PM
 */



$proceed = isset($_GET['isAjax']);
if ($proceed)
    $proceed = $_GET['isAjax'];

if ($proceed) {

    $ajax=json_decode(file_get_contents('php://input'),true);
    try {
        switch ($ajax['action']) {
            case 'update_features':
                $data=$ajax['data'];
                $this->addWidgetOption($data['featureData'],$data['block_title'],$data['block_description']);
                $output['status']=true;
                $output['message']="Data Entered Successfully";
                break;
            case 'import_features':
                $opts=$this->getWidgetDefault();
                $default_featuredata=$opts['data'];
                if(!$default_featuredata){
                    throw new Exception('No data to import!');
                }
                $opts=$this->getWidgetOption();
                $current_featureData=(array)$opts['data'];
                $final_featuredata=array_merge($default_featuredata,$current_featureData);
                $this->addWidgetOption($final_featuredata,$opts['title'],$opts['description']);
                $output['status']=true;
                $output['message']="Data Imported Successfully";

                break;



            default:
                throw new Exception('Not Quite My Action.');

        }
    } catch (Exception $e) {
        $output['status'] = false;
        $output['message'] = $e->getMessage();

    } finally {
        if (isset($output)){
            header('Content-Type: application/json');
            echo(json_encode($output));
        }

    }


} else {
    die('Invalid Access');
}