<?php
/**
 * Created by PhpStorm.
 * User: Ayas
 * Date: 6/17/19
 * Time: 7:37 AM
 */
$fh=$this->formHandler;

if($this->context['pid']!=0){
    $isDashboard=false;
}else{
    $isDashboard=true;
}


//fetch contact data of page in widget options
$opts=$this->getWidgetOption();


$formOut=$this->render('adminView.twig',[
    'endpoint'=>$actionURL,
    'block_title'=>$opts['title'],
    'block_description'=>$opts['description'],
    'featureData'=>$opts['data'],
    'all'=>$opts,
    'import_btn'=>!$isDashboard,
],false);