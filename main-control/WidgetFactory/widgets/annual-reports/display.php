<?php
/**
 * Created by PhpStorm.
 * User: ays29
 * Date: 6/5/19
 * Time: 1:15 AM
 */
$select=$this->sql->select('annual_reports');

$select->columns([
    'id'=>'id',
    'year'=>new Zend\Db\Sql\Expression("date_format(`rep_year`,'%Y')"),
    'title'=>'cons_rep_title',
    'thumbnail'=>'cons_rep_thumb',
    'main_rep_pdf'=>'cons_rep_pdf',
    'sup_reports'=>'sup_rep_details'

]);
$select->where('pid='.$this->context['pid']);
$select->order('year DESC');
$selectStatement=$this->sql->prepareStatementForSqlObject($select);
$results=$selectStatement->execute();


$this->sqlResults->initialize($results);
$resultArray=$this->sqlResults->toArray();
foreach ($resultArray as $key=>$index) {
    $resultArray[$key]['sup_reports']=(array)json_decode($index['sup_reports']);
}



$select2=$this->sql->select('annual_reports');
$select2->columns([
    'year'=>new Zend\Db\Sql\Expression("date_format(`rep_year`,'%Y')"),
]);
$select2->order('year DESC');
$select2->where('pid='.$this->context['pid']);
$select2->group(['year']);
$selectStatement2=$this->sql->prepareStatementForSqlObject($select2);
$results2=$selectStatement2->execute();
$this->sqlResults->initialize($results2);
$resultArray2=$this->sqlResults->toArray();



$output= $this->render('display.twig',[
    'data'=>$resultArray,
    'fyears'=>$resultArray2,
    'attach_path'=>$this->get_uploads_uri(),
    'tpos'=>$this->context['tpos']
]);

//$output=var_export($output);
