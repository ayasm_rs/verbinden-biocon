<?php

//fetch records for edit view
$select=$this->sql->select('annual_reports');

$select->columns([
    'id'=>'id',
    'f_year'=>new Zend\Db\Sql\Expression("date_format(`rep_year`,'%Y')"),
    'title'=>'cons_rep_title',

]);
$select->where('pid='.$this->context['pid']);
$select->order('f_year DESC');
$selectStatement=$this->sql->prepareStatementForSqlObject($select);
$results=$selectStatement->execute();


$this->sqlResults->initialize($results);
$resultArray=array();
foreach ($this->sqlResults as $row) {
    $resultArray[]=(array)$row;
}





$ajaxuri=$actionURL;
$import_btn=true;
if($this->context['pid']==0){
    $import_btn=false;
}

$formOut=$this->render('customform.twig',[
    'actionUrl'=>$ajaxuri,
    'blank'=>basename(__FILE__),
    'attach_path'=>$this->get_uploads_uri(),
    'data'=>$resultArray,
    'import_btn'=>$import_btn,
],false);


