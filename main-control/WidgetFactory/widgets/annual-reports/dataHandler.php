<?php
/**
 * Created by PhpStorm.
 * User: ays29
 * Date: 5/30/19
 * Time: 5:02 PM
 */

$fh = $this->formHandler;


// set our upload directory
$fh->upload_dir = $this->get_uploads_dir();

// rename our upload with a 32-character hash(php7 only)
$fh->upload_rename = 'hash';

// only allow PDFs and images to be uploaded
$fh->upload_accepted_types = 'pdf,pdf,images';
$fh->upload_max_filesize = 8145728;

//Handle Ajax
if (isset($_GET['isAjax'])) {
    if ($_GET['isAjax']) {

        try {
            switch ($fh->post('action')) {
                case "insert":
                    //require all fields
                    $fh->required = "*";

                    $file = array();
                    $data;

                    //consolidated report params
                    $cons_ann_rep['title'] = $fh->post('cons_ann_rep_title');
                    error_reporting(0); //suppressing warning messages for file uploads
                    $suppl_rep['files'] = $fh->post('suppl_rep_file');
                    $cons_ann_rep['pdf'] = $fh->post('cons_ann_rep_pdf')['name'];
                    $cons_ann_rep['thumb'] = $fh->post('cons_ann_rep_thumb')['name'];
                    error_reporting(E_ALL);

                    $rep_yr = $fh->post('rep_yr');
                    $date_format = "Y";
                    $rep_date = DateTime::createFromFormat($date_format, $rep_yr);

                    //supplementary reports
                    $suppl_rep['titles'] = $fh->post('suppl_rep_title');
                    //throw new Exception(json_encode($suppl_rep));

                    $sup_rep_obj = [];

                    if (gettype($suppl_rep['titles']) == 'array') {
                        for ($i = 0; $i < sizeof($suppl_rep['titles']); $i++) {
                            $sup_rep_obj[$i] = array(
                                "title" => $suppl_rep['titles'][$i],
                                "pdf" => $suppl_rep['files'][$i]['name'],
                            );

                        }
                    } else {
                        $sup_rep_obj[] = array(
                            "title" => $suppl_rep['titles'],
                            "pdf" => $suppl_rep['files']['name'],
                        );
                    }


                    //check if record exists for year-page combination and update
                    $select = $this->sql->select();
                    $select
                        ->from('annual_reports')
                        ->columns([
                            'id' => 'id',
                            'pid' => 'pid',
                            'year' => new Zend\Db\Sql\Expression("date_format(`rep_year`,'%Y')")
                        ])
                        ->where->equalTo('pid', $this->context['pid'])
                        ->where->equalTo(new Zend\Db\Sql\Expression("date_format(`annual_reports`.`rep_year`,'%Y')"), $rep_yr);
                    $select->group(['pid', 'year']);


                    $validator = new Zend\Validator\Db\RecordExists($select);
                    $validator->setAdapter($this->dbAdapter);
                    $statement = $this->sql->prepareStatementForSqlObject($validator->getSelect());
                    $this->sqlResults->initialize($statement->execute());
                    $data = $this->sqlResults->toArray();


                    //$sqlstr=$this->sql->buildSqlString($validator->getSelect());
                    //throw new Exception($rep_yr);


                    // If found then update the record

                    if (sizeof($data) > 0) {
                        $update = $this->sql->update('annual_reports');
                        $update->set([
                            'cons_rep_title' => $cons_ann_rep['title'],
                            'cons_rep_thumb' => $cons_ann_rep['thumb'],
                            'cons_rep_pdf' => $cons_ann_rep['pdf'],
                            'sup_rep_details' => json_encode($sup_rep_obj),
                            'rep_year' => date_format($rep_date, 'Y-m-d'),
                        ]);
                        $update->where([
                            'pid' => $this->context['pid'],
                            'id' => $data[0]['id']
                        ]);
                        $query = $this->sql->prepareStatementForSqlObject($update);

                    } //else insert new record
                    else {
                        $messages = $validator->getMessages();
                        foreach ($messages as $message) {
                            $fh->info_message($message);
                        }


                        $insert = $this->sql->insert();
                        $insert->into('annual_reports');
                        $insert->values([
                            'pid' => $this->context['pid'],
                            'cons_rep_title' => $cons_ann_rep['title'],
                            'cons_rep_thumb' => $cons_ann_rep['thumb'],
                            'cons_rep_pdf' => $cons_ann_rep['pdf'],
                            'sup_rep_details' => json_encode($sup_rep_obj),
                            'rep_year' => date_format($rep_date, 'Y-m-d'),

                        ]);
                        $query = $this->sql->prepareStatementForSqlObject($insert);


                    }
                    $query->execute();


                    break;
                case 'edit':
                    $fh->required='did';
                    //ingest data
                    // var_dump($_POST);
                    // var_dump($_FILES);
                    // $uploaddir = '/var/www/biocon/html/downloads/';
                    // $uploadfile = $uploaddir . basename($_FILES['cons_ann_rep_thumb']['name']);

                    
                    // if (move_uploaded_file($_FILES['cons_ann_rep_thumb']['tmp_name'], $uploadfile)) {
                    //     $fh->info_message( "File is valid, and was successfully uploaded.");
                    // } else {
                    //     $fh->info_message("Possible file upload attack!");
                    // }



                    $in['did']=$fh->post('did','Record Identifier');
                    $in['cons_ann_rep_title']=$fh->post('cons_ann_rep_title');
                    $in['cons_ann_rep_thumb_existing']=$fh->post('cons_ann_rep_thumb_existing');
                    $in['cons_ann_rep_pdf_existing']=$fh->post('cons_ann_rep_pdf_existing');
                    $in['rep_yr']=$fh->post('rep_yr','Report Year');
                    $date_format = "Y";
                    $rep_date = DateTime::createFromFormat($date_format, $in['rep_yr']);
                    $in['rep_yr']=date_format($rep_date, 'Y-m-d');
                    $in['suppl_rep_title']=$fh->post('suppl_rep_title');
                    $in['suppl_rep_file_existing']=$fh->post('suppl_rep_file_existing');
                    error_reporting(E_ALL);
                    $fh->upload_accepted_types = 'pdf,pdf';
                    $in['cons_ann_rep_pdf']=$fh->post('cons_ann_rep_pdf');
                    $in['suppl_rep_file']=$fh->post('suppl_rep_file');
                    if($_FILES['cons_ann_rep_thumb']['name']!=''){
                        $fh->upload_accepted_types = 'images';
                        $in['cons_ann_rep_thumb']=$fh->post('cons_ann_rep_thumb');
                    }
                    
                    
                    error_reporting(E_ALL);

                   /*  echo("\n IN:");
                    var_dump($in); */





                    //Prepare supplementary report json object
                    $supplRep_obj=[];
                        //Process Here
                    $files=$_FILES['suppl_rep_file'];
                    $files_counter=-1;
                    if(gettype($in['suppl_rep_title'])=='array'){
                        for($i = 0; $i < sizeof($in['suppl_rep_title']); $i++){
                            if($files['name'][$i]!=''){
                                $files_counter++;
                            }
                            $supplRep_obj[$i]=array(
                                "title" => $in['suppl_rep_title'][$i],
                                "pdf" => ($files['name'][$i]=='')?$in['suppl_rep_file_existing'][$i]:$in['suppl_rep_file'][$files_counter]['name'],
                            );

                        }
                    }else{
                        $supplRep_obj[]=array(
                            "title" => $in['suppl_rep_title'],
                            "pdf" => ($in['suppl_rep_file']=='')?$in['suppl_rep_file_existing']:$in['suppl_rep_file']['name'],
                        );
                    }
                    $in['suppl_rep']=json_encode($supplRep_obj);

                    //Update records
                    $update=$this->sql->update('annual_reports');
                    $update->set([
                        'cons_rep_title'=>$in['cons_ann_rep_title'],
                        'cons_rep_thumb'=>($in['cons_ann_rep_thumb']=='')?$in['cons_ann_rep_thumb_existing']:$in['cons_ann_rep_thumb']['name'],
                        'cons_rep_pdf'=>($in['cons_ann_rep_pdf']=='')?:$in['cons_ann_rep_pdf']['name'],
                        'sup_rep_details'=>$in['suppl_rep'],
                        'rep_year'=>$in['rep_yr'],
                    ]);
                    $update->where([
                        'id'=>$in['did'],
                        'pid'=>$this->context['pid'],
                    ]);
                    //echo $this->sql->buildSqlString($update);
                    $query= $this->sql->prepareStatementForSqlObject($update);
                    $query->execute();


                    break;
                case "fetch":
                    $rep_yr = $fh->post('year');
                    $select = $this->sql->select();
                    $select
                        ->from('annual_reports')
                        ->columns([
                            'id'=>'id',
                            'year' => new Zend\Db\Sql\Expression("date_format(`rep_year`,'%Y')"),
                            'title' => 'cons_rep_title',
                            'thumbnail' => 'cons_rep_thumb',
                            'main_rep_pdf' => 'cons_rep_pdf',
                            'sup_reports' => 'sup_rep_details'
                        ])
                        ->where->equalTo('pid', $this->context['pid'])
                        ->where->equalTo(new Zend\Db\Sql\Expression("date_format(`annual_reports`.`rep_year`,'%Y')"), $rep_yr);
                    $select->group(['pid', 'year']);
                    $select->limit(1);
                    $query = $this->sql->prepareStatementForSqlObject($select);
                    $this->sqlResults->initialize($query->execute());
                    $retrieved=$this->sqlResults->toArray();
                    foreach ($retrieved as $key=>$index) {
                        $retrieved[$key]['sup_reports']=(array)json_decode($index['sup_reports']);
                    }
                    $out['info']=$retrieved;


                    break;
                case "delete":

                    $id = $fh->post('did');
                    $delete = $this->sql->delete('annual_reports');
                    $delete->where([
                        'id' => $id,
                        'pid' => $this->context['pid']
                    ]);
                    $sqlString = $this->sql->buildSqlString($delete);
                    $statement = $this->dbAdapter->query($sqlString);
                    $statement->execute();


                    break;
                case 'getdummy':
                    $select = $this->sql->select('annual_reports');
                    $select->where(['pid' => 0]);
                    $select->columns([
                        'cons_rep_title'=>'cons_rep_title',
                        'cons_rep_thumb'=>'cons_rep_thumb',
                        'cons_rep_pdf'=>'cons_rep_pdf',
                        'sup_rep_details'=>'sup_rep_details',
                        'rep_year'=>'rep_year',
                        'pid' => new \Zend\Db\Sql\Expression($this->context['pid'])
                    ]);
                    $selectStr = $this->sql->buildSqlString($select);
                    $insertSelect_Q = "INSERT INTO `annual_reports` (`cons_rep_title`,`cons_rep_thumb`,`cons_rep_pdf`,`sup_rep_details`,`rep_year`,`pid`)  " . $selectStr;
                    //throw new Exception($insertSelect_Q);
                    $statement = $this->dbAdapter->query($insertSelect_Q);
                    $statement->execute();
                    break;
            }
            $out['success'] = !$fh->errors();
            $out['messages'] = $fh->messages();
        }
        catch
            (Exception $e){
                $out['success'] = false;
                $out['messages'] = $e->getFile() . "::" . $e->getMessage();
            }
        finally{
                header('Content-Type: application/json');
                echo json_encode($out);

            }






    }
}



