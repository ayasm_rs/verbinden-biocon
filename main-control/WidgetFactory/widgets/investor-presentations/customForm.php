<?php
$whereList=[
    'pid'=>$this->context['pid'],
    'tpos'=>$this->context['tpos'],
];
//fetch records for edit view
$select=$this->sql->select('investor_presentations');

$select->columns([
    'id'=>'id',
    'f_year'=>new \Zend\Db\Sql\Expression('DATE_FORMAT(`date`,"%Y")'),
    'date'=>new \Zend\Db\Sql\Expression('DATE_FORMAT(`date`,"%d %b %Y")'),
    'jsdate'=>new \Zend\Db\Sql\Expression('DATE_FORMAT(`date`,"%Y-%m-%d")'),
    'title'=>'title',
    'attachment'=>'attachment',
]);
$select->where($whereList);
$select->order('date DESC');
$selectStatement=$this->sql->prepareStatementForSqlObject($select);
$results=$selectStatement->execute();


$this->sqlResults->initialize($results);
$resultArray=array();
foreach ($this->sqlResults as $row) {
    $resultArray[]=(array)$row;
}



$select2=$this->sql->select('investor_presentations');
$select2->columns([
    'f_year'=>new \Zend\Db\Sql\Expression("`getFYear` (`date`)"),
]);
$select2->order('f_year DESC');
$select2->where($whereList);

$select2->group(['f_year']);
$selectStatement2=$this->sql->prepareStatementForSqlObject($select2);
$results2=$selectStatement2->execute();
$this->sqlResults->initialize($results2);
$resultArray2=array();
foreach ($this->sqlResults as $row) {
    $resultArray2[]=$row->f_year;
}

$ajaxuri=$actionURL;
$import_btn=true;
if($this->context['pid']==0){
    $import_btn=false;
}

$opts=$this->getWidgetOption();

$formOut=$this->render('customform.twig',[
    'actionUrl'=>$ajaxuri,
    'blank'=>basename(__FILE__),
    'data'=>$resultArray,
    'opts'=>$opts['data'],
    'attach_path'=>$this->get_uploads_uri(),
    'fyears'=>$resultArray2,
    'import_btn'=>$import_btn,
],false);


