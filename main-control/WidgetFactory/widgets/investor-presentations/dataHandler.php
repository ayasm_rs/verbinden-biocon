<?php
/**
 * Created by PhpStorm.
 * User: ays29
 * Date: 5/30/19
 * Time: 5:02 PM
 */

$fh = $this->formHandler;

//require all fields
$fh->required = "*";
// set our upload directory
$fh->upload_dir = $this->get_uploads_dir();
//maximum uplolad size
$fh->upload_max_filesize = 20000000;
// rename our upload with a 32-character hash(php7 only)
$fh->upload_rename = 'hash';

// only allow pdfs and presentations to be uploaded
$fh->upload_accepted_types = 'pdf,ppt,pptx';


//Handle Ajax
if(isset($_GET['isAjax'])){
    if($_GET['isAjax']){
        try{
            switch($fh->post('action')){
                case "insert":
                    $file=array();
                    $data;
                    $titles=$fh->post('title','Title');
                    $dates=$fh->post('date','Date');
                    error_reporting(0);
                    $file=$fh->post('file','Presentation File');
                    error_reporting(E_ALL);
                    if($fh->errors()){
                        throw new Exception($fh->messages());
                    }

                    $insert = $this->sql->insert();
                    $insert->into('investor_presentations');
                    $insert->columns(['date', 'title', 'attachment','link_1_title','link_1_destination']);
                    if(gettype($titles)=='array'){
                        for($i=0;$i<sizeof($titles);$i++){
                            $truthy=true;

                            if($truthy){
                                $insert->values([
                                    'date' =>date('Y-m-d',strtotime($dates[$i]) ),
                                    'title' => $titles[$i],
                                    'attachment' => $file[$i]['name'] ,
                                    'pid'=>$this->context['pid'],
                                    'tpos'=>$this->context['tpos'],

                                ]);
                                $query=$this->sql->prepareStatementForSqlObject($insert);
                                $query->execute();
                            }

                        }
                    }
                    else{
                        if (!$fh->errors()) {
                            $insert = $this->sql->insert();
                            $insert->into('investor_presentations');
                            $insert->columns(['date', 'title', 'attachment','link_1_title','link_1_destination']);
                            $insert->values([
                                'date' =>date('Y-m-d',strtotime($dates) ),
                                'title' => $titles,
                                'attachment' => $file['name'] ,
                                'pid'=>$this->context['pid'],
                                'tpos'=>$this->context['tpos'],
                            ]);
                            $query=$this->sql->prepareStatementForSqlObject($insert);
                            $query->execute();
                            $fh->success_message('Entry Successful');
                        }
                    }


                    break;
                case 'options':
                    $fh->required = "button_title,button_url";
                    error_reporting(0);
                    $pdfFile=$fh->post('ipo_pdf','IPO PDF');
                    error_reporting(E_ALL);
                    if(isset($pdfFile['name'])&&$pdfFile['name']!=''){
                        $data['ipo_pdf']=$pdfFile['name'];
                        $data['button_title']=$fh->post('button_title','Button Title');
                        $data['button_url']=$fh->post('button_url','Button URL');
                        $this->addWidgetOption($data,'test');
                    }else{
                        $fh->info_message('Button title & URL updated.');
                        $opts=$this->getWidgetOption();
                        $resource=$opts['data'];
                        $data['ipo_pdf']=$resource['ipo_pdf'];
                        $data['button_title']=$fh->post('button_title','Button Title');
                        $data['button_url']=$fh->post('button_url','Button URL');
                        $this->addWidgetOption($data,'test');

                    }


                    //$fh->success_message('Entry Successful');
                    break;
                case "update":
                    $id=$fh->post('did');
                    $title=$fh->post('title');
                    $date=$fh->post('date');
                    error_reporting(0);
                    $file=$fh->post('file');
                    error_reporting(E_ALL);
                    if($file!=''){
                        //delete previous file
                        $select=$this->sql->select('investor_presentations');
                        $select->limit(1);
                        $select->columns(['attachment']);
                        $select->where([
                            'id'=>$id,
                            'pid'=> $this->context['pid'],
                            'tpos'=> $this->context['tpos'],
                        ]);
                        $sqlString=$this->sql->buildSqlString($select);
                        $statement=$this->dbAdapter->query($sqlString);
                        $this->sqlResults->initialize($statement->execute());
                        $filedata=array();

                        foreach ($this->sqlResults as $row) {
                            $filedata=$row->attachment;
                        }
                        $ogfile=$this->get_uploads_dir().$filedata;

                        //find if more than 1 records use the file anddon't delete if true
                        $select->columns(['count'=>new \Zend\Db\Sql\Expression('COUNT(*)')]);
                        $select->where(['attachment'=>$ogfile]);
                        $statement=$this->sql->prepareStatementForSqlObject($select);
                        $this->sqlResults->initialize($statement->execute());
                        $try_delete=false;
                        foreach ($this->sqlResults as $row) {
                            $try_delete=$row->count>1?true:false;
                        }
                        var_dump($try_delete);
                        if($try_delete){

                            if(file_exists($ogfile)){
                                $deletion=unlink($ogfile);
                                if(!$deletion){
                                    $fh->error_message('File Not Deleted.');
                                }else{
                                    $fh->info_message('File Deleted.');
                                }
                            }

                        }

                        $update=$this->sql->update('investor_presentations');
                        $update->set([
                            'title'=>$title,
                            'date'=>$date,
                            'attachment'=>$file['name'],
                        ]);
                        $update->where([
                            'id'=>$id,
                            'pid'=> $this->context['pid'],
                            'tpos'=> $this->context['tpos'],
                        ]);
                        $sqlString=$this->sql->buildSqlString($update);
                        $statement=$this->dbAdapter->query($sqlString);
                        $statement->execute();

                    }else{
                        $update=$this->sql->update('investor_presentations');
                        $update->set([
                            'title'=>$title,
                            'date'=>$date,
                        ]);
                        $update->where([
                            'id'=>$id,
                            'pid'=> $this->context['pid'],
                            'tpos'=> $this->context['tpos'],
                        ]);
                        $sqlString=$this->sql->buildSqlString($update);
                        $statement=$this->dbAdapter->query($sqlString);
                        $statement->execute();
                    }
                    break;
                case "delete":
                    $id=$fh->post('did');

                    //delete previous file
                    $select=$this->sql->select('investor_presentations');
                    $select->limit(1);
                    $select->columns(['attachment']);
                    $select->where([
                        'id'=>$id,
                        'pid'=> $this->context['pid']
                    ]);
                    $sqlString=$this->sql->buildSqlString($select);
                    $statement=$this->dbAdapter->query($sqlString);
                    $this->sqlResults->initialize($statement->execute());
                    $filedata=array();

                    foreach ($this->sqlResults as $row) {
                        $filedata=$row->attachment;
                    }
                    $ogfile=$this->get_uploads_dir().$filedata;

                    //find if more than 1 records use the file anddon't delete if true
                    $select->columns(['count'=>new \Zend\Db\Sql\Expression('COUNT(*)')]);
                    $select->where(['attachment'=>$ogfile]);
                    $statement=$this->sql->prepareStatementForSqlObject($select);
                    $this->sqlResults->initialize($statement->execute());
                    $try_delete=false;
                    foreach ($this->sqlResults as $row) {
                        $try_delete=$row->count>1?true:false;
                    }
                    if($try_delete){

                        if(file_exists($ogfile)){
                            $deletion=unlink($ogfile);
                            if(!$deletion){
                                $fh->error_message('File Not Deleted.');
                            }else{
                                $fh->info_message('File Deleted.');
                            }
                        }

                    }


                    //delete DB Entry
                    $delete=$this->sql->delete('investor_presentations');
                    $delete->where([
                        'id'=>$id,
                        'pid'=> $this->context['pid']
                    ]);
                    $sqlString=$this->sql->buildSqlString($delete);
                    $statement=$this->dbAdapter->query($sqlString);
                    $statement->execute();


                    break;
                case 'getdummy':
                    if($this->context['tid']!=0){
                        $defaults=$this->getWidgetDefault();
                        $this->addWidgetOption($defaults['data']);

                        $select=$this->sql->select('investor_presentations');
                        $select->where(['pid'=>0]);
                        $select->columns([
                            'date'=>'date',
                            'title'=>'title',
                            'attachment'=>'attachment',
                            'pid'=>new \Zend\Db\Sql\Expression($this->context['pid']),
                            'tpos'=>new \Zend\Db\Sql\Expression($this->context['tpos']),
                        ]);
                        $selectStr=$this->sql->buildSqlString($select);
                        $insertSelect_Q="INSERT INTO `investor_presentations` (`date`,`title`,`attachment`,`pid`,`tpos`) ".$selectStr;
                        $statement=$this->dbAdapter->query($insertSelect_Q);
                        $statement->execute();
                    }
                    break;
            }
            $output['success']=!$fh->errors();
            $output['message']=$fh->messages();
        }catch(Exception $e){
            $output['success']=false;
            $output['message']=$e->getMessage();
        }finally{
            echo json_encode($output);

        }






    }
}




$this->submitAdminMessages($fh->messages());
