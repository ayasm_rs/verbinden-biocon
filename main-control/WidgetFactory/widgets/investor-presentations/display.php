<?php
/**
 * Created by PhpStorm.
 * User: ays29
 * Date: 6/5/19
 * Time: 1:15 AM
 */
$whereList=[
    'pid'=>$this->context['pid'],
    'tpos'=>$this->context['tpos'],
];


$select=$this->sql->select('investor_presentations');

$select->columns([
    'f_year'=>new \Zend\Db\Sql\Expression('DATE_FORMAT(`date`,"%Y")'),
    'title'=>'title',
    'attachment'=>'attachment',
]);
$select->where($whereList);
$select->order('date DESC');
$selectStatement=$this->sql->prepareStatementForSqlObject($select);
$results=$selectStatement->execute();


$this->sqlResults->initialize($results);
$resultArray=array();
foreach ($this->sqlResults as $row) {
    $resultArray[]=(array)$row;
}



$select2=$this->sql->select('investor_presentations');
$select2->columns([
    'f_year'=>new \Zend\Db\Sql\Expression("`getFYear` (`date`)")
]);
$select2->order('f_year DESC');
$select2->where($whereList);
$select2->group(['f_year']);
$selectStatement2=$this->sql->prepareStatementForSqlObject($select2);
$results2=$selectStatement2->execute();
$this->sqlResults->initialize($results2);
$resultArray2=array();
foreach ($this->sqlResults as $row) {
    $resultArray2[]=$row->f_year;
}

$widgetOpts=$this->getWidgetOption();


$output= $this->render('display.twig',[
    'data'=>$resultArray,
    'fyears'=>$resultArray2,
    'widget_opts'=>$widgetOpts['data'],
    'attach_path'=>$this->get_uploads_uri(),
    'tpos'=>$this->context['tpos']
]);
