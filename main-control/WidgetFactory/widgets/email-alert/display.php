<?php
/**
 * Created by PhpStorm.
 * User: ays29
 * Date: 6/17/19
 * Time: 7:41 AM
 */
$output= $this->render('display.html',[
    'wid'=>$this->context['tid'],
    'pid'=>$this->context['pid'],
    'tpos'=>$this->context['tpos'],
    'recaptchaSKey'=>$this->config->get('recaptcha')->get('site_key')
]);