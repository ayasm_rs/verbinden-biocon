<?php
/**
 * Created by PhpStorm.
 * User: ays29
 * Date: 6/5/19
 * Time: 1:15 AM
 */
$select=$this->sql->select('disclosures');

$select->columns([
    'f_year'=>new \Zend\Db\Sql\Expression("`getFYear` (`date`)"),
    'quarter'=>new \Zend\Db\Sql\Expression("`getFYQuarter` (`date`)"),
    'month'=>new \Zend\Db\Sql\Expression('DATE_FORMAT(`date`,"%M %Y")'),
    'day'=>new \Zend\Db\Sql\Expression('DATE_FORMAT(`date`,"%d")'),
    'date'=>new \Zend\Db\Sql\Expression('DATE_FORMAT(`date`,"%d\n %b %Y")'),
    'title'=>'title',
    'attachment'=>'attachment',
]);
$select->where('pid='.$this->context['pid']);

$select->order('date ASC');

$selectStatement=$this->sql->prepareStatementForSqlObject($select);
$results=$selectStatement->execute();


$this->sqlResults->initialize($results);
$resultArray=array();
foreach ($this->sqlResults as $row) {
    $resultArray[]=(array)$row;
}



$select2=$this->sql->select('disclosures');
$select2->columns([
    'f_year'=>new \Zend\Db\Sql\Expression("`getFYear` (`date`)")
]);
$select2->where('pid='.$this->context['pid']);
$select2->order('f_year DESC');
$select2->group(['f_year']);
$selectStatement2=$this->sql->prepareStatementForSqlObject($select2);
$results2=$selectStatement2->execute();
$this->sqlResults->initialize($results2);
$resultArray2=array();
foreach ($this->sqlResults as $row) {
    $resultArray2[]=$row->f_year;
}
$dmonth=[];
//Arrange data by month
foreach( $resultArray as $disclosure){
    $dmonth[$disclosure['month']][]=$disclosure;
}

foreach ($dmonth as $key => $value) {
    $dmonth[$key]=array_reverse($value);
    
}

$output= $this->render('disclosures.twig',[
    'data'=>$resultArray,
    'altdata'=>$dmonth,
    'fyears'=>$resultArray2,
    'attach_path'=>$this->get_uploads_uri(),
    'tpos'=>$this->context['tpos']
]);

