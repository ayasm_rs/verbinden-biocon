<?php
/**
 * Created by PhpStorm.
 * User: Ayas
 * Date: 6/22/19
 * Time: 8:06 PM
 */



$proceed = isset($_GET['isAjax']);
if ($proceed)
    $proceed = $_GET['isAjax'];

if ($proceed) {

    $ajax=(array)json_decode(file_get_contents('php://input'));
    try {
        switch ($ajax['action']) {
            case 'update_contacts':
                $this->addWidgetOption($ajax['data']);
                $output['status']=true;
                $output['message']="Data Entered Successfully";
                break;
            case 'import_contacts':
                $opts=$this->getWidgetDefault();
                $default_contactdata=$opts['data'];
                if(!$default_contactdata){
                    throw new Exception('No data to import!');
                }
                $opts=$this->getWidgetOption();
                $current_contactData=(array)$opts['data'];
                $final_contactdata=array_merge($default_contactdata,$current_contactData);
                $this->addWidgetOption($final_contactdata);
                $output['status']=true;
                $output['message']="Data Imported Successfully";

                break;
            case 'fetch_subscriber_pg':
                $select=$this->sql->select('mailing_list');
                $select->limit(20);
                $select->order('added_date DESC');
                if($this->context['pid']!=0){
                    $select->where(['referrer_pid'=>$this->context['pid']]);
                }else{
                    $select->join(
                        ['p'=>'page'],              // table name
                        'referrer_pid=p.id',      // expression to join on (will be quoted by platform object before insertion),
                        ['page_name'],
                        $select::JOIN_LEFT
                    );
                }
                $select->offset($ajax['data']*20);
                $statement=$this->sql->prepareStatementForSqlObject($select);
                $this->sqlResults->initialize($statement->execute());
                $data= $this->sqlResults->toArray();
                $output['status']=true;
                $output['message']="Data Retrieved Successfully";
                $output['data']=$data;
                break;


            default:
                throw new Exception('Not Quite My Action.');

        }
    } catch (Exception $e) {
        $output['status'] = false;
        $output['message'] = $e->getMessage();

    } finally {
        if (isset($output)){
            header('Content-Type: application/json');
            echo(json_encode($output));
        }

    }


} else {
    die('Invalid Access');
}