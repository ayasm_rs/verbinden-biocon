<?php

$output= $this->render('display.html',[
    'wid'=>$this->context['tid'],
    'pid'=>$this->context['pid'],
    'tpos'=>$this->context['tpos'],
    'recaptchaSKey'=>$this->config->get('recaptcha')->get('site_key')
]);