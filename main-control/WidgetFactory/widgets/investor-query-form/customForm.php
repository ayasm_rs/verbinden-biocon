<?php
/**
 * Created by PhpStorm.
 * User: Ayas
 * Date: 6/13/19
 * Time: 2:16 PM
 */

$isDashboard=true;

$select=$this->sql->select();
$select->from(['e'=>'enquiries']);
$select->join(
    ['p'=>'page'],              // table name
    'e.pid=p.id',      // expression to join on (will be quoted by platform object before insertion),
    ['page_heading'],
    $select::JOIN_LEFT
);
if($this->context['pid']!=0){
    $select->where(['pid'=>$this->context['pid']]);
    $isDashboard=false;
}

//get the count for pagination
$select->columns(['count'=>new \Zend\Db\Sql\Expression('COUNT(*)')]);
$str = $this->sql->buildSqlString($select);
$result=$this->dbAdapter->query($str)->execute();
$count= (int) $result->current()['count'];
$max_pages=floor($count/10);



//fetch the data
$select->columns(['*']);
$select->order('date DESC');
$select->limit(10);
$str=$this->sql->buildSqlString($select);
//echo("\n<br/>".$str);
$statement=$this->dbAdapter->query($str);
$this->sqlResults->initialize($statement->execute());
$data= $this->sqlResults->toArray();



$formOut=$this->render('adminview.html',[
    'data'=>$data,
    'max_pages'=>$max_pages,
    'isDash'=>$isDashboard
],false);