<?php

/**
 * Created by PhpStorm.
 * User: ays29
 * Date: 6/13/19
 * Time: 3:04 PM
 */

$insert = $this->sql->insert('enquiries');
$input = array_merge($_POST, ['date' => date_format(new DateTime('now'), 'Y/m/d H:i:s'), 'pid' => $this->context['pid']]);
$input['shareholder'] = ($input['shareholder'] == 'on') ? true : false;
$recaptchaRes = $input['g-recaptcha-response'];
unset($input['g-recaptcha-response']);

/**
 * Handle Recaptcha
 */
$client = new Zend\Http\Client('https://www.google.com/recaptcha/api/siteverify', array(
    'maxredirects' => 0,
    'timeout'      => 30
));
// Performing a POST request
$client->setMethod('POST');
//Set data
$client->setParameterPost(array(
    'secret'  => $this->config->get('recaptcha')->get('secret_key'),
    'response' => $recaptchaRes,
));
$response = $client->send();
$ReCaptchaValidation = json_decode($response->getBody(), true);
$output['recaptcha'] = $ReCaptchaValidation;

/**
 * Handle DB validation
 */
$validator = new Zend\Validator\Db\NoRecordExists([
    'table'   => 'enquiries',
    'field'   => 'email',
    'adapter' => $this->dbAdapter,
]);

/**
 * Insert data after passing validation
 */
if ($ReCaptchaValidation['success']&&$validator->isValid($input['email'])) {
    $insert->values($input);
    $str = $this->sql->buildSqlString($insert);
    $statement = $this->dbAdapter->query($str);
    $statement->execute();
    
    $output['status'] = true;
    $output['messages'] = "Subscriber added successfully!";
    $output['data'] = $input;
    
}else{
    $output['status'] = false;
    $output['messages'] = $validator->getMessages();

}

echo json_encode($output);
