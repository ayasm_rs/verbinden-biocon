<?php
/**
 * Created by PhpStorm.
 * User: ays29
 * Date: 6/18/19
 * Time: 6:50 AM
 */

$offset=((int)$_GET['page'])*10;

$select=$this->sql->select();
$select->from(['e'=>'enquiries']);
$select->join(
    ['p'=>'page'],              // table name
    'e.pid=p.id',      // expression to join on (will be quoted by platform object before insertion),
    ['page_heading'],
    $select::JOIN_LEFT
);
$select->order('date DESC');
if($this->context['pid']!=0){
    $select->where(['pid'=>$this->context['pid']]);
}

//fetch the data
$select->limit(10);

$select->offset($offset);
$str=$this->sql->buildSqlString($select);
//echo("\n<br/>".$str);
$statement=$this->dbAdapter->query($str);
$this->sqlResults->initialize($statement->execute());
$data= $this->sqlResults->toArray();

echo json_encode(['status'=>true,'data'=>$data]);
