<?php
/**
 * Created by PhpStorm.
 * User: Ayas
 * Date: 6/17/19
 * Time: 7:37 AM
 */
$isDashboard=true;

$select=$this->sql->select('mailing_list');
$select->limit(20);
$select->order('added_date DESC');
$select->join(
    ['p'=>'page'],              // table name
    'referrer_pid=p.id',      // expression to join on (will be quoted by platform object before insertion),
    ['page_name'],
    $select::JOIN_LEFT
);
if($this->context['pid']!=0){
    $select->where(['referrer_pid'=>$this->context['pid']]);
    $isDashboard=false;
}

//get max page count of subscribers for pagination
$select->columns(['count'=>new \Zend\Db\Sql\Expression('COUNT(*)')]);
$str = $this->sql->buildSqlString($select);
$result=$this->dbAdapter->query($str)->execute();
$count= (int) $result->current()['count'];
$max_pages=floor($count/20);



//get Subscriber data
$select->columns(['*']);
$statement=$this->sql->prepareStatementForSqlObject($select);
$this->sqlResults->initialize($statement->execute());
$data= $this->sqlResults->toArray();

//fetch contact data of page in widget options
$opts=$this->getWidgetOption();
$contactData=$opts['data'];

$formOut=$this->render('adminView.html',[
    'data'=>$data,
    'endpoint'=>$actionURL,
    'max_pages'=>$max_pages,
    'contactdata'=>$contactData,
    'import_btn'=>!$isDashboard
],false);