<?php

/**
 * Created by PhpStorm.
 * User: ays29
 * Date: 6/17/19
 * Time: 8:36 AM
 */





$fh = $this->formHandler;


try {
    switch ($_POST['action']) {
        case 'subscribe':
            $data = $_POST;
            unset($data['action']); //remove unnecessary action field from data
            $lists = $data['alert_types'];
            $data['alert_types'] = implode(',', $data['alert_types']);

            $data['referrer_pid'] = $this->context['pid'];
            $recaptchaRes = $data['g-recaptcha-response'];
            unset($data['g-recaptcha-response']);

            /**
             * Handle Recaptcha
             */
            $client = new Zend\Http\Client('https://www.google.com/recaptcha/api/siteverify', array(
                'maxredirects' => 0,
                'timeout'      => 30
            ));
            // Performing a POST request
            $client->setMethod('POST');
            //Set data
            $client->setParameterPost(array(
                'secret'  => $this->config->get('recaptcha')->get('secret_key'),
                'response' => $recaptchaRes,
            ));
            $response = $client->send();
            $ReCaptchaValidation = json_decode($response->getBody(), true);
            if (!$ReCaptchaValidation['success']) {
                throw new Exception("Recaptcha Failed");
                
            }
            $output['recaptcha'] = $ReCaptchaValidation;

            /**
             * Handle DB validation
             */
            $validator = new Zend\Validator\Db\NoRecordExists([
                'table'   => 'mailing_list',
                'field'   => 'email',
                'adapter' => $this->dbAdapter,
            ]);


            /**
             * Insert data after passing validation
             */
            if ($ReCaptchaValidation['success'] && $validator->isValid($data['email'])) {
                $insert = $this->sql->insert('mailing_list');
                $insert->values($data);
                $str = $this->sql->buildSqlString($insert);
                $statement = $this->dbAdapter->query($str);
                $statement->execute();

                /**
                 * Send mail to admin
                 */
                $mailbody = $this->render('mail.twig', ['data' => $data]);
                $response = $this->mail('sendgrid', $this->config->get('system_email'), $this->config->get('admin_email'), 'A new subscriber was added to the site', $mailbody);

                /**
                 * Send Welcome mail to subscriber
                 */
                $mailbody = $this->render('welcome_mail.twig', ['data' => $data]);
                $response = $this->mail('sendgrid', $this->config->get('system_email'), $data['email'], 'You have been subscribed to biocon alerts.', $mailbody);


                $output['mailstatus'] = $response->body();

                $output['status'] = true;
                $output['messages'] = "Subscriber added successfully!";
            } else {
               throw new Exception("User already subscribed");
               
            }



            break;
        case 'unsubscribe':
            $data = $_POST;
            unset($data['action']); //remove unnecessary action field from data

            /**
             * Handle DB validation
             */
            $validator = new Zend\Validator\Db\RecordExists([
                'table'   => 'mailing_list',
                'field'   => 'email',
                'adapter' => $this->dbAdapter,
            ]);

            /**
             * update DB if email is valid
             */
            if ($validator->isValid($data['email'])) {


                $update = $this->sql->update('mailing_list');
                $update->set(['status'=>false]);
                $update->where([
                    'email' => $data['email']
                ]);
                $res = $this->sql->prepareStatementForSqlObject($update)->execute();

                $output['status'] = true;
                $output['messages'] = 'User unsubscribed.';
                $output['data'] = $res;
            } else {
                throw new Exception("User does Not Exist");
                
            }
            break;

        default:
            throw new Exception("Invalid Action");
            
            break;
    }
} catch (Exception $e) {
    $output['status'] = false;
    $output['messages'] = $e->getMessage();
}finally{
    echo json_encode($output);
}


