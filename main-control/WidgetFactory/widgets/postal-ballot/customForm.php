<?php

//fetch records for edit view
$select=$this->sql->select('postal_ballots');

$select->columns([
    'id'=>'id',
    'month'=>new \Zend\Db\Sql\Expression('DATE_FORMAT(`date`,"%M %Y")'),
    'date'=>new \Zend\Db\Sql\Expression('DATE_FORMAT(`date`,"%d %b %Y")'),
    'jsdate'=>new \Zend\Db\Sql\Expression('DATE_FORMAT(`date`,"%Y-%m-%d")'),
    'title'=>'title',
    'attachment'=>'attachment',
]);
$select->where('pid='.$this->context['pid']);
$select->order('date DESC');
$selectStatement=$this->sql->prepareStatementForSqlObject($select);
$results=$selectStatement->execute();


$this->sqlResults->initialize($results);
$resultArray=array();
foreach ($this->sqlResults as $row) {
    $resultArray[]=(array)$row;
}




$ajaxuri=$actionURL;
$import_btn=true;
if($this->context['pid']==0){
    $import_btn=false;
}

$formOut=$this->render('customform.twig',[
    'actionUrl'=>$ajaxuri,
    'blank'=>basename(__FILE__),
    'data'=>$resultArray,
    'attachment_url_path'=>$this->get_uploads_uri(),
    'import_btn'=>$import_btn,
],false);

