<?php
/**
 * Created by PhpStorm.
 * User: ays29
 * Date: 6/5/19
 * Time: 1:15 AM
 */
$select=$this->sql->select('postal_ballots');

$select->columns([
    'plaindate'=>'date',
    'month'=>new \Zend\Db\Sql\Expression('DATE_FORMAT(`date`,"%M %Y")'),
    'title'=>'title',
    'attachment'=>'attachment',
]);
$select->where('pid='.$this->context['pid']);
$select->order('plaindate DESC');
$select->group(['month','date','title','attachment']);
$selectStatement=$this->sql->prepareStatementForSqlObject($select);
$results=$selectStatement->execute();
$this->sqlResults->initialize($results);
$resultArray=$this->sqlResults->toArray();

$finaldata=[];
foreach ($resultArray as $result){
    $finaldata[$result['month']][]=$result;
}

$output= $this->render('display.twig',[
    'data'=>$finaldata,
    'attach_path'=>$this->get_uploads_uri(),
    'tpos'=>$this->context['tpos']
]);
