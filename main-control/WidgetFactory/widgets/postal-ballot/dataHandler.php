<?php
/**
 * Created by PhpStorm.
 * User: ays29
 * Date: 5/30/19
 * Time: 5:02 PM
 */

$fh = $this->formHandler;

//require all fields
$fh->required = "*";
// set our upload directory
$fh->upload_dir = $this->get_uploads_dir();

// rename our upload with a 32-character hash(php7 only)
$fh->upload_rename = 'hash';

// only allow PDFs to be uploaded
$fh->upload_accepted_types = 'pdf';


//Handle Ajax
if(isset($_GET['isAjax'])){
    if($_GET['isAjax']){

        switch($fh->post('action')){
            case "insert":
                $file=array();
                $data;
                $titles=$fh->post('title');
                $dates=$fh->post('date');
                error_reporting(0);
                $file=$fh->post('file');
                error_reporting(E_ALL);

                $insert = $this->sql->insert();
                $insert->into('postal_ballots');
                $insert->columns(['date', 'title', 'attachment','pid']);
                if(gettype($titles)=='array'){
                    for($i=0;$i<sizeof($titles);$i++){
                        $truthy=true;

                        if($truthy){
                            $insert->values([
                                'date' =>date('Y-m-d',strtotime($dates[$i]) ),
                                'title' => $titles[$i],
                                'attachment' => $file[$i]['name'] ,
                                'pid'=>$this->context['pid'],
                            ]);
                            $query=$this->sql->prepareStatementForSqlObject($insert);
                            $query->execute();
                        }

                    }
                }
                else{
                    if (!$fh->errors()) {
                        $insert = $this->sql->insert();
                        $insert->into('postal_ballots');
                        $insert->columns(['date', 'title', 'attachment']);
                        $insert->values([
                            'date' =>date('Y-m-d',strtotime($dates) ),
                            'title' => $titles,
                            'attachment' => $file['name'] ,
                            'pid'=>$this->context['pid'],
                        ]);
                        $query=$this->sql->prepareStatementForSqlObject($insert);
                        $query->execute();
                        $fh->success_message('Entry Successful');
                        $this->addWidgetOption();
                    }
                }


                break;
            case "update":
                $id=$fh->post('did');
                $title=$fh->post('title');
                $date=$fh->post('date');

                error_reporting(0);
                $file=$fh->post('file');
                error_reporting(E_ALL);
                if($file!=''){
                    //delete previous file
                    $select=$this->sql->select('postal_ballots');
                    $select->limit(1);
                    $select->columns(['attachment']);
                    $select->where([
                        'id'=>$id,
                        'pid'=> $this->context['pid']
                    ]);
                    $sqlString=$this->sql->buildSqlString($select);
                    $statement=$this->dbAdapter->query($sqlString);
                    $this->sqlResults->initialize($statement->execute());
                    $filedata=array();

                    foreach ($this->sqlResults as $row) {
                        $filedata=$row->attachment;
                    }

                    $ogfile=$this->get_uploads_dir().$filedata;

                    if(file_exists($ogfile)){
                        $deletion=unlink($ogfile);
                        if(!$deletion){
                            $fh->error_message('File Not Deleted.');
                        }else{
                            $fh->info_message('File Deleted.');
                        }
                    }

                    $update=$this->sql->update('postal_ballots');
                    $update->set([
                        'title'=>$title,
                        'date'=>$date,
                        'attachment'=>$file['name']
                    ]);
                    $update->where([
                        'id'=>$id,
                        'pid'=> $this->context['pid']
                    ]);
                    $sqlString=$this->sql->buildSqlString($update);
                    $statement=$this->dbAdapter->query($sqlString);
                    $statement->execute();
                    die();
                }else{
                    echo 'No file uploaded!';
                    $update=$this->sql->update('postal_ballots');
                    $update->set([
                        'title'=>$title,
                        'date'=>$date,
                    ]);
                    $update->where([
                        'id'=>$id,
                        'pid'=> $this->context['pid']
                    ]);
                    $sqlString=$this->sql->buildSqlString($update);
                    $statement=$this->dbAdapter->query($sqlString);
                    $statement->execute();
                    die();
                }







                break;
            case "delete":

                $id=$fh->post('did');
                $delete=$this->sql->delete('postal_ballots');
                $delete->where([
                    'id'=>$id,
                    'pid'=> $this->context['pid']
                ]);
                $sqlString=$this->sql->buildSqlString($delete);
                $statement=$this->dbAdapter->query($sqlString);
                $statement->execute();


                break;
            case 'getdummy':
                $select=$this->sql->select('postal_ballots');
                $select->where(['pid'=>0]);
                $select->columns([
                    'date'=>'date',
                    'title'=>'title',
                    'attachment'=>'attachment',
                    'pid'=>new \Zend\Db\Sql\Expression($this->context['pid'])

                ]);
                $selectStr=$this->sql->buildSqlString($select);
                $insertSelect_Q="INSERT INTO `postal_ballots` (`date`,`title`,`attachment`,`pid`)  ".$selectStr;
                $statement=$this->dbAdapter->query($insertSelect_Q);
                $statement->execute();
                break;
        }




    }
}




$this->submitAdminMessages($fh->messages());
