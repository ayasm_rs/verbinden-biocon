<?php
/**
 * Created by PhpStorm.
 * User: ays29
 * Date: 5/30/19
 * Time: 5:01 PM
 */
//Set timezone

//Form descriptor
$fields = array(
    ['name' => 'disclosure_date', 'type' => 'date','value'=> date("mm/dd/yyyy"), 'label' => 'Ending Year', 'id' => '', 'fieldset' => '', 'data' => array('2019' => '2018-19'), 'default' => 'Choose Year', 'class' =>''],
    ['name' => 'disclosure_title', 'type' => 'text', 'label' => 'Disclosure Title', 'id' => 'disclosure_title', 'value' => 'New Disclosure', 'maxlength' => '70', 'class' => '', 'fieldset' => ''],
    ['name' => 'pdf', 'type' => 'file', 'label' => 'Upload Disclosure PDF', 'id' => 'disclosure_file', 'class' => 'input_field', 'fieldset' => ''],
);

$formDescriptor = array();
$formDescriptor['name'] = 'disclosure_form';
$formDescriptor['multipart'] = true;
$formDescriptor['fields'] = $fields;
$formDescriptor['allowed_types']="document/pdf";
//end form descriptor