<?php

//fetch records for edit view
$select=$this->sql->select('dividend_history');

$select->columns([
    'id'=>'id',
    'title'=>'title',
    'attachment'=>'attachment',
]);
$select->where([
    'pid'=>$this->context['pid'],
    'tpos'=>$this->context['tpos'],
]);

$selectStatement=$this->sql->prepareStatementForSqlObject($select);
$results=$selectStatement->execute();


$this->sqlResults->initialize($results);
$resultArray=$this->sqlResults->toArray();




$ajaxuri=$actionURL;
$import_btn=true;
if($this->context['pid']==0){
    $import_btn=false;
}

$formOut=$this->render('customform.twig',[
    'actionUrl'=>$ajaxuri,
    'attach_path'=>$this->get_uploads_uri(),
    'data'=>$resultArray,
    'import_btn'=>$import_btn,
],false);


