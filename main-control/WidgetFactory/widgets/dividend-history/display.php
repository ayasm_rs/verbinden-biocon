<?php
/**
 * Created by PhpStorm.
 * User: ays29
 * Date: 6/5/19
 * Time: 1:15 AM
 */
$select=$this->sql->select('dividend_history');

$select->columns([
    'title'=>'title',
    'attachment'=>'attachment',
]);
$select->where([
    'pid'=>$this->context['pid'],
    'tpos'=>$this->context['tpos'],
]);

$selectStatement=$this->sql->prepareStatementForSqlObject($select);
$results=$selectStatement->execute();


$this->sqlResults->initialize($results);
$resultArray=$this->sqlResults->toArray();





$output= $this->render('display.twig',[
    'data'=>$resultArray,
    'attach_path'=>$this->get_uploads_uri(),
    'tpos'=>$this->context['tpos']
]);
