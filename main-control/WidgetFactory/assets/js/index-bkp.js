// vars
var result = document.querySelector('.result'),
    img_result = document.querySelector('.img-result'),
    img_w = document.querySelector('.img-w'),
    img_h = document.querySelector('.img-h'),
    options = document.querySelector('.options'),
    save = document.querySelector('.save'),
    cancel = document.querySelector('.cancel'),
    cropped = document.querySelector('.cropped'),
    dwn = document.querySelector('.download'),
    upload = document.querySelector('#file-input'),
    cropper = '';

upload.addEventListener('click', function (e) {
	$(".se-pre-con").fadeIn("slow");
});

// on change show image with crop options
upload.addEventListener('change', function (e) {
$(".se-pre-con").fadeOut("slow");
var _URL = window.URL || window.webkitURL;
var flag;
  if (e.target.files.length) {
		file = this.files[0];
	    img = new Image();
		img.onload = function () {
			
			if((this.width>500 && this.height>400) ){
				//$('#loading').css('display','block');
				
				flag=1;
				$('#errormsg').hide();
				 // start file reader
				$('main.page').show();
				$('.avatar-view').hide(); 
				$('#img_dis_shw').hide();
				var reader = new FileReader();
				reader.onload = function (e) {
				  if (e.target.result) {
					// create new image
					var img = document.createElement('img');
					img.id = 'image';
					img.src = e.target.result;
					// clean result before
					result.innerHTML = '';
					// append new image
					result.appendChild(img);
					// show save btn and options
					save.classList.remove('hide');
					cancel.classList.remove('hide');
					options.classList.remove('hide');
					// init cropper
					cropper = new Cropper(img, {
		aspectRatio: 16 / 9,
		
		minCropBoxWidth: 100,
		zoom: function (e) {
		e.preventDefault();
	  }
		//maxCropBoxWidth: 100,
		});
				  }
				};
				reader.readAsDataURL(e.target.files[0]);
  
			}else{
				$("#file-input").val('');
				flag=0;
				$('#errormsg').show();
				
			}
			
			return false;
		};
		img.src = _URL.createObjectURL(file);
		
   }else{
	 $('.avatar-view').show();$('main.page').hide(); 
  }
});

// save on click
save.addEventListener('click', function (e) {
  e.preventDefault();
  // get result to data uri
  var imgSrc = cropper.getCroppedCanvas({
    width: img_w.value // input value
  }).toDataURL();
  // remove hide class of img
  cropped.classList.remove('hide');
  img_result.classList.remove('hide');
  $('#img_dis_shw').show();

  // show image cropped
  cropped.src = imgSrc;
  $('.avatar-view img').attr('src',imgSrc);
 // dwn.classList.remove('hide');
  //dwn.download = 'imagename.png';
  //dwn.setAttribute('href', imgSrc);
});
cancel.addEventListener('click', function (e) {
  e.preventDefault();
  $('.avatar-view').show();$('main.page').hide();
  $('#img_dis_shw').show();

});

function stopZoom (e) {
  e.preventDefault();
}