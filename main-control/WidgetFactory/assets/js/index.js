// vars

var img_con=1;

var cnt=0;

var upload = document.querySelector('.mul_img'),cropper = '';
// on change show image with crop options
upload.addEventListener('change', function (e) { 
var _URL = window.URL || window.webkitURL;
var data_id=$(this).attr("data-id");
cnt=data_id;
var result = document.querySelector('.result_'+data_id),
    img_result = document.querySelector('.img-result-'+data_id),
    img_w = document.querySelector('.img-w-'+data_id),
    img_h = document.querySelector('.img-h-'+data_id),
    options = document.querySelector('.options_'+data_id),
    /* save = document.querySelector('.save_'+data_id),
    cancel = document.querySelector('.cancel_'+data_id), */
    cropped = document.querySelector('.cropped_'+data_id),
    dwn = document.querySelector('.download');
var flag;
  if (e.target.files.length) {
		file = this.files[0];
	    img = new Image();
		img.onload = function () {
			if((this.width>400 && this.height>200) ){
				//$('#loading').css('display','block');
				
				flag=1;
				$('#errormsg').hide();
				 // start file reader
				$('main.page').show();
				$('.avatar-view').hide(); 
				
				var reader = new FileReader();
				reader.onload = function (e) {
				  if (e.target.result) {
					// create new image
					var img = document.createElement('img');
					img.id = 'image';
					img.src = e.target.result;
					// clean result before
					result.innerHTML = '';
					// append new image
					result.appendChild(img);
					// show save btn and options
					save.classList.remove('hide');
					cancel.classList.remove('hide');
					options.classList.remove('hide');
					// init cropper
					cropper = new Cropper(img, {
		aspectRatio: 8 / 5,
		
		minCropBoxWidth: 200,
		maxCropBoxWidth: 100,});
				  }
				};
				reader.readAsDataURL(e.target.files[0]);
  
			}else{
				$("#file-input").val('');
				flag=0;
				$('#errormsg').show();
				
			}
			
			return false;
		};
		img.src = _URL.createObjectURL(file);
		
   }else{
	 $('.avatar-view').show();$('main.page').hide(); 
  }
});

// save on click
$(".save_"+data_id).on('click', function (e) {
  e.preventDefault();
  // get result to data uri
  var imgSrc = cropper.getCroppedCanvas({
    width: img_w.value // input value
  }).toDataURL();
  // remove hide class of img
  cropped.classList.remove('hide');
  img_result.classList.remove('hide');
  // show image cropped
  cropped.src = imgSrc;
  
  
  
  //$('.avatar-view img').attr('src',imgSrc);
  

  $('.avatar-view'+cnt).append('<div id="remove_image'+img_con+'" style="margin:10px 10px"><input type="hidden" name="image_name[]" value="'+imgSrc+'" ><img src="'+imgSrc+'" width="120px"><br><span class="btn btn-danger" style="width: -webkit-fill-available;" onclick="$(\'#remove_image'+img_con+'\').remove()">Delete</span></div>');
  
  img_con++;
 // dwn.classList.remove('hide');
  //dwn.download = 'imagename.png';
  //dwn.setAttribute('href', imgSrc);
});
$(".cancel_"+data_id).on('click', function (e) {
  e.preventDefault();
  $('.avatar-view').show();$('main.page').hide()
});