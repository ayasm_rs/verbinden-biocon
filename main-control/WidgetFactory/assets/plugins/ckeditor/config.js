/**
 * @license Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here.
	// For complete reference see:
	// http://docs.ckeditor.com/#!/api/CKEDITOR.config

	// The toolbar groups arrangement, optimized for two toolbar rows.
	config.toolbarGroups = [
		{ name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },
		{ name: 'editing',     groups: [ 'find', 'selection', 'spellchecker' ] },
		{ name: 'links' },
		{ name: 'insert' },
		{ name: 'forms' },
		{ name: 'tools' },
		{ name: 'document',	   groups: [ 'mode', 'document', 'doctools' ] },
		{ name: 'others' },
		'/',
		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
		{ name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ] },
		{ name: 'styles' },
		{ name: 'colors' },
		{ name: 'about' }
	];

	// Remove some buttons provided by the standard plugins, which are
	// not needed in the Standard(s) toolbar.
	config.removeButtons = 'Underline,Subscript,Superscript';

	// Set the most common block elements.
	config.format_tags = 'p;h1;h2;h3;pre';

	// Simplify the dialog windows.
	config.removeDialogTabs = 'image:advanced;link:advanced';

	config.extraPlugins = 'font,richcombo,justify,colorbutton,panelbutton,floatpanel,panel,colordialog,dialog,dialogui';
	 // Define changes to default configuration here:
	config.allowedContent = true;

	config.contentsCss = 'assets/css/custom-fonts/font.css';

	//the next line add the new font to the combobox in CKEditor

	//config.font_names = '<Cutsom Font Name>/<YourFontName>;' + config.font_names;
	config.font_names = 'Arial/Arial, Helvetica, sans-serif;' +
						'Comic Sans MS/Comic Sans MS, cursive;' +
						'Courier New/Courier New, Courier, monospace;' +
						'Georgia/Georgia, serif;' +
						'Lucida Sans Unicode/Lucida Sans Unicode, Lucida Grande, sans-serif;' +
						'Tahoma/Tahoma, Geneva, sans-serif;' +
						'Times New Roman/Times New Roman, Times, serif;' +
						'Trebuchet MS/Trebuchet MS, Helvetica, sans-serif;' +
						'Verdana/Verdana, Geneva, sans-serif';

	config.font_names = 'Roboto/Roboto Regular;' + config.font_names;
};