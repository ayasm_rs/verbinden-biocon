<form method="POST" name="xxfrm" id="xxfrm" enctype="multipart/form-data">
    <div class="row">
        <div class="col-xs-12 col-lg-12 col-xl-12">
            <div class="card-box">
                <h4 class="header-title m-t-0 m-b-30">Widget Details</h4>
                <section>
                    <p class="text-muted m-b-20 font-13 pull-right">
                        (* All fields are mandatory)
                    </p>

                    <!-- start our value drivers div  -->
                    <h5 class="heading_cust">Widget One</h5>

                    <hr>
                    <section class="row">
                        <div class="col-xs-12 col-lg-12 col-xl-12">
                            <section>
                                <div class="row">
                                    <div class="col-xs-12 col-lg-6 col-xl-6">
                                        <div class="form-group clearfix">
                                            <label class="col-lg-2 control-label " for="" >Title
                                            </label>
                                            <div class="col-lg-10">
                                                <input type="text" name="title_value" id="title_value" class="form-control" value="<?php echo $title; ?>" />
                                                <br />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-lg-6 col-xl-6">
                                        <div class="form-group clearfix">
                                            <label class="col-lg-2 control-label " for="" >Short Description
                                            </label>
                                            <div class="col-lg-10">
                                                <textarea name="value_description" id="value_description" class="form-control" ><?php echo $description; ?></textarea>
                                                <br />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group clearfix">
                                    <div class="col-lg-10">
                                        <div class="block7_add_menu_toggle row " >

                                        </div>
                                        <div id="add_menu_item">
                                            <br>
                                            + &nbsp;<a class="btn btn-primary waves-effect waves-light m-r-5 m-b-10" id="block7_add_menu">Add Item</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group clearfix">
                                </div>
                            </section>
                        </div>
                        <!-- end col-->
                    </section>
                    <!-- end our value drivers div  -->



                </section>
            </div>
        </div>
        <div class="col-lg-12 form-group clearfix">
            <div class="card-box">
                <div class="col-lg-10 text-xs-center">
                    <input type="hidden" name="create_page" id="create_page" value="submit" />
                    <button class="btn btn-primary"  type="button" name="sub_btn" id="sub_btn">Update Widget</button>

                </div>
            </div>
        </div>
    </div>
</form>
<button class="btn btn-custom waves-effect waves-light btn-sm page_create_succcess"  style="display:none;">Click me</button>