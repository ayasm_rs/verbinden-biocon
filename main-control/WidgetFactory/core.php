<?php
/*/**
 *
 * User: Ayaskant
 * Date: 5/28/19
 * Time: 12:33 PM
 */

require_once('vendor/autoload.php');
require_once('formr/class.formr.php');
error_reporting(E_ALL);

use Zend\Config\Factory;
use Zend\Captcha;
use Zend\Form\Element;
use Zend\Db\Sql\Sql;
use Zend\Db\Adapter\Driver\ResultInterface;
use Zend\Db\ResultSet\ResultSet;

require_once("vendor/twig/twig/lib/Twig/Autoloader.php");

Twig_Autoloader::register();


class Widgetfactory
{
    /*
    * Application setup, database connection, data sanitization and user
    * validation routines are here.
    */
    private $config; // Create a Zend Config Object

    private $dbAdapter; //database connection handler
    private $sql; //SQL Object
    private $sqlResults;

    private $masterRenderer;
    private $subRenderer;
    private $outputbuffer;
    public $pages;

    public $info;
    private $formHandler;
    private $context;
    private $adminMessages = false;



    function __construct($widgetID = false, $pageID = false, $tpos = false)
    {
        $configF = new Zend\Config\Factory();
        $this->context['tid'] = $widgetID ? ($widgetID) : (isset($_GET['tid']) ? $_GET['tid'] : 0);
        $this->context['pid'] = $pageID ? ($pageID) : (isset($_GET['pid']) ? $_GET['pid'] : 0);
        $this->context['tpos'] = $tpos ? ($tpos) : (isset($_GET['tpos']) ? $_GET['tpos'] : 0);


        try {
            $this->config = $configF->fromFile(dirname(__FILE__) . '/config/config.php', true);

            $this->dbAdapter = new Zend\Db\Adapter\Adapter([
                'driver' => $this->config->database->get('driver'),
                'database' => $this->config->database->get('database'),
                'username' => $this->config->database->get('username'),
                'password' => $this->config->database->get('password'),
            ]);
            $this->sql = new Sql($this->dbAdapter);
            $this->sqlResults = new ResultSet();
        } catch (Exception $e) {
            print_r($e);
        }
        //load list of pages


        $masterLoader = new \Twig\Loader\FilesystemLoader(dirname(__FILE__) . '/templates');
        $this->masterRenderer = new \Twig\Environment($masterLoader, [
            'cache' => false,
            'auto_reload ' => true,
        ]);

        $this->info = $this->GetInfo();
        $this->formHandler = new Formr('bootstrap');
        $subloader = new \Twig\Loader\FilesystemLoader(dirname(__FILE__) . '/widgets/' . $this->get_widget_slug() . '/views/');
        $this->subRenderer = new \Twig\Environment($subloader, [
            'cache' => false,
            'auto_reload ' => true,
        ]);
    }

    /**
     * Prints settings view for widget
     *
     * @return void
     */
    public function renderAdminView()
    {
        $form = $this->createForm();
        echo $this->masterRenderer->render('index.html', ['name' => $this->info['template_name'], 'form' => $form, 'outputs' => $this->outputbuffer, 'messages' => $this->adminMessages]);
    }

    /**
     * Internal function to fetch widget information from database
     *
     * @return array|boolean
     */
    private function GetInfo()
    {
        $statement = $this->dbAdapter->query('SELECT * FROM `templates` WHERE `id` = ? LIMIT 1', [$this->context['tid']]);
        $out = $statement->toArray();
        if (!empty($out)) {
            return $out[0];
        } else {
            return false;
        }
    }

/**
 * Returns absolute path of widget files on disk
 *
 * @return string
 */
    private function get_abs_widget_path()
    {
        $path = dirname(__FILE__) . '/widgets/' . $this->get_widget_slug() . '/';

        return $path;
    }
    private function  get_widget_slug()
    {
        $slug = str_replace(' ', '-', strtolower($this->info['template_name']));
        return $slug;
    }
    /**
     * creates uploads directory/returns absolute upload directory path for widget. 
     * Will create sub directories for each widget if 
     * 'to_uploads_sub_dir' setting is enabled in config.
     *
     * @return string
     */
    private function get_uploads_dir()
    {
        $toSubDir=$this->config->get('to_uploads_sub_dir');  
        
        $filename =  $this->config->get('uploads_dir') . ($toSubDir?$this->get_widget_slug()."/":'') ;
        if (!file_exists($filename)) {
            mkdir($filename, 0777);
        }
        return $filename;
    }
    /**
     * returns url path for uploads
     *
     * @return string
     */
    private function get_uploads_uri()
    {
        $toSubDir=$this->config->get('to_uploads_sub_dir');    
        $config = $this->config;
        $uri = $config->get('webhost') . $config->get('uploads_uri_path') . ($toSubDir?$this->get_widget_slug(). "/":'') ;
        return $uri;
    }

    /**
     * Generates admin form based on formDescriptor object in formDescriptor.php 
     * or loads admin form provided in customform.php
     *
     * @param boolean $fields
     * @return void
     */
    public function createForm()
    {
        require_once($this->get_abs_widget_path() . 'formDescriptor.php');
        $customFormPath = $this->get_abs_widget_path() . 'customForm.php';
        $has_custom_form = file_exists($customFormPath);
        $formOut = '';
        $actionURL = "?pid=" . $this->context['pid'] . "&tid=" . $this->context['tid'] . "&tpos=" . $this->context['tpos'] . "&verb=process";

        if ($has_custom_form) {

            require_once($customFormPath);
        } else {
            $fieldsets = array();
            $fgen = $this->formHandler;


            foreach ($formDescriptor['fields'] as $field) {
                if (!isset($fieldsets[$field['fieldset']])) {
                    $fieldsets[$field['fieldset']] = '';
                }
                $fieldsets[$field['fieldset']] .= '<div class="col-xs-6 col-lg-6 col-md-6 col-xl-6"><div class="form-group clearfix">';

                switch ($field['type']) {
                    case "text":
                        $data = array(
                            'name' => $field['name'],
                            'label' => $field['label'],
                            'value' => $field['value']
                        );
                        //pass optional values
                        if (isset($field['maxlength'])) {
                            $data['maxlength'] = $field['maxlength'];
                        }
                        if (isset($field['id'])) {
                            $data['id'] = $field['id'];
                        }
                        if (isset($field['class'])) {
                            $data['class'] = $field['class'];
                        }
                        if (isset($field['string'])) {
                            $data['string'] = $field['string'];
                        }
                        if (isset($field['inline'])) {
                            $data['inline'] = $field['inline'];
                        }
                        $fieldsets[$field['fieldset']] .= $fgen->input_text($data);
                        break;
                    case "checkbox":
                        $fieldsets[$field['fieldset']] .= "<p>Field type needs setup.</p>";

                        break;
                    case "file":
                        $fieldsets[$field['fieldset']] .= $fgen->input_upload($field['name'], $field['label'], '', '', '', '');

                        break;
                    case "hidden":
                        $fieldsets[$field['fieldset']] .= "<p>Field type needs setup.</p>";

                        break;
                    case "image":
                        $fieldsets[$field['fieldset']] .= "<p>Field type needs setup.</p>";

                        break;
                    case "password":
                        $fieldsets[$field['fieldset']] .= "<p>Field type needs setup.</p>";

                        break;
                    case "radio":
                        $fieldsets[$field['fieldset']] .= "<p>Field type needs setup.</p>";

                        break;
                    case "select":

                        $fieldsets[$field['fieldset']] .= $fgen->input_select($field['name'], $field['label'], '', '', "", '', $field['default'], $field['data']);

                        break;
                    case "textarea":
                        $fieldsets[$field['fieldset']] .= "<p>Field type needs setup.</p>";

                        break;

                    case "color":
                        $fieldsets[$field['fieldset']] .= "<p>Field type needs setup.</p>";

                        break;
                    case "date":
                        $data = array(
                            'name' => $field['name'],
                            'label' => $field['label'],
                            'value' => $field['value']
                        );
                        //pass optional values
                        if (isset($field['maxlength'])) {
                            $data['maxlength'] = $field['maxlength'];
                        }
                        if (isset($field['id'])) {
                            $data['id'] = $field['id'];
                        }
                        if (isset($field['class'])) {
                            $data['class'] = $field['class'];
                        }
                        if (isset($field['string'])) {
                            $data['string'] = $field['string'];
                        }
                        if (isset($field['inline'])) {
                            $data['inline'] = $field['inline'];
                        }
                        $fieldsets[$field['fieldset']] .= $fgen->input_date($data);

                        break;
                    case "datetime":
                        $fieldsets[$field['fieldset']] .= "<p>Field type needs setup.</p>";

                        break;
                    case "email":
                        $fieldsets[$field['fieldset']] .= "<p>Field type needs setup.</p>";

                        break;
                    case "month":
                        $fieldsets[$field['fieldset']] .= "<p>Field type needs setup.</p>";

                        break;
                    case "number":
                        $data = array(
                            'name' => $field['name'],
                            'label' => $field['label'],
                            'value' => $field['value']
                        );
                        //pass optional values
                        if (isset($field['maxlength'])) {
                            $data['maxlength'] = $field['maxlength'];
                        }
                        if (isset($field['id'])) {
                            $data['id'] = $field['id'];
                        }
                        if (isset($field['class'])) {
                            $data['class'] = $field['class'];
                        }
                        if (isset($field['string'])) {
                            $data['string'] = $field['string'];
                        }
                        if (isset($field['inline'])) {
                            $data['inline'] = $field['inline'];
                        }
                        $fieldsets[$field['fieldset']] .= $fgen->input_number($data);

                        break;
                    case "range":
                        $fieldsets[$field['fieldset']] .= "<p>Field type needs setup.</p>";

                        break;
                    case "search":
                        $fieldsets[$field['fieldset']] .= "<p>Field type needs setup.</p>";

                        break;
                    case "tel":
                        $fieldsets[$field['fieldset']] .= "<p>Field type needs setup.</p>";

                        break;
                    case "time":
                        $fieldsets[$field['fieldset']] .= "<p>Field type needs setup.</p>";

                        break;
                    case "url":
                        $fieldsets[$field['fieldset']] .= "<p>Field type needs setup.</p>";

                        break;
                }
                $fieldsets[$field['fieldset']] .= '</div></div>';
            }



            if (isset($formDescriptor['multipart']) && $formDescriptor['multipart']) {
                $formOut .= $fgen->form_open_multipart('', '', $actionURL, '', 'class="widget_form"');
            } else {
                $formOut .= $fgen->form_open('', '', $actionURL, '', 'class="widget_form"');
            }

            foreach ($fieldsets as $k => $v) {
                $noFieldset = false;
                if ($k == '') {
                    $noFieldset = true;
                }

                $formOut .= $noFieldset ? '' : $fgen->fieldset_open($k);
                $formOut .= $v;
                $formOut .= $noFieldset ? '' : $fgen->fieldset_close();
            }
            //submit button
            $formOut .= "<div class='col-xs-12 col-md-12 col-lg-12 col-xl-12'>";
            $formOut .= $fgen->input_submit('submitForm', '&nbsp;', 'Submit Form', 'buttonID', 'class="btn btn-primary"');
            $formOut .= "</div>";
            $formOut .= $fgen->form_close();
        }

        return $formOut;
    }
    /**
     * Handle Ajax for widget admin view 
     *
     * @return void
     */
    public function process()
    {
        $path = $this->get_abs_widget_path() . 'dataHandler.php';
        if (file_exists($path)) {
            require_once($path);
        } else {
            echo "Missing data handler.";
        }
    }
    /**
     * Handle Ajax for Widget frontend
     *
     * @return void
     */
    public function capture()
    {
        $path = $this->get_abs_widget_path() . 'dataCapturer.php';
        if (file_exists($path)) {
            require_once($path);
        } else {
            echo "Missing data handler.";
        }
    }


    /**
     * load Twig views for widgets
     *
     * @param string $templatename Name of file with extension in widget views folder
     * @param boolean $values data to be passed into the Twig view 
     * @param boolean $to_output_buffer Indicate whether or not the view should be added to admin display
     * @return string
     */
    public function render($templatename, $values = false, $to_output_buffer = true)
    {
        if ($values) {
            $templateOut = $this->subRenderer->render($templatename, $values);
        } else {
            $templateOut = $this->subRenderer->render($templatename, []);
        }

        if ($to_output_buffer) {
            $this->outputbuffer[] = $templateOut;
        }
        return $templateOut;
    }
    //returns html string to display widget on frontend
    public function display()
    {
        $output = "";
        require($this->get_abs_widget_path() . 'display.php');

        return $output;
    }
    /**
     * Function to send mail
     *
     * @param string $configname name of method to use. E.g.:'sendgrid' 
     * @param string $from Sender email ID
     * @param string $to Receiver email ID
     * @param string $subject Mail Subject
     * @param string $content Content of email.
     * @return void
     */
    public function mail($configname, $from, $to, $subject, $content)
    {
        switch ($configname) {
            case 'sendgrid':
                return $this->sendgridmail($from, $to, $subject, $content);
                break;

            default:
                throw new Exception("Invalid mail mechanism", 1);


                break;
        }
    }
    /**
     * Send mail using Sendgrid. 
     *
     * @param [type] $from
     * @param [type] $to
     * @param [type] $subject
     * @param [type] $content
     * @return void
     */
    private function sendgridmail($from, $to, $subject, $content)
    {
        $sendgridconfig=$this->config->get('mail')->get('sendgrid');
        

        // using SendGrid's PHP Library
        // https://github.com/sendgrid/sendgrid-php

        $sendgrid = new \SendGrid($sendgridconfig->get('api_key'));
        $email = new \SendGrid\Mail\Mail(); 

        $email->addTo($to);
        $email->setSubject($subject);
        $email->addContent('text/html',$content);
        $email->setFrom($from);

        return $sendgrid->send($email);
        
    }

    //passes error messages to admin
    private function submitAdminMessages($message)
    {
        $this->adminMessages[] = $message;
    }
    //Widget Option Entry Block
    private function checkWidgetOpts($default = false)
    {
        $select = $this->sql->select('template_details');
        $select->where->equalTo('temp_id', $this->context['tid']) // this gets overridden
            ->and->equalTo('page_id_td', $default ? 0 : $this->context['pid'])
            ->and->equalTo('temp_pos_id', $default ? 0 : $this->context['tpos']);

        $validator = new Zend\Validator\Db\RecordExists($select);
        $validator->setAdapter($this->dbAdapter);
        $status = $validator->isValid($temp_id = $this->context['tid']);
        //echo($this->sql->buildSqlString($select));
        return $status;
    }
    private function addWidgetOption($data = false, $Title = false, $Description = false)
    {
        $proceed = $this->checkWidgetOpts();

        if ($proceed) {
            $update = $this->sql->update('template_details');
            $update->where->equalTo('temp_id', $this->context['tid']) // this gets overridden
                ->and->equalTo('page_id_td', $this->context['pid'])
                ->and->equalTo('temp_pos_id', $this->context['tpos']);
            $update->set([
                'resource' => json_encode($data),
                'title' => $Title ? $Title : '',
                'description' => $Description ? $Description : ''
            ]);
            $query = $update;
        } else {
            $insert = $this->sql->insert('template_details');
            $insert->values([
                'temp_id' => $this->context['tid'],
                'title' => $Title ? $Title : '',
                'description' => $Description ? $Description : '',
                'sub_icon' => '~',
                'sub_title' => '~',
                'sub_description' => '~',
                'sub_link' => '~',
                'sub_link2' => '~',
                'sub_bg_color' => '~',
                'sub_image' => '~',
                'sub_year' => '~',
                'location_id' => '~',
                'resource' => json_encode($data),
                'page_id_td' => $this->context['pid'],
                'temp_pos_id' => $this->context['tpos'],
            ]);
            $query = $insert;
        }
        $statement = $this->sql->prepareStatementForSqlObject($query);
        $results = $statement->execute();
    }
    private function getWidgetOption()
    {
        if ($this->checkWidgetOpts()) {
            $select = $this->sql->select('template_details');
            $select->columns(['title', 'description', 'resource']);
            $select->where([
                'temp_id' => $this->context['tid'],
                'page_id_td' => $this->context['pid'],
                'temp_pos_id' => $this->context['tpos'],

            ]);
            $select->limit(1);
            $statement = $this->sql->prepareStatementForSqlObject($select);
            $results = $this->sqlResults->initialize($statement->execute());
            $rawOpts = $results->toArray()[0];
            $opts['title'] = $rawOpts['title'];
            $opts['description'] = $rawOpts['description'];
            $opts['data'] = json_decode($rawOpts['resource'], true);
        } else {
            $opts['title'] = '';
            $opts['description'] = '';
            $opts['data'] = false;
        }
        return $opts;
    }
    private function getWidgetDefault()
    {
        if ($this->checkWidgetOpts(true)) {
            $select = $this->sql->select('template_details');
            $select->columns(['title', 'description', 'resource']);
            $select->where([
                'temp_id' => $this->context['tid'],
                'page_id_td' => 0,
                'temp_pos_id' => 0,

            ]);
            $select->limit(1);
            $statement = $this->sql->prepareStatementForSqlObject($select);
            $results = $this->sqlResults->initialize($statement->execute());
            $rawOpts = $results->toArray()[0];
            $opts['title'] = $rawOpts['title'];
            $opts['description'] = $rawOpts['description'];
            $opts['data'] = json_decode($rawOpts['resource'], true);
        } else {
            $opts['title'] = '';
            $opts['description'] = '';
            $opts['data'] = false;
        }
        return $opts;
    }
}
