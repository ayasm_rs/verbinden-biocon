<?PHP
require_once("./include/membersite_config.php");
include("./include/mysqli_connect.php");
include("./include/html_codes.php");
include("./include/function.php");
$common_function = new common_function($connect);
$page_list= $common_function->page_list();
$option='<option value=""> Select page link</option>';
for($i=0; $i < count($page_list); $i++)
{
	$option .='<option value="'.$page_list[$i]["id"].'">'.$page_list[$i]["page_name"].'</option>';
}
$temp_id="2";

admin_way_top();

admin_top_bar();


admin_left_menu();

//After login check for page access

$id_user=$fgmembersite->idUser();
$admin = 0;
$temp_id=mysqli_real_escape_string($connect,$_GET['tid']);
$pid=mysqli_real_escape_string($connect,$_GET['pid']);
$tpos=mysqli_real_escape_string($connect,$_GET['tpos']);
$select_title="SELECT * FROM `template_title_desc` WHERE `temp_id`='$temp_id' AND page_id_td='$pid' AND temp_pos_id='$tpos' ";
$run_title=mysqli_query($connect,$select_title);

while($row=mysqli_fetch_array($run_title)){
	$title=$row['title'];
	$description=$row['description'];
}

$select_event="SELECT * FROM `template_details` WHERE temp_id='$temp_id' AND page_id_td='$pid' AND temp_pos_id='$tpos' ";
$run_query=mysqli_query($connect,$select_event);

if(isset($_POST['create_page'])){
	
	$delete_count=count($_POST['delete_item']);
	if($delete_count>0){
	for($delete=0;$delete<$delete_count;$delete++){
		$delete_query="DELETE FROM `template_details` WHERE id='".$_POST['delete_item'][$delete]."'";
		mysqli_query($connect, $delete_query);
	}
	}
	$title_value = htmlspecialchars(mysqli_real_escape_string($connect, $_POST['title_value']),ENT_QUOTES, 'UTF-8');
	$value_description = htmlspecialchars(mysqli_real_escape_string($connect, $_POST['value_description']),ENT_QUOTES, 'UTF-8');
	mysqli_query($connect,"UPDATE `template_title_desc` SET `title`='$title_value',`description`='$value_description' WHERE temp_id='$temp_id' ");
	$count=count($_POST['block7_id']);
	
	for($key=0;$key<$count;$key++)
	{
		//echo "ccccccccc".$count;exit;
		$block7_menu_file =  $_POST['block7_menu_file'][$key];
		$block7_id =  $_POST['block7_id'][$key];
		$remove_image_id =  $_POST['remove_image_id'][$key];
		$block7_menu_title = htmlspecialchars(mysqli_real_escape_string($connect, $_POST['block7_menu_title'][$key]),ENT_QUOTES, 'UTF-8');
		$block7_menu_desc = htmlspecialchars(mysqli_real_escape_string($connect, $_POST['block7_menu_desc'][$key]),ENT_QUOTES, 'UTF-8');
		$select_block7 = htmlspecialchars(mysqli_real_escape_string($connect, $_POST['select_block7'][$key]),ENT_QUOTES, 'UTF-8');
		$block7_menu_color = htmlspecialchars(mysqli_real_escape_string($connect, $_POST['block7_menu_color'][$key]),ENT_QUOTES, 'UTF-8');
		//$file_name ="";
		$file_image ="";
		$file_img=htmlspecialchars(mysqli_real_escape_string($connect, $_POST['block7_2_menu_file'][$key]),ENT_QUOTES, 'UTF-8');
		if($file_img!=""){
			
			$name='image'.date('ymdis').rand(0,99999);
			$file_image = str_replace(" ","-",$name).".png";
			//echo $file;exit;
			list($type, $file_img) = explode(';', $file_img);
			list(, $file_img)      = explode(',', $file_img);
			file_put_contents("../images/".$file_image , base64_decode($file_img));
			if($block7_id!=""){
				$sql_img="UPDATE `template_details` SET `sub_image`='".$file_image."' WHERE temp_id='$temp_id' AND id=".$block7_id;
				mysqli_query($connect, $sql_img);
			}
		}
		if($block7_menu_file!="" && $remove_image_id=="yes")
		{
			$file=htmlspecialchars(mysqli_real_escape_string($connect, $_POST['block7_menu_file'][$key]),ENT_QUOTES, 'UTF-8');
			$name='icon'.date('ymdis').rand(0,99999);
			$file_name = str_replace(" ","-",$name).".png";
			//echo $file;exit;
			list($type, $file) = explode(';', $file);
			list(, $file)      = explode(',', $file);
			file_put_contents("../images/icons/".$file_name , base64_decode($file));
			$sql="UPDATE `template_details` SET `sub_icon`='".$file_name."', `sub_title`='".$block7_menu_title."', `sub_description`='".$block7_menu_desc."', `sub_link`='".$select_block7."', `sub_bg_color`='".$block7_menu_color."' WHERE temp_id='$temp_id' AND id=".$block7_id;
		}elseif($block7_menu_file=="" && $remove_image_id=="yes"){
			$sql="UPDATE `template_details` SET  `sub_title`='".$block7_menu_title."', `sub_description`='".$block7_menu_desc."', `sub_link`='".$select_block7."', `sub_bg_color`='".$block7_menu_color."' WHERE temp_id='$temp_id' AND id=".$block7_id;
		}elseif($remove_image_id=="no"){
			$sql="UPDATE `template_details` SET `sub_icon`='', `sub_title`='".$block7_menu_title."', `sub_description`='".$block7_menu_desc."', `sub_link`='".$select_block7."', `sub_bg_color`='".$block7_menu_color."' WHERE temp_id='$temp_id' AND id=".$block7_id;
			$file_name="";
		}
	
		if($block7_id==""){
		$sql="INSERT INTO `template_details`(`temp_id`, `title`, `description`, `sub_icon`, `sub_title`, `sub_description`, `sub_link`, `sub_bg_color`, `sub_image`, `page_id_td`, `temp_pos_id`) VALUES ('$temp_id', '', '', '".$file_name."', '".$block7_menu_title."','".$block7_menu_desc."','".$select_block7."','".$block7_menu_color."','".$file_image."','".$pid."','".$tpos."')";
		mysqli_query($connect, $sql);
		$page_id = mysqli_insert_id($connect);    
    $date_time=date('Y-m-d H:i:s');
	//mysqli_query($connect,"INSERT INTO `latest_updates`(`temp_page_id`, `edited_by`, `edited_type`, `updated_date`, `approved_by`, `page_id`, `table_name`, `updated_date_time`) VALUES ('','$id_user','added','$date','','$page_id','global quality','$date_time')");
		}else{
		
		mysqli_query($connect, $sql);
		}
	}
	
	
}


//redirect code


    $id_user=$fgmembersite->idUser();
	$approver = 0;$admin=0;
	if($fgmembersite->UserRole()=="admin"){
		$admin=1;
	}
	if($fgmembersite->UserRole()!="admin"){
		echo "<script>window.location.href='dashboard.php'</script>";
	}

//mysqli_query($connect, "TRUNCATE table temp_data");
?>
									
<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<style>
.cr_img img{max-width:500px !important;height:auto !important;}
</style>
<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">
			
			<div class="row">
				<div class="col-xs-12">
					<div class="page-title-box">
						<h4 class="page-title">Widget Two</h4>
						<!--button class="btn btn-primary"  type="button" name="create_page" onclick="$('#sub_btn').click()" style="float:right">Create Page</button-->
								
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
			<!-- end row -->
                        <form method="POST" name="xxfrm" id="xxfrm" enctype="multipart/form-data">
				<div class="row">
					<div class="col-xs-12 col-lg-12 col-xl-12">
						<div class="card-box">
							<h4 class="header-title m-t-0 m-b-30">Widget Details</h4>
							<section>
								<p class="text-muted m-b-20 font-13 pull-right">
									(* All fields are mandatory)
								</p>
						
					  <!-- start our value drivers div  -->
						 <h5 class="heading_cust">Widget Two</h5>
							<!--img src="assets/images/users/block07.jpg" data-toggle="modal" data-target="#myModal"  data-title="Block 6" class="block-img">
							
							<span>(Preview Image)</span-->
						 <hr>
						 <section class="row">
							<div class="col-xs-12 col-lg-12 col-xl-12">
							   <section>
								  <div class="row">
									 <div class="col-xs-12 col-lg-6 col-xl-6">
										<div class="form-group clearfix">
										   <label class="col-lg-2 control-label " for="" >Title
										   </label>
										   <div class="col-lg-10">
											  <input type="text" name="title_value" id="title_value" class="form-control" value="<?php echo $title; ?>" />
											  <br />
										   </div>
										</div>
									 </div>
									 <div class="col-xs-12 col-lg-6 col-xl-6">
										<div class="form-group clearfix">
										   <label class="col-lg-2 control-label " for="" >Short Description
										   </label>
										   <div class="col-lg-10">
											  <textarea name="value_description" id="value_description" class="form-control" ><?php echo $description; ?></textarea>
											  <br />
										   </div>
										</div>
									 </div>
								  </div>
								  <div class="form-group clearfix">
									 <div class="col-lg-10">
										<div class="block7_add_menu_toggle row " >
										<?php 
										$j=1;
										$script="";
										while($rows=mysqli_fetch_array($run_query)){ 
										$image_remove="no";
										$image_show="none";
										if($rows["sub_icon"]!=""){
										$image_remove="yes";
										$image_show="black";
										}
										echo '<div id="div_block7_'.$j.'" class="col-sm-6 count_class" ><div class="row"><div style="padding:10px;margin:10px;border: 1px solid #ccc;float: left;width: 100%;" class="row"><div class="col-md-6" style="float:left;width:100%;"><main class="page"><div class="box"><label>New Image Upload</label><br><input type="file" id="file_input_block7_'.$j.'" class="file-input" ><textarea class="hide" name="block7_menu_file[]" id="block7_menu_file_block7_'.$j.'" ></textarea></div><div class="box-2" id="box-2_block7_'.$j.'"><div class="result_block7_'.$j.'"></div></div><div class="box-2 img-result_block7_'.$j.'"><img class="cropped_block7_'.$j.'" id="cropped_block7_'.$j.'" src="../images/icons/'.$rows["sub_icon"].'" alt=""><span class="btn btn-danger" id="remove_buttion_'.$j.'" style="display:'.$image_show.'" onclick=remove_image('.$j.'); >Remove</span></div><div class="box"><div class="options_block7_'.$j.' "><input type="number" min="0" class="hide img-w_block7_'.$j.'" value="150" min="100" max="1200" /></div><a class="btn-success btn-sm btn save_block7_'.$j.' " onclick=$(".cropped_block7_'.$j.'").show();$("#remove_image_'.$j.'").val("yes");$("#remove_buttion_'.$j.'").show();$(this).hide();$("#box-2_block7_'.$j.'").hide() style="display:none;width: calc(100%/2 - 2em);padding: 0.5em;">Crop</a></div></main></div><div class="col-md-12" ><label>Title*</label><input type="text" value="'.$rows["sub_title"].'"  class="form-control" name="block7_menu_title[]"  /><input type="hidden" value="'.$rows["id"].'"  class="form-control" id="delete_item_'.$j.'" name="block7_id[]"  /><input type="hidden" value="'.$image_remove.'"  class="form-control" name="remove_image_id[]" id="remove_image_'.$j.'" /></div><div class="col-md-12" ><label>Short Description*</label><textarea class="form-control" name="block7_menu_desc[]"  >'.$rows["sub_description"].'</textarea></div><div class="col-md-6" ><label>Select Link*</label><select class="form-control" name="select_block7[]"  >';
										for($i=0; $i < count($page_list); $i++)
										{
											$selectd="";
											if($page_list[$i]["id"]==$rows["sub_link"]){$selectd="selected";}
											echo '<option value="'.$page_list[$i]["id"].'" '.$selectd.'>'.$page_list[$i]["page_name"].'</option>';
										}
										echo '</select></div><div class="col-md-6" ><label>background Color*</label><input type="color" class="" name="block7_menu_color[]" value="'.$rows["sub_bg_color"].'"  /></div><div class="col-md-6" style="float:left;width:100%;"><main class="page"><div class="box"><label>New Image Upload</label><br><input type="file" id="file_input_block7_2_'.$j.'" class="file-input" ><textarea class="hide" name="block7_2_menu_file[]" id="block7_2_menu_file_block7_2_'.$j.'" ></textarea></div><div class="box-2" id="box-2_block7_2_'.$j.'"><div class="result_block7_2_'.$j.'"></div></div><div class="box-2 img-result_block7_2_'.$j.'"><img class="cropped_block7_2_'.$j.'" id="cropped_block7_2_'.$j.'" src="../images/'.$rows["sub_image"].'" alt=""></div><div class="box"><div class="options_block7_2_'.$j.' "><input type="number" min="0" class="hide img-w_block7_2_'.$j.'" value="262" min="100" max="1200" /><input type="number" min="0" class="hide img-h_block7_2_'.$j.'" value="275" min="100" max="1200" /></div><a class="btn-success btn-sm btn save_block7_2_'.$j.' " onclick=$(".cropped_block7_2_'.$j.'").show();$(this).hide();$("#box-2_block7_2_'.$j.'").hide() style="display:none;width: calc(100%/2 - 2em);padding: 0.5em;">Crop</a></div></main></div><div class="col-md-12" ><a class="btn btn-danger btn-sm " onclick=remove_div('.$j.'); style="float:right">Remove</a></div></div></div></div>';

										$script .='<script>document.querySelector("#file_input_block7_'.$j.'").addEventListener("change",function(e){var t=document.querySelector(".result_block7_'.$j.'"),r=(document.querySelector(".img-result_block7_'.$j.'"),document.querySelector(".img-w_block7_'.$j.'"),document.querySelector(".img-h_block7_'.$j.'"),document.querySelector(".options_block7_'.$j.'"));document.querySelector(".save_block7_'.$j.'"),document.querySelector(".cropped_block7_'.$j.'"),document.querySelector("#file_input_block7_'.$j.'");if(e.target.files.length){var o=new FileReader;o.onload=function(e){e.target.result&&($(".save_block7_'.$j.'").show(),$("#box-2_block7_'.$j.'").show(),$(".cropped_block7_'.$j.'").hide(),img=document.createElement("img"),img.id="image",img.src=e.target.result,t.innerHTML="",t.appendChild(img),r.classList.remove("hide"),cropper_block7_'.$j.' = new Cropper(img))},o.readAsDataURL(e.target.files[0])}}),document.querySelector(".save_block7_'.$j.'").addEventListener("click",function(e){document.querySelector(".result_block7_'.$j.'");var t=document.querySelector(".img-result_block7_'.$j.'"),r=document.querySelector(".img-w_block7_'.$j.'"),o=(document.querySelector(".img-h_block7_'.$j.'"),document.querySelector(".options_block7_'.$j.'"),document.querySelector(".save_block7_'.$j.'"),document.querySelector(".cropped_block7_'.$j.'"));document.querySelector("#file_input_block7_'.$j.'");e.preventDefault();var c=cropper_block7_'.$j.'.getCroppedCanvas({width:r.value}).toDataURL();o.classList.remove("hide"),t.classList.remove("hide"),o.src=c;document.querySelector("#block7_menu_file_block7_'.$j.'").value=c;});document.querySelector("#file_input_block7_2_'.$j.'").addEventListener("change",function(e){var t=document.querySelector(".result_block7_2_'.$j.'"),r=(document.querySelector(".img-result_block7_2_'.$j.'"),document.querySelector(".img-w_block7_2_'.$j.'"),document.querySelector(".img-h_block7_2_'.$j.'"),document.querySelector(".options_block7_2_'.$j.'"));document.querySelector(".save_block7_2_'.$j.'"),document.querySelector(".cropped_block7_2_'.$j.'"),document.querySelector("#file_input_block7_2_'.$j.'");if(e.target.files.length){var o=new FileReader;o.onload=function(e){e.target.result&&($(".save_block7_2_'.$j.'").show(),$("#box-2_block7_2_'.$j.'").show(),$(".cropped_block7_2_'.$j.'").hide(),img=document.createElement("img"),img.id="image",img.src=e.target.result,t.innerHTML="",t.appendChild(img),r.classList.remove("hide"),cropper_block7_2_'.$j.' = new Cropper(img,{aspectRatio: 262 / 275}))},o.readAsDataURL(e.target.files[0])}}),document.querySelector(".save_block7_2_'.$j.'").addEventListener("click",function(e){document.querySelector(".result_block7_2_'.$j.'");var t=document.querySelector(".img-result_block7_2_'.$j.'"),r=document.querySelector(".img-w_block7_2_'.$j.'"),o=(document.querySelector(".img-h_block7_2_'.$j.'"),document.querySelector(".options_block7_2_'.$j.'"),document.querySelector(".save_block7_2_'.$j.'"),document.querySelector(".cropped_block7_2_'.$j.'"));document.querySelector("#file_input_block7_2_'.$j.'");e.preventDefault();var c=cropper_block7_2_'.$j.'.getCroppedCanvas({width:r.value}).toDataURL();o.classList.remove("hide"),t.classList.remove("hide"),o.src=c;document.querySelector("#block7_2_menu_file_block7_2_'.$j.'").value=c;});</script>';
										$j++;
							 } ?>
							 </div>
										<div id="add_menu_item">
										<br>
										+ &nbsp;<a class="btn btn-primary waves-effect waves-light m-r-5 m-b-10" id="block7_add_menu">Add Item</a>
										</div>
									 </div>
								  </div>
								  <div class="form-group clearfix">
								  </div>
							   </section>
							</div>
							<!-- end col-->
						 </section>
					  <!-- end our value drivers div  -->
					  
					  
									
							</section>
						</div>
					</div>
					<div class="col-lg-12 form-group clearfix">
						<div class="card-box">
							<div class="col-lg-10 text-xs-center">
							<input type="hidden" name="create_page" id="create_page" value="submit" />
							<button class="btn btn-primary"  type="button" name="sub_btn" id="sub_btn">Update Widget</button>
								
							</div>
						</div>
					</div>
				</div>
				</form>
			<button class="btn btn-custom waves-effect waves-light btn-sm page_create_succcess"  style="display:none;">Click me</button>
		</div> <!-- container -->

	</div> <!-- content -->

</div>



<!--center>
<div class="container-narrow">
  <div>
    <h1>jQuery jQuery Awesome Cropper Demo</h1>
    
    <form role="form">
      <input id="sample_input" type="hidden" name="test[image]">
    </form>
  </div>
  <hr>
  
</div></center-->

<!-- End content-page -->




<!----------- Banner crop plugin  ------------------> 
	
		<link rel="stylesheet" type="text/css" href="simple_cropper/css/style.css" />
		<link rel="stylesheet" type="text/css" href="simple_cropper/css/style-example.css" />
		<link rel="stylesheet" type="text/css" href="simple_cropper/css/jquery.Jcrop.css" />

		<!-- Js files-->
		<script type="text/javascript" src="simple_cropper/scripts/jquery-1.10.2.min.js"></script>
		<script type="text/javascript" src="simple_cropper/scripts/jquery.Jcrop.js"></script>
		<script type="text/javascript" src="simple_cropper/scripts/jquery.SimpleCropper.js"></script>
		<script>
		 $('.cropme').simpleCropper();
		</script>
		
<!---------------- croper  --------------------->
<link rel="stylesheet" href="assets/css/choosenJs/prism.css">
	<link rel="stylesheet" href="assets/css/choosenJs/chosen.css">
	<link rel="stylesheet" href="assets/css/custom.css">
		<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/cropper/2.3.4/cropper.min.css'>
			
	
    <script src="assets/js/choosenJs/chosen.jquery.js" type="text/javascript">
    </script>
    <script src="assets/js/choosenJs/prism.js" type="text/javascript" charset="utf-8">
    </script>
    <script src="assets/js/choosenJs/init.js" type="text/javascript" charset="utf-8">
    </script>
		<script src='https://cdnjs.cloudflare.com/ajax/libs/cropperjs/0.8.1/cropper.min.js'></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>		
<script>
$(function() {
    // Multiple images preview in browser
    var imagesPreview = function(input, placeToInsertImagePreview) {

        if (input.files) {
            var filesAmount = input.files.length;

            for (i = 0; i < filesAmount; i++) {
                var reader = new FileReader();

                reader.onload = function(event) {
                    $($.parseHTML('<img>')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                }

                reader.readAsDataURL(input.files[i]);
            }
        }

    };

    $('#gallery-photo-add').on('change', function() {
        imagesPreview(this, 'div.gallery');
    });
});
</script>
            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->

<!-- Custom box css -->
<link href="assets/plugins/custombox/css/custombox.min.css" rel="stylesheet">

 <!-- Modal-Effect -->
<script src="assets/plugins/custombox/js/custombox.min.js"></script>
<script src="assets/plugins/custombox/js/legacy.min.js"></script>
			
<!-- Sweet Alert css -->
<link href="assets/plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css" />
<!-- Sweet Alert js -->
<script src="assets/plugins/bootstrap-sweetalert/sweet-alert.min.js"></script>
<script src="assets/pages/jquery.sweet-alert.init.js"></script>

<!-- Editor -->
<script src="//code.jquery.com/jquery-1.12.0.min.js"></script>


<script type="text/javascript">

var validExt = ".png, .jpeg, .jpg";
var validExt1 = ".png";
function fileExtValidate(fdata,id) { 
 var filePath = fdata.value;
 var getFileExt = filePath.substring(filePath.lastIndexOf('.') + 1).toLowerCase();
 if(id.includes("PImg") || id.includes("mPimg"))
 var pos = validExt1.indexOf(getFileExt);
else
var pos = validExt.indexOf(getFileExt);
	
 if(pos < 0) {
 	alert("This file is not allowed, please upload valid file.");
 	return false;
  } else {
  	return true;
  }
}
$(document).ready(function(){

$("#add_more").on("change",".d_priority",function(){
	var len_priority=$(".d_priority").length;
	var priority_value=$(this).val();
	var this_pri=$(this);
	var n_priority=$(".d_priority").index(this);
	for(i=0;i<len_priority;i++){
		//$(".d_priority:nth-child(1)").val();
		if(priority_value==$(".d_priority:eq( "+i+" )").val() && n_priority!=i){
		//$(this).removeClass("form-control");		
        this_pri.css("border","1px solid #fb0505");
		alert("Change the priority");
		}else{
		this_pri.css("border","1px solid #ccc");
		}
		}
	//alert($(".d_priority").length);
	
});

/**************************ADD MORE FIELDS**************************************/
var max_fields      = 20; //maximum input boxes allowed
    var wrapper         = $("#add_more"); //Fields wrapper
    var add_button      = $(".add_field_button"); //Add button ID
   
    var x = 2; //initlal text box count
    $(add_button).click(function(e){ //on add input button click
	//debugger;
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
$(wrapper).append('<div class="col-lg-12 form-group remove_div_'+x+'"> <hr> </div><div class="col-lg-6 form-group remove_div_'+x+'"><label class="control-label " for="meta_description">Image '+x+'*</label><br/><input type="hidden" class=" form-control pimage" name="pimage[]" id="pimage_'+x+'" class="imgs" data-id="mPimg_'+x+'" ><br/><div class="container"><div class="row"><div class="panel panel-body"><div class="span4 cropme landscape" id="landscape"></div></div></div></div><img src="" id="mPimg_'+x+'" class="imsrc" width="150px"/><span class="errorcls pimageerror" id="pimageerror"></span></div><div class="col-lg-12 form-group remove_div_'+x+'"><label class="control-label " for="meta_description">Description '+x+'*</label><textarea id="description'+x+'" name="description[]" class=" form-control description"  ></textarea><span class="errorcls descriptionerror" id="descriptionerror"></span><br><a href="#" onclick="$(\'.remove_div_'+x+'\').remove();return false;" class="btn btn-primary remove_field">Remove</a><br></div>'); //add input box
CKEDITOR.replace( 'description'+x+'' );
$('#pimage_'+x).awesomeCropper(
        { width: 1165, height: 500, debug: true }
        );
x++; //text box increment

        }
    });
   
    /* $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('section').remove(); x--;
    }) */
/*******************************************************************************/
$(wrapper).on("change",".imgs", function(e) {
var id=$(this).attr("data-id");
var filename=$(this);
if(fileExtValidate(this,id)) {
var fr = new FileReader;
var ext = this.files[0].type.split('/').pop().toLowerCase();
if($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {
    alert('invalid extension!');
}
else{
	var size = parseFloat(this.files[0].size / 1024).toFixed(2);
    fr.onload = function(e) { // file is loaded
     	var img = new Image;
		img.onload = function() {
			
			 var imgwidth = this.width;
			var imgheight = this.height;
			var flag=0;
			if(id.includes("PImg") || id.includes("mPimg"))
			{
				if(imgwidth>550 || imgheight>520)
				{
				flag=1;
				alert("Max image dimesion 550*460");
				filename.val("");
				}
			}
			else
			{
				if(imgwidth<725 || imgheight<239)
				{
				flag=1;
				alert("Min image dimesion 725*239");
				filename.val("");
				}
			}
			if(flag==0)
			{
            $('#'+id).attr('src', e.target.result);
			}
			
			};
		img.src = fr.result; // is the data URL because called with readAsDataURL
    };
    fr.readAsDataURL(this.files[0]); // I'm using a <input type="file"> for demonstrating
}	
	}
	else
		$(this).val("");
		
});
	
/******************************************************************************/
$(".imgs").on('change', function () {
var id=$(this).attr("data-id");
var filename=$(this);
if(fileExtValidate(this,id)) {
var fr = new FileReader;
var ext = this.files[0].type.split('/').pop().toLowerCase();
if($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {
    alert('invalid extension!');
}
else{
	var size = parseFloat(this.files[0].size / 1024).toFixed(2);
    fr.onload = function(e) { // file is loaded
     	
		var img = new Image;
		
		img.onload = function() {
			
            var imgwidth = this.width;
			var imgheight = this.height;
			var flag=0;
			if(id.includes("PImg") || id.includes("mPimg"))
			{
				if(imgwidth>550 || imgheight>520)
				{
				flag=1;
				filename.val("");
				alert("Max image dimesion 550*460");
				
				}
			}
			else
			{
				if(imgwidth<725 || imgheight<239)
				{
				flag=1;
				filename.val("");
				alert("Min image dimesion 725*239");
				}
			}
			if(flag==0)
			{
            $('#'+id).attr('src', e.target.result);
			}
					
			};
		img.src = fr.result; // is the data URL because called with readAsDataURL
    };
    fr.readAsDataURL(this.files[0]); // I'm using a <input type="file"> for demonstrating
}	
	}
	else
		$(this).val("");
		
});	
	$('.page_create_succcess' ).click(function () {
		swal({title:"Status!", text:"Widget updated successfully.", icon:"success"},
	function(){
		window.location="template-two.php?pid=<?php echo $pid.'&tid='.$temp_id.'&tpos='.$tpos; ?>";
	});
		
	});	
	<?php  if(isset($_POST['create_page'])){ ?>
	$('.page_create_succcess').click();	
	<?php } ?>
	


	
});


$(document).on('click',"#sub_btn",function(){
				var xxfrm = $("#xxfrm");
			/* if(validatedescription() & validatepimage() & validatep_description())
                { */
                    xxfrm.submit();
                /* } 
                
				                                           
                function validatep_description()
                {
                  if(CKEDITOR.instances['p_description'].getData().length<2)
                   {

                     p_description.removeClass("form-control");		
                     p_description.addClass("form-control errortxtbox");
                     p_descriptionerror.text("Please enter overview");

                     return false;	
                   }
                   else
                   {
                     p_description.removeClass("form-control errortxtbox");	
                     p_description.addClass("form-control");		
                     p_descriptionerror.text("");	

                     return true;
                   }    
                   
                } ///////////////  
				
				function validatedescription()
                {
				var description;
				var description_id;
                var descriptionerror;
				var error_states=true;
				for(i=0;i<$('input[name="pimage[]"]').length;i++){
				description_id=$(".description:eq("+i+")").attr("id");
				description=$(".description:eq("+i+")");
				//alert(CKEDITOR.instances[description_id].getData().length);
				descriptionerror=$(".descriptionerror:eq("+i+")");
                   if(CKEDITOR.instances[description_id].getData().length<2)
                   {
                     description.removeClass("form-control");		
                     description.addClass("form-control errortxtbox");
                     descriptionerror.text("Please enter description"); 

                     error_states=false;	
                   }
                   else
                   {
                     description.removeClass("form-control errortxtbox");	
                     description.addClass("form-control");		
                     descriptionerror.text(""); 
                     //return true;
                   }   
				}
				if(error_states==false){return false;}else{return true;}
                } ///////////////  
				
				function validatepimage()
                {

				var pimage;
                var pimageerror;
				var error_states=true;
				for(i=0;i<$('input[name="pimage[]"]').length;i++){
				//debugger;
				pimage=$(".pimage:eq("+i+")");
				pimageerror=$(".pimageerror:eq("+i+")");
                  if(pimage.val().length<1)
                   {
						//alert("error");
                     pimage.removeClass("form-control");		
                     pimage.addClass("form-control errortxtbox");
                     pimageerror.text("Please select image"); 

                     error_states=false;	
                   }
                   else
                   {
                     pimage.removeClass("form-control errortxtbox");	
                     pimage.addClass("form-control");		
                     pimageerror.text(""); 	
					//alert("success");
                     //return true;
                   }    
				}
				if(error_states==false){return false;}else{return true;}
                } ///////////////  
				
                */                           
                
                
		});

/* $(document).on("click", ".confirm",function(){
	window.location.href = "page-existing.php";
}); */
$("#page_title").keyup(function(){
	var leng=$("#page_title").val().length;
	$("#page_title_len").text(" [ "+(60-leng)+" character remaining ]");
});

</script>



<style>
.radio, .checkbox{
	display:inline-block;
}
.all_widgets{
    margin-left: 39px;	
	border-radius: 75%;
    width: 63px;
    text-align: center;
    padding: 10px;
    cursor: pointer;
    color: black;
    border: solid 1px #64b0f2;
	background-color:#64b0f2;
}

</style>

<!-- custom scroll bar --->
<!--script src="js/jquery.mCustomScrollbar.concat.js"></script-->
<link rel="stylesheet" type="text/css" href="css/jquery.mCustomScrollbar.css" />
<!--<script src="js/page-create-boardofdirectory.js"></script>-->


<!-- Css files siva -->
<link href="components/imgareaselect/css/imgareaselect-default.css" rel="stylesheet" media="screen">
<link rel="stylesheet" href="css/jquery.awesome-cropper.css">


<?php
admin_right_bar();

admin_footer();
?>
<script>
	$("#page-create").attr("class","active");
</script>

<!-- /container by siva --> 

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> 
<script src="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script> 
<script src="components/imgareaselect/scripts/jquery.imgareaselect.js"></script> 
<script src="build/jquery.awesome-cropper.js"></script> 
<script>
    $(document).ready(function () {
        $('#pimage_1').awesomeCropper(
        { width: 1165, height: 500, debug: true }
        );
    });
</script> 
<style>
.awesome-cropper>img{
	width:150px;
}	
canvas {
    max-width: 150px;
}
#add_more .form-group {
    margin-bottom: 0;
}
.page_title .form-group {
    margin-bottom: 30px;
}
.cropme img{
	width:100%;
	height:100%;
}
</style>

<script>
   // Replace the <textarea id="editor1"> with a CKEditor
   // instance, using default configuration.
  
	
	<!----------- Add menu button click code for 7th Block  ------------------>  
	var n=<?php echo $j; ?>;
	if(n>4){
		$("#add_menu_item").hide();
		}else{
		$("#add_menu_item").show();	
		}
	$("#block7_add_menu").click(function(){
		n++;
		var count = $(".count_class").length;
		if(count>2){
		$("#add_menu_item").hide();
		}else{
		$("#add_menu_item").show();
		}
		$(".block7_add_menu_toggle").append('<div id="div_block7_'+n+'" class="col-sm-6 count_class" ><div class="row"><div style="padding:10px;margin:10px;border: 1px solid #ccc;float: left;width: 100%;" class="row"><div class="col-md-6" style="float:left;width:100%;"><main class="page"><div class="box"><label>New Image Upload</label><br><input type="file" id="file_input_block7_'+n+'" class="file-input" ><textarea class="hide" name="block7_menu_file[]" id="block7_menu_file_block7_'+n+'" ></textarea></div><div class="box-2" id="box-2_block7_'+n+'"><div class="result_block7_'+n+'"></div></div><div class="box-2 img-result_block7_'+n+'"><img class="cropped_block7_'+n+'" id="cropped_block7_'+n+'" src="" alt=""></div><div class="box"><div class="options_block7_'+n+' "><span class="btn btn-danger" id="remove_buttion_'+n+'" style="display:none" onclick=remove_image('+n+'); >Remove</span><input type="number" min="0" class="hide img-w_block7_'+n+'" value="150" min="100" max="1200" /></div><a class="btn-success btn-sm btn save_block7_'+n+' " onclick=$(".cropped_block7_'+n+'").show();$("#remove_image_'+n+'").val("yes");$("#remove_buttion_'+n+'").show();$(this).hide();$("#box-2_block7_'+n+'").hide() style="display:none;width: calc(100%/2 - 2em);padding: 0.5em;">Crop</a></div></main></div><div class="col-md-12" ><label>Title*</label><input type="text" class="form-control" name="block7_menu_title[]"  /><input type="hidden" value=""  class="form-control" name="block7_id[]" id="delete_item_'+n+'" /><input type="hidden" value=""  class="form-control" name="remove_image_id[]" id="remove_image_'+n+'" /></div><div class="col-md-12" ><label>Short Description*</label><textarea class="form-control" name="block7_menu_desc[]"  ></textarea></div><div class="col-md-6" ><label>Select Link*</label><select class="form-control" name="select_block7[]"  ><?php echo $option; ?></select></div><div class="col-md-6" ><label>background Color*</label><input type="color" class="" name="block7_menu_color[]"  /></div><div class="col-md-6" style="float:left;width:100%;"><main class="page"><div class="box"><label>New Image Upload</label><br><input type="file" id="file_input_block7_2_'+n+'" class="file-input" ><textarea class="hide" name="block7_2_menu_file[]" id="block7_2_menu_file_block7_2_'+n+'" ></textarea></div><div class="box-2" id="box-2_block7_2_'+n+'"><div class="result_block7_2_'+n+'"></div></div><div class="box-2 img-result_block7_2_'+n+'"><img class="cropped_block7_2_'+n+'" id="cropped_block7_2_'+n+'" src="" alt=""></div><div class="box"><div class="options_block7_2_'+n+' "><input type="number" min="0" class="hide img-w_block7_2_'+n+'" value="262" min="100" max="1200" /><input type="number" min="0" class="hide img-h_block7_2_'+n+'" value="275" min="100" max="1200" /></div><a class="btn-success btn-sm btn save_block7_2_'+n+' " onclick=$(".cropped_block7_2_'+n+'").show();$(this).hide();$("#box-2_block7_2_'+n+'").hide() style="display:none;width: calc(100%/2 - 2em);padding: 0.5em;">Crop</a></div></main></div><div class="col-md-12" ><a class="btn btn-danger btn-sm " onclick=remove_div('+n+') style="float:right">Remove</a></div></div></div></div>');
		
		$('body').append('<script>document.querySelector("#file_input_block7_'+n+'").addEventListener("change",function(e){var t=document.querySelector(".result_block7_'+n+'"),r=(document.querySelector(".img-result_block7_'+n+'"),document.querySelector(".img-w_block7_'+n+'"),document.querySelector(".img-h_block7_'+n+'"),document.querySelector(".options_block7_'+n+'"));document.querySelector(".save_block7_'+n+'"),document.querySelector(".cropped_block7_'+n+'"),document.querySelector("#file_input_block7_'+n+'");if(e.target.files.length){var o=new FileReader;o.onload=function(e){e.target.result&&($(".save_block7_'+n+'").show(),$("#box-2_block7_'+n+'").show(),$(".cropped_block7_'+n+'").hide(),img=document.createElement("img"),img.id="image",img.src=e.target.result,t.innerHTML="",t.appendChild(img),r.classList.remove("hide"),cropper_block7_'+n+' = new Cropper(img))},o.readAsDataURL(e.target.files[0])}}),document.querySelector(".save_block7_'+n+'").addEventListener("click",function(e){document.querySelector(".result_block7_'+n+'");var t=document.querySelector(".img-result_block7_'+n+'"),r=document.querySelector(".img-w_block7_'+n+'"),o=(document.querySelector(".img-h_block7_'+n+'"),document.querySelector(".options_block7_'+n+'"),document.querySelector(".save_block7_'+n+'"),document.querySelector(".cropped_block7_'+n+'"));document.querySelector("#file_input_block7_'+n+'");e.preventDefault();var c=cropper_block7_'+n+'.getCroppedCanvas({width:r.value}).toDataURL();o.classList.remove("hide"),t.classList.remove("hide"),o.src=c;document.querySelector("#block7_menu_file_block7_'+n+'").value=c;});document.querySelector("#file_input_block7_2_'+n+'").addEventListener("change",function(e){var t=document.querySelector(".result_block7_2_'+n+'"),r=(document.querySelector(".img-result_block7_2_'+n+'"),document.querySelector(".img-w_block7_2_'+n+'"),document.querySelector(".img-h_block7_2_'+n+'"),document.querySelector(".options_block7_2_'+n+'"));document.querySelector(".save_block7_2_'+n+'"),document.querySelector(".cropped_block7_2_'+n+'"),document.querySelector("#file_input_block7_2_'+n+'");if(e.target.files.length){var o=new FileReader;o.onload=function(e){e.target.result&&($(".save_block7_2_'+n+'").show(),$("#box-2_block7_2_'+n+'").show(),$(".cropped_block7_2_'+n+'").hide(),img=document.createElement("img"),img.id="image",img.src=e.target.result,t.innerHTML="",t.appendChild(img),r.classList.remove("hide"),cropper_block7_2_'+n+' = new Cropper(img,{aspectRatio: 262 / 275}))},o.readAsDataURL(e.target.files[0])}}),document.querySelector(".save_block7_2_'+n+'").addEventListener("click",function(e){document.querySelector(".result_block7_2_'+n+'");var t=document.querySelector(".img-result_block7_2_'+n+'"),r=document.querySelector(".img-w_block7_2_'+n+'"),o=(document.querySelector(".img-h_block7_2_'+n+'"),document.querySelector(".options_block7_2_'+n+'"),document.querySelector(".save_block7_2_'+n+'"),document.querySelector(".cropped_block7_2_'+n+'"));document.querySelector("#file_input_block7_2_'+n+'");e.preventDefault();var c=cropper_block7_2_'+n+'.getCroppedCanvas({width:r.value}).toDataURL();o.classList.remove("hide"),t.classList.remove("hide"),o.src=c;document.querySelector("#block7_2_menu_file_block7_2_'+n+'").value=c;});<\/script>');
		
	});
function remove_image(id){
	var remove_image=document.getElementById("remove_image_"+id);
	var file_input_block7=document.getElementById("file_input_block7_"+id);
	var cropped_block7=document.getElementById("cropped_block7_"+id);
	var remove_buttion=document.getElementById("remove_buttion_"+id);
	remove_buttion.style.display="none";
	cropped_block7.style.display="none";
	file_input_block7.value=null;
	remove_image.value="no";
}
function remove_div(ids){
	var delete_item=document.getElementById("delete_item_"+ids).value;
	var div_block7=document.getElementById("div_block7_"+ids);
	div_block7.remove();
	$("form").append('<input type="hidden" name="delete_item[]" value="'+delete_item+'" >');
	var count = $(".count_class").length;
		if(count>3){
		$("#add_menu_item").hide();
		}else{
		$("#add_menu_item").show();
		}
}


</script>
<?php echo $script; ?>