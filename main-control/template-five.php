<?PHP
error_reporting(1);
ini_set("display_errors",1);
require_once("./include/membersite_config.php");
include("./include/mysqli_connect.php");
include("./include/html_codes.php");
include("./include/function.php");
$common_function = new common_function($connect);
$page_list= $common_function->page_list();
$option='';
for($i=0; $i < count($page_list); $i++)
{
$option .='<option value="'.$page_list[$i]["id"].'" >'.$page_list[$i]["page_name"].'</option>';
}

admin_way_top();

admin_top_bar();


admin_left_menu();

//After login check for page access
$id=5;
$id_user=$fgmembersite->idUser();
$admin = 0;
$temp_id=mysqli_real_escape_string($connect,$_GET['tid']);
$pid=mysqli_real_escape_string($connect,$_GET['pid']);
$tpos=mysqli_real_escape_string($connect,$_GET['tpos']);
$select_title="SELECT * FROM `template_title_desc` WHERE `temp_id`='$temp_id' AND page_id_td='$pid' AND temp_pos_id='$tpos' ";
$run_title=mysqli_query($connect,$select_title);


    $id_user=$fgmembersite->idUser();
	$approver = 0;$admin=0;
	if($fgmembersite->UserRole()=="admin"){
		$admin=1;
	}
	if($fgmembersite->UserRole()!="admin"){
		echo "<script>window.location.href='dashboard.php'</script>";
	}
	
	
if(isset($_POST['sub_btn']) && $_POST['create_page']=='submit')
{
//$deleted_img=explode("*",$_POST['deleted_img']);
$all_image_names=array();
$str=array();

	$exist_image=htmlspecialchars(mysqli_real_escape_string($connect, implode('~',$_POST['exist_image'])),ENT_QUOTES, 'UTF-8');
	$count_img = count($_POST['image_new']);
	for($j=0;$j<$count_img;$j++){
		$file2=htmlspecialchars(mysqli_real_escape_string($connect, $_POST['image_new'][$j]),ENT_QUOTES, 'UTF-8');
		$name1=date('ymdis').rand(0,99999);
		$file_name2 = str_replace(" ","-",$name1).".jpg";
		list($type, $file2) = explode(';', $file2);
		list(, $file2)      = explode(',', $file2);
		file_put_contents("images/temp_img/".$file_name2 , base64_decode($file2));
		$exist_image .="~".$file_name2;
	}
	//echo 	$file_name2;exit;
	
	

$new_links=$_POST['links'];
$tmid=htmlspecialchars(mysqli_real_escape_string($connect, $_POST['temp_id']),ENT_QUOTES, 'UTF-8');
$tmp_det=htmlspecialchars(mysqli_real_escape_string($connect, $_POST['tmp_det']),ENT_QUOTES, 'UTF-8');
$links=htmlspecialchars(mysqli_real_escape_string($connect, $new_links),ENT_QUOTES, 'UTF-8');
$tmp_title=htmlspecialchars(mysqli_real_escape_string($connect, $_POST['tmp_title']),ENT_QUOTES, 'UTF-8');
$tmp_description=htmlspecialchars(mysqli_real_escape_string($connect, $_POST['tmp_description']),ENT_QUOTES, 'UTF-8');
$bgcolor=htmlspecialchars(mysqli_real_escape_string($connect, $_POST['bgcolor']),ENT_QUOTES, 'UTF-8');
$file=htmlspecialchars(mysqli_real_escape_string($connect, $_POST['block_menu_file_block'][0]),ENT_QUOTES, 'UTF-8');

	/*************image***************/
	if($file!='')
	{
	$name='icon'.date('ymdis').rand(0,99999);
	$file_name = str_replace(" ","-",$name).".png";
	//echo $file;exit;
	list($type, $file) = explode(';', $file);
	list(, $file)      = explode(',', $file);
	file_put_contents("../images/icons/".$file_name , base64_decode($file));
	$str[]="sub_icon='".$file_name."'";
	}
	$qy="";
	if(!empty($str))
		$qy.=",".implode(',',$str);
	/**************ends image***************/
	
$query="UPDATE template_details SET title='".$tmp_title."', description='".$tmp_description."', sub_link='".$links."', sub_bg_color='".$bgcolor."', sub_image='".$exist_image."' ".$qy." WHERE id='".$tmp_det."' AND temp_id='".$tmid."'";

mysqli_query($connect,$query);
}	
/*************************/
$select_event="SELECT * FROM `template_details` WHERE temp_id='$temp_id' AND page_id_td='$pid'  ";
$run_product=mysqli_query($connect,$select_event);
$tot_num_rws=mysqli_num_rows($run_product);
?>
<style>
.cropme_new
{
    float: left;
    background-color: #f1f1f1;
    margin-bottom: 5px;
    margin-right: 5px;
    background-image: url(simple_cropper/images/UploadLight.png);
    background-position: center center;
    background-repeat: no-repeat;
    cursor: pointer;
}
.page
{
	
	float:none !important;
}
.total_blk
{
border: 1px solid #a8a6a6;
}
.imm{
	max-width: 25%;
    float: left;
    padding: 5px;
}
</style>									
<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">
			
			<div class="row">
				<div class="col-xs-12">
					<div class="page-title-box">
						
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
			<!-- end row -->
                <form method="POST" name="xxfrm" id="xxfrm" action="" onsubmit="return valid();" enctype="multipart/form-data">
				<div class="row">
					<div class="col-xs-12 col-lg-12 col-xl-12">
						<div class="card-box">
							<h4 class="header-title m-t-0 m-b-30">Widget Details</h4>
							<section>
							<input type="hidden" name="temp_id" value="<?php echo $id; ?>"/>
								<p class="text-muted m-b-20 font-13 pull-right">
									(* All fields are mandatory)
								</p>
								<div class="clearfix"></div>
										<?php
										$script="";
										
										while($row_event=mysqli_fetch_array($run_product)){
										$title=$row_event["title"];
										$description=$row_event["description"];
										$sub_icon=$row_event["sub_icon"];
										$sub_link=$row_event["sub_link"];
										$sub_bg_color=$row_event["sub_bg_color"];
										$num_rws=$row_event["id"];
										$sub_image=explode('~',$row_event["sub_image"]);
										$option1='';
										
										for($i=0; $i < count($page_list); $i++)
										{
											$sel='';
											if($page_list[$i]["id"]==$sub_link)
											{
												$sel='selected';
											}
											$option1 .='<option value="'.$page_list[$i]["id"].'" '.$sel.'>'.$page_list[$i]["page_name"].'</option>';
										}
										
										?>
										
								<div class="row each_box_<?php echo $num_rws ?> total_blk" >
								<input type="hidden" name="tmp_det" value="<?php echo $num_rws ?>" />
									<div class="col-lg-6 form-group">
										<div class="col-lg-12 form-group">
											<label class="control-label " for="tmp_title">Title *</label>
                                            <input id="tmp_title_<?php echo $num_rws; ?>" name="tmp_title" value="<?php echo $title; ?>"  type="text" class=" form-control" maxlength="20" required="required">
										</div>
										<div class="col-lg-12 form-group">
											<label class="control-label " for="tmp_description_">Description</label>
                                            <textarea id="tmp_description_<?php echo $num_rws; ?>" name="tmp_description"class=" form-control" maxlength="200"><?php echo $description; ?></textarea> 
										</div>
										<div class="col-lg-12 form-group">
											<label class="control-label " for="cmp_metadescription">Background Color</label>
											<input type="color" name="bgcolor" id="bgcolor" value="<?php echo $sub_bg_color; ?>" required="required">
										</div>
										<div class="col-lg-12 form-group">
											<label class="control-label " for="cmp_metadescription">Links</label>
                                           <select id="dates-field2" class="form-control" name="links" required="required">
											<?php echo $option1; ?>
										</select>
										</div>
										
										<div class="col-lg-12 form-group">
											<label class="control-label " for="cmp_metadescription">Icon</label>
                                        <!-------------------------icon------------------->
										<div class="form-group text-center">
                                    <div class="col-md-6" style="width:100%;">
									<main class="page"><div class="box"><input type="file" id="file-input_block_<?php echo $num_rws ?>" class="file-input" accept="image/x-png, image/jpeg"><textarea class="hide" name="block_menu_file_block[]" id="block_menu_file_block_<?php echo $num_rws ?>" ></textarea></div><div class="box-2" id="box-2_block_<?php echo $num_rws ?>"><div class="result_block_<?php echo $num_rws ?>"></div></div><div class="box-2 img-result_block_<?php echo $num_rws ?>"><img class="cropped_block_<?php echo $num_rws ?>" src="<?php echo "../images/icons/".$sub_icon;?>" alt=""></div><div class="box"><div class="options_block_<?php echo $num_rws ?> "><input type="number" min="0" class="hide img-w_block_<?php echo $num_rws ?>" value="150" min="100" max="1200" /></div><a class="btn-success btn-sm btn save_block_<?php echo $num_rws ?> " onclick='$(".cropped_block_<?php echo $num_rws ?>").show();$(this).hide();$("#box-2_block_<?php echo $num_rws ?>").hide()' style="display:none;width: calc(100%/2 - 2em);padding: 0.5em;">Crop</a></div></main></div>
							<?php
							$script.='<script>document.querySelector("#file-input_block_'.$num_rws.'").addEventListener("change",function(e){if ( this.files[0].type.match(/^image\//) ) {var t=document.querySelector(".result_block_'.$num_rws.'"),r=(document.querySelector(".img-result_block_'.$num_rws.'"),document.querySelector(".img-w_block_'.$num_rws.'"),document.querySelector(".img-h_block_'.$num_rws.'"),document.querySelector(".options_block_'.$num_rws.'"));document.querySelector(".save_block_'.$num_rws.'"),document.querySelector(".cropped_block_'.$num_rws.'"),document.querySelector("#file-input_block_'.$num_rws.'");if(e.target.files.length){var o=new FileReader;o.onload=function(e){e.target.result&&($(".save_block_'.$num_rws.'").show(),$("#box-2_block_'.$num_rws.'").show(),$(".cropped_block_'.$num_rws.'").hide(),img=document.createElement("img"),img.id="image",img.src=e.target.result,t.innerHTML="",t.appendChild(img),r.classList.remove("hide"),cropper_block_'.$num_rws.' = new Cropper(img))},o.readAsDataURL(e.target.files[0])}}else{alert("Invalid Formate"); $(this).val("");}}),document.querySelector(".save_block_'.$num_rws.'").addEventListener("click",function(e){document.querySelector(".result_block_'.$num_rws.'");var t=document.querySelector(".img-result_block_'.$num_rws.'"),r=document.querySelector(".img-w_block_'.$num_rws.'"),o=(document.querySelector(".img-h_block_'.$num_rws.'"),document.querySelector(".options_block_'.$num_rws.'"),document.querySelector(".save_block_'.$num_rws.'"),document.querySelector(".cropped_block_'.$num_rws.'"));document.querySelector("#file-input_block_'.$num_rws.'");e.preventDefault();var c=cropper_block_'.$num_rws.'.getCroppedCanvas({width:r.value}).toDataURL();o.classList.remove("hide"),t.classList.remove("hide"),o.src=c;document.querySelector("#block_menu_file_block_'.$num_rws.'").value=c;});</script>';
							?>
                            </div>
							</div>
							</div>
							<div class="col-lg-6 form-group">
								<label class="control-label " for="cmp_metadescription">Add Images</label>
								<input id="fileupload" type="file" multiple="multiple" name="mul_img[]"/>
								<div id="dvPreview">
								<?php 
								if(count($sub_image)>0)
								{
									foreach($sub_image as $key=>$val)
									{
										if($val!='')
										{
								?>
								<div id='remove_img_exist_<?php echo $key; ?>' class='imm'><img src='images/temp_img/<?php echo $val; ?>' style='height:100px;width: 100px' /><span class='img_rem_exist btn btn-danger' data-id='<?php echo $num_rws; ?>~<?php echo $val; ?>'>Remove</span>
								<input type="hidden" class="all_ext" name="exist_image[]" value="<?php echo $val; ?>" />
								</div>
								<?php
										}
									}
								}
								?>
								</div>
							</div>
							<?php
											
								}
								?>
								</div>
									<input type="hidden" name="deleted_img" id="deleted_img" value="" />
									<input type="hidden" name="total_img_count" id="total_img_count" value="<?php echo count($sub_image); ?>" />
							</section>
						</div>
					</div>
					<div class="col-lg-12 form-group clearfix">
						<div class="card-box">
							<div class="col-lg-10 text-xs-center">
							<input type="hidden" name="create_page" id="create_page" value="submit" />
							<input class="btn btn-primary"  type="submit" name="sub_btn" id="sub_btn" value="Update Widget"/>
								
							</div>
						</div>
					</div>
					<div id="del_ids_div"></div>
				</div>
				<button class="btn btn-custom waves-effect waves-light btn-sm page_create_succcess" style="display:none" >Click me</button>
				</form>
			
		</div> <!-- container -->

	</div> <!-- content -->

</div>

<!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->
			
<!-- Custom box css -->
<link href="assets/css/custom.css" rel="stylesheet">

<link href="assets/plugins/custombox/css/custombox.min.css" rel="stylesheet">

 <!-- Modal-Effect -->
<script src="assets/plugins/custombox/js/custombox.min.js"></script>
<script src="assets/plugins/custombox/js/legacy.min.js"></script>
			
<!-- Sweet Alert css -->
<link href="assets/plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css" />
<!-- Sweet Alert js -->
<script src="assets/plugins/bootstrap-sweetalert/sweet-alert.min.js"></script>
<script src="assets/pages/jquery.sweet-alert.init.js"></script>

<!-- Editor -->
<script src="//code.jquery.com/jquery-1.12.0.min.js"></script>

<?php
admin_right_bar();
admin_footer();
transparent_cropper();
?>
<script type="text/javascript" id="main_script">
<?php  if(isset($_POST['sub_btn']) && $_POST['create_page']=='submit'){ ?>
alert("Widget updated successfully.");
<?php } ?>
$(document).ready(function(e){
var arr=[];
var arr_exist=[];
$(function() {
    $('.multiselect-ui').multiselect({
		numberDisplayed: 2,
        includeSelectAllOption: true
    });
});
$(document).on("change","#fileupload",function (e) {
	    if (typeof (FileReader) != "undefined") {
			arr=[];
			arr_exist=[];
			//$("#deleted_img").val("");
			var i=<?php echo count($sub_image); ?>;
			var div="";
			
			/**************chk for existing************/
			$(".img_rem_exist").each(function(e){
				arr_exist.push($(this).attr('data-id'));
			});
			
            var dvPreview = $("#dvPreview");
            //dvPreview.html("");
			var regex = /\.(jpg|jpeg|png)$/i;
            $($(this)[0].files).each(function () {
                var file = $(this);
				var fs=(parseInt(file[0].size)*0.000001);
				if(fs<=1)
				{
					
                if (regex.test(file[0].name.toLowerCase())) {
                    var reader = new FileReader();
					 dvPreview.show();
                    reader.onload = function (e) {
						var img="<div id='remove_img_"+i+"' class='imm'><img src='"+e.target.result+"' style='height:100px;width: 100px' /><span class='img_rem btn btn-danger' data-id='"+i+"'>Remove</span><textarea name='image_new[]' class='image_new' style='display:none' >"+e.target.result+"</textarea></div>";
/*                         var img = $("<img />");
                        img.attr("style", "height:100px;width: 100px");
                        img.attr("src", ); */
                        dvPreview.append(img);
						var total_img_count=$("#total_img_count").val(i);
						i++;
						
                    }
                    reader.readAsDataURL(file[0]);
                } else {
                    alert(file[0].name + " is not a valid image file.");
                   dvPreview.hide();
					$("#fileupload").val("");
                    return false;
                }
				$("#fileupload").val("");
			}else {
                    alert("Upload Max 2MB Image");
                    dvPreview.hide();
					$("#fileupload").val("");
                    return false;
                }
            });
        } else {
            alert("This browser does not support HTML5 FileReader.");
        }
    });

$(document).on("click","#browse_btn",function(e){
	$("#fileupload").trigger("change");
});

$(document).on("click",".img_rem_exist",function(e){
	var ext=[];
	$(".all_ext").each(function(e){
		ext.push($(this).val());
	});
	
	if(confirm("Do you want to remove the image!"))
	{
		var data=$(this).attr("data-id");
		
		$.ajax({
			url:"ajax_calls.php",
			data:"&id="+data+"&type=remove_the_multiple_template_image&ext="+ext,
			type: 'POST',
			success:function(data)
			{
				window.location.href='template-five.php?pid=<?php echo $pid.'&tid='.$temp_id.'&tpos='.$tpos; ?>";
				//location.reload();
			}
			
		});
	}
	else
	{
		
	}
});

$(document).on("click",".img_rem",function(e){
	if(confirm("Do you want to remove the image!"))
	{
	var data=$(this).attr("data-id");
	$("#total_img_count").val(parseInt($("#total_img_count").val())-1);
	$("#remove_img_"+data).remove();
	/* 
	alert(data);
	console.log();
	var d=[];
	$(".image_new").each(function(e){
		d.push($(this).val());
	});
	console.log(d);
	var name=$("#fileupload")[0].files[data].name;
	arr.push(name);
	$("#deleted_img").val(arr.join("*"));
	$("#total_img_count").val(parseInt($("#total_img_count").val())-1);
	$("#remove_img_"+data).remove(); */
	
	}
});
});


	
</script>


<?php echo $script; ?>