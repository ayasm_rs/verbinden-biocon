<?PHP
require_once("./include/membersite_config.php");
include("./include/mysqli_connect.php");
include("./include/html_codes.php");

admin_way_top_dashboard();
	
admin_top_bar();

admin_left_menu();

/* ************** Logic Function *****************/

function get_temp_id_user($idparam,$connect){

  $result_data  = mysqli_query($connect," select * from biocon_users where id_user = ".$idparam);
  $row_cnt 	= $result_data->num_rows;
  
  if($row_cnt==0)
  {
    $ret_value = 0;
  }
  else
  {
      $row_data  = mysqli_fetch_assoc($result_data);
      $ret_value = $row_data["ref_id"];
  }
  
  return $ret_value;
  
} // function get_temp_id_user($id){

/* ************** Logic Function *****************/

$id =$fgmembersite->idUser();
if($fgmembersite->UserRole()!="admin"){
	$id =$fgmembersite->idUser();
	$role = $fgmembersite->role();
	$highDesignation = $fgmembersite->highDesignation();
	if($highDesignation == 'research' || $role == 'research'){
		$finid = get_temp_id_user($id,$connect);						
		echo "<script>window.location.href='myprofile-research-people.php?id=".$finid."&profile=myprofile'</script>";
	}
	elseif($highDesignation == 'placement' || $role == 'placement'){
		
		echo "<script>window.location.href='page-edit-placement-people.php?id=".$id."&profile=myprofile'</script>";
	}
	else{
                
                $finid = get_temp_id_user($id,$connect);

		echo "<script>window.location.href='myprofile-faculty.php?id=".$finid."&profile=myprofile'</script>";
	}
							
}
?>

            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
					
						<div class="row">
							<div class="col-xs-12">
								<div class="page-title-box">
                                    <h4 class="page-title">Dashboard</h4>
                                    <div class="clearfix"></div>
                                </div>
							</div>
						</div>
                        <!-- end row -->
	
						 <div class="col-xs-12 col-md-6">
                                        <div class="card-box">
                                            <h4 class="header-title m-t-0 m-b-20">Latest Updates</h4>

                                            <div id="scrolldivid"  class="inbox-widget nicescroll" style="height: 320px;">
                                                
												<?php
												$sql = " select lup.*,usr.name,usr.profile_pic from latest_updates lup , biocon_users usr WHERE lup.edited_by = usr.id_user ORDER BY lup.id DESC LIMIT 0,4 ";
												
												$query = mysqli_query($connect,$sql);
												
												while($row = mysqli_fetch_array($query)){

														$edited_type 	= $row["edited_type"];
														$updated_date 	= $row["updated_date_time"];
														$timestamp 		= strtotime($updated_date); 
														$table_name 	= $row['table_name'];
														$updated_date 	= date( 'd-m-Y h:i:s a', $timestamp );
														$updated_date1  = date( 'h:i:s a', $timestamp );
														$page_url		= '';
														$page_nav_name	= '';
														
														$approved_by    = $row["approved_by"];
														
														$url 			= "";
														
														$user 			= $row['name'];
														$profile_pic 	= $row['profile_pic'];
														
														$temp_id 		= $row['temp_page_id'];
														$page_id 		= $row['page_id'];
														$latest_id 		= $row['id'];
														
														
														if($profile_pic==""){
															$profile_pic = "user-default-icon.jpg";
														}

														if(trim($user)==""){
															$user = "Admin";
														}	
												
												?>
												
												<div class="inbox-item">
                                                        <div class="inbox-item-img"><img src="../main-control/user_images/<?php echo $profile_pic; ?>" class="img-circle" alt=""></div>
															<p class="inbox-item-author"><?php echo $user; ?></p>
															<p class="inbox-item-text" ><?php echo $user ."&nbsp;has ". $edited_type ."&nbsp;". $table_name ."&nbsp;On&nbsp;". $updated_date; ?></p>
															<p class="inbox-item-date"><?php echo $updated_date1; ?></p>
                                                </div>
													
												<?php
												
												} // while($row = mysqli_fetch_array($query)){
												
												?>
												
												
												
												<br>
												<p align="center"><button type="button" class="btn btn-primary" id="loadmorebtn" style="font-weight:bold; padding-top:7px;">Load More&nbsp;&nbsp;<img src="images/down-arrow-icons.png" style="valign:center;" /> </button></p>
												
                                            </div>
											
											

                                        </div>
										
										
										
                                    </div>
									
									<input type="hidden" name="pagenum" id="pagenum" value="1" />
									<input type="hidden" name="hiduserid" id="hiduserid" value="<?php echo $id;?>" />
									
									
					</div>
                </div> <!-- content -->



            </div>
            <!-- End content-page -->


            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->
			
			
			
			<script language="javascript">
			function myTrim(x) {
                    return x.replace(/^\s+|\s+$/gm,'');
			}
			$(function(){
				
						
				
				$("#loadmorebtn").click(function(){
					
					
					var pagenum = $("#pagenum").val();
					pagenum++;
					
					$.ajax({
					type: "POST",
					async: false, 
					url: "ajaxCalls-dashboard.php",
					data: {"type":"appenddataslatestupdate","pagenum":pagenum,"hiduserid":$("#hiduserid").val(),"bindedtype":"1"},
						success: function(data) {
							
								
								$('div .inbox-item:last').after(data);
								
								$("#pagenum").val(pagenum);
								
								var currheight = $( "#scrolldivid" ).prop('scrollHeight');
								$('#scrolldivid').scrollTop(currheight);
								
								
							
							
							
						}
					
					});
					
				});	
				
			});	
			</script>
			
			
			
			

<?php
admin_right_bar();

admin_footer_dashboard();
?>
