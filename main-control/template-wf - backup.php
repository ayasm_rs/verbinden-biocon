<?PHP
error_reporting(E_ALL);
ini_set('display_errors', 1);
require_once("./include/membersite_config.php");
//skip all html if ajax call
if(isset($_GET['isAjax'])&&$_GET['isAjax']==true){
	require_once("WidgetFactory/loader.php");
	exit(0);
}

include("./include/mysqli_connect.php");
include("./include/html_codes.php");
include("./include/function.php");
$common_function = new common_function($connect);
$page_list= $common_function->page_list();
$option='<option value=""> Select page link</option>';
for($i=0; $i < count($page_list); $i++)
{
    $option .='<option value="'.$page_list[$i]["id"].'">'.$page_list[$i]["page_name"].'</option>';
}
$temp_id=mysqli_real_escape_string($connect,$_GET['tid']);
$pid=mysqli_real_escape_string($connect,$_GET['pid']);
$tpos=mysqli_real_escape_string($connect,$_GET['tpos']);
$select_title="SELECT * FROM `template_title_desc` WHERE `temp_id`='$temp_id' AND page_id_td='$pid' AND temp_pos_id='$tpos' ";
$run_title=mysqli_query($connect,$select_title);

$select_event="SELECT * FROM `template_details` WHERE temp_id='$temp_id' AND page_id_td='$pid' AND temp_pos_id='$tpos' ";
$run_query=mysqli_query($connect,$select_event);


admin_way_top();

admin_top_bar();


admin_left_menu();

//After login check for page access

$id_user=$fgmembersite->idUser();
$admin = 0;

while($row=mysqli_fetch_array($run_title)){
    $title=$row['title'];
    $description=$row['description'];
}

if(isset($_POST['create_page'])){

    $delete_count=count($_POST['delete_item']);
    if($delete_count>0){
        for($delete=0;$delete<$delete_count;$delete++){
            $delete_query="DELETE FROM `template_details` WHERE id='".$_POST['delete_item'][$delete]."'";
            mysqli_query($connect, $delete_query);
        }
    }
    $title_value = htmlspecialchars(mysqli_real_escape_string($connect, $_POST['title_value']),ENT_QUOTES, 'UTF-8');
    $value_description = htmlspecialchars(mysqli_real_escape_string($connect, $_POST['value_description']),ENT_QUOTES, 'UTF-8');
    mysqli_query($connect,"UPDATE `template_title_desc` SET `title`='$title_value',`description`='$value_description' WHERE temp_id='$temp_id' AND page_id_td='$pid' ");
    $count=count($_POST['block7_id']);

    for($key=0;$key<$count;$key++)
    {
        //echo "ccccccccc".$count;exit;
        $block7_menu_file =  $_POST['block7_menu_file'][$key];
        $block7_id =  $_POST['block7_id'][$key];
        $remove_image_id =  $_POST['remove_image_id'][$key];
        $block7_menu_title = htmlspecialchars(mysqli_real_escape_string($connect, $_POST['block7_menu_title'][$key]),ENT_QUOTES, 'UTF-8');
        $block7_menu_desc = htmlspecialchars(mysqli_real_escape_string($connect, $_POST['block7_menu_desc'][$key]),ENT_QUOTES, 'UTF-8');
        $select_block7 = htmlspecialchars(mysqli_real_escape_string($connect, $_POST['select_block7'][$key]),ENT_QUOTES, 'UTF-8');
        $block7_menu_color = htmlspecialchars(mysqli_real_escape_string($connect, $_POST['block7_menu_color'][$key]),ENT_QUOTES, 'UTF-8');
        //$file_name ="";
        if($block7_menu_file!="" && $remove_image_id=="yes")
        {
            $file=htmlspecialchars(mysqli_real_escape_string($connect, $_POST['block7_menu_file'][$key]),ENT_QUOTES, 'UTF-8');
            $name='icon'.date('ymdis').rand(0,99999);
            $file_name = str_replace(" ","-",$name).".png";
            //echo $file;exit;
            list($type, $file) = explode(';', $file);
            list(, $file)      = explode(',', $file);
            file_put_contents("../images/icons/".$file_name , base64_decode($file));
            $sql="UPDATE `template_details` SET `sub_icon`='".$file_name."', `sub_title`='".$block7_menu_title."', `sub_description`='".$block7_menu_desc."', `sub_link`='".$select_block7."', `sub_bg_color`='".$block7_menu_color."' WHERE temp_id='$temp_id' AND id=".$block7_id;
        }elseif($block7_menu_file=="" && $remove_image_id=="yes"){
            $sql="UPDATE `template_details` SET  `sub_title`='".$block7_menu_title."', `sub_description`='".$block7_menu_desc."', `sub_link`='".$select_block7."', `sub_bg_color`='".$block7_menu_color."' WHERE temp_id='$temp_id' AND id=".$block7_id;
        }elseif($remove_image_id=="no"){
            $sql="UPDATE `template_details` SET `sub_icon`='', `sub_title`='".$block7_menu_title."', `sub_description`='".$block7_menu_desc."', `sub_link`='".$select_block7."', `sub_bg_color`='".$block7_menu_color."' WHERE temp_id='$temp_id' AND id=".$block7_id;
            $file_name="";
        }

        if($block7_id==""){
            $sql="INSERT INTO `template_details`(`temp_id`, `title`, `description`, `sub_icon`, `sub_title`, `sub_description`, `sub_link`, `sub_bg_color`, `sub_image`, `page_id_td`, `temp_pos_id`) VALUES ('$temp_id', '', '', '".$file_name."', '".$block7_menu_title."','".$block7_menu_desc."','".$select_block7."','".$block7_menu_color."','','".$pid."','".$tpos."')";
            mysqli_query($connect, $sql);
            $page_id = mysqli_insert_id($connect);
            $date_time=date('Y-m-d H:i:s');
            //mysqli_query($connect,"INSERT INTO `latest_updates`(`temp_page_id`, `edited_by`, `edited_type`, `updated_date`, `approved_by`, `page_id`, `table_name`, `updated_date_time`) VALUES ('','$id_user','added','$date','','$page_id','global quality','$date_time')");
        }else{

            mysqli_query($connect, $sql);
        }
    }


}


//redirect code


$id_user=$fgmembersite->idUser();
$approver = 0;$admin=0;
if($fgmembersite->UserRole()=="admin"){
    $admin=1;
}
if($fgmembersite->UserRole()!="admin"){
    echo "<script>window.location.href='dashboard.php'</script>";
}

//mysqli_query($connect, "TRUNCATE table temp_data");
?>

    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <style>
        .cr_img img{max-width:500px !important;height:auto !important;}
    </style>
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">

                <div class="row">
                    <div class="col-xs-12">
                        <div class="page-title-box">
                            <h4 class="page-title">Edit Widget</h4>
                            <!--button class="btn btn-primary"  type="button" name="create_page" onclick="$('#sub_btn').click()" style="float:right">Create Page</button-->

                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <!-- end row -->
               <?php
               $widget_factory_folder="WidgetFactory/";
               require_once("WidgetFactory/loader.php");?>
            </div> <!-- container -->

        </div> <!-- content -->

    </div>



    <!-- End content-page -->




    <!----------- Banner crop plugin  ------------------>

    <link rel="stylesheet" type="text/css" href="simple_cropper/css/style.css" />
    <link rel="stylesheet" type="text/css" href="simple_cropper/css/style-example.css" />
    <link rel="stylesheet" type="text/css" href="simple_cropper/css/jquery.Jcrop.css" />

    <!-- Js files-->
	<script src="https://cdn.jsdelivr.net/npm/vue"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.0/axios.min.js"></script>
	
 
   
    <!-- ============================================================== -->
    <!-- End Right content here -->
    <!-- ============================================================== -->

    <!-- Custom box css -->
    <link href="assets/plugins/custombox/css/custombox.min.css" rel="stylesheet">

    <!-- Modal-Effect -->
    <script src="assets/plugins/custombox/js/custombox.min.js"></script>
    <script src="assets/plugins/custombox/js/legacy.min.js"></script>

    <!-- Sweet Alert css -->
    <link href="assets/plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css" />
    <!-- Sweet Alert js -->
    <script src="assets/plugins/bootstrap-sweetalert/sweet-alert.min.js"></script>
    <script src="assets/pages/jquery.sweet-alert.init.js"></script>

    <!-- Editor -->
    <script src="//code.jquery.com/jquery-1.12.0.min.js"></script>


    <script type="text/javascript">

    var validExt = ".png, .jpeg, .jpg";
    var validExt1 = ".png";
    function fileExtValidate(fdata,id) {
        var filePath = fdata.value;
        var getFileExt = filePath.substring(filePath.lastIndexOf('.') + 1).toLowerCase();
        if(id.includes("PImg") || id.includes("mPimg"))
            var pos = validExt1.indexOf(getFileExt);
        else
            var pos = validExt.indexOf(getFileExt);

        if(pos < 0) {
            alert("This file is not allowed, please upload valid file.");
            return false;
        } else {
            return true;
        }
    }
    $(document).ready(function(){

        $("#add_more").on("change",".d_priority",function(){
            var len_priority=$(".d_priority").length;
            var priority_value=$(this).val();
            var this_pri=$(this);
            var n_priority=$(".d_priority").index(this);
            for(i=0;i<len_priority;i++){
                //$(".d_priority:nth-child(1)").val();
                if(priority_value==$(".d_priority:eq( "+i+" )").val() && n_priority!=i){
                    //$(this).removeClass("form-control");
                    this_pri.css("border","1px solid #fb0505");
                    alert("Change the priority");
                }else{
                    this_pri.css("border","1px solid #ccc");
                }
            }
            //alert($(".d_priority").length);

        });

        /**************************ADD MORE FIELDS**************************************/
        var max_fields      = 20; //maximum input boxes allowed
        var wrapper         = $("#add_more"); //Fields wrapper
        var add_button      = $(".add_field_button"); //Add button ID

        var x = 2; //initlal text box count
        $(add_button).click(function(e){ //on add input button click
            //debugger;
            e.preventDefault();
            if(x < max_fields){ //max input box allowed
                $(wrapper).append('<div class="col-lg-12 form-group remove_div_'+x+'"> <hr> </div><div class="col-lg-6 form-group remove_div_'+x+'"><label class="control-label " for="meta_description">Image '+x+'*</label><br/><input type="hidden" class=" form-control pimage" name="pimage[]" id="pimage_'+x+'" class="imgs" data-id="mPimg_'+x+'" ><br/><div class="container"><div class="row"><div class="panel panel-body"><div class="span4 cropme landscape" id="landscape"></div></div></div></div><img src="" id="mPimg_'+x+'" class="imsrc" width="150px"/><span class="errorcls pimageerror" id="pimageerror"></span></div><div class="col-lg-12 form-group remove_div_'+x+'"><label class="control-label " for="meta_description">Description '+x+'*</label><textarea id="description'+x+'" name="description[]" class=" form-control description"  ></textarea><span class="errorcls descriptionerror" id="descriptionerror"></span><br><a href="#" onclick="$(\'.remove_div_'+x+'\').remove();return false;" class="btn btn-primary remove_field">Remove</a><br></div>'); //add input box
                CKEDITOR.replace( 'description'+x+'' );
                $('#pimage_'+x).awesomeCropper(
                    { width: 1165, height: 500, debug: true }
                );
                x++; //text box increment

            }
        });

        /* $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
         e.preventDefault(); $(this).parent('section').remove(); x--;
         }) */
        /*******************************************************************************/
        $(wrapper).on("change",".imgs", function(e) {
            var id=$(this).attr("data-id");
            var filename=$(this);
            if(fileExtValidate(this,id)) {
                var fr = new FileReader;
                var ext = this.files[0].type.split('/').pop().toLowerCase();
                if($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {
                    alert('invalid extension!');
                }
                else{
                    var size = parseFloat(this.files[0].size / 1024).toFixed(2);
                    fr.onload = function(e) { // file is loaded
                        var img = new Image;
                        img.onload = function() {

                            var imgwidth = this.width;
                            var imgheight = this.height;
                            var flag=0;
                            if(id.includes("PImg") || id.includes("mPimg"))
                            {
                                if(imgwidth>550 || imgheight>520)
                                {
                                    flag=1;
                                    alert("Max image dimesion 550*460");
                                    filename.val("");
                                }
                            }
                            else
                            {
                                if(imgwidth<725 || imgheight<239)
                                {
                                    flag=1;
                                    alert("Min image dimesion 725*239");
                                    filename.val("");
                                }
                            }
                            if(flag==0)
                            {
                                $('#'+id).attr('src', e.target.result);
                            }

                        };
                        img.src = fr.result; // is the data URL because called with readAsDataURL
                    };
                    fr.readAsDataURL(this.files[0]); // I'm using a <input type="file"> for demonstrating
                }
            }
            else
                $(this).val("");

        });

        /******************************************************************************/
        $(".imgs").on('change', function () {
            var id=$(this).attr("data-id");
            var filename=$(this);
            if(fileExtValidate(this,id)) {
                var fr = new FileReader;
                var ext = this.files[0].type.split('/').pop().toLowerCase();
                if($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {
                    alert('invalid extension!');
                }
                else{
                    var size = parseFloat(this.files[0].size / 1024).toFixed(2);
                    fr.onload = function(e) { // file is loaded

                        var img = new Image;

                        img.onload = function() {

                            var imgwidth = this.width;
                            var imgheight = this.height;
                            var flag=0;
                            if(id.includes("PImg") || id.includes("mPimg"))
                            {
                                if(imgwidth>550 || imgheight>520)
                                {
                                    flag=1;
                                    filename.val("");
                                    alert("Max image dimesion 550*460");

                                }
                            }
                            else
                            {
                                if(imgwidth<725 || imgheight<239)
                                {
                                    flag=1;
                                    filename.val("");
                                    alert("Min image dimesion 725*239");
                                }
                            }
                            if(flag==0)
                            {
                                $('#'+id).attr('src', e.target.result);
                            }

                        };
                        img.src = fr.result; // is the data URL because called with readAsDataURL
                    };
                    fr.readAsDataURL(this.files[0]); // I'm using a <input type="file"> for demonstrating
                }
            }
            else
                $(this).val("");

        });
        $('.page_create_succcess' ).click(function () {
            swal({title:"Status!", text:"Widget updated successfully.", icon:"success"},
                function(){
                    window.location="template-one.php?pid=<?php echo $pid.'&tid='.$temp_id.'&tpos='.$tpos; ?>";
                });

        });
        <?php  if(isset($_POST['create_page'])){ ?>
        $('.page_create_succcess').click();
        <?php } ?>




    });


    $(document).on('click',"#sub_btn",function(){
        var xxfrm = $("#xxfrm");

        xxfrm.submit();


    });


    $("#page_title").keyup(function(){
        var leng=$("#page_title").val().length;
        $("#page_title_len").text(" [ "+(60-leng)+" character remaining ]");
    });

    </script>



    <style>
        .radio, .checkbox{
            display:inline-block;
        }
        .all_widgets{
            margin-left: 39px;
            border-radius: 75%;
            width: 63px;
            text-align: center;
            padding: 10px;
            cursor: pointer;
            color: black;
            border: solid 1px #64b0f2;
            background-color:#64b0f2;
        }

    </style>

    <!-- custom scroll bar --->
    <!--script src="js/jquery.mCustomScrollbar.concat.js"></script-->
    <link rel="stylesheet" type="text/css" href="css/jquery.mCustomScrollbar.css" />
    <!--<script src="js/page-create-boardofdirectory.js"></script>-->


    <!-- Css files siva -->
    <link href="components/imgareaselect/css/imgareaselect-default.css" rel="stylesheet" media="screen">
    <link rel="stylesheet" href="css/jquery.awesome-cropper.css">


<?php
admin_right_bar();

admin_footer();
?>
    <script>
        $("#page-create").attr("class","active");
    </script>

    <!-- /container by siva -->

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    <script src="components/imgareaselect/scripts/jquery.imgareaselect.js"></script>
    <script src="build/jquery.awesome-cropper.js"></script>
    <script>
        $(document).ready(function () {
            $('#pimage_1').awesomeCropper(
                { width: 1165, height: 500, debug: true }
            );
        });
    </script>
    <style>
        .awesome-cropper>img{
            width:150px;
        }
        canvas {
            max-width: 150px;
        }
        #add_more .form-group {
            margin-bottom: 0;
        }
        .page_title .form-group {
            margin-bottom: 30px;
        }
        .cropme img{
            width:100%;
            height:100%;
        }
    </style>


