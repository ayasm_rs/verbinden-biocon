<?PHP
error_reporting(E_ALL);
ini_set('display_errors', 1);
require_once("./include/membersite_config.php");

//skip all html if ajax call
if(isset($_GET['isAjax'])&&$_GET['isAjax']==true){
	require_once("WidgetFactory/loader.php");
	exit(0);
}

include("./include/mysqli_connect.php");
include("./include/html_codes.php");
include("./include/function.php");
$common_function = new common_function($connect);
$page_list= $common_function->page_list();
$option='<option value=""> Select page link</option>';
for($i=0; $i < count($page_list); $i++)
{
    $option .='<option value="'.$page_list[$i]["id"].'">'.$page_list[$i]["page_name"].'</option>';
}
$temp_id=mysqli_real_escape_string($connect,$_GET['tid']);
$pid=mysqli_real_escape_string($connect,$_GET['pid']);
$tpos=mysqli_real_escape_string($connect,$_GET['tpos']);
$select_title="SELECT * FROM `template_title_desc` WHERE `temp_id`='$temp_id' AND page_id_td='$pid' AND temp_pos_id='$tpos' ";
$run_title=mysqli_query($connect,$select_title);

$select_event="SELECT * FROM `template_details` WHERE temp_id='$temp_id' AND page_id_td='$pid' AND temp_pos_id='$tpos' ";
$run_query=mysqli_query($connect,$select_event);


admin_way_top();

admin_top_bar();
//library block
echo '
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

<!--Scripts-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.0/axios.min.js"></script>
<script src="https://rawgit.com/vuejs/vue/dev/dist/vue.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vue-resource@1.5.1"></script>
<script src="https://cdn.jsdelivr.net/npm/lodash@4.13.1/lodash.min.js"></script>

';

admin_left_menu();

//After login check for page access

$id_user=$fgmembersite->idUser();
$admin = 0;

while($row=mysqli_fetch_array($run_title)){
    $title=$row['title'];
    $description=$row['description'];
}

if(isset($_POST['create_page'])){

    $delete_count=count($_POST['delete_item']);
    if($delete_count>0){
        for($delete=0;$delete<$delete_count;$delete++){
            $delete_query="DELETE FROM `template_details` WHERE id='".$_POST['delete_item'][$delete]."'";
            mysqli_query($connect, $delete_query);
        }
    }
    $title_value = htmlspecialchars(mysqli_real_escape_string($connect, $_POST['title_value']),ENT_QUOTES, 'UTF-8');
    $value_description = htmlspecialchars(mysqli_real_escape_string($connect, $_POST['value_description']),ENT_QUOTES, 'UTF-8');
    mysqli_query($connect,"UPDATE `template_title_desc` SET `title`='$title_value',`description`='$value_description' WHERE temp_id='$temp_id' AND page_id_td='$pid' ");
    $count=count($_POST['block7_id']);

    for($key=0;$key<$count;$key++)
    {
        //echo "ccccccccc".$count;exit;
        $block7_menu_file =  $_POST['block7_menu_file'][$key];
        $block7_id =  $_POST['block7_id'][$key];
        $remove_image_id =  $_POST['remove_image_id'][$key];
        $block7_menu_title = htmlspecialchars(mysqli_real_escape_string($connect, $_POST['block7_menu_title'][$key]),ENT_QUOTES, 'UTF-8');
        $block7_menu_desc = htmlspecialchars(mysqli_real_escape_string($connect, $_POST['block7_menu_desc'][$key]),ENT_QUOTES, 'UTF-8');
        $select_block7 = htmlspecialchars(mysqli_real_escape_string($connect, $_POST['select_block7'][$key]),ENT_QUOTES, 'UTF-8');
        $block7_menu_color = htmlspecialchars(mysqli_real_escape_string($connect, $_POST['block7_menu_color'][$key]),ENT_QUOTES, 'UTF-8');
        //$file_name ="";
        if($block7_menu_file!="" && $remove_image_id=="yes")
        {
            $file=htmlspecialchars(mysqli_real_escape_string($connect, $_POST['block7_menu_file'][$key]),ENT_QUOTES, 'UTF-8');
            $name='icon'.date('ymdis').rand(0,99999);
            $file_name = str_replace(" ","-",$name).".png";
            //echo $file;exit;
            list($type, $file) = explode(';', $file);
            list(, $file)      = explode(',', $file);
            file_put_contents("../images/icons/".$file_name , base64_decode($file));
            $sql="UPDATE `template_details` SET `sub_icon`='".$file_name."', `sub_title`='".$block7_menu_title."', `sub_description`='".$block7_menu_desc."', `sub_link`='".$select_block7."', `sub_bg_color`='".$block7_menu_color."' WHERE temp_id='$temp_id' AND id=".$block7_id;
        }elseif($block7_menu_file=="" && $remove_image_id=="yes"){
            $sql="UPDATE `template_details` SET  `sub_title`='".$block7_menu_title."', `sub_description`='".$block7_menu_desc."', `sub_link`='".$select_block7."', `sub_bg_color`='".$block7_menu_color."' WHERE temp_id='$temp_id' AND id=".$block7_id;
        }elseif($remove_image_id=="no"){
            $sql="UPDATE `template_details` SET `sub_icon`='', `sub_title`='".$block7_menu_title."', `sub_description`='".$block7_menu_desc."', `sub_link`='".$select_block7."', `sub_bg_color`='".$block7_menu_color."' WHERE temp_id='$temp_id' AND id=".$block7_id;
            $file_name="";
        }

        if($block7_id==""){
            $sql="INSERT INTO `template_details`(`temp_id`, `title`, `description`, `sub_icon`, `sub_title`, `sub_description`, `sub_link`, `sub_bg_color`, `sub_image`, `page_id_td`, `temp_pos_id`) VALUES ('$temp_id', '', '', '".$file_name."', '".$block7_menu_title."','".$block7_menu_desc."','".$select_block7."','".$block7_menu_color."','','".$pid."','".$tpos."')";
            mysqli_query($connect, $sql);
            $page_id = mysqli_insert_id($connect);
            $date_time=date('Y-m-d H:i:s');
            //mysqli_query($connect,"INSERT INTO `latest_updates`(`temp_page_id`, `edited_by`, `edited_type`, `updated_date`, `approved_by`, `page_id`, `table_name`, `updated_date_time`) VALUES ('','$id_user','added','$date','','$page_id','global quality','$date_time')");
        }else{

            mysqli_query($connect, $sql);
        }
    }


}


//redirect code


$id_user=$fgmembersite->idUser();
$approver = 0;$admin=0;
if($fgmembersite->UserRole()=="admin"){
    $admin=1;
}
if($fgmembersite->UserRole()!="admin"){
    echo "<script>window.location.href='dashboard.php'</script>";
}

//mysqli_query($connect, "TRUNCATE table temp_data");
?>
  <!-- Js files-->
	<script src="https://cdn.jsdelivr.net/npm/vue"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.0/axios.min.js"></script>
    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <style>
        .cr_img img{max-width:500px !important;height:auto !important;}
    </style>
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">

                <div class="row">
                    <div class="col-xs-12">
                        <div class="page-title-box">
                            <h4 class="page-title">Edit Widget</h4>
                            <!--button class="btn btn-primary"  type="button" name="create_page" onclick="$('#sub_btn').click()" style="float:right">Create Page</button-->

                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <!-- end row -->
               <?php
               $widget_factory_folder="WidgetFactory/";
               require_once("WidgetFactory/loader.php");?>
            </div> <!-- container -->

        </div> <!-- content -->

    </div>



    <!-- End content-page -->




    <!----------- Banner crop plugin  ------------------>
   

  
	
 
   
    <!-- ============================================================== -->
    <!-- End Right content here -->
    <!-- ============================================================== -->

    <!-- Custom box css -->
    <link href="assets/plugins/custombox/css/custombox.min.css" rel="stylesheet">

    <!-- Modal-Effect -->
    <script src="assets/plugins/custombox/js/custombox.min.js"></script>
    <script src="assets/plugins/custombox/js/legacy.min.js"></script>

    <!-- Sweet Alert css -->
    <link href="assets/plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css" />
    <!-- Sweet Alert js -->
    <script src="assets/plugins/bootstrap-sweetalert/sweet-alert.min.js"></script>
    <script src="assets/pages/jquery.sweet-alert.init.js"></script>

    <!-- Editor -->
    <script src="//code.jquery.com/jquery-1.12.0.min.js"></script>

    <style>
        .radio, .checkbox{
            display:inline-block;
        }
        .all_widgets{
            margin-left: 39px;
            border-radius: 75%;
            width: 63px;
            text-align: center;
            padding: 10px;
            cursor: pointer;
            color: black;
            border: solid 1px #64b0f2;
            background-color:#64b0f2;
        }

    </style>

    <!-- custom scroll bar --->
    <!--script src="js/jquery.mCustomScrollbar.concat.js"></script-->
    <link rel="stylesheet" type="text/css" href="css/jquery.mCustomScrollbar.css" />
    <!--<script src="js/page-create-boardofdirectory.js"></script>-->


    <!-- Css files siva -->
    <link href="components/imgareaselect/css/imgareaselect-default.css" rel="stylesheet" media="screen">
    <link rel="stylesheet" href="css/jquery.awesome-cropper.css">


<?php
admin_right_bar();

admin_footer();
?>
  

    <!-- /container by siva -->

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    <!-- jQuery  -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/tether.min.js"></script><!-- Tether for Bootstrap -->
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/detect.js"></script>
        <script src="assets/js/fastclick.js"></script>
        <script src="assets/js/jquery.blockUI.js"></script>
        <script src="assets/js/waves.js"></script>
        <script src="assets/js/jquery.nicescroll.js"></script>
        <script src="assets/js/jquery.scrollTo.min.js"></script>
        <script src="assets/js/jquery.slimscroll.js"></script>
        <script src="assets/plugins/switchery/switchery.min.js"></script>
    <style>
        .awesome-cropper>img{
            width:150px;
        }
        canvas {
            max-width: 150px;
        }
        #add_more .form-group {
            margin-bottom: 0;
        }
        .page_title .form-group {
            margin-bottom: 30px;
        }
        .cropme img{
            width:100%;
            height:100%;
        }
    </style>


