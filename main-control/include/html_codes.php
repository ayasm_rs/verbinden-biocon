<?php
error_reporting(0);
include('mysqli_connect.php');
date_default_timezone_set('Asia/Kolkata');
//Site Details 
$site_details = '';

$query_site_details = mysqli_query($connect, "SELECT * FROM site_details");
while($row = mysqli_fetch_array($query_site_details)){
	$site_details .= $row['value'].'~';
}

$site_details_exp = explode("~", $site_details);
$site_name = $site_details_exp[0];
$site_url = $site_details_exp[1];
$logo = $site_details_exp[2];
$logo_small = $site_details_exp[3];
$favicon = $site_details_exp[4];

//Functions for Admin Portal
function admin_way_top(){
	$connect = $GLOBALS['connect'];
	$fgmembersite = $GLOBALS['fgmembersite'];
	$site_name = $GLOBALS['site_name'];
	$favicon = $GLOBALS['favicon'];
	if(!$fgmembersite->CheckLogin())
	{
		$fgmembersite->RedirectToURL("shuru.php");
		exit;
	}
	
	echo '<!DOCTYPE html>
	<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="">

		<!-- App Favicon -->
		<link rel="shortcut icon" href="../'.$favicon.'">

		<!-- App title -->
		<title>'.$site_name.' Admin Portal</title>

		<!-- Jquery -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
		
		<!--Morris Chart CSS -->
		<link rel="stylesheet" href="assets/plugins/morris/morris.css">

		<!--Form Wizard-->
        <link rel="stylesheet" type="text/css" href="assets/plugins/jquery.steps/demo/css/jquery.steps.css" />
		
		<!-- Switchery css -->
		<link href="assets/plugins/switchery/switchery.min.css" rel="stylesheet" />

		<!-- App CSS -->
		<link href="assets/css/style.css" rel="stylesheet" type="text/css" />

		<!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn\'t work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
		<!-- Modernizr js -->
		<script src="assets/js/modernizr.min.js"></script>
		
		
		<!-- Tagit -->
		<link href="assets/tagit/jquery.tagit.css" rel="stylesheet" type="text/css">
		<link href="assets/tagit/tagit.ui-zendesk.css" rel="stylesheet" type="text/css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" type="text/javascript" charset="utf-8"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js" type="text/javascript" charset="utf-8"></script>
		<script src="assets/tagit/tag-it.js" type="text/javascript" charset="utf-8"></script>
		<!-- Trigger the modal with a button -->
		
	</head>
	';
}

function admin_way_top_dashboard(){
	$connect = $GLOBALS['connect'];
	$fgmembersite = $GLOBALS['fgmembersite'];
	$site_name = $GLOBALS['site_name'];
	$favicon = $GLOBALS['favicon'];
	if(!$fgmembersite->CheckLogin())
	{
		$fgmembersite->RedirectToURL("index.php");
		exit;
	}
	
	echo '<!DOCTYPE html>
	<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="">

		<!-- App Favicon -->
		<link rel="shortcut icon" href="../'.$favicon.'">

		<!-- App title -->
		<title>'.$site_name.' Admin Portal</title>

		<!-- Jquery -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
		
		<!--Morris Chart CSS -->
		<link rel="stylesheet" href="assets/plugins/morris/morris.css">

		
		
		<!-- Switchery css -->
		<link href="assets/plugins/switchery/switchery.min.css" rel="stylesheet" />

		<!-- App CSS -->
		<link href="assets/css/style.css" rel="stylesheet" type="text/css" />

		<!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn\'t work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
		<!-- Modernizr js -->
		<script src="assets/js/modernizr.min.js"></script>
		
		
		
		
	</head>
	';
}

function getting_notification_count()
{
  include('mysqli_connect.php');
  $fgmembersite = $GLOBALS['fgmembersite'];
  $id_user = $fgmembersite->idUser();
  $admin = 0;   
  
  $chk_designation = $fgmembersite->highDesignation();
  $chk_role        = $fgmembersite->role();
  
    if($chk_role == 'admin')
    {
    $id_user = $fgmembersite->idUser();    
    }    
    else if( ($chk_designation == 'research' || $chk_role  == 'research') || ($chk_designation == 'placement' || $chk_role == 'placement'))
    {
       $id_user  = $fgmembersite->idUser();
       $id_user  = get_temp_id_user_html($id_user,$connect);
    }
    else
    {
       $id_user  = $fgmembersite->idUser();
       $id_user  = get_temp_id_user_html($id_user,$connect); 
    } 
    
    $page_access_urls ="";
    $tejas_users = mysqli_query($connect,"SELECT * FROM tejas_users WHERE ref_id = ".$id_user);
    while($row = mysqli_fetch_array($tejas_users)){
            $page_access_urls = $row['page_access_ids'];
            $edit_type = $row['edit_type'];
    }

    $page_access_urls_Array = explode("~",$page_access_urls);

    $myprofile =0;$edited_type ="";$temp_page_id_same_user ="";
    
    $latest_updates = mysqli_query($connect,"SELECT * FROM latest_updates where table_name='temp_tejas_users'"); 
    while($row11 = mysqli_fetch_array($latest_updates)){
	
	if( ($row11['edited_by'] == $row11['page_id']) && ($row11['edited_by'] == $id_user)  ){

		$temp_page_id_same_user = $row11['temp_page_id'];
                
                
		
		$edited_type = $row11['edited_type'];
		if($edited_type=="edited"){
			$myprofile++;
		}
		else{
			$myprofile=0;
		}
		
                //echo "Id :: ".$temp_page_id_same_user." >> ".$row11["id"]."<br>";
	}
        
    }
    
    if($edited_type =="edited"){
	$myprofile_sql_edited_by_same_user = mysqli_query($connect,"SELECT * FROM temp_tejas_users where id_user =".$temp_page_id_same_user); 
		if(mysqli_num_rows($myprofile_sql_edited_by_same_user)){
			$myprofile++;
		}
    }
    $count_alert = $myprofile;
    
    if($fgmembersite->UserRole()=="admin"){
	$admin = 1;
    }
    if($fgmembersite->UserRole()=="admin" ){
            $myprofile =0;
    }
    
    $program = $research = $aboutus = $admissions = $donate = $careers = $governing_body = $administration = $faculty = $staff = $research_people = $placement_team = 0;
$program_array = array();
$research_array = array();
$aboutus_array = array();
$admissions_array = array();
$careers_array = array();
$donate_array = array();
$people_array = array();
$other_array = array();
$fullwidth_array =array();
$people =0;

$program = $research = $aboutus = $admissions = $donate = $careers = $governing_body = $administration = $faculty = $staff = $research_people = $placement_team = $media = $alumni_speak = $list_of_holidays = $tickers = $recruiting_companies = $testimonials = $fullwidth =  $banners = 0;

    foreach($page_access_urls_Array as $page_access_url){
	if($page_access_url=="governing_body" || $page_access_url=="administration" || $page_access_url=="faculty" || $page_access_url=="staff" || $page_access_url =="research_people" || $page_access_url =="placement_team"){
		array_push($people_array,$page_access_url);
		$page_access_url = 0;
		 
	}
	if($page_access_url=="alumni_speak" || $page_access_url=="list_of_holidays" || $page_access_url=="tickers" || $page_access_url=="recruiting_companies" ||  $page_access_url=="testimonials" || $page_access_url =="media" || $page_access_url =="banners" ){
		array_push($other_array,$page_access_url);
		$page_access_url = 0;
		 
	}
	
	$sql = mysqli_query($connect,"SELECT * FROM pages WHERE id=".$page_access_url); 
	while($row = mysqli_fetch_array($sql)){
		 if($row['page_nav_name'] == 'P'){
			 $program = 1;
			 array_push($program_array,$row['id']);
		 }
		 if($row['page_nav_name'] == 'R'){
			 $research = 1;
			 array_push($research_array,$row['id']);
		 }
		 if($row['page_nav_name'] == 'A'){
			 $aboutus = 1;
			 array_push($aboutus_array,$row['id']);
		 }
		  if($row['page_nav_name'] == 'Ad'){
			 $admissions = 1;
			 array_push($admissions_array,$row['id']);
		 }
		 if($row['page_nav_name'] == 'C'){
			 $careers = 1;
			 array_push($careers_array,$row['id']);
		 }
		 if($row['page_nav_name'] == 'D'){
			 $donate = 1;
			 array_push($donate_array,$row['id']);
		 }
		 if($row['page_nav_name'] == ''){
			 $fullwidth = 1;
			 array_push($fullwidth_array,$row['id']);
		 }
		 if($row['page_nav_name'] == 'B'){
			 $blog = 1;
			 array_push($blog_array,$row['id']);
		 }
		
	}
	
	
    }

    if(in_array("governing_body",$people_array,true)){
            $governing_body = 1;
    }
    if(in_array("administration",$people_array,true)){
            $administration = 1;
    }
    if(in_array("faculty",$people_array,true)){
            $faculty = 1;
    }
    if(in_array("staff",$people_array,true)){
            $staff = 1;
    }
    if(in_array("research_people",$people_array,true)){
            $research_people = 1;
    }
    if(in_array("placement_team",$people_array,true)){
            $placement_team = 1;
    }
    if(in_array("media",$other_array,true)){
            $media = 1;
    }
    if(in_array("alumni_speak",$other_array,true)){
            $alumni_speak = 1;
    }
    if(in_array("list_of_holidays",$other_array,true)){
            $list_of_holidays = 1;
    }
    if(in_array("tickers",$other_array,true)){
            $tickers = 1;
    }

    if(in_array("banners",$other_array,true)){
            $banners = 1;
    }

    if(in_array("recruiting_companies",$other_array,true)){
            $recruiting_companies = 1;
    }
	if(in_array("testimonials",$other_array,true)){
            $testimonials = 1;
    }
	
    $sql_temp = mysqli_query($connect,"SELECT * FROM temp_tejas_users"); 
     if(mysqli_num_rows($sql_temp)){
             $people++;
    }
	/*
    $sql_temp = mysqli_query($connect,"SELECT * FROM temp_testimonials"); 
    if(mysqli_num_rows($sql_temp)){
             $testimonials++;
    }
    */
    // About Us 
    
    $id_user = $fgmembersite->idUser();
    $already_added_ids1 = 0;
    
    $rec_count = 0;
    
    if($aboutus || $admin ){                                                                                            
												
                                                                                                
        $sql_temp_programs = mysqli_query($connect,"SELECT * FROM temp_pages WHERE page_nav_name='A' AND  admin_status = 0 "); 
            while($row_temp_programs = mysqli_fetch_array($sql_temp_programs)){
                $temp_id            =  $row_temp_programs['id'];
                $program_page_url   = $row_temp_programs['page_url'];
                $last_updates_sql   = mysqli_query($connect,"SELECT * FROM latest_updates WHERE temp_page_id='".$temp_id."' AND table_name = 'temp_pages' ");
                $temp_page_url = "";$l_id ="";$date = "";
                while($last_updates_row = mysqli_fetch_array($last_updates_sql)){
                    $page_id        =  $last_updates_row['page_id'];
                    $edited_by      = $last_updates_row['edited_by'];
                    $edited_type    = $last_updates_row['edited_type'];
                    $get_name       = mysqli_fetch_assoc(  mysqli_query($connect,"SELECT name FROM tejas_users WHERE id_user=".$edited_by));
                    $user_name      = $get_name['name'];
                    $l_id           = $last_updates_row['id'];
                    $date = $last_updates_row['updated_date_time'];
                }
													
		$rec_count++;											
	}
        
    } // if($aboutus || $admin ){ 
    
    // Admissions 
    
    if($admissions || $admin ){
												
        $sql_temp_programs = mysqli_query($connect,"SELECT * FROM temp_pages WHERE page_nav_name='Ad' AND admin_status = 0 "); 
            while($row_temp_programs = mysqli_fetch_array($sql_temp_programs)){
                $temp_id =  $row_temp_programs['id'];
                $program_page_url = $row_temp_programs['page_url'];
                $last_updates_sql = mysqli_query($connect,"SELECT * FROM latest_updates WHERE temp_page_id='".$temp_id."' AND table_name = 'temp_pages' ");
                $temp_page_url = "";$l_id ="";$date = "";
                    while($last_updates_row = mysqli_fetch_array($last_updates_sql)){
                    $page_id =  $last_updates_row['page_id'];
                    $edited_by = $last_updates_row['edited_by'];
                    $edited_type = $last_updates_row['edited_type'];
                    $get_name = mysqli_fetch_assoc(  mysqli_query($connect,"SELECT name FROM tejas_users WHERE id_user=".$edited_by));
                    $user_name = $get_name['name'];
                    $l_id = $last_updates_row['id'];
                    $date = $last_updates_row['updated_date_time'];
                    }
		
                    $rec_count++;
													
                }
    } // if($admissions || $admin ){
    
    // Careers 
    if($careers || $admin ){
												
        $sql_temp_programs = mysqli_query($connect,"SELECT * FROM temp_pages WHERE page_nav_name='C' AND admin_status = 0 "); 
            while($row_temp_programs = mysqli_fetch_array($sql_temp_programs)){
                $temp_id =  $row_temp_programs['id'];
                $program_page_url = $row_temp_programs['page_url'];
                $last_updates_sql = mysqli_query($connect,"SELECT * FROM latest_updates WHERE temp_page_id='".$temp_id."' AND table_name = 'temp_pages' ");
                $temp_page_url = "";$l_id ="";$date = "";
                    while($last_updates_row = mysqli_fetch_array($last_updates_sql)){
                    $page_id =  $last_updates_row['page_id'];
                    $edited_by = $last_updates_row['edited_by'];
                    $edited_type = $last_updates_row['edited_type'];
                    $get_name = mysqli_fetch_assoc(  mysqli_query($connect,"SELECT name FROM tejas_users WHERE id_user=".$edited_by));
                    $user_name = $get_name['name'];
                    $l_id = $last_updates_row['id'];
                    $date = $last_updates_row['updated_date_time'];
														
                    }
													
                    $rec_count++;
            }
    } // if($careers || $admin ){
    
    // Donate
    if($donate || $admin ){
												
        $sql_temp_programs = mysqli_query($connect,"SELECT * FROM temp_pages WHERE page_nav_name='D' AND admin_status = 0 "); 
            while($row_temp_programs = mysqli_fetch_array($sql_temp_programs)){
                $temp_id =  $row_temp_programs['id'];
                $program_page_url = $row_temp_programs['page_url'];
                $last_updates_sql = mysqli_query($connect,"SELECT * FROM latest_updates WHERE temp_page_id='".$temp_id."' AND table_name = 'temp_pages' ");
                $temp_page_url = "";$l_id ="";$date = "";
                    while($last_updates_row = mysqli_fetch_array($last_updates_sql)){
                        $page_id =  $last_updates_row['page_id'];
                        $edited_by = $last_updates_row['edited_by'];
                        $edited_type = $last_updates_row['edited_type'];
                        $get_name = mysqli_fetch_assoc(  mysqli_query($connect,"SELECT name FROM tejas_users WHERE id_user=".$edited_by));
                        $user_name = $get_name['name'];
                        $l_id = $last_updates_row['id'];
                        $date = $last_updates_row['updated_date_time'];
                    }
													
                     $rec_count++;
                }
    } // if($donate || $admin ){
    
    // Program
    if($program || $admin ){
												
            $sql_temp_programs = mysqli_query($connect,"SELECT * FROM temp_pages WHERE page_nav_name='P' AND admin_status = 0 "); 
                while($row_temp_programs = mysqli_fetch_array($sql_temp_programs)){
                    $temp_id =  $row_temp_programs['id'];
                    $program_page_url = $row_temp_programs['page_url'];
                    $last_updates_sql = mysqli_query($connect,"SELECT * FROM latest_updates WHERE temp_page_id='".$temp_id."' AND table_name = 'temp_pages' ");
                    $temp_page_url = "";$l_id ="";$date = "";
                    while($last_updates_row = mysqli_fetch_array($last_updates_sql)){
                        $page_id =  $last_updates_row['page_id'];
                        $edited_by = $last_updates_row['edited_by'];
                        $edited_type = $last_updates_row['edited_type'];
                        $get_name = mysqli_fetch_assoc(  mysqli_query($connect,"SELECT name FROM tejas_users WHERE id_user=".$edited_by));
                        $user_name = $get_name['name'];
                        $l_id = $last_updates_row['id'];
                        $date = $last_updates_row['updated_date_time'];
                    }
			
                    $rec_count++;
													
                }
    }
    
    // Research
    
    if($research || $admin ){
												
        $sql_temp_programs = mysqli_query($connect,"SELECT * FROM temp_pages WHERE page_nav_name='R' AND admin_status = 0 "); 
            while($row_temp_programs = mysqli_fetch_array($sql_temp_programs)){
                $temp_id =  $row_temp_programs['id'];
                $program_page_url = $row_temp_programs['page_url'];
                $last_updates_sql = mysqli_query($connect,"SELECT * FROM latest_updates WHERE temp_page_id='".$temp_id."' AND table_name = 'temp_pages' ");
                $temp_page_url = "";$l_id ="";$date = "";
                while($last_updates_row = mysqli_fetch_array($last_updates_sql)){
                    $page_id =  $last_updates_row['page_id'];
                    $edited_by = $last_updates_row['edited_by'];
                    $edited_type = $last_updates_row['edited_type'];
                    $get_name = mysqli_fetch_assoc(  mysqli_query($connect,"SELECT name FROM tejas_users WHERE id_user=".$edited_by));
                    $user_name = $get_name['name'];
                    $l_id = $last_updates_row['id'];
                    $date = $last_updates_row['updated_date_time'];
                }
		
                $rec_count++;
													
            }
    }
    
    // Blog 
    if($blog || $admin ){
												
    $sql_temp_programs = mysqli_query($connect,"SELECT * FROM temp_pages WHERE page_nav_name='B' AND admin_status = 0 "); 
        while($row_temp_programs = mysqli_fetch_array($sql_temp_programs)){
            $temp_id =  $row_temp_programs['id'];
            $program_page_url = $row_temp_programs['page_url'];
            $last_updates_sql = mysqli_query($connect,"SELECT * FROM latest_updates WHERE temp_page_id='".$temp_id."' AND table_name = 'temp_pages' ");
            $temp_page_url = "";$l_id ="";$date = "";
                while($last_updates_row = mysqli_fetch_array($last_updates_sql)){
                $page_id =  $last_updates_row['page_id'];
                $edited_by = $last_updates_row['edited_by'];
                $edited_type = $last_updates_row['edited_type'];
                $get_name = mysqli_fetch_assoc(  mysqli_query($connect,"SELECT name FROM tejas_users WHERE id_user=".$edited_by));
                $user_name = $get_name['name'];
                $l_id = $last_updates_row['id'];
                $date = $last_updates_row['updated_date_time'];
                }
		
              $rec_count++;  
													
        }
    }
    
    // Govering Body
    
    $already_added_ids = array();
    if($governing_body || $admin  ){
												
    $sql_temp_users = mysqli_query($connect,"SELECT * FROM temp_tejas_users WHERE designation  LIKE '%Governing Body%' OR highDesignation LIKE '%Governing Body%' AND admin_status = 0  ORDER BY name ASC"); 
        while($row_temp_users = mysqli_fetch_array($sql_temp_users)){
            $temp_id =  $row_temp_users['id_user'];
													
            $name = $row_temp_users['name'];
            $nameUrl = str_replace("&","%26",$name);
            $last_updates_sql = mysqli_query($connect,"SELECT * FROM latest_updates WHERE temp_page_id='".$temp_id."' ");
            $temp_page_url = "";$l_id ="";$date = "";
            while($last_updates_row = mysqli_fetch_array($last_updates_sql)){
                $page_id =  $last_updates_row['page_id'];
                $edited_by = $last_updates_row['edited_by'];
                $edited_type = $last_updates_row['edited_type'];
                $latest_id = $last_updates_row['id'];
                $get_name = mysqli_fetch_assoc(  mysqli_query($connect,"SELECT name FROM tejas_users WHERE id_user=".$edited_by));
                $user_name = $get_name['name'];
                $l_id = $last_updates_row['id'];
                $date = $last_updates_row['updated_date_time'];
                array_push($already_added_ids,$temp_id);
            }
                                                   
													
            $nameUrl = str_replace("&","%26",$name);
            if($date!=""){
		$rec_count++;											
            }
												
        }
    }
    
    // Faculty    
    if($faculty || $admin || $count_alert){
        if($myprofile){
													
            $sql_temp_users =  mysqli_query($connect,"SELECT * FROM temp_tejas_users where (role='faculty' OR highDesignation LIKE '%faculty%' OR designation LIKE '%faculty%') AND id_user NOT IN('".$already_added_ids1."') and id_user =".$temp_page_id_same_user." AND admin_status = 0");
            if($admin){	
            $sql_temp_users = mysqli_query($connect,"SELECT * FROM temp_tejas_users WHERE (role='faculty' OR highDesignation LIKE '%faculty%' OR designation LIKE '%faculty%') AND id_user NOT IN('".$already_added_ids1."') AND admin_status = 0 "); 
            }													
												
            
        }else{
            $already_added_ids1 = implode(",",$already_added_ids);
            $sql_temp_users = mysqli_query($connect,"SELECT * FROM temp_tejas_users WHERE (role='faculty' OR highDesignation LIKE '%faculty%' OR designation LIKE '%faculty%') AND id_user NOT IN('".$already_added_ids1."') AND admin_status = 0 "); 
        }
	
												
        while($row_temp_users = mysqli_fetch_array($sql_temp_users)){
            $temp_id =  $row_temp_users['id_user'];
            //$program_page_url = $row_temp_users['page_url'];
            $name = $row_temp_users['name'];
            $nameUrl = str_replace("'","",$name);
	    $last_updates_sql = mysqli_query($connect,"SELECT * FROM latest_updates WHERE temp_page_id='".$temp_id."' ");
            $temp_page_url = "";$l_id ="";$date = "";
            
            while($last_updates_row = mysqli_fetch_array($last_updates_sql)){
                $page_id =  $last_updates_row['page_id'];
                $edited_by = $last_updates_row['edited_by'];
                $edited_type = $last_updates_row['edited_type'];
                $latest_id = $last_updates_row['id'];
                $get_name = mysqli_fetch_assoc(  mysqli_query($connect,"SELECT name FROM tejas_users WHERE id_user=".$edited_by));
                $user_name = $get_name['name'];
                $l_id = $last_updates_row['id'];
                $date = $last_updates_row['updated_date_time'];
                array_push($already_added_ids,$temp_id);
            }
            //$nameUrl = str_replace("&","",$nameUrl);
            //$nameUrl = str_replace(".","-",$nameUrl);
            $nameUrl = str_replace("&","%26",$name);
													
		$rec_count++;										
            }
    }
    
    if($admin){
												
        $sql_temp_users = mysqli_query($connect,"SELECT * FROM temp_tejas_users WHERE role='admin' AND admin_status = 0"); 
            while($row_temp_users = mysqli_fetch_array($sql_temp_users)){
                $temp_id =  $row_temp_users['id_user'];
                //$program_page_url = $row_temp_users['page_url'];
                $name = $row_temp_users['name'];
                $nameUrl = str_replace("'","",$name);
                $last_updates_sql = mysqli_query($connect,"SELECT * FROM latest_updates WHERE temp_page_id='".$temp_id."' ");
                $temp_page_url = "";$l_id ="";$date = "";
                    while($last_updates_row = mysqli_fetch_array($last_updates_sql)){
                        $page_id =  $last_updates_row['page_id'];
                        $edited_by = $last_updates_row['edited_by'];
                        $edited_type = $last_updates_row['edited_type'];
                        $latest_id = $last_updates_row['id'];
                        $get_name = mysqli_fetch_assoc(  mysqli_query($connect,"SELECT name FROM tejas_users WHERE id_user=".$edited_by));
                        $user_name = $get_name['name'];
                        $l_id = $last_updates_row['id'];
                        $date = $last_updates_row['updated_date_time'];
                    }
                    //$nameUrl = str_replace("&","",$nameUrl);
                    //$nameUrl = str_replace(".","-",$nameUrl);
                    $nameUrl = str_replace("&","%26",$name);
													
			$rec_count++;									
            }
    }
    
    if($administration || $admin || $count_alert){
        if($myprofile){
            $sql_temp_users =  mysqli_query($connect,"SELECT * FROM temp_tejas_users where (designation  LIKE '%administration%' OR highDesignation LIKE '%administration%')AND id_user NOT IN('".$already_added_ids1."') and id_user =".$temp_page_id_same_user." AND admin_status = 0");
            if($admin){	
                $sql_temp_users = mysqli_query($connect,"SELECT * FROM temp_tejas_users WHERE (designation  LIKE '%administration%' OR highDesignation LIKE '%administration%')AND id_user NOT IN('".$already_added_ids1."') AND admin_status = 0  ORDER BY name ASC");
            }												   
        }else{
            $already_added_ids1 = implode(",",$already_added_ids);
            $sql_temp_users = mysqli_query($connect,"SELECT * FROM temp_tejas_users WHERE (designation  LIKE '%administration%' OR highDesignation LIKE '%administration%')AND id_user NOT IN('".$already_added_ids1."') AND admin_status = 0 ORDER BY name ASC"); 
        }
        
        while($row_temp_users = mysqli_fetch_array($sql_temp_users)){
            $temp_id =  $row_temp_users['id_user'];
            //$program_page_url = $row_temp_users['page_url'];
            $name = $row_temp_users['name'];
													
            $last_updates_sql = mysqli_query($connect,"SELECT * FROM latest_updates WHERE temp_page_id='".$temp_id."' ");
            $temp_page_url = "";$l_id ="";$date = "";
            
            while($last_updates_row = mysqli_fetch_array($last_updates_sql)){
                $page_id =  $last_updates_row['page_id'];
                $edited_by = $last_updates_row['edited_by'];
                $edited_type = $last_updates_row['edited_type'];
                $latest_id = $last_updates_row['id'];
                $get_name = mysqli_fetch_assoc( mysqli_query($connect,"SELECT name FROM tejas_users WHERE id_user=".$edited_by));
                $user_name = $get_name['name'];
                $l_id = $last_updates_row['id'];
                $date = $last_updates_row['updated_date_time'];
                array_push($already_added_ids,$temp_id);
            }
            //$nameUrl = str_replace("&","",$nameUrl);
            //$nameUrl = str_replace(".","-",$nameUrl);
            $nameUrl = str_replace("&","%26",$name);

            $rec_count++;
												
            }
    }
    
    if($research_people || $admin || $count_alert){
        if($myprofile){
            $sql_temp_users =  mysqli_query($connect,"SELECT * FROM temp_tejas_users where role='research' and id_user =".$temp_page_id_same_user." AND admin_status = 0 "); 
            if($admin){
            $sql_temp_users = mysqli_query($connect,"SELECT * FROM temp_tejas_users WHERE role='research' AND admin_status = 0 ORDER BY name ASC"); 
            }
        }else{
        $sql_temp_users = mysqli_query($connect,"SELECT * FROM temp_tejas_users WHERE role='research' AND admin_status = 0 ORDER BY name ASC"); 
        }
        
        while($row_temp_users = mysqli_fetch_array($sql_temp_users)){
            $temp_id =  $row_temp_users['id_user'];
            //$program_page_url = $row_temp_users['page_url'];
            $name = $row_temp_users['name'];
            $nameUrl = str_replace("'","",$name);
            $last_updates_sql = mysqli_query($connect,"SELECT * FROM latest_updates WHERE temp_page_id='".$temp_id."' ");
            $temp_page_url = "";$l_id ="";$date = "";
            
            while($last_updates_row = mysqli_fetch_array($last_updates_sql)){
                $page_id =  $last_updates_row['page_id'];
                $edited_by = $last_updates_row['edited_by'];
                $edited_type = $last_updates_row['edited_type'];
                $latest_id = $last_updates_row['id'];
                $get_name = mysqli_fetch_assoc(  mysqli_query($connect,"SELECT name FROM tejas_users WHERE id_user=".$edited_by));
                $user_name = $get_name['name'];
                $l_id = $last_updates_row['id'];
                $date = $last_updates_row['updated_date_time'];
            }
            
            //$nameUrl = str_replace("&","",$nameUrl);
            //$nameUrl = str_replace(".","-",$nameUrl);
            $nameUrl = str_replace("&","%26",$name);
													
            $rec_count++;
        }
    }
    
    if($staff || $admin || $count_alert){
												
        $already_added_ids1 = implode(",",$already_added_ids);
        if($myprofile){
            $sql_temp_users =  mysqli_query($connect,"SELECT * FROM temp_tejas_users where (highDesignation  LIKE '%staff%'  OR highDesignation LIKE '%Staff%' OR highDesignation LIKE '%staff%')  AND id_user NOT IN('".$already_added_ids1."') and id_user =".$temp_page_id_same_user." AND admin_status = 0"); 
            if($admin){
                $sql_temp_users = mysqli_query($connect,"SELECT * FROM temp_tejas_users WHERE  (highDesignation  LIKE '%staff%' OR highDesignation LIKE '%Staff%' OR highDesignation LIKE '%staff%')  AND id_user NOT IN('".$already_added_ids1."') AND admin_status = 0 ORDER BY name ASC");
            }
        }else{
													
            $sql_temp_users = mysqli_query($connect,"SELECT * FROM temp_tejas_users WHERE  (highDesignation  LIKE '%staff%' OR highDesignation LIKE '%Staff%' OR highDesignation LIKE '%staff%')  AND id_user NOT IN('".$already_added_ids1."') AND admin_status = 0 ORDER BY name ASC"); 
        }
        
        while($row_temp_users = mysqli_fetch_array($sql_temp_users)){
            
            $temp_id =  $row_temp_users['id_user'];
            //$program_page_url = $row_temp_users['page_url'];
            $name = $row_temp_users['name'];
            $nameUrl = str_replace("'","%27",$name);
            $last_updates_sql = mysqli_query($connect,"SELECT * FROM latest_updates WHERE temp_page_id='".$temp_id."' ");
            $temp_page_url = "";$l_id ="";$date = "";
            
            while($last_updates_row = mysqli_fetch_array($last_updates_sql)){
            
                $page_id =  $last_updates_row['page_id'];
                $edited_by = $last_updates_row['edited_by'];
                $edited_type = $last_updates_row['edited_type'];
                $latest_id = $last_updates_row['id'];
                $get_name = mysqli_fetch_assoc(  mysqli_query($connect,"SELECT name FROM tejas_users WHERE id_user=".$edited_by));
                $user_name = $get_name['name'];
                $l_id = $last_updates_row['id'];
                $date = $last_updates_row['updated_date_time'];
                array_push($already_added_ids,$temp_id);
            }
            //$nameUrl = str_replace("&","",$nameUrl);
            //$nameUrl = str_replace(".","-",$nameUrl);
            $nameUrl = str_replace("&","%26",$name);
													
            $rec_count++;
            
        }
    }
    
    if($placement_team || $admin || $count_alert){
												
        $already_added_ids1 = implode(",",$already_added_ids);
            if($myprofile){
                $sql_temp_users =  mysqli_query($connect,"SELECT * FROM temp_tejas_users where (role='placement' OR highDesignation LIKE '%placement%')  AND id_user NOT IN('".$already_added_ids1."') and id_user =".$temp_page_id_same_user." AND admin_status = 0"); 
            }else{
                $sql_temp_users = mysqli_query($connect,"SELECT * FROM temp_tejas_users WHERE (role='placement' OR highDesignation LIKE '%placement%')  AND id_user NOT IN('".$already_added_ids1."') AND admin_status = 0 ORDER BY name ASC"); 
            }
            
            while($row_temp_users = mysqli_fetch_array($sql_temp_users)){
                
                $temp_id =  $row_temp_users['id_user'];
                //$program_page_url = $row_temp_users['page_url'];
                $name = $row_temp_users['name'];
                $nameUrl = str_replace("'","%27",$name);
                $last_updates_sql = mysqli_query($connect,"SELECT * FROM latest_updates WHERE temp_page_id='".$temp_id."' ");
                $temp_page_url = "";$l_id ="";$date = "";
                while($last_updates_row = mysqli_fetch_array($last_updates_sql)){
                    $page_id =  $last_updates_row['page_id'];
                    $edited_by = $last_updates_row['edited_by'];
                    $edited_type = $last_updates_row['edited_type'];
                    $latest_id = $last_updates_row['id'];
                    $get_name = mysqli_fetch_assoc(  mysqli_query($connect,"SELECT name FROM tejas_users WHERE id_user=".$edited_by));
                    $user_name = $get_name['name'];
                    $l_id = $last_updates_row['id'];
                    $date = $last_updates_row['updated_date_time'];
                    array_push($already_added_ids,$temp_id);
                }
                //$nameUrl = str_replace("&","",$nameUrl);
                //$nameUrl = str_replace(".","-",$nameUrl);
                //$nameUrl = str_replace("'","",$nameUrl);
                $nameUrl = str_replace("&","%26",$name);
													
		$rec_count++;										
        }
    }
    
    if($media || $admin){
												
        $sql_temp_users = mysqli_query($connect,"SELECT * FROM temp_media where admin_status = 0 ORDER BY updated_date_time DESC"); 
            while($row_temp_users = mysqli_fetch_array($sql_temp_users)){
                $temp_id =  $row_temp_users['id'];
                $media_name = $row_temp_users['media_title'];
                $last_updates_sql = mysqli_query($connect,"SELECT * FROM latest_updates WHERE temp_page_id='".$temp_id."' and table_name = 'temp_media' ");
                $temp_page_url = "";$l_id ="";$date = "";
                
                while($last_updates_row = mysqli_fetch_array($last_updates_sql)){
                    $page_id =  $last_updates_row['page_id'];
                    $edited_by = $last_updates_row['edited_by'];
                    $edited_type = $last_updates_row['edited_type'];
                    $latest_id = $last_updates_row['id'];
                    $get_name = mysqli_fetch_assoc(  mysqli_query($connect,"SELECT name FROM tejas_users WHERE id_user=".$edited_by));
                    $user_name = $get_name['name'];
                    $l_id = $last_updates_row['id'];
                    $date = $last_updates_row['updated_date_time'];
                }
                                                 	
		$rec_count++;										
        }
    }
    
    if($alumni_speak || $admin){
												
        $sql_temp_users = mysqli_query($connect,"SELECT * FROM temp_alumni_speak where admin_status = 0 ORDER BY updated_date_time DESC"); 
            while($row_temp_users = mysqli_fetch_array($sql_temp_users)){
                $temp_id =  $row_temp_users['id'];
                //$program_page_url = $row_temp_users['page_url'];
                $alumni_name = $row_temp_users['title'];
													
                $last_updates_sql = mysqli_query($connect,"SELECT * FROM latest_updates WHERE temp_page_id='".$temp_id."' ");
                $temp_page_url = "";$l_id ="";$date = "";
                while($last_updates_row = mysqli_fetch_array($last_updates_sql)){
                    $page_id =  $last_updates_row['page_id'];
                    $edited_by = $last_updates_row['edited_by'];
                    $edited_type = $last_updates_row['edited_type'];
                    $latest_id = $last_updates_row['id'];
                    $get_name = mysqli_fetch_assoc(  mysqli_query($connect,"SELECT name FROM tejas_users WHERE id_user=".$edited_by));
                    $user_name = $get_name['name'];
                    $l_id = $last_updates_row['id'];
                    $date = $last_updates_row['updated_date_time'];
                }
                                                    
		$rec_count++;											
												
        }
    }
    
    if($list_of_holidays || $admin){
												
        $sql_temp_users = mysqli_query($connect,"SELECT * FROM temp_calendar where admin_status = 0 ORDER BY title ASC"); 
            while($row_temp_users = mysqli_fetch_array($sql_temp_users)){
                $temp_id =  $row_temp_users['id'];
                $calendar_name = $row_temp_users['title'];											
                $last_updates_sql = mysqli_query($connect,"SELECT * FROM latest_updates WHERE temp_page_id='".$temp_id."' and table_name = 'temp_calendar' ");
                $temp_page_url = "";$l_id ="";$date = "";
                while($last_updates_row = mysqli_fetch_array($last_updates_sql)){
                    $page_id =  $last_updates_row['page_id'];
                    $edited_by = $last_updates_row['edited_by'];
                    $edited_type = $last_updates_row['edited_type'];
                    $latest_id = $last_updates_row['id'];
                    $get_name = mysqli_fetch_assoc(  mysqli_query($connect,"SELECT name FROM tejas_users WHERE id_user=".$edited_by));
                    $user_name = $get_name['name'];
                    $l_id = $last_updates_row['id'];
                    $date = $last_updates_row['updated_date_time'];
                }
                                                  
													
		$rec_count++;										
        }
    }
    
    if($tickers || $admin){
												
        $sql_temp_users = mysqli_query($connect,"SELECT * FROM temp_home_ticker where admin_status = 0 ORDER BY updated_date_time DESC"); 
            while($row_temp_users = mysqli_fetch_array($sql_temp_users)){
                $temp_id =  $row_temp_users['id'];
                $ticker_name = $row_temp_users['ticker'];												
                $last_updates_sql = mysqli_query($connect,"SELECT * FROM latest_updates WHERE temp_page_id='".$temp_id."' and table_name ='temp_home_ticker' ");
                $temp_page_url = "";$l_id ="";$date = "";
                while($last_updates_row = mysqli_fetch_array($last_updates_sql)){
                    $page_id =  $last_updates_row['page_id'];
                    $edited_by = $last_updates_row['edited_by'];
                    $edited_type = $last_updates_row['edited_type'];
                    $latest_id = $last_updates_row['id'];
                    $get_name = mysqli_fetch_assoc(  mysqli_query($connect,"SELECT name FROM tejas_users WHERE id_user=".$edited_by));
                    $user_name = $get_name['name'];
                    $l_id = $last_updates_row['id'];
                    $date = $last_updates_row['updated_date_time'];
                }
                                                  
                $rec_count++;
												
        }
    }
    
    if($recruiting_companies || $admin){
												
        $sql_temp_users = mysqli_query($connect,"SELECT * FROM temp_recruting_companies ORDER BY title ASC"); 
        while($row_temp_users = mysqli_fetch_array($sql_temp_users)){
            $temp_id =  $row_temp_users['id'];
            $comp_name = $row_temp_users['title'];												
            $last_updates_sql = mysqli_query($connect,"SELECT * FROM latest_updates WHERE temp_page_id='".$temp_id."' ");
            $temp_page_url = "";$l_id ="";$date = "";
            while($last_updates_row = mysqli_fetch_array($last_updates_sql)){
                $page_id =  $last_updates_row['page_id'];
                $edited_by = $last_updates_row['edited_by'];
                $edited_type = $last_updates_row['edited_type'];
                $latest_id = $last_updates_row['id'];
                $get_name = mysqli_fetch_assoc(  mysqli_query($connect,"SELECT name FROM tejas_users WHERE id_user=".$edited_by));
                $user_name = $get_name['name'];
                $l_id = $last_updates_row['id'];
                $date = $last_updates_row['updated_date_time'];
            }
                                                  
            $rec_count++;												
												
        }
    }
    
    if($testimonials || $admin){
												
        $sql_temp_users = mysqli_query($connect,"SELECT * FROM temp_testimonials WHERE admin_status = 0 ORDER BY updated_date_time DESC"); 
            while($row_temp_users = mysqli_fetch_array($sql_temp_users)){
                $temp_id =  $row_temp_users['id'];
                $test_name = $row_temp_users['Name'];													
                $last_updates_sql = mysqli_query($connect,"SELECT * FROM latest_updates WHERE temp_page_id='".$temp_id."' ");
                $temp_page_url = "";$l_id ="";$date = "";
                while($last_updates_row = mysqli_fetch_array($last_updates_sql)){
                    $page_id =  $last_updates_row['page_id'];
                    $edited_by = $last_updates_row['edited_by'];
                    $edited_type = $last_updates_row['edited_type'];
                    $latest_id = $last_updates_row['id'];
                    $get_name = mysqli_fetch_assoc(  mysqli_query($connect,"SELECT name FROM tejas_users WHERE id_user=".$edited_by));
                    $user_name = $get_name['name'];
                    $l_id = $last_updates_row['id'];
                    $date = $last_updates_row['updated_date_time'];
                }
                                                  
		$rec_count++;											
												
        }
    }
    
    if($fullwidth || $admin ){
                                                                                                
        $page_url ="";
        $sql_temp_programs = mysqli_query($connect,"SELECT * FROM temp_pages WHERE page_nav_name='' and admin_status = 0 "); 
            while($row_temp_programs = mysqli_fetch_array($sql_temp_programs)){
                $temp_id =  $row_temp_programs['id'];
                $program_page_url = $row_temp_programs['page_url'];
                                                                                                        
                $last_updates_sql = mysqli_query($connect,"SELECT * FROM latest_updates WHERE temp_page_id='".$temp_id."' and table_name = 'temp_pages' ");
                $temp_page_url = "";$l_id ="";$date = "";
                while($last_updates_row = mysqli_fetch_array($last_updates_sql)){
                    $page_id =  $last_updates_row['page_id'];
                    $edited_by = $last_updates_row['edited_by'];
                    $edited_type = $last_updates_row['edited_type'];
                    $get_name = mysqli_fetch_assoc(  mysqli_query($connect,"SELECT name FROM tejas_users WHERE id_user=".$edited_by));
                    $user_name = $get_name['name'];
                    $l_id = $last_updates_row['id'];
                    $date = $last_updates_row['updated_date_time'];
														
                }
													
               $rec_count++;     
        }
    }
    
    if($admin)
    {
                                                                                               
        $last_updates_sql = mysqli_query($connect,"SELECT t1.*, t2.title FROM latest_updates t1 INNER JOIN homepagebanner t2 ON t2.id = t1.temp_page_id WHERE t1.table_name = 'homepagebanner' AND t1.edited_type in ('created','edited') and t1.approved_by = '' ");
                                                                                                
        while($last_updates_row = mysqli_fetch_array($last_updates_sql)){
                                                                                                
            $date           = $last_updates_row['updated_date_time'];
            $l_id           = $last_updates_row['id'];
            $edited_type    = $last_updates_row['edited_type'];
            $title          = $last_updates_row["title"];
                                                                                                
            $rec_count++;
                                                                                            
        } // while($last_updates_row = mysqli_fetch_array($last_updates_sql)){
                                                                                            
    } // if(admin)    
    
    return $rec_count;  
  
} // function getting_notification_count()

function admin_top_bar(){
	$logo_small = $GLOBALS['logo_small'];
	$fgmembersite = $GLOBALS['fgmembersite'];
	
	$current_url = $_SERVER['REQUEST_URI'];
	$id_user=$fgmembersite->idUser();
	$pages_url = 'page';
	$highDesignation = $fgmembersite->highDesignation();
	$role = $fgmembersite->role();
	$page_access_urls ="";$approver=0;


	include('mysqli_connect.php');
	echo '
	<body class="fixed-left">

        <!-- Begin page -->
        <div id="wrapper">

            <!-- Top Bar Start -->
            <div class="topbar">

                <!-- LOGO -->
                <div class="topbar-left">
                    <a href="dashboard.php" class="logo">
                        <span><img style="height:50%;" src="../'. $logo_small .'"></span>
					</a>
                </div>


                <nav class="navbar navbar-custom">
					
                    <ul class="nav navbar-nav">
                        <li class="nav-item">
                            <button class="button-menu-mobile open-left waves-light waves-effect">
                                <i class="zmdi zmdi-menu"></i>
                            </button>
                        </li>
                        <!--li class="nav-item hidden-mobile">
                            <form role="search" class="app-search" method="get" action="search.php">
                                <input type="text" placeholder="Search..." name="search_key" class="form-control" required maxlength="35">
                                <a href="#" type="submit" onclick="$(\'.app-search\').submit();"><i class="fa fa-search"></i></a>
                            </form>
                        </li-->
                    </ul>

                    <ul class="nav navbar-nav pull-right">';
					$id_user=$fgmembersite->idUser();
					$admin = 0;
					$count_alert=0;$count_alert_admin=0;
					$page_access_urls ="";$approver=0;
					$other_array = array();
					$tejas_users = mysqli_query($connect,"SELECT * FROM tejas_users WHERE id_user=".$id_user);
					while($row = mysqli_fetch_array($tejas_users)){
						$editer_type_id = $row['edit_type'];
                                               // echo $editer_type_id;
						$page_access_urls = $row['page_access_ids'];
						if($row['edit_type']=="approver"){
							$approver++;
						}
					}
					
                    if($approver){ 
					  if(!empty($page_access_urls)){
						$page_access_urls_Array = explode("~",$page_access_urls);
						$program = $research = 0;
						$program_array = array();
						$research_array = $people_array=  array();
						$count_alert = 0;$count_alert_admin = 0;
						foreach($page_access_urls_Array as $page_access_url){
							if($page_access_url=="governing_body" || $page_access_url=="administration" || $page_access_url=="faculty" || $page_access_url=="staff" || $page_access_url =="research_people" || $page_access_url =="placement_team"){
								array_push($people_array,$page_access_url);
								$page_access_url = 0;
								 
							}
						   if(($page_access_url=="alumni_speak" || $page_access_url=="list_of_holidays" || $page_access_url=="tickers" || $page_access_url=="recruiting_companies" || $page_access_url =="media" || $page_access_url=="testimonials")&& $page_access_url != 0){
								array_push($other_array,$page_access_url);
								$page_access_url = 0;
								 
							}
							
							$sql = mysqli_query($connect,"SELECT * FROM pages WHERE id=".$page_access_url); 
							while($row = mysqli_fetch_array($sql)){
								 if($row['page_nav_name'] == 'P'){
									 $program = 1;
									 array_push($program_array,$row['id']);
									 $sql_temp = mysqli_query($connect,"SELECT * FROM temp_pages WHERE page_nav_name='P' "); 
									 if(mysqli_num_rows($sql_temp)){
										 $count_alert++;
									 }
								 }
								 if($row['page_nav_name'] == 'R'){
									 $research = 1;
									 array_push($research_array,$row['id']);
									 $sql_temp = mysqli_query($connect,"SELECT * FROM temp_pages WHERE page_nav_name='R' "); 
									 if(mysqli_num_rows($sql_temp)){
										 $count_alert++;
									 }
								 }
								 if($row['page_nav_name'] == 'A'){
									 $research = 1;
									 array_push($research_array,$row['id']);
									 $sql_temp = mysqli_query($connect,"SELECT * FROM temp_pages WHERE page_nav_name='A' "); 
									 if(mysqli_num_rows($sql_temp)){
										 $count_alert++;
									 }
								 }
								 if($row['page_nav_name'] == 'Ad'){
									 $research = 1;
									 array_push($research_array,$row['id']);
									 $sql_temp = mysqli_query($connect,"SELECT * FROM temp_pages WHERE page_nav_name='Ad' "); 
									 if(mysqli_num_rows($sql_temp)){
										 $count_alert++;
									 }
								 } 
								 if($row['page_nav_name'] == 'D'){
									 $research = 1;
									 array_push($research_array,$row['id']);
									 $sql_temp = mysqli_query($connect,"SELECT * FROM temp_pages WHERE page_nav_name='D' "); 
									 if(mysqli_num_rows($sql_temp)){
										 $count_alert++;
									 }
								 }
								 if($row['page_nav_name'] == 'C'){
									 $research = 1;
									 array_push($research_array,$row['id']);
									 $sql_temp = mysqli_query($connect,"SELECT * FROM temp_pages WHERE page_nav_name='C' "); 
									 if(mysqli_num_rows($sql_temp)){
										 $count_alert++;
									 }
								 }
								
							}
													
						}
						if(in_array("governing_body",$people_array,true)){
							$sql_temp = mysqli_query($connect,"SELECT * FROM temp_tejas_users  WHERE designation  LIKE '%Governing Body%' OR highDesignation LIKE '%Governing Body%' "); 
							 if(mysqli_num_rows($sql_temp)){
								$count_alert++;
							 }
						  }
						  if(in_array("administration",$people_array,true)){
							$sql_temp = mysqli_query($connect,"SELECT * FROM temp_tejas_users  where (designation  LIKE '%administration%' OR highDesignation LIKE '%administration%') "); 
							 if(mysqli_num_rows($sql_temp)){
								$count_alert++;
							 }
						  } 
						  if(in_array("faculty",$people_array,true)){
							  
							$sql_temp = mysqli_query($connect,"SELECT * FROM temp_tejas_users  where role='faculty' OR highDesignation LIKE '%faculty%' OR designation LIKE '%faculty%' "); 
							 if(mysqli_num_rows($sql_temp)){
								$count_alert++;
							 }
						  }
						  if(in_array("staff",$people_array,true)){
							$sql_temp = mysqli_query($connect,"SELECT * FROM temp_tejas_users WHERE  (highDesignation  LIKE '%staff%' OR highDesignation LIKE '%Staff%')  AND id_user NOT IN('".$already_added_ids1."') "); 
							 if(mysqli_num_rows($sql_temp)){
								$count_alert++;
							 }
						  } 
						  if(in_array("research_people",$people_array,true)){
							$sql_temp = mysqli_query($connect,"SELECT * FROM temp_tejas_users  WHERE role='research' "); 
							 if(mysqli_num_rows($sql_temp)){
								$count_alert++;
							 }
						  }
						  if(in_array("placement_team",$people_array,true)){
							$sql_temp = mysqli_query($connect,"SELECT * FROM temp_tejas_users   where (role='placement' OR highDesignation LIKE '%placement%')  "); 
							 if(mysqli_num_rows($sql_temp)){
								$count_alert++;
							 }
						  }
						  
						  
						  
						}
						
						
					}
					/*for their profile approval */
					$myprofile =0;$edited_type ="";$temp_page_id_same_user ="";
					$latest_updates = mysqli_query($connect,"SELECT * FROM latest_updates where table_name='temp_tejas_users'"); 
					while($row11 = mysqli_fetch_array($latest_updates)){
						if( ($row11['edited_by'] == $row11['page_id']) && ($id_user=$row11['page_id']) && ($row11['edited_by'] == $id_user)  ){
							$temp_page_id_same_user = $row11['temp_page_id'];
							$myprofile =0;
							$edited_type = $row11['edited_type'];
							$myprofile_sql = mysqli_query($connect,"SELECT * FROM temp_tejas_users where id_user =".$temp_page_id_same_user); 
							if(mysqli_num_rows($myprofile_sql)){
								$myprofile++;
							}
							
						}
					}
					if($edited_type =="edited"){
						$myprofile_sql_edited_by_same_user = mysqli_query($connect,"SELECT * FROM temp_tejas_users where id_user =".$temp_page_id_same_user); 
							if(mysqli_num_rows($myprofile_sql_edited_by_same_user)){
								$count_alert++;
							}
					}
					/*for their profile approval end--*/
					if(!empty($other_array)){
						$count_alert++;
					}
					if($fgmembersite->UserRole()=="admin"){
						
						$sql_temp_pages = mysqli_query($connect,"SELECT * FROM temp_pages"); 
						if(mysqli_num_rows($sql_temp_pages)){
							$count_alert_admin++;
						}
						$sql_temp = mysqli_query($connect,"SELECT * FROM temp_tejas_users"); 
						 if(mysqli_num_rows($sql_temp)){
							 $count_alert_admin++;
						 }
						 $sql_temp = mysqli_query($connect,"SELECT * FROM temp_alumni_speak"); 
						 if(mysqli_num_rows($sql_temp)){
							 $count_alert_admin++;
						 }
						 $sql_temp = mysqli_query($connect,"SELECT * FROM temp_calendar"); 
						 if(mysqli_num_rows($sql_temp)){
							 $count_alert_admin++;
						 }
						 $sql_temp = mysqli_query($connect,"SELECT * FROM temp_home_ticker"); 
						 if(mysqli_num_rows($sql_temp)){
							 $count_alert_admin++;
						 }
						 $sql_temp = mysqli_query($connect,"SELECT * FROM temp_media"); 
						 if(mysqli_num_rows($sql_temp)){
							 $count_alert_admin++;
						 }
						 $sql_temp = mysqli_query($connect,"SELECT * FROM temp_recruting_companies"); 
						 if(mysqli_num_rows($sql_temp)){
							 $count_alert_admin++;
						 }
						 $sql_temp = mysqli_query($connect,"SELECT * FROM temp_testimonials"); 
						 if(mysqli_num_rows($sql_temp)){
							 $count_alert_admin++;
						 }
                          					 
						 
					}
				    $myprofile =0;
					$latest_updates = mysqli_query($connect,"SELECT * FROM latest_updates where table_name='temp_tejas_users'"); 
					while($row11 = mysqli_fetch_array($latest_updates)){
						if( ($row11['edited_by'] == $row11['page_id']) && ($id_user=$row11['page_id']) && ($row11['edited_by'] == $id_user)  ){
							$temp_page_id = $row11['temp_page_id'];
							$myprofile =0;
							$myprofile_sql = mysqli_query($connect,"SELECT * FROM temp_tejas_users where id_user =".$fgmembersite->idUser()); 
							//echo "SELECT * FROM temp_tejas_users where id_user =".$temp_page_id;
							if(mysqli_num_rows($myprofile_sql)){
								$myprofile++;
							}
							
						}
					}
					//echo "cal".$count_alert; echo "ca".$count_alert_admin; echo "mp".$myprofile; echo $editer_type_id;exit();
					if($editer_type_id == "approver" || $fgmembersite->UserRole()=="admin" || $editer_type_id == "editor"){
					if($count_alert || $count_alert_admin || $myprofile){ 
                       echo ' <li class="nav-item dropdown notification-list" style="display:block">
                            <a class="nav-link dropdown-toggle arrow-none waves-light waves-effect"  href="all-edited-pages.php" role="button"
                               aria-haspopup="false" aria-expanded="false">
                                <!--i class="zmdi zmdi-notifications-none noti-icon"></i-->';
								
                            if(trim(getting_notification_count())!="0")
						    {    
								echo '<span class="noti-icon-badge"></span>';
						    }
                            echo '</a>
                            
                        </li>';
					}
					}

                        echo '
                        <li class="nav-item dropdown notification-list" style="display:none">
                            <a class="nav-link dropdown-toggle arrow-none waves-light waves-effect" data-toggle="dropdown" href="#" role="button"
                               aria-haspopup="false" aria-expanded="false">
                                <!--i class="zmdi zmdi-notifications-none noti-icon"></i-->
                                <span class="noti-icon-badge"></span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right dropdown-arrow dropdown-lg" aria-labelledby="Preview">
                                <!-- item-->
                                <div class="dropdown-item noti-title">
                                    <h5><small><span class="label label-danger pull-xs-right">7</span>Notification</small></h5>
                                </div>

                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item notify-item">
                                    <div class="notify-icon bg-success"><i class="icon-bubble"></i></div>
                                    <p class="notify-details">Robert S. Taylor commented on Admin<small class="text-muted">1min ago</small></p>
                                </a>

                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item notify-item">
                                    <div class="notify-icon bg-info"><i class="icon-user"></i></div>
                                    <p class="notify-details">New user registered.<small class="text-muted">1min ago</small></p>
                                </a>

                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item notify-item">
                                    <div class="notify-icon bg-danger"><i class="icon-like"></i></div>
                                    <p class="notify-details">Carlos Crouch liked <b>Admin</b><small class="text-muted">1min ago</small></p>
                                </a>

                                <!-- All-->
                                <a href="javascript:void(0);" class="dropdown-item notify-item notify-all">
                                    View All
                                </a>

                            </div>
                        </li>


                        <li class="nav-item dropdown notification-list" style="display:none">
                            <a class="nav-link dropdown-toggle arrow-none waves-light waves-effect" data-toggle="dropdown" href="#" role="button"
                               aria-haspopup="false" aria-expanded="false">
                                <i class="zmdi zmdi-email noti-icon"></i>
                                <span class="noti-icon-badge"></span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right dropdown-arrow dropdown-arrow-success dropdown-lg" aria-labelledby="Preview">
                                <!-- item-->
                                <div class="dropdown-item noti-title bg-success">
                                    <h5><small><span class="label label-danger pull-xs-right">7</span>Messages</small></h5>
                                </div>

                                <!-- item-->
								<!--
                                <a href="javascript:void(0);" class="dropdown-item notify-item">
                                    <div class="notify-icon bg-faded">
                                        <img src="assets/images/users/avatar-2.jpg" alt="img" class="img-circle img-fluid">
                                    </div>
                                    <p class="notify-details">
                                        <b>Robert Taylor</b>
                                        <span>New tasks needs to be done</span>
                                        <small class="text-muted">1min ago</small>
                                    </p>
                                </a>
								-->
                                <!-- item-->
								<!--
                                <a href="javascript:void(0);" class="dropdown-item notify-item">
                                    <div class="notify-icon bg-faded">
                                        <img src="assets/images/users/avatar-3.jpg" alt="img" class="img-circle img-fluid">
                                    </div>
                                    <p class="notify-details">
                                        <b>Carlos Crouch</b>
                                        <span>New tasks needs to be done</span>
                                        <small class="text-muted">1min ago</small>
                                    </p>
                                </a>
								-->
                                <!-- item-->
								<!--
                                <a href="javascript:void(0);" class="dropdown-item notify-item">
                                    <div class="notify-icon bg-faded">
                                        <img src="assets/images/users/avatar-4.jpg" alt="img" class="img-circle img-fluid">
                                    </div>
                                    <p class="notify-details">
                                        <b>Robert Taylor</b>
                                        <span>New tasks needs to be done</span>
                                        <small class="text-muted">1min ago</small>
                                    </p>
                                </a>
								-->
                                <!-- All-->
                                <a href="javascript:void(0);" class="dropdown-item notify-item notify-all">
                                    View All
                                </a>

                            </div>
                        </li>

                        <li class="nav-item dropdown notification-list" style="display:none">
                            <a class="nav-link waves-effect waves-light right-bar-toggle" href="javascript:void(0);">
                                <i class="zmdi zmdi-format-subject noti-icon"></i>
                            </a>
                        </li>

                        <li class="nav-item dropdown notification-list">
                            <a class="nav-link dropdown-toggle arrow-none waves-effect waves-light nav-user" data-toggle="dropdown" href="#" role="button"
                               aria-haspopup="false" aria-expanded="false">
                                <img src="../main-control/user_images/'.$fgmembersite->ProfilePic().'" alt="user" class="img-circle">
                            </a>
                            <div class="dropdown-menu dropdown-menu-right dropdown-arrow profile-dropdown " aria-labelledby="Preview">
                                <!-- item-->
                                <div class="dropdown-item noti-title">
                                    <h5 class="text-overflow"><small>Welcome '.$fgmembersite->UserFullName().'!</small> </h5>
                                </div>

                                <!-- item-->
                                <a href="'; 
								if($highDesignation == 'research' || $role == 'research'){
									echo 'myprofile-research-people.php';
                                                                        $id_user1 = $fgmembersite->idUser();
                                                                        $id_user1 = get_temp_id_user_html($id_user1,$connect);
								}
								elseif($highDesignation == 'placement' || $role == 'placement'){
									echo 'page-edit-placement-people.php';
                                                                        $id_user1 = $fgmembersite->idUser();
								}
								elseif( $role == 'admin'){
									echo 'my-profile.php';
                                                                        $id_user1 = $fgmembersite->idUser();
								}
								else{
									echo 'myprofile-faculty.php';
                                                                        $id_user1 = $fgmembersite->idUser();
                                                                        $id_user1 = get_temp_id_user_html($id_user1,$connect);
                                                                        
								}
								
								echo '?id='.$id_user1.'&profile=myprofile" class="dropdown-item notify-item">
									<i class="zmdi zmdi-account"></i><span> My Profile </span> 
								</a>

                                <!-- item-->
                                <a href="logout.php" class="dropdown-item notify-item">
                                    <i class="zmdi zmdi-power"></i> <span>Logout</span>
                                </a>

                            </div>
                        </li>

                    </ul>

                </nav>

            </div>
            <!-- Top Bar End -->
	';
	 //if($approver==0){ echo "<script>$('.notification-list').css('display','none');</script>";  }
}



function get_temp_id_user_html($idparam,$connect){

  $result_data  = mysqli_query($connect," select * from tejas_users where id_user = ".$idparam);
  $row_cnt 	= $result_data->num_rows;
  
  if($row_cnt==0)
  {
    $ret_value = 0;
  }
  else
  {
      $row_data  = mysqli_fetch_assoc($result_data);
      $ret_value = $row_data["ref_id"];
  }
  
  return $ret_value;
  
} // function get_temp_id_user($id){

function admin_left_menu(){
	include("../includes/mysqli_connect.php");
	$current_url = $_SERVER['REQUEST_URI'];
	$dashboard_url = 'dashboard';
	$my_profile_url = 'my-profile';
	$fgmembersite = $GLOBALS['fgmembersite'];
	$id_user=$fgmembersite->idUser();
	$userEmail = $fgmembersite->UserEmail();
	$pages_url = 'page';
	$widgets_url='widget';
	$page_access_urls ="";
	$template_url='template';
	$highDesignation = $fgmembersite->highDesignation();
	$role = $fgmembersite->role();
	
        $finid = get_temp_id_user_html($id_user,$connect);
        
	echo '
	<!-- ========== Left Sidebar Start ========== -->
		<div class="left side-menu">
			<div class="sidebar-inner slimscrollleft">

				<!--- Sidemenu -->
				<div id="sidebar-menu">';
				if($fgmembersite->UserRole()=="admin"){
					echo '<ul>
						<li class="text-muted menu-title">Navigation</li>
						<li class="has_sub">
							<a href="'.$dashboard_url.'.php" class="waves-effect ';
							if(strpos($current_url, $dashboard_url) !== false){ echo 'active';}
							echo '"><i class="zmdi zmdi-view-dashboard"></i><span> Dashboard </span> </a>
						</li>
						
						<!--li class="has_sub">
							<a href="my-profile.php" class="waves-effect ';
							if(strpos($current_url, $my_profile_url) !== false){ echo 'active';}
							echo '"><i class="zmdi zmdi-account"></i><span> My Profile </span> </a>
						</li-->
						
						<li class="has_sub">
							<a href="javascript:void(0);" class="waves-effect ';
							if((strpos($current_url, $pages_url) !== false) && (strpos($current_url, $widgets_url) == false)){ echo 'active';}
							echo '"><i class="zmdi zmdi-collection-text"></i> <span> Pages </span> <span class="menu-arrow"></span></a>
							<ul class="list-unstyled">
								<li id="page-create"'; if((strpos($current_url, "page-create.php") !== false) ){ echo 'active';}
								echo '><a href="page-create.php">Create New Page</a></li>
								<li id="page-existing"'; if((strpos($current_url, "page-existing.php") !== false) ){ echo 'active';}
								echo '><a href="page-existing.php">Existing Pages</a></li>
							</ul>
						</li>
						
						<li class="has_sub">
							<a href="javascript:void(0);" class="waves-effect ';
							if((strpos($current_url, $template_url) !== false) && (strpos($current_url, $widgets_url) == false)){ echo 'active';}
							echo '"><i class="zmdi zmdi-collection-text"></i> <span> Widget </span> <span class="menu-arrow"></span></a>
							<ul class="list-unstyled">
								<!--li id="page-create"><a href="template-create.php">Create New Widget</a></li-->
								<li id="page-existing"><a href="template-existing.php">View Widgets</a></li>
							</ul>
						</li>
						
						
						';
							
					echo '</ul>';
					
				}else{	
										
					echo '<ul>
						<li class="text-muted menu-title">Navigation</li>
						<li class="has_sub">
							<a href="'.$dashboard_url.'.php" class="waves-effect ';
							if(strpos($current_url, $dashboard_url) !== false){ echo 'active';}
							echo '"><i class="zmdi zmdi-view-dashboard"></i><span> Dashboard </span> </a>
						</li>
						
						<li class="has_sub">
							<a href="my-profile.php" class="waves-effect ';
							if(strpos($current_url, $my_profile_url) !== false){ echo 'active';}
							echo '"><i class="zmdi zmdi-account"></i><span> My Profile </span> </a>
						</li>
						
						<li class="has_sub">
							<a href="javascript:void(0);" class="waves-effect ';
							if((strpos($current_url, $pages_url) !== false) && (strpos($current_url, $widgets_url) == false)){ echo 'active';}
							echo '"><i class="zmdi zmdi-collection-text"></i> <span> Pages </span> <span class="menu-arrow"></span></a>
							<ul class="list-unstyled">
								<li id="page-create"><a href="page-create.php">Create New Page</a></li>
								<li id="page-existing"><a href="page-existing.php">Existing Pages</a></li>
							</ul>
						</li>			
						
						
						';
						
							
					echo '</ul>';
				}
					
					
					echo '<div class="clearfix"></div>
				</div>
				<!-- Sidebar -->
				<div class="clearfix"></div>

			</div>

		</div>
		<!-- Left Sidebar End -->
	';
}

function admin_image_cropper(){
	echo '<link rel="stylesheet" type="text/css" href="simple_cropper/css/style.css" />
		<link rel="stylesheet" type="text/css" href="simple_cropper/css/style-example.css" />
		<link rel="stylesheet" type="text/css" href="simple_cropper/css/jquery.Jcrop.css" />

		<!-- Js files-->
		<script type="text/javascript" src="simple_cropper/scripts/jquery-1.10.2.min.js"></script>
		<script type="text/javascript" src="simple_cropper/scripts/jquery.Jcrop.js"></script>
		<script type="text/javascript" src="simple_cropper/scripts/jquery.SimpleCropper.js"></script>
		
		<script>
		
		 $(".cropme").simpleCropper();
		</script>
		
		';
		
	
}
function admin_right_bar(){
	echo '
	<!-- Right Sidebar -->
		<div class="side-bar right-bar">
			<div class="nicescroll">
				<ul class="nav nav-tabs text-xs-center">
					<li class="nav-item">
						<a href="#home-2"  class="nav-link active" data-toggle="tab" aria-expanded="false">
							Activity
						</a>
					</li>
					<li class="nav-item">
						<a href="#messages-2" class="nav-link" data-toggle="tab" aria-expanded="true">
							Settings
						</a>
					</li>
				</ul>

				<div class="tab-content">
					<div class="tab-pane fade in active" id="home-2">
						<div class="timeline-2">
							<div class="time-item">
								<div class="item-info">
									<small class="text-muted">5 minutes ago</small>
									<p><strong><a href="#" class="text-info">John Doe</a></strong> Uploaded a photo <strong>"DSC000586.jpg"</strong></p>
								</div>
							</div>

							<div class="time-item">
								<div class="item-info">
									<small class="text-muted">30 minutes ago</small>
									<p><a href="#" class="text-info">Lorem</a> commented your post.</p>
									<p><em>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam laoreet tellus ut tincidunt euismod. "</em></p>
								</div>
							</div>

							<div class="time-item">
								<div class="item-info">
									<small class="text-muted">59 minutes ago</small>
									<p><a href="#" class="text-info">Jessi</a> attended a meeting with<a href="#" class="text-success">John Doe</a>.</p>
									<p><em>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam laoreet tellus ut tincidunt euismod. "</em></p>
								</div>
							</div>

							<div class="time-item">
								<div class="item-info">
									<small class="text-muted">1 hour ago</small>
									<p><strong><a href="#" class="text-info">John Doe</a></strong>Uploaded 2 new photos</p>
								</div>
							</div>

							<div class="time-item">
								<div class="item-info">
									<small class="text-muted">3 hours ago</small>
									<p><a href="#" class="text-info">Lorem</a> commented your post.</p>
									<p><em>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam laoreet tellus ut tincidunt euismod. "</em></p>
								</div>
							</div>

							<div class="time-item">
								<div class="item-info">
									<small class="text-muted">5 hours ago</small>
									<p><a href="#" class="text-info">Jessi</a> attended a meeting with<a href="#" class="text-success">John Doe</a>.</p>
									<p><em>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam laoreet tellus ut tincidunt euismod. "</em></p>
								</div>
							</div>
						</div>
					</div>

					<div class="tab-pane fade" id="messages-2">

						<div class="row m-t-20">
							<div class="col-xs-8">
								<h5 class="m-0">Notifications</h5>
								<p class="text-muted m-b-0"><small>Do you need them?</small></p>
							</div>
							<div class="col-xs-4 text-right">
								<input type="checkbox" checked data-plugin="switchery" data-color="#64b0f2" data-size="small"/>
							</div>
						</div>

						<div class="row m-t-20">
							<div class="col-xs-8">
								<h5 class="m-0">API Access</h5>
								<p class="m-b-0 text-muted"><small>Enable/Disable access</small></p>
							</div>
							<div class="col-xs-4 text-right">
								<input type="checkbox" checked data-plugin="switchery" data-color="#64b0f2" data-size="small"/>
							</div>
						</div>

						<div class="row m-t-20">
							<div class="col-xs-8">
								<h5 class="m-0">Auto Updates</h5>
								<p class="m-b-0 text-muted"><small>Keep up to date</small></p>
							</div>
							<div class="col-xs-4 text-right">
								<input type="checkbox" checked data-plugin="switchery" data-color="#64b0f2" data-size="small"/>
							</div>
						</div>

						<div class="row m-t-20">
							<div class="col-xs-8">
								<h5 class="m-0">Online Status</h5>
								<p class="m-b-0 text-muted"><small>Show your status to all</small></p>
							</div>
							<div class="col-xs-4 text-right">
								<input type="checkbox" checked data-plugin="switchery" data-color="#64b0f2" data-size="small"/>
							</div>
						</div>

					</div>
				</div>
			</div> <!-- end nicescroll -->
		</div>
		<!-- /Right-bar -->
	';
}

function admin_footer(){
	echo '<footer class="footer text-right">';
                echo date('Y'); echo ' &copy; '; $site_name = $GLOBALS['site_name']; echo $site_name.', All Rights Reserved';
				
            echo '</footer>

        </div>
        <!-- END wrapper -->


        <script>
            var resizefunc = [];

        </script>

        <!-- jQuery  -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/tether.min.js"></script><!-- Tether for Bootstrap -->
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/detect.js"></script>
        <script src="assets/js/fastclick.js"></script>
        <script src="assets/js/jquery.blockUI.js"></script>
        <script src="assets/js/waves.js"></script>
        <script src="assets/js/jquery.nicescroll.js"></script>
        <script src="assets/js/jquery.scrollTo.min.js"></script>
        <script src="assets/js/jquery.slimscroll.js"></script>
        <script src="assets/plugins/switchery/switchery.min.js"></script>
        <!-- ckeditor js -->
        <script type="text/javascript" src="assets/plugins/ckeditor/ckeditor.js"></script>
        <!--Morris Chart-->
		<script src="assets/plugins/morris/morris.min.js"></script>
		<script src="assets/plugins/raphael/raphael-min.js"></script>

        <!-- Counter Up  -->
        <script src="assets/plugins/waypoints/lib/jquery.waypoints.js"></script>
        <script src="assets/plugins/counterup/jquery.counterup.min.js"></script>

        <!-- App js -->
        <script src="assets/js/jquery.core.js"></script>
        <script src="assets/js/jquery.app.js"></script>

        <!-- Page specific js -->
        <script src="assets/pages/jquery.dashboard.js"></script>
		
		<!--Form Wizard-->
        <script src="assets/plugins/jquery.steps/build/jquery.steps.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>

        <!--wizard initialization-->
        <script src="assets/pages/jquery.wizard-init.js" type="text/javascript"></script>
		
		<!-- Modal-Effect -->
        <script src="assets/plugins/custombox/js/custombox.min.js"></script>
        <script src="assets/plugins/custombox/js/legacy.min.js"></script>
    </body>
</html>
	';
}

function admin_footer_dashboard(){
	echo '<footer class="footer text-right">';
                echo date('Y'); echo ' &copy; '; $site_name = $GLOBALS['site_name']; echo $site_name.', All Rights Reserved';
				
            echo '</footer>

        </div>
        <!-- END wrapper -->


        <script>
            var resizefunc = [];

        </script>

        <!-- jQuery  -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/tether.min.js"></script><!-- Tether for Bootstrap -->
        <script src="assets/js/bootstrap.min.js"></script>
        
        <script src="assets/js/jquery.nicescroll.js"></script>
        
        <script src="assets/plugins/switchery/switchery.min.js"></script>
        
        <!--Morris Chart-->
		<script src="assets/plugins/morris/morris.min.js"></script>
		<script src="assets/plugins/raphael/raphael-min.js"></script>

        

        <!-- App js -->
        <script src="assets/js/jquery.core.js"></script>
        <script src="assets/js/jquery.app.js"></script>

        <!-- Page specific js -->
        <script src="assets/pages/jquery.dashboard.js"></script>
		
		
		
    </body>
</html>
	';
}


function wayTop(){
	echo '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8">
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
        <meta name="format-detection" content="telephone=no" />
        <!-- Favicon -->
        <link rel="shortcut icon" href="images/favicon.ico">';
}

function basepath_getting()
{
  return $_SERVER["DOCUMENT_ROOT"];
}


function transparent_cropper()
{
	echo "
<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/cropper/2.3.4/cropper.min.css'>
	<script src='https://cdnjs.cloudflare.com/ajax/libs/cropperjs/0.8.1/cropper.min.js'></script>
";
}

function sess_datas()
{
    $connect = $GLOBALS['connect'];
    $fgmembersite = $GLOBALS['fgmembersite'];
    $site_name = $GLOBALS['site_name'];
    $favicon = $GLOBALS['favicon'];
    
        if(!$fgmembersite->CheckLogin())
	{
		$fgmembersite->RedirectToURL("shuru.php");
		exit;
	}
        
        return $_SESSION;
        
}

function convertAndSaveImages($fileData,$heading,$exist_img=''){
	$allFileNames = "";
	if($fileData){
			if(strpos($fileData, ';base64') !== false){
				$data = $fileData;
				if($data){
					list($type, $data) = explode(';', $data);
					list(, $data)      = explode(',', $data);
					$data = base64_decode($data);
					$heading = preg_replace('/[^A-Za-z0-9\-]/', '', $heading);
					$rand = rand();
					$allFileNames .= $file_name = $heading.'_'.$rand.'.jpg';
					file_put_contents('../images/banner-images/'.$file_name, $data);
					if($exist_img!='')
					{
						$ilink='../images/banner-images/'.$exist_img;
						unlink($ilink);
					}
						
				}
				
			}else{
				$allFileNames .= $fileData;
				}
			
		}
	return rtrim($allFileNames,",");
}

 
?>