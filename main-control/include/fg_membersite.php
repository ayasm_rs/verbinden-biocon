<?PHP
//require_once("class.phpmailer.php");
include_once(dirname(__FILE__)."/../mailscript/PHPMailer/PHPMailerAutoload.php");
require_once("formvalidator.php");
date_default_timezone_set('Asia/Kolkata');
class FGMembersite
{
    var $admin_email;
    var $from_address;
    
    var $username;
    var $pwd;
    var $database;
    var $tablename;
    var $connection;
    var $rand_key;
    
    var $error_message;
    
    //-----Initialization -------
    function FGMembersites()
    {
        $this->sitename = 'http://webdevelopment.southcentralus.cloudapp.azure.com/biocon/';
        $this->rand_key = '0iQx5oBk66oVZep';
    }
    
    function InitDB($host,$uname,$pwd,$database,$tablename)
    {
        $this->db_host  = $host;
        $this->username = $uname;
        $this->pwd  = $pwd;
        $this->database  = $database;
        $this->tablename = $tablename;
		
		
        
    }
    function SetAdminEmail($email)
    {
        $this->admin_email = $email;
    }
    
    function SetWebsiteName($sitename)
    {
        $this->sitename = $sitename;
    }
    
    function SetRandomKey($key)
    {
        $this->rand_key = $key;
    }
    
    //-------Main Operations ----------------------
    function RegisterUser()
    {
        if(!isset($_POST['submitted']))
        {
           return false;
        }
        
        $formvars = array();
        
        if(!$this->ValidateRegistrationSubmission())
        {
            return false;
        }
        
        $this->CollectRegistrationSubmission($formvars);
        
        if(!$this->SaveToDatabase($formvars))
        {
            return false;
        }
        
        if(!$this->SendUserConfirmationEmail($formvars))
        {
            return false;
        }

        $this->SendAdminIntimationEmail($formvars);
        
        return true;
    }

    function ConfirmUser()
    {
        if(empty($_GET['code'])||strlen($_GET['code'])<=10)
        {
            $this->HandleError("Please provide the confirm code");
            return false;
        }
        $user_rec = array();
        if(!$this->UpdateDBRecForConfirmation($user_rec))
        {
            return false;
        }
        
        $this->SendUserWelcomeEmail($user_rec);
        
        $this->SendAdminIntimationOnRegComplete($user_rec);
        
        return true;
    }    
    
    function Login()
    {
		if(!$this->DBLogin())
        {
            $this->HandleError("Database login failed!");
            return false;
        }
        if(empty($_POST['username']))
        {
            $this->HandleError("UserName is empty!");
            return false;
        }
        
        if(empty($_POST['password']))
        {
            $this->HandleError("Password is empty!");
            return false;
        }
        
        $username = trim($_POST['username']);
        $password = trim($_POST['password']);
		
		
        if(!isset($_SESSION)){ session_start(); }
        if(!$this->CheckLoginInDB($username,$password))
        {
            return false;
        }
		////////////////// otp security removed  ///////////
		
        $user_name = $_SESSION['username_of_user'];
        $_SESSION[$this->GetLoginSessionVar()] = $user_name; 
		
		 $otp_qry = "Select * from $this->tablename where username='$username' ";
        $otp_result = mysqli_query($this->connection,$otp_qry);
		$otp_row = mysqli_fetch_assoc($otp_result);
            $_SESSION['name_of_user']  = $otp_row['name'];
            $_SESSION['email_of_user'] = $otp_row['email'];
            $_SESSION['phone_number'] = $otp_row['phone_number'];
            $_SESSION['username_of_user'] = $otp_row['username'];
            $_SESSION['password_of_user'] = $otp_row['password'];
            $_SESSION['user_role'] = 'admin';
            $_SESSION['profile_pic'] = $otp_row['profile_pic'];
            $_SESSION['id_user'] = $otp_row['id_user']; 
            $_SESSION['highDesignation'] = $otp_row['highDesignation']; 
            $_SESSION['role'] = $otp_row['role']; 
			
		////////////////// otp security removed  ///////////
        //echo "<pre>";print_r($_SESSION);exit();
        //$_SESSION[$this->GetLoginSessionVar()] = $username;
        
        return true;
    }

    function Otp_Login()
    {
        if(empty($_POST['otp']))
        {
            $this->HandleError("otp is empty!");
            return false;
        }
        
        $otp = trim($_POST['otp']);
        
        if(!isset($_SESSION)){ session_start(); }
        $user_name = $_SESSION['username_of_user'];
       
        
        $_SESSION[$this->GetLoginSessionVar()] = $user_name;
        
        return true;
    }
    
    function CheckLogin()
    {
         if(!isset($_SESSION)){ session_start(); }

         $sessionvar = $this->GetLoginSessionVar();
         
         if(empty($_SESSION[$sessionvar]))
         {
            return false;
         }
         return true;
    }

    function CheckotpInDB($otp,$username){
        if(!$this->DBLogin())
        {
            $this->HandleError("Database login failed!");
            return false;
        } 
        $otp = $this->SanitizeForSQL($otp);
        $otp_qry = "Select * from $this->tablename where username='$username' ";
        $otp_result = mysqli_query($this->connection,$otp_qry);
        if(!$otp_result || mysqli_num_rows($otp_result) <= 0)
        {
            $this->HandleError("Error logging in. Your session is expired please login again");
            return false;
        }
        $otp_row = mysqli_fetch_assoc($otp_result);
        $present_time = strtotime(date("Y-m-d H:i:s"));
        $otp_time = strtotime($otp_row['otp_validity']);
        $validity_check = round(abs($present_time - $otp_time) / 60,2);
        if ($validity_check >= 15) {
            $this->HandleError("OTP expired,please login again.");
            session_destroy();
            return false;
        }
        $otp_check = password_verify($otp,$otp_row['one_time_password']);
        if (!$otp_check) {
            $otp_failed_attempts = $otp_row['otp_attempts'] + 1;
            $time_of_use = date("Y-m-d H:i:s");
            $upd_qry = "Update $this->tablename SET otp_attempts = '$otp_failed_attempts', otp_validity = '$time_of_use' where username='$username' ";
            mysqli_query($this->connection,$upd_qry);
            $failed_attempts_qry = "Select otp_attempts,name,email from $this->tablename where username='$username' ";
            $total_failed_otp = mysqli_query($this->connection,$failed_attempts_qry);
            $total_otp_attempts = mysqli_fetch_assoc($total_failed_otp);
            $name = $total_otp_attempts['name'];
            $email = $total_otp_attempts['email'];
            $this->HandleError("Error logging in. OTP does not match.");
            if ($total_otp_attempts['otp_attempts'] >= 3) {
                $admin_email_qry = "Select email from $this->tablename where id_user= 1 ";
                $email_result = mysqli_query($this->connection,$admin_email_qry);
                $admin_email = mysqli_fetch_assoc($email_result);
                $this->otp_failed_mail($admin_email['email'],$name,$email);
                session_destroy();
                $this->RedirectToURL("login.php");
            }
            return false;
        }else{
            $upd_qry = "Update $this->tablename SET otp_attempts = 0, otp_validity = '',email_attempts = 0 where username='$username' ";
            mysqli_query($this->connection,$upd_qry);
            $_SESSION['name_of_user']  = $otp_row['name'];
            $_SESSION['email_of_user'] = $otp_row['email'];
            $_SESSION['phone_number'] = $otp_row['phone_number'];
            $_SESSION['username_of_user'] = $otp_row['username'];
            $_SESSION['password_of_user'] = $otp_row['password'];
            $_SESSION['user_role'] = 'admin';
            $_SESSION['profile_pic'] = $otp_row['profile_pic'];
            $_SESSION['id_user'] = $otp_row['id_user']; 
            $_SESSION['highDesignation'] = $otp_row['highDesignation']; 
            $_SESSION['role'] = $otp_row['role']; 
            return true; 
        }
            
    }

    function verification_session()
    {
        //error_reporting(E_ALL);
         if(!isset($_SESSION)){ session_start(); }

         $sessionvar = $_SESSION['username_of_user'];
         //echo "<pre>";print_r($_SESSION);exit();
         if(empty($sessionvar))
         {
            return false;
         }
         return true;
    }
    
    function UserFullName()
    {
        return isset($_SESSION['name_of_user'])?$_SESSION['name_of_user']:'';
    }
    
    function UserEmail()
    {
        return isset($_SESSION['email_of_user'])?$_SESSION['email_of_user']:'';
    }
	
	function PhoneNumber()
    {
        return isset($_SESSION['phone_number'])?$_SESSION['phone_number']:'';
    }
	
	function UserName()
    {
        return isset($_SESSION['username_of_user'])?$_SESSION['username_of_user']:'';
    }
	
	function UserPassword()
    {
        return isset($_SESSION['password_of_user'])?$_SESSION['password_of_user']:'';
    }
	
	function UserRole()
    {
        return isset($_SESSION['user_role'])?$_SESSION['user_role']:'';
    }
	
	function ProfilePic()
    {
        return isset($_SESSION['profile_pic'])?$_SESSION['profile_pic']:'';
    }
    
	function idUser()
    {
        return isset($_SESSION['id_user'])?$_SESSION['id_user']:'';
    }
	
	function highDesignation()
    {
        return isset($_SESSION['highDesignation'])?$_SESSION['highDesignation']:'';
    }
	
	function role()
    {
        return isset($_SESSION['role'])?$_SESSION['role']:'';
    }
	
    function LogOut()
    {
        session_start();
        
        $sessionvar = $this->GetLoginSessionVar();
        
        $_SESSION[$sessionvar]=NULL;
        
        unset($_SESSION[$sessionvar]);
        session_destroy();
    }
    
    function EmailResetPasswordLink()
    {
        //echo "<pre>";print_r($_POST);exit();
        if(empty($_POST['email']))
        {
            $this->HandleError("Email is empty!");
            return false;
        }
        $user_rec = array();
        if(false === $this->GetUserFromEmail($_POST['email'], $user_rec))
        {
            return false;
        }
        if(false === $this->SendResetPasswordLink($user_rec))
        {
            return false;
        }
        return true;
    }
    
    function ResetPassword()
    {
        if(empty($_GET['email']))
        {
            $this->HandleError("Email is empty!");
            return false;
        }
        if(empty($_GET['code']))
        {
            $this->HandleError("reset code is empty!");
            return false;
        }
        $email = trim($_GET['email']);
        $code = trim($_GET['code']);
        
        if($this->GetResetPasswordCode($email) != $code)
        {
            $this->HandleError("Bad reset code!");
            return false;
        }
        
        $user_rec = array();
        if(!$this->GetUserFromEmail($email,$user_rec))
        {
            return false;
        }
        
        $new_password = $this->ResetUserPasswordInDB($user_rec);
        if(false === $new_password || empty($new_password))
        {
            $this->HandleError("Error updating new password");
            return false;
        }
        
        if(false == $this->SendNewPassword($user_rec,$new_password))
        {
            $this->HandleError("Error sending new password");
            return false;
        }
        return true;
    }
    
    function ChangePassword()
    {
        if(!$this->CheckLogin())
        {
            $this->HandleError("Not logged in!");
            return false;
        }
        
        if(empty($_POST['oldpwd']))
        {
            $this->HandleError("Old password is empty!");
            return false;
        }
        if(empty($_POST['newpwd']))
        {
            $this->HandleError("New password is empty!");
            return false;
        }
        
        $user_rec = array();
        if(!$this->GetUserFromEmail($this->UserEmail(),$user_rec))
        {
            return false;
        }
        
        $pwd = trim($_POST['oldpwd']);
        
        if($user_rec['password'] != md5($pwd))
        {
            $this->HandleError("The old password does not match!");
            return false;
        }
        $newpwd = trim($_POST['newpwd']);
        
        if(!$this->ChangePasswordInDB($user_rec, $newpwd))
        {
            return false;
        }
        return true;
    }
    
    //-------Public Helper functions -------------
    function GetSelfScript()
    {
        return htmlentities($_SERVER['PHP_SELF']);
    }    
    
    function SafeDisplay($value_name)
    {
        if(empty($_POST[$value_name]))
        {
            return'';
        }
        return htmlentities($_POST[$value_name]);
    }
    
    function RedirectToURL($url)
    {
        header("Location: $url");
        exit;
    }
    
    function GetSpamTrapInputName()
    {
        return 'sp'.md5('KHGdnbvsgst'.$this->rand_key);
    }
    
    function GetErrorMessage()
    {
        if(empty($this->error_message))
        {
            return '';
        }
        $errormsg = nl2br(htmlentities($this->error_message));
        return $errormsg;
    }    
    //-------Private Helper functions-----------
    
    function HandleError($err)
    {
        $this->error_message .= $err."\r\n";
    }
    
    function HandleDBError($err)
    {
        $this->HandleError($err."\r\n mysqlerror:".mysqli_error());
    }
    
    function GetFromAddress()
    {
        if(!empty($this->from_address))
        {
            return $this->from_address;
        }

        $host = $_SERVER['SERVER_NAME'];

        $from ="nobody@$host";
        return $from;
    } 
    
    function GetLoginSessionVar()
    {
        $retvar = md5($this->rand_key);
        $retvar = 'usr_'.substr($retvar,0,10);
        return $retvar;
    }
    
    function CheckLoginInDB($username,$password)
    {
        if(!$this->DBLogin())
        {
            $this->HandleError("Database login failed!");
            return false;
        } 
        $username = $this->SanitizeForSQL($username);
        $qry = "Select * from $this->tablename where username='$username' ";
        $result = mysqli_query($this->connection,$qry);
        if(!$result || mysqli_num_rows($result) <= 0)
        {
            $this->HandleError("Error logging in. The username or password does not match");
            return false;
        }
        $row = mysqli_fetch_assoc($result);
        $checking = password_verify($password,$row['password']);
        if ($checking) {
            $_SESSION['username_of_user'] = $row['username']; 
            $randon_otp = $this->create_otp();
            $otp_validity = date("Y-m-d H:i:s");
            $hashed_otp = password_hash($randon_otp, PASSWORD_DEFAULT);
            $qry = "Update $this->tablename SET one_time_password = '$hashed_otp',otp_validity = '$otp_validity' where username='$username' ";
                mysqli_query($this->connection,$qry);
            //$this->send_otp($row['email'],$randon_otp);
            if ($row['failed_attempts'] > 0) {
            	$qry = "Update $this->tablename SET failed_attempts = 0,blocked_time = '' where username='$username' ";
        		mysqli_query($this->connection,$qry);
            }
            return true;
        }else{
            $failed_attempts = $row['failed_attempts'] + 1;
            $timestamp = date("Y-m-d H:i:s");
            $upd_qry = "Update $this->tablename SET failed_attempts = '$failed_attempts', blocked_time = '$timestamp' where username='$username' ";
            mysqli_query($this->connection,$upd_qry);
            $failed_attempts_qry = "Select failed_attempts,name,email from $this->tablename where username='$username' ";
            $total_failed_result = mysqli_query($this->connection,$failed_attempts_qry);
            $total_failed_attempts = mysqli_fetch_assoc($total_failed_result);
            $name = $total_failed_attempts['name'];
            $email = $total_failed_attempts['email'];
        	if ($total_failed_attempts['failed_attempts'] >= 3) {
                $admin_email_qry = "Select email from $this->tablename where id_user= 1 ";
                $email_result = mysqli_query($this->connection,$admin_email_qry);
                $admin_email = mysqli_fetch_assoc($email_result);
                $this->portal_blocking_mail($admin_email['email'],$name,$email);
        		$this->block_login_page();
        		return false;
        	}
            $this->HandleError("Error logging in. The username or password does not match");
            return false;
        }       
    }

    function block_login_page(){
    	if(!$this->DBLogin())
        {
            $this->HandleError("Database login failed!");
            return false;
        }
    	$qry = "Select id_user,failed_attempts from $this->tablename";
        $result = mysqli_query($this->connection,$qry);
        $result_data = array();
	    while($row = mysqli_fetch_assoc($result))
	    {
	        $result_data[] = $row;
	    }
        foreach ($result_data as $key => $resultvalue) {
	        if ($resultvalue['failed_attempts'] >= 3) {
	        	$current_time = date("Y-m-d H:i:s");
	        	$id = $resultvalue['id_user'];
	        	$user_qry = "Select id_user,failed_attempts,blocked_time from $this->tablename where id_user= '$id' ";
        		$user_result = mysqli_query($this->connection,$user_qry);
                $hrscheck = mysqli_fetch_assoc($user_result);
                $date1 = new DateTime(date("Y-m-d H:i:s"));
                $date2 = new DateTime($hrscheck['blocked_time']);
                $diff = $date2->diff($date1);
                $hours = $diff->h;
                $hours = $hours + ($diff->days*24);
                $timer = $hours;
                //echo round((strtotime($hrscheck['blocked_time']) - strtotime(date("Y-m-d H:i:s")))/(60*60));
	        	if ($timer < 24) {
                    echo "<h2>Access Denied..We Suspect Unusual login activity.<br/> please contact Administrator..</h2>";
                    exit();
	        	}else{
                    $this->update_blocking();
                }
	        }
        }
    }

    function update_blocking(){
        if(!$this->DBLogin())
        {
            $this->HandleError("Database login failed!");
            return false;
        }  
        $update_qry = "Select id_user,failed_attempts from $this->tablename where failed_attempts >= 2";
        $result_qry = mysqli_query($this->connection,$update_qry);
        $no_of_rows = mysqli_num_rows($result_qry);
        $blocked_data = mysqli_fetch_assoc($result_qry);
        $blocked_id = $blocked_data['id_user'];
        if ($no_of_rows > 0) {
            $qry = "Update $this->tablename SET failed_attempts = 0,blocked_time = '' where id_user='$blocked_id' ";
            mysqli_query($this->connection,$qry);
            return true;
        }

    }
    
    function UpdateDBRecForConfirmation(&$user_rec)
    {
        if(!$this->DBLogin())
        {
            $this->HandleError("Database login failed!");
            return false;
        }   
        $confirmcode = $this->SanitizeForSQL($_GET['code']);
        
        $result = mysqli_query($this->connection,"Select name, email from $this->tablename where confirmcode='$confirmcode'");   
        if(!$result || mysqli_num_rows($result) <= 0)
        {
            $this->HandleError("Wrong confirm code.");
            return false;
        }
        $row = mysqli_fetch_assoc($result);
        $user_rec['name'] = $row['name'];
        $user_rec['email']= $row['email'];
        
        $qry = "Update $this->tablename Set confirmcode='y' Where  confirmcode='$confirmcode'";
        
        if(!mysqli_query( $this->connection,$qry))
        {
            $this->HandleDBError("Error inserting data to the table\nquery:$qry");
            return false;
        }      
        return true;
    }
    
    function ResetUserPasswordInDB($user_rec)
    {
        $new_password = $this->create_password();
        $user_password = $new_password;
        $password = password_hash($user_password, PASSWORD_DEFAULT, ['cost' => 11]);
        
        if(false == $this->ChangePasswordInDB($user_rec,$password))
        {
            return false;
        }
        return $new_password;
    }
    
    function ChangePasswordInDB($user_rec, $newpwd)
    {
        $newpwd = $this->SanitizeForSQL($newpwd);
        
        $qry = "Update $this->tablename Set password='".$newpwd."' Where  id_user=".$user_rec['id_user']."";
        
        if(!mysqli_query( $this->connection, $qry))
        {
            $this->HandleDBError("Error updating the password \nquery");
            return false;
        }     
        return true;
    }
    
    function GetUserFromEmail($email,&$user_rec)
    {
        if(!$this->DBLogin())
        {
            $this->HandleError("Database login failed!");
            return false;
        }   
        $email = $this->SanitizeForSQL($email);
        
        $result = mysqli_query($this->connection,"Select * from $this->tablename where email='$email'");  

        if(!$result || mysqli_num_rows($result) <= 0)
        {
            $this->HandleError("There is no user with email: $email");
            return false;
        }
        $user_rec = mysqli_fetch_assoc($result);
        return true;
    }
    
    function SendUserWelcomeEmail(&$user_rec)
    {
       
        
		$exp = explode("@","shanmuga.p@iverbinden.com");
		$fromName=$exp[0];
		$from="shanmuga.p@iverbinden.com";
		$mail = new PHPMailer;
		$mail->isSMTP(); 
		$mail->Host = 'smtp.gmail.com';    
		$mail->SMTPAuth = true;
		$mail->Username = 'shanmugi.designer@gmail.com';
		$mail->Password = '@Shanmu@28';
		$mail->SMTPSecure = 'tls';

		$mail->From = $from;
		$mail->FromName = $fromName;
		$mail->addAddress('shanmuga.p@iverbinden.com', 'Receiver');
		$mail->AddReplyTo($from, $fromName);
		$mail->SetFrom($from, $fromName);
		$mail->isHTML(true);

		$mail->Subject = "Welcome to ".$this->sitename;
		$mail->Body    = "Hello ".$user_rec['name']."\r\n\r\n".
			"Welcome! Your registration  with ".$this->sitename." is completed.\r\n".
			"\r\n".
			"Regards,\r\n".
			"Webmaster\r\n".
			$this->sitename;
		if(!$mail->send()) {
			return false;
		 }  return true;
        
    }
    
    function SendAdminIntimationOnRegComplete(&$user_rec)
    {
        if(empty($this->admin_email))
        {
            return false;
        }
        $mailer = new PHPMailer();
        
        $mailer->CharSet = 'utf-8';
        
        $mailer->AddAddress($this->admin_email);
        
        $mailer->Subject = "Registration Completed: ".$user_rec['name'];

        $mailer->From = $this->GetFromAddress();         
        
        $mailer->Body ="A new user registered at ".$this->sitename."\r\n".
        "Name: ".$user_rec['name']."\r\n".
        "Email address: ".$user_rec['email']."\r\n";
        
        if(!$mailer->Send())
        {
            return false;
        }
        return true;
    }
    
    function GetResetPasswordCode($email)
    {
       return substr(md5($email.$this->sitename.$this->rand_key),0,10);
    }
    
    function SendResetPasswordLink($user_rec)
    {
        //echo "<pre>";print_r($user_rec);exit();
          $mail = $this->smtp_details(); 
          $to = $user_rec['email'];
          $email=$user_rec['email'];
          $mail->setFrom('info@tejasnetworks.com', 'Tejas Networkes');
          $mail->addAddress($to,$to);
          $site_url="http://www.tejasnetworks.com/";
          $mail->isHTML(true);
          $link = $this->GetAbsoluteURLFolder().
                '/resetpwd.php?email='.
                urlencode($email).'&code='.
                urlencode($this->GetResetPasswordCode($email));
          $messageparam = '<table border="0" cellpadding="0" cellspacing="0"> 
        <tr> 
            <td colspan="3" height="20"></td> 
        </tr> 
        <tr> 
            <td width="20"></td> 
            <td align="left" style="font-size:16px; color:#000001; font-family:serif;"> 
            Hello '.$user_rec['name'].' <br> There was a request to reset your password at '.$this->Getbase_name().' <br>Please click the link below to complete the request:<br> '.$link.'<br><br> Regards<br> support team<br>'.$this->Getbase_name().'  .
            </td> 
            <td width="20"></td> 
        </tr> 
        <tr> 
            <td colspan="3" height="20"></td> 
        </tr> 
        </table>';
          $mail->Subject = "OTP for Tejas Networkes login";
          $mail->Body    = $messageparam;
		  $mail->send();
          //echo "<pre>";print_r($mail);exit();
         //echo  $mail->send();exit;
		
    }
    
    function SendNewPassword($user_rec, $new_password)
    {
          $mail = $this->smtp_details(); 
          $to = $user_rec['email'];
          $email=$user_rec['email'];
          $mail->setFrom('info@tejasnetworks.com', 'Tejas Networkes');
          $mail->addAddress($to,$to);
          
          $mail->isHTML(true);
          $messageparam = '<table border="0" cellpadding="0" cellspacing="0"> 
        <tr> 
            <td colspan="3" height="20"></td> 
        </tr> 
        <tr> 
            <td width="20"></td> 
            <td align="left" style="font-size:16px; color:#000001; font-family:serif;"> 
            Hello '.$user_rec['name'].' <br><br> Your password is reset successfully. '.$this->Getbase_name().' <br>Here is your updated login:<br> username: '.$user_rec['username'].'<br>password: '.$new_password.'<br><br> Regards<br> support team<br>'.$this->Getbase_name().'  .
            </td> 
            <td width="20"></td> 
        </tr> 
        <tr> 
            <td colspan="3" height="20"></td> 
        </tr> 
        </table>';
          $mail->Subject = "OTP for Tejas Networkes login";
          $mail->Body    = $messageparam;
          //echo "<pre>";print_r($mail);exit();
          if($mail->send()){
            return true;
          }else{
            return false;
          }
    }    
    
    function ValidateRegistrationSubmission()
    {
        //This is a hidden input field. Humans won't fill this field.
        if(!empty($_POST[$this->GetSpamTrapInputName()]) )
        {
            //The proper error is not given intentionally
            $this->HandleError("Automated submission prevention: case 2 failed");
            return false;
        }
        
        $validator = new FormValidator();
        $validator->addValidation("name","req","Please fill in Name");
        $validator->addValidation("email","email","The input for Email should be a valid email value");
        $validator->addValidation("email","req","Please fill in Email");
        $validator->addValidation("username","req","Please fill in UserName");
        $validator->addValidation("password","req","Please fill in Password");

        
        if(!$validator->ValidateForm())
        {
            $error='';
            $error_hash = $validator->GetErrors();
            foreach($error_hash as $inpname => $inp_err)
            {
                $error .= $inpname.':'.$inp_err."\n";
            }
            $this->HandleError($error);
            return false;
        }        
        return true;
    }
    
    function CollectRegistrationSubmission(&$formvars)
    {
        $formvars['name'] = $this->Sanitize($_POST['name']);
        $formvars['email'] = $this->Sanitize($_POST['email']);
        $formvars['username'] = $this->Sanitize($_POST['username']);
        $formvars['password'] = $this->Sanitize($_POST['password']);
    }
    
    function SendUserConfirmationEmail(&$formvars)
    {
        $mailer = new PHPMailer();
        
        $mailer->CharSet = 'utf-8';
        
        $mailer->AddAddress($formvars['email'],$formvars['name']);
        
        $mailer->Subject = "Your registration with ".$this->sitename;

        $mailer->From = $this->GetFromAddress();        
        
        $confirmcode = $formvars['confirmcode'];
        
        $confirm_url = $this->GetAbsoluteURLFolder().'/confirmreg.php?code='.$confirmcode;
        
        $mailer->Body ="Hello ".$formvars['name']."\r\n\r\n".
        "Thanks for your registration with ".$this->sitename."\r\n".
        "Please click the link below to confirm your registration.\r\n".
        "$confirm_url\r\n".
        "\r\n".
        "Regards,\r\n".
        "Webmaster\r\n".
        $this->sitename;

        if(!$mailer->Send())
        {
            $this->HandleError("Failed sending registration confirmation email.");
            return false;
        }
        return true;
    }
    function GetAbsoluteURLFolder()
    {
        $scriptFolder = (isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] == 'on')) ? 'https://' : 'http://';
        $scriptFolder .= $_SERVER['HTTP_HOST'] . dirname($_SERVER['REQUEST_URI']);
        return $scriptFolder;
    }
    function Getbase_name()
    {
        $scriptFolder = (isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] == 'on')) ? 'https://' : 'http://';
        $scriptFolder .= $_SERVER['HTTP_HOST'];
        return $scriptFolder;
    }
    
    function SendAdminIntimationEmail(&$formvars)
    {
        if(empty($this->admin_email))
        {
            return false;
        }
        $mailer = new PHPMailer();
        
        $mailer->CharSet = 'utf-8';
        
        $mailer->AddAddress($this->admin_email);
        
        $mailer->Subject = "New registration: ".$formvars['name'];

        $mailer->From = $this->GetFromAddress();         
        
        $mailer->Body ="A new user registered at ".$this->sitename."\r\n".
        "Name: ".$formvars['name']."\r\n".
        "Email address: ".$formvars['email']."\r\n".
        "UserName: ".$formvars['username'];
        
        if(!$mailer->Send())
        {
            return false;
        }
        return true;
    }
    
    function SaveToDatabase(&$formvars)
    {
        if(!$this->DBLogin())
        {
            $this->HandleError("Database login failed!");
            return false;
        }
        if(!$this->Ensuretable())
        {
            return false;
        }
        if(!$this->IsFieldUnique($formvars,'email'))
        {
            $this->HandleError("This email is already registered");
            return false;
        }
        
        if(!$this->IsFieldUnique($formvars,'username'))
        {
            $this->HandleError("This UserName is already used. Please try another username");
            return false;
        }        
        if(!$this->InsertIntoDB($formvars))
        {
            $this->HandleError("Inserting to Database failed!");
            return false;
        }
        return true;
    }
    
    function IsFieldUnique($formvars,$fieldname)
    {
        $field_val = $this->SanitizeForSQL($formvars[$fieldname]);
        $qry = "select username from $this->tablename where $fieldname='".$field_val."'";
        $result = mysqli_query($this->connection,$qry);   
        if($result && mysqli_num_rows($result) > 0)
        {
            return false;
        }
        return true;
    }
    
    function DBLogin()
    {
		
		$this->connection = new mysqli($this->db_host, $this->username, $this->pwd, $this->database);
		if(!$this->connection)
		{
			$this->HandleDBError("Database Login failed! Please make sure that the DB login credentials provided are correct");
            return false;
		}
		
		if(!mysqli_query($this->connection,"SET NAMES 'UTF8'"))
		{
			 $this->HandleDBError('Error setting utf8 encoding');
             return false;
		}			
		
		
        return true;
    }    
    
    function Ensuretable()
    {
        $result = mysqli_query($this->connection, "SHOW COLUMNS FROM $this->tablename");   
        if(!$result || mysqli_num_rows($result) <= 0)
        {
            return $this->CreateTable();
        }
        return true;
    }
    
    function CreateTable()
    {
        $qry = "Create Table $this->tablename (".
                "id_user INT NOT NULL AUTO_INCREMENT ,".
                "name VARCHAR( 128 ) NOT NULL ,".
                "email VARCHAR( 64 ) NOT NULL ,".
                "phone_number VARCHAR( 16 ) NOT NULL ,".
                "username VARCHAR( 16 ) NOT NULL ,".
                "password VARCHAR( 32 ) NOT NULL ,".
                "confirmcode VARCHAR(32) ,".
                "PRIMARY KEY ( id_user )".
                ")";
                
        if(!mysqli_query($this->connection,$qry))
        {
            $this->HandleDBError("Error creating the table \nquery was\n $qry");
            return false;
        }
        return true;
    }
    
    function InsertIntoDB(&$formvars)
    {
    
        $confirmcode = $this->MakeConfirmationMd5($formvars['email']);
        
        $formvars['confirmcode'] = $confirmcode;
        
        $insert_query = 'insert into '.$this->tablename.'(
                name,
                email,
                username,
                password,
                confirmcode
                )
                values
                (
                "' . $this->SanitizeForSQL($formvars['name']) . '",
                "' . $this->SanitizeForSQL($formvars['email']) . '",
                "' . $this->SanitizeForSQL($formvars['username']) . '",
                "' . md5($formvars['password']) . '",
                "' . $confirmcode . '"
                )';      
        if(!mysqli_query( $this->connection, $insert_query))
        {
            $this->HandleDBError("Error inserting data to the table\nquery:$insert_query");
            return false;
        }        
        return true;
    }
    function MakeConfirmationMd5($email)
    {
        $randno1 = rand();
        $randno2 = rand();
        return md5($email.$this->rand_key.$randno1.''.$randno2);
    }
    function SanitizeForSQL($str)
    {
        if( function_exists( "mysqli_real_escape_string" ) )
        {
              $ret_str = mysqli_real_escape_string($this->connection,$str );
        }
        else
        {
              $ret_str = addslashes( $str );
        }
        return $ret_str;
    }
    
 /*
    Sanitize() function removes any potential threat from the
    data submitted. Prevents email injections or any other hacker attempts.
    if $remove_nl is true, newline chracters are removed from the input.
    */
    function Sanitize($str,$remove_nl=true)
    {
        $str = $this->StripSlashes($str);

        if($remove_nl)
        {
            $injections = array('/(\n+)/i',
                '/(\r+)/i',
                '/(\t+)/i',
                '/(%0A+)/i',
                '/(%0D+)/i',
                '/(%08+)/i',
                '/(%09+)/i'
                );
            $str = preg_replace($injections,'',$str);
        }

        return $str;
    }    
    function StripSlashes($str)
    {
        if(get_magic_quotes_gpc())
        {
            $str = stripslashes($str);
        }
        return $str;
    }  
    
    function page_404_errorpage()
    {
                
        @header("HTTP/1.0 404 Not Found");
        echo '<!DOCTYPE html><html><head><title>Page not found</title><meta charset="utf-8">';
        echo '</head><body>';
        echo "<h1>404 Not Found</h1>";
        echo "<p>The page that you have requested could not be found.</p>";
        echo '</body></html>';
        exit();      
                    
    } // function page_404_errorpage()
    
    function getquesrting_length($param=array())
    {
        $final_str = false;
                    
        foreach ($param as $key => $value) {
                    
        if(strlen($value)>35)
        {
            $final_str = true;
        }    
                        
        } // foreach ($param as $key => $value) {
                        
        return $final_str;
                    
    } // function getquesrting_length($param)


    function smtp_details()
    {
        $mail = new PHPMailer;

        $mail->SMTPDebug = 0;                               // Enable verbose debug output

        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = 'smtp.sendgrid.net';  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = 'azure_0b1edb9cba48325f6c06dd8b2a8dc973@azure.com';                 // SMTP username
        $mail->Password = '_S&k4gcZD6qcB$s!';                           // SMTP password
        //$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 25;                                    // TCP port to connect to
        
        return $mail;

    }

    function portal_blocking_mail($emailparam,$name,$email){
      $mail = $this->smtp_details(); 
      $to = $emailparam;
      
      $mail->setFrom('info@tejasnetworks.com', 'Tejas Networkes');
      $mail->addAddress($to,$to);
      
      $mail->isHTML(true);
      $client_ip = $this->get_client_ip();
      $messageparam = '<table border="0" cellpadding="0" cellspacing="0"> 
    <tr> 
        <td colspan="3" height="20"></td> 
    </tr> 
    <tr> 
        <td width="20"></td> 
        <td align="left" style="font-size:16px; color:#000001; font-family:serif;"> 
        Hello Admin <br> Portal is Blocked Due to three failed login attempts.Please take an action http://webdevelopment.southcentralus.cloudapp.azure.com/tejas-newsite/main-control/shuru.php <br> login Activity By:-<br>Name: '.$name.' <br>Email: '.$email.'<br>Ip: '.$client_ip.' <br><br> Regards<br> support team.  
        </td> 
        <td width="20"></td> 
    </tr> 
    <tr> 
        <td colspan="3" height="20"></td> 
    </tr> 
</table>';
      $mail->Subject = "Unusual Activity on Login";
      $mail->Body    = $messageparam;
      //echo "<pre>";print_r($mail);exit();
      $mail->send(); 
    }

    function otp_failed_mail($emailparam,$name,$email){
      $mail = $this->smtp_details(); 
      $to = $emailparam;
      
      $mail->setFrom('info@tejasnetworks.com', 'Tejas Networkes');
      $mail->addAddress($to,$to);
      
      $mail->isHTML(true);
      $client_ip = $this->get_client_ip();
      $messageparam = '<table border="0" cellpadding="0" cellspacing="0"> 
    <tr> 
        <td colspan="3" height="20"></td> 
    </tr> 
    <tr> 
        <td width="20"></td> 
        <td align="left" style="font-size:16px; color:#000001; font-family:serif;"> 
        Hello Admin <br> OTP is failed for three times by the following user.Please take an action <br> login Activity By:-<br>Name: '.$name.' <br>Email: '.$email.'<br>Ip: '.$client_ip.' <br><br> Regards<br> support team.  
        </td> 
        <td width="20"></td> 
    </tr> 
    <tr> 
        <td colspan="3" height="20"></td> 
    </tr> 
</table>';
      $mail->Subject = "Unusual Activity on OTP";
      $mail->Body    = $messageparam;
      //echo "<pre>";print_r($mail);exit();
      $mail->send(); 
    }

    function send_otp($emailparam,$randon_otp){
      $mail = $this->smtp_details(); 
      $to = $emailparam;
      
      $mail->setFrom('info@tejasnetworks.com', 'Tejas Networkes');
      $mail->addAddress($to,$to);
      
      $mail->isHTML(true);
      $messageparam = '<table border="0" cellpadding="0" cellspacing="0"> 
    <tr> 
        <td colspan="3" height="20"></td> 
    </tr> 
    <tr> 
        <td width="20"></td> 
        <td align="left" style="font-size:16px; color:#000001; font-family:serif;"> 
        Hello user, <br> Please use the below mentioned one time password for logging onto Tejas Networkes admin portal.<br><br>OTP: '.$randon_otp.'  <br><br>Thanks & Regards<br>
Tejas Networkes Mail Robot 
        </td> 
        <td width="20"></td> 
    </tr> 
    <tr> 
        <td colspan="3" height="20"></td> 
    </tr> 
</table>';
      $mail->Subject = "OTP for Tejas Networkes login";
      $mail->Body    = $messageparam;
      //echo "<pre>";print_r($mail);exit();
      $mail->send(); 
    }

    function resend_mail(){
        if(!isset($_SESSION)){ session_start(); }
        if(!$this->DBLogin())
        {
            return false;
        }
        $User_name = $_SESSION['username_of_user'];
        if (!isset($User_name)) {
            return false;
        }
        $mail_qry = "Select email,email_attempts from $this->tablename where username='$User_name' ";
        $mail_result = mysqli_query($this->connection,$mail_qry);
        $mail_data = mysqli_fetch_assoc($mail_result);
        $email = $mail_data['email'];
        $randon_otp = $this->create_otp();
        $otp_validity = date("Y-m-d H:i:s");
        $hashed_otp = password_hash($randon_otp, PASSWORD_DEFAULT);
        $email_attempts = $mail_data['email_attempts'] + 1;
        $qry = "Update $this->tablename SET one_time_password = '$hashed_otp',otp_validity = '$otp_validity',email_attempts = '$email_attempts' where username='$User_name' ";
            mysqli_query($this->connection,$qry);
        if ($email_attempts >= 2) {
                return false;
            }    
        $this->send_otp($email,$randon_otp);
        return true;
    }

    function create_otp(){
        $alpha   = str_shuffle('ABCDEFGHIJKLMNOPQRSTUVWXYZ');
        // Create a string of all numeric characters and randomly shuffle them
        $numeric = str_shuffle('0123456789');
        // Grab the 4 first alpha characters + the 2 first numeric characters
        $code = substr($alpha, 0, 4) . substr($numeric, 0, 2);
        // Shuffle the code to get the alpha and numeric in random positions
        $code = str_shuffle($code);
        return $code;
    }

    function create_password(){
        $alpha   = str_shuffle('ABCDEFGHIJKLMNOPQRSTUVWXYZ');
        // Create a string of all numeric characters and randomly shuffle them
        $numeric = str_shuffle('0123456789');
        // Grab the 4 first alpha characters + the 2 first numeric characters
        $special_cahracters = str_shuffle('&^$#@!_-~*');
        // Grab the 4 special characters
        $code = substr($alpha, 0, 4) . substr($numeric, 0, 2) . substr($special_cahracters, 0, 4);
        // Shuffle the code to get the alpha and numeric in random positions
        $code = str_shuffle($code);
        return $code;
    }

    function get_client_ip(){
        $ip =   getenv('HTTP_CLIENT_IP')?:
                getenv('HTTP_X_FORWARDED_FOR')?:
                getenv('HTTP_X_FORWARDED')?:
                getenv('HTTP_FORWARDED_FOR')?:
                getenv('HTTP_FORWARDED')?:
                getenv('REMOTE_ADDR');
        return $ip;
    }

}
?>