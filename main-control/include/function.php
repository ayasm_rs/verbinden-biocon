<?PHP
//require_once("class.phpmailer.php");
require_once("mysqli_connect.php");
date_default_timezone_set('Asia/Kolkata');

class common_function
{
    var $connection;

    function __construct(&$connection)
    {
        $this->db = $connection;
    }

    function page_list()
    {
        $select = "SELECT * FROM `page` WHERE page_name!='' ORDER BY page_name ASC ";
        $query = mysqli_query($this->db, $select);
        if (mysqli_num_rows($query) > 0) {
            while ($row = mysqli_fetch_array($query)) {
                $inner_array[] = $row;
            }
            return $inner_array;
        } else {
            return $inner_array;
        }
    }

    function temp_list()
    {
        $select = "SELECT a.*,GROUP_CONCAT(b.page_title) as page_title,GROUP_CONCAT(b.page_heading) as page_heading FROM templates a LEFT JOIN page b ON FIND_IN_SET(a.id,REPLACE(b.temp_ids,'~',',')) GROUP BY a.id ORDER BY a.id ";
        $query = mysqli_query($this->db, $select);
        if (mysqli_num_rows($query) > 0) {
            while ($row = mysqli_fetch_array($query)) {
                $inner_array[] = $row;
            }
            return $inner_array;
        } else {
            return $inner_array;
        }
    }

    function location_list()
    {
        $select = "SELECT * FROM location";
        $query = mysqli_query($this->db, $select);
        if (mysqli_num_rows($query) > 0) {
            while ($row = mysqli_fetch_array($query)) {
                $inner_array[] = $row;
            }
            return $inner_array;
        } else {
            return $inner_array;
        }
    }
}

?>