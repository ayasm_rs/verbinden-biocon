<?PHP
require_once("./include/membersite_config.php");
include("./include/mysqli_connect.php");
include("./include/html_codes.php");
$fgmembersite->block_login_page();

if(isset($_POST['submitted']))
{
   if($fgmembersite->Login())
   {
        $fgmembersite->RedirectToURL("dashboard.php");
   }
    
}

?>
<?php 
?>
<!DOCTYPE html>
<html>
<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">
		<meta name="robots" content="noindex">
        <!-- App Favicon -->
        <link rel="shortcut icon" href="assets/images/favicon.ico">

        <!-- App title -->
        <title><?php echo $site_name; ?></title>

        <!-- App CSS -->
        <link href="assets/css/style.css" rel="stylesheet" type="text/css" />
		<link href="css/custom.css" rel="stylesheet" type="text/css" />
		
		<script type='text/javascript' src='js/gen_validatorv31.js'></script>

        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <script src="assets/js/modernizr.min.js"></script>
		<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>-->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script>
		$(document).ready(function () {
			
		$.ajax({
				type: "POST",
				url: "ajaxCallsip.php",
				data: {"action":"getip"} ,
				success:function(msg){ //alert(msg);
					if(msg ==0){ 
							//window.location.href="../index.php";
							$("body").show();
						} else{
							$("body").show();
						}
						
				}
		
		});
		}); //undefined or null
		</script>
    </head>


    <body style="display:none">
		<section class="login-container" id='fg_membersite'>
        <div class="account-pages"></div>
        <div class="clearfix"></div>
        <div class="wrapper-page">

        	<div class="account-bg">
                <div class="card-box m-b-0">
                    <div class="text-xs-center m-t-20">
                        <img src="../images/biocon-logo.png" style="width: 100%;">
                    </div>
                    <div class="m-t-30 m-b-20">
                        <div class="col-xs-12 text-xs-center">
                            <h6 class="text-muted text-uppercase m-b-0 m-t-0">User Login</h6>
                        </div>
                        <form class="form-horizontal m-t-20" id="forms-login" method='post' action="<?php echo $fgmembersite->GetSelfScript(); ?>">
						<input type='hidden' name='submitted' id='submitted' value='1'/>
							<div style="margin-bottom:10px;"><center><span class='error' style="color:red"><?php echo $fgmembersite->GetErrorMessage(); ?></span></center></div>
							
                            <div class="form-group ">
                                <div class="col-xs-12">
                                    <input class="form-control" type="text" id="username" name="username" required placeholder="Username" value='<?php echo $fgmembersite->SafeDisplay('username') ?>'>
									<span id='login_username_errorloc' class='error' style="color:red"></span>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-xs-12">
                                    <input class="form-control" type="password" id="password" name="password" required placeholder="Password">
									<span id='login_password_errorloc' class='error' style="color:red"></span>
                                </div>
                            </div>
							<!--div style="text-align:right;"><a href="reset-pwd-req.php">Forgot Password?&nbsp;&nbsp;&nbsp;&nbsp;</a></div-->
							<div class="form-group text-center m-t-30">
                                <div class="col-xs-12">
									<button type="submit" class="btn btn-danger waves-effect waves-light pull-right">Login
									   <span class="btn-label btn-label-right"><i class="fa fa-arrow-right"></i>
									   </span>
									</button>
									<div class="clearfix"></div>
                                </div>
                            </div>

                        </form>

                    </div>
                </div>
            </div>
            <!-- end card-box-->

        </div>
        <!-- end wrapper page -->


        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/tether.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/detect.js"></script>
        <script src="assets/js/fastclick.js"></script>
        <script src="assets/js/jquery.blockUI.js"></script>
        <script src="assets/js/waves.js"></script>
        <script src="assets/js/jquery.nicescroll.js"></script>
        <script src="assets/js/jquery.scrollTo.min.js"></script>
        <script src="assets/js/jquery.slimscroll.js"></script>
        <script src="assets/plugins/switchery/switchery.min.js"></script>

        <!-- App js -->
        <script src="assets/js/jquery.core.js"></script>
        <script src="assets/js/jquery.app.js"></script>
	</section>
		
		<script type='text/javascript'>
		// <![CDATA[
	
		var frmvalidator  = new Validator("forms-login");
		frmvalidator.EnableOnPageErrorDisplay();
		frmvalidator.EnableMsgsTogether();

		frmvalidator.addValidation("username","req","Please provide your username");
		
		frmvalidator.addValidation("password","req","Please provide the password");

	// ]]>
	</script>

    </body>
</html>