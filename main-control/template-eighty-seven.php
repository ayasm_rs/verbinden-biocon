<?PHP
require_once("./include/membersite_config.php");
include("./include/mysqli_connect.php");
include("./include/html_codes.php");
include("./include/function.php");
$common_function = new common_function($connect);
$page_list= $common_function->page_list();
$option='<option value=""> Select page link</option>';
for($i=0; $i < count($page_list); $i++)
{
	$option .='<option value="'.$page_list[$i]["id"].'">'.$page_list[$i]["page_name"].'</option>';
}
$temp_id="87";

$temp_id=mysqli_real_escape_string($connect,$_GET['tid']);
$pid=mysqli_real_escape_string($connect,$_GET['pid']);
$tpos=mysqli_real_escape_string($connect,$_GET['tpos']);
$select_title="SELECT * FROM `template_title_desc` WHERE `temp_id`='$temp_id' AND page_id_td='$pid' AND temp_pos_id='$tpos' ";
$run_title=mysqli_query($connect,$select_title);

$row=mysqli_fetch_array($run_title);
$title=$row['title'];
$description=$row['description'];
$shortdescription=$row['shortdescription'];
$bimg_file=$row['bimg_file'];

admin_way_top();

admin_top_bar();


admin_left_menu();

//After login check for page access

$id_user=$fgmembersite->idUser();
$admin = 0;
 
$select_event="SELECT * FROM `template_details` WHERE temp_id='$temp_id' AND page_id_td='$pid' AND temp_pos_id='$tpos' ";
$run_query=mysqli_query($connect,$select_event);

if($_POST['create_page']=='submit'){
		
	$delete_count=count($_POST['delete_item']);
	if($delete_count>0){
		for($delete=0;$delete<$delete_count;$delete++){
			$del=$_POST['delete_item'][$delete];
			$delete_query="DELETE FROM `template_details` WHERE id='".$del."'";
			mysqli_query($connect, $delete_query);
		}
	}
	$temp_id = htmlspecialchars(mysqli_real_escape_string($connect, $_POST['temp_id']),ENT_QUOTES, 'UTF-8');
	$title_value = htmlspecialchars(mysqli_real_escape_string($connect, $_POST['title_value']),ENT_QUOTES, 'UTF-8');
	$shortdescription = htmlspecialchars(mysqli_real_escape_string($connect, $_POST['value_description']),ENT_QUOTES, 'UTF-8');
	$value_description_des = htmlspecialchars(mysqli_real_escape_string($connect, $_POST['value_description_des']),ENT_QUOTES, 'UTF-8');
	$block7_menu_file = htmlspecialchars(mysqli_real_escape_string($connect, $_POST['block7_menu_file']),ENT_QUOTES, 'UTF-8');
	$str="";
	
	if($block7_menu_file!="")
		{
			$file=htmlspecialchars(mysqli_real_escape_string($connect, $_POST['block7_menu_file']),ENT_QUOTES, 'UTF-8');
			$name='icon'.date('ymdis').rand(0,99999);
			$file_name = str_replace(" ","-",$name).".png";
			list($type, $file) = explode(';', $file);
			list(, $file)      = explode(',', $file);
			file_put_contents("../images/icons/".$file_name , base64_decode($file));
			
			$str=", bimg_file='".$file_name."' ";
		}
	$upsql="UPDATE template_title_desc SET title='".$title_value."',description='".$value_description_des."', shortdescription='".$shortdescription."' ".$str." WHERE temp_id='".$temp_id."' ";
	mysqli_query($connect,$upsql);
	
	$count=count($_POST['block7_menu_title']);
	
	for($key=0;$key<$count;$key++)
	{
		$block7_id =  $_POST['block7_id'][$key];
		$remove_image_id =  $_POST['remove_image_id'][$key];
		$block7_menu_title = htmlspecialchars(mysqli_real_escape_string($connect, $_POST['block7_menu_title'][$key]),ENT_QUOTES, 'UTF-8');
		$new_chart_heading=array();
		
	
		foreach($_POST['chart_heading'] as $k=>$v)
		{ $new_chart_heading[]=$v; }
		
		$new_chart_value=array();
		foreach($_POST['chart_value'] as $k=>$v)
		{ $new_chart_value[]=$v; }
	    
		$new_chart_bgcolor=array();
		foreach($_POST['chart_bgcolor'] as $k=>$v)
		{ $new_chart_bgcolor[]=$v; }
		
		$chartdata=array();
		$chartdata[]=implode('*',$new_chart_heading[$key]);
		$chartdata[]=implode('*',$new_chart_value[$key]);
		$chartdata[]=implode('*',$new_chart_bgcolor[$key]);
		
						
		if($block7_menu_title!="" && $block7_id!='' && $remove_image_id=='no')
		{
			$sql="UPDATE template_details SET title='".$block7_menu_title."',resource='".implode('~',$chartdata)."' WHERE temp_id='".$temp_id."' AND id=".$block7_id;
			mysqli_query($connect, $sql);
		}
		else{
			
		$sql="INSERT INTO template_details(temp_id, title,resource, `page_id_td`, `temp_pos_id`) VALUES ('".$temp_id."', '".$block7_menu_title."','".implode('~',$chartdata)."','".$pid."','".$tpos."')";
		mysqli_query($connect, $sql);
		}
		
	
	}

	
}


//redirect code


    $id_user=$fgmembersite->idUser();
	$approver = 0;$admin=0;
	if($fgmembersite->UserRole()=="admin"){
		$admin=1;
	}
	if($fgmembersite->UserRole()!="admin"){
		echo "<script>window.location.href='dashboard.php'</script>";
	}

//mysqli_query($connect, "TRUNCATE table temp_data");
?>
									
<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<style>
.cr_img img{max-width:500px !important;height:auto !important;}
</style>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">
			
			<div class="row">
				<div class="col-xs-12">
					<div class="page-title-box">
						<h4 class="page-title">Widget Eighty Seven</h4>
						<!--button class="btn btn-primary"  type="button" name="create_page" onclick="$('#sub_btn').click()" style="float:right">Create Page</button-->
								
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
			<!-- end row -->
                        <form method="POST" name="xxfrm" id="xxfrm" enctype="multipart/form-data">
						<input type="hidden" value="<?php echo $temp_id; ?>" name="temp_id" />
				<div class="row">
					<div class="col-xs-12 col-lg-12 col-xl-12">
						<div class="card-box">
							<h4 class="header-title m-t-0 m-b-30">Widget Details</h4>
							<section>
								<p class="text-muted m-b-20 font-13 pull-right">
									(* All fields are mandatory)
								</p>
						
					  <!-- start our value drivers div  -->
						 <h5 class="heading_cust">Widget One</h5>
							<!--img src="assets/images/users/block07.jpg" data-toggle="modal" data-target="#myModal"  data-title="Block 6" class="block-img">
							
							<span>(Preview Image)</span-->
						 <hr>
						 <section class="row">
							<div class="col-xs-12 col-lg-12 col-xl-12">
							   <section>
								  <div class="row">
									 <div class="col-xs-12 col-lg-6 col-xl-6">
										<div class="form-group clearfix">
										   <label class="col-lg-2 control-label " for="" >Title
										   </label>
										   <div class="col-lg-10">
											  <input type="text" name="title_value" id="title_value" class="form-control" value="<?php echo $title; ?>" />
											  <br />
										   </div>
										</div>
									 </div>
									 <div class="col-xs-12 col-lg-12 col-xl-12">
										<div class="form-group clearfix">
										   <label class="col-lg-2 control-label " for="" >Short Description
										   </label>
										   <div class="col-lg-10">
											  <textarea name="value_description" id="value_description" class="form-control" ><?php echo $shortdescription; ?></textarea>
											  <br />
										   </div>
										</div>
									 </div>
									<div class="col-xs-12 col-lg-12 col-xl-12">
										<div class="form-group clearfix">
										   <label class="col-lg-2 control-label " for="" >Description
										   </label>
										   <div class="col-lg-10">
											  <textarea name="value_description_des" id="value_description_des" class="form-control" ><?php echo $description; ?></textarea>
											  <br />
										   </div>
										</div>
									 </div> 
								<div class="col-xs-12 col-lg-6 col-xl-6">
								<div class="form-group clearfix">
								   <label class="col-lg-2 control-label " for="" >Image <?php echo ($bimg_file=='')?"*":"";  ?> 
								   </label>
								  
								   <div class="col-lg-10">
									  <main class="page"><div class="box"><input type="file" id="file_input_block7" class="file-input form-control" <?php echo ($bimg_file=='')?"required='required'":"";  ?> ><textarea name="block7_menu_file" class="hide" id="block7_menu_file" ></textarea></div><div class="box-2" id="box-2_block7"><div class="result_block7"></div></div><div class="box-2 img-result_block7"><img class="cropped_block7" id="cropped_block7" src="../images/icons/<?php echo $bimg_file;  ?>" alt=""></div><div class="box"><div class="options_block7"><input type="number" min="0" class="hide img-w_block7" value="669" min="100" max="1200" /><input type="number" min="0" class="hide img-h_block7" value="767" min="100" max="1200" /></div><a class="btn-success btn-sm btn save_block7" onclick='$(".cropped_block7").show(); $("#remove_image").val("yes"); $("#remove_buttion").show(); $(this).hide(); $("#box-2_block7").hide();' style="display:none;width: calc(100%/2 - 2em);padding: 0.5em;">Crop</a></div></main>
								   </div>
								   </div>
							</div>
									
								  </div>
								  <div class="form-group clearfix">
									 <div class="col-lg-10">
										<div class="block7_add_menu_toggle row " >
										<?php 
										$j=1;
										$script="";
										while($rows=mysqli_fetch_array($run_query)){ 
										$image_remove="no";
										$image_show="none";
										if($rows["sub_icon"]!=""){
										$image_remove="yes";
										$image_show="black";
										}
										$chart_cnt=$rows['id'];
										echo '<div id="div_block7_'.$j.'" class="col-sm-6 count_class" ><div style="border: 1px solid #ccc;margin:10px;" ><input type="hidden" id="count_chart_id_'.$chart_cnt.'" value="'.$chart_cnt.'" /><div class="row"><div style="padding:10px;margin:10px;float: left;width: 100%;" class="row"><div class="col-md-12" ><label>Title*</label><input type="text" value="'.$rows["title"].'"  class="form-control" name="block7_menu_title[]"  required="required"/><input type="hidden" value="'.$rows["id"].'"  class="form-control" id="delete_item_'.$j.'" name="block7_id[]"  /><input type="hidden" value="'.$image_remove.'"  class="form-control" name="remove_image_id[]" id="remove_image_'.$j.'" /></div>';
										
										$resource=explode('~',$rows['resource']);
										
										$heading=explode("*",$resource[0]);
										$values=explode("*",$resource[1]);
										$bgcolor=explode("*",$resource[2]);
										
										$cnnt=count($heading);
										
										$idd=$chart_cnt;
										
										
	
										echo '<div class="col-md-6" ><table id="addMoreVal'.$idd.'"><tr><th colspan="3"><a data-id="'.$idd.'" class="add_chart_val" style="cursor:pointer">Add</a></th></tr><tr><th>Heading</th><th>Value</th><th>Color</th></tr>'; 
																															
										for($k=0;$k<$cnnt;$k++)
										{
											$nn=$idd."".$k;
											echo '<tr id="remove_chart_val_'.$nn.'" data-id="'.$nn.'"><td><input type="text" class="chart_heading" name="chart_heading['.$idd.'][]" value="'.$heading[$k].'" required="required" /></td><td><input type="text" class="chart_value" name="chart_value['.$idd.'][]" value="'.$values[$k].'"  required="required" /></td><td><input type="color" name="chart_bgcolor['.$idd.'][]" class="chart_bgcolor" value="'.$bgcolor[$k].'"/></td>';
											if($k!=0)
											{
											echo '<td><a data-id="'.$nn.'" class="remove_chart_val" style="cursor:pointer">Remove</a></td></tr>';
											}
										}
										echo '</table></div>
										<div class="col-md-12" ><a class="preview" data-id="'.$rows["id"].'" data-idd="'.$j.'"   id="prv'.$rows["id"].'" style="cursor:pointer">Preview</a></div>
										</div><div id="new_chart_'.$rows["id"].'" style="margin: 0 auto;"></div></div><div class="col-md-12" ><a class="btn btn-danger btn-sm " onclick=remove_div('.$j.');  style="cursor:pointer">Remove</a></div></div></div>';

										$script .='$("#prv'.$rows["id"].'").click();';
										$j++;
							 } ?>
							 </div>
										<div id="add_menu_item">
										<br>
										+ &nbsp;<a class="btn btn-primary waves-effect waves-light m-r-5 m-b-10" id="block7_add_menu">Add Item</a>
										</div>
									 </div>
								  </div>
								  <div class="form-group clearfix">
								  </div>
							   </section>
							</div>
							<!-- end col-->
						 </section>
					  <!-- end our value drivers div  -->
					  
					  
									
							</section>
						</div>
					</div>
					<div class="col-lg-12 form-group clearfix">
						<div class="card-box">
							<div class="col-lg-10 text-xs-center">
							<input type="hidden" name="create_page" id="create_page" value="submit" />
							<input type="submit" class="btn btn-primary"  name="sub_btn" id="sub_btn" value="Update Widget">
							</div>
						</div>
					</div>
				</div>
				</form>
			<button class="btn btn-custom waves-effect waves-light btn-sm page_create_succcess"  style="display:none;">Click me</button>
		</div> <!-- container -->

	</div> <!-- content -->

</div>



<!--center>
<div class="container-narrow">
  <div>
    <h1>jQuery jQuery Awesome Cropper Demo</h1>
    
    <form role="form">
      <input id="sample_input" type="hidden" name="test[image]">
    </form>
  </div>
  <hr>
  
</div></center-->

<!-- End content-page -->




<!----------- Banner crop plugin  ------------------> 
	
		<link rel="stylesheet" type="text/css" href="simple_cropper/css/style.css" />
		<link rel="stylesheet" type="text/css" href="simple_cropper/css/style-example.css" />
		<link rel="stylesheet" type="text/css" href="simple_cropper/css/jquery.Jcrop.css" />

		<!-- Js files-->
		<script type="text/javascript" src="simple_cropper/scripts/jquery-1.10.2.min.js"></script>
		<script type="text/javascript" src="simple_cropper/scripts/jquery.Jcrop.js"></script>
		<script type="text/javascript" src="simple_cropper/scripts/jquery.SimpleCropper.js"></script>
		<script>
		 $('.cropme').simpleCropper();
		</script>
		
<!---------------- croper  --------------------->
<link rel="stylesheet" href="assets/css/choosenJs/prism.css">
	<link rel="stylesheet" href="assets/css/choosenJs/chosen.css">
	<link rel="stylesheet" href="assets/css/custom.css">
		<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/cropper/2.3.4/cropper.min.css'>
			
	
    <script src="assets/js/choosenJs/chosen.jquery.js" type="text/javascript">
    </script>
    <script src="assets/js/choosenJs/prism.js" type="text/javascript" charset="utf-8">
    </script>
    <script src="assets/js/choosenJs/init.js" type="text/javascript" charset="utf-8">
    </script>
		<script src='https://cdnjs.cloudflare.com/ajax/libs/cropperjs/0.8.1/cropper.min.js'></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>		
<script>
$(function() {
    // Multiple images preview in browser
    var imagesPreview = function(input, placeToInsertImagePreview) {

        if (input.files) {
            var filesAmount = input.files.length;

            for (i = 0; i < filesAmount; i++) {
                var reader = new FileReader();

                reader.onload = function(event) {
                    $($.parseHTML('<img>')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                }

                reader.readAsDataURL(input.files[i]);
            }
        }

    };

    $('#gallery-photo-add').on('change', function() {
        imagesPreview(this, 'div.gallery');
    });
});
</script>
            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->

<!-- Custom box css -->
<link href="assets/plugins/custombox/css/custombox.min.css" rel="stylesheet">

 <!-- Modal-Effect -->
<script src="assets/plugins/custombox/js/custombox.min.js"></script>
<script src="assets/plugins/custombox/js/legacy.min.js"></script>
			
<!-- Sweet Alert css -->
<link href="assets/plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css" />
<!-- Sweet Alert js -->
<script src="assets/plugins/bootstrap-sweetalert/sweet-alert.min.js"></script>
<script src="assets/pages/jquery.sweet-alert.init.js"></script>

<!-- Editor -->
<script src="//code.jquery.com/jquery-1.12.0.min.js"></script>


<script type="text/javascript">

var validExt = ".png, .jpeg, .jpg";
var validExt1 = ".png";
function fileExtValidate(fdata,id) { 
 var filePath = fdata.value;
 var getFileExt = filePath.substring(filePath.lastIndexOf('.') + 1).toLowerCase();
 if(id.includes("PImg") || id.includes("mPimg"))
 var pos = validExt1.indexOf(getFileExt);
else
var pos = validExt.indexOf(getFileExt);
	
 if(pos < 0) {
 	alert("This file is not allowed, please upload valid file.");
 	return false;
  } else {
  	return true;
  }
}
$(document).ready(function(){

$("#add_more").on("change",".d_priority",function(){
	var len_priority=$(".d_priority").length;
	var priority_value=$(this).val();
	var this_pri=$(this);
	var n_priority=$(".d_priority").index(this);
	for(i=0;i<len_priority;i++){
		//$(".d_priority:nth-child(1)").val();
		if(priority_value==$(".d_priority:eq( "+i+" )").val() && n_priority!=i){
		//$(this).removeClass("form-control");		
        this_pri.css("border","1px solid #fb0505");
		alert("Change the priority");
		}else{
		this_pri.css("border","1px solid #ccc");
		}
		}
	//alert($(".d_priority").length);
	
});

/**************************ADD MORE FIELDS**************************************/
var max_fields      = 20; //maximum input boxes allowed
    var wrapper         = $("#add_more"); //Fields wrapper
    var add_button      = $(".add_field_button"); //Add button ID
   
    var x = 2; //initlal text box count
    $(add_button).click(function(e){ //on add input button click
	//debugger;
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
$(wrapper).append('<div class="col-lg-12 form-group remove_div_'+x+'"> <hr> </div><div class="col-lg-6 form-group remove_div_'+x+'"><label class="control-label " for="meta_description">Image '+x+'*</label><br/><input type="hidden" class=" form-control pimage" name="pimage[]" id="pimage_'+x+'" class="imgs" data-id="mPimg_'+x+'" ><br/><div class="container"><div class="row"><div class="panel panel-body"><div class="span4 cropme landscape" id="landscape"></div></div></div></div><img src="" id="mPimg_'+x+'" class="imsrc" width="150px"/><span class="errorcls pimageerror" id="pimageerror"></span></div><div class="col-lg-12 form-group remove_div_'+x+'"><label class="control-label " for="meta_description">Description '+x+'*</label><textarea id="description'+x+'" name="description[]" class=" form-control description"  ></textarea><span class="errorcls descriptionerror" id="descriptionerror"></span><br><a href="#" onclick="$(\'.remove_div_'+x+'\').remove();return false;" class="btn btn-primary remove_field">Remove</a><br></div>'); //add input box
CKEDITOR.replace( 'description'+x+'' );
$('#pimage_'+x).awesomeCropper(
        { width: 1165, height: 500, debug: true }
        );
x++; //text box increment

        }
    });
   
    /* $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('section').remove(); x--;
    }) */
/*******************************************************************************/
$(wrapper).on("change",".imgs", function(e) {
var id=$(this).attr("data-id");
var filename=$(this);
if(fileExtValidate(this,id)) {
var fr = new FileReader;
var ext = this.files[0].type.split('/').pop().toLowerCase();
if($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {
    alert('invalid extension!');
}
else{
	var size = parseFloat(this.files[0].size / 1024).toFixed(2);
    fr.onload = function(e) { // file is loaded
     	var img = new Image;
		img.onload = function() {
			
			 var imgwidth = this.width;
			var imgheight = this.height;
			var flag=0;
			if(id.includes("PImg") || id.includes("mPimg"))
			{
				if(imgwidth>550 || imgheight>520)
				{
				flag=1;
				alert("Max image dimesion 550*460");
				filename.val("");
				}
			}
			else
			{
				if(imgwidth<725 || imgheight<239)
				{
				flag=1;
				alert("Min image dimesion 725*239");
				filename.val("");
				}
			}
			if(flag==0)
			{
            $('#'+id).attr('src', e.target.result);
			}
			
			};
		img.src = fr.result; // is the data URL because called with readAsDataURL
    };
    fr.readAsDataURL(this.files[0]); // I'm using a <input type="file"> for demonstrating
}	
	}
	else
		$(this).val("");
		
});
	
/******************************************************************************/
$(".imgs").on('change', function () {
var id=$(this).attr("data-id");
var filename=$(this);
if(fileExtValidate(this,id)) {
var fr = new FileReader;
var ext = this.files[0].type.split('/').pop().toLowerCase();
if($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {
    alert('invalid extension!');
}
else{
	var size = parseFloat(this.files[0].size / 1024).toFixed(2);
    fr.onload = function(e) { // file is loaded
     	
		var img = new Image;
		
		img.onload = function() {
			
            var imgwidth = this.width;
			var imgheight = this.height;
			var flag=0;
			if(id.includes("PImg") || id.includes("mPimg"))
			{
				if(imgwidth>550 || imgheight>520)
				{
				flag=1;
				filename.val("");
				alert("Max image dimesion 550*460");
				
				}
			}
			else
			{
				if(imgwidth<725 || imgheight<239)
				{
				flag=1;
				filename.val("");
				alert("Min image dimesion 725*239");
				}
			}
			if(flag==0)
			{
            $('#'+id).attr('src', e.target.result);
			}
					
			};
		img.src = fr.result; // is the data URL because called with readAsDataURL
    };
    fr.readAsDataURL(this.files[0]); // I'm using a <input type="file"> for demonstrating
}	
	}
	else
		$(this).val("");
		
});	
	$('.page_create_succcess' ).click(function () {
		swal({title:"Status!", text:"Widget updated successfully.", icon:"success"},
	function(){
		window.location="template-eighty-seven.php?pid=<?php echo $pid.'&tid='.$temp_id.'&tpos='.$tpos; ?>";
	});
		
	});	
	<?php  if(isset($_POST['create_page'])){ ?>
	$('.page_create_succcess').click();	
	<?php } ?>
	


	
});


$("#page_title").keyup(function(){
	var leng=$("#page_title").val().length;
	$("#page_title_len").text(" [ "+(60-leng)+" character remaining ]");
});

</script>



<style>
.radio, .checkbox{
	display:inline-block;
}
.all_widgets{
    margin-left: 39px;	
	border-radius: 75%;
    width: 63px;
    text-align: center;
    padding: 10px;
    cursor: pointer;
    color: black;
    border: solid 1px #64b0f2;
	background-color:#64b0f2;
}

</style>

<!-- custom scroll bar --->
<!--script src="js/jquery.mCustomScrollbar.concat.js"></script-->
<link rel="stylesheet" type="text/css" href="css/jquery.mCustomScrollbar.css" />
<!--<script src="js/page-create-boardofdirectory.js"></script>-->


<!-- Css files siva -->
<link href="components/imgareaselect/css/imgareaselect-default.css" rel="stylesheet" media="screen">
<link rel="stylesheet" href="css/jquery.awesome-cropper.css">


<?php
admin_right_bar();

admin_footer();
?>
<script>
	$("#page-create").attr("class","active");
</script>

<!-- /container by siva --> 

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> 
<script src="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script> 
<script src="components/imgareaselect/scripts/jquery.imgareaselect.js"></script> 
<script src="build/jquery.awesome-cropper.js"></script> 
<script>
    $(document).ready(function () {
        $('#pimage_1').awesomeCropper(
        { width: 1165, height: 500, debug: true }
        );
    });
</script> 
<style>
.awesome-cropper>img{
	width:150px;
}	
canvas {
    max-width: 150px;
}
#add_more .form-group {
    margin-bottom: 0;
}
.page_title .form-group {
    margin-bottom: 30px;
}
.cropme img{
	width:100%;
	height:100%;
}
</style>

<script>
   // Replace the <textarea id="editor1"> with a CKEditor
   // instance, using default configuration.
  
	
	<!----------- Add menu button click code for 7th Block  ------------------>  
	var n=<?php echo $j; ?>;
	if(n>3){
		$("#add_menu_item").hide();
		}else{
		$("#add_menu_item").show();	
		}
	$("#block7_add_menu").click(function(){
		n++;
		var count = $(".count_class").length;
		if(count>1){
		$("#add_menu_item").hide();
		}else{
		$("#add_menu_item").show();
		}
		
		
		$(".block7_add_menu_toggle").append('<div id="div_block7_'+n+'" class="col-sm-6 count_class" ><div style="border: 1px solid #ccc;margin:10px;" ><input type="hidden" id="count_chart_id_'+n+'" value="'+n+'" /><div class="row"><div style="padding:10px;margin:10px;float: left;width: 100%;" class="row"><div class="col-md-12" ><label>Title*</label><input type="text" value=""  class="form-control" name="block7_menu_title[]"  required="required"/><input type="hidden" value="'+n+'"  class="form-control" id="delete_item_'+n+'" name="block7_id[]"  /><input type="hidden" value=""  class="form-control" name="remove_image_id[]" id="remove_image_'+n+'" /></div><div class="col-md-6" ><table id="addMoreVal'+n+'"><tr><th colspan="3"><a data-id="'+n+'" class="add_chart_val" style="cursor:pointer">Add</a></th></tr><tr><th>Heading</th><th>Value</th><th>Color</th></tr><tr><td><input type="text" class="chart_heading" name="chart_heading['+n+'][]" value="" required="required"/></td><td><input type="text" class="chart_value" name="chart_value['+n+'][]" value="" required="required" /></td><td><input type="color" name="chart_bgcolor['+n+'][]" class="chart_bgcolor"/></td></tr></table></div><div class="col-md-12" ><a class="preview" data-id="'+n+'" data-idd="0" style="cursor:pointer">Preview</a></div></div><div id="new_chart_'+n+'" style="margin: 0 auto;"></div></div><div class="col-md-12" ><a class="btn btn-danger btn-sm " onclick=remove_div('+n+'); style="cursor:pointer">Remove</a></div></div></div>');
		
	});
function remove_image(id){
	var remove_image=document.getElementById("remove_image_"+id);
	var file_input_block7=document.getElementById("file_input_block7_"+id);
	var cropped_block7=document.getElementById("cropped_block7_"+id);
	var remove_buttion=document.getElementById("remove_buttion_"+id);
	remove_buttion.style.display="none";
	cropped_block7.style.display="none";
	file_input_block7.value=null;
	remove_image.value="no";
}
function remove_div(ids){
	var delete_item=document.getElementById("delete_item_"+ids).value;
	var div_block7=document.getElementById("div_block7_"+ids);
	div_block7.remove();
	$("form").append('<input type="hidden" name="delete_item[]" value="'+delete_item+'" >');
	var count = $(".count_class").length;
		if(count>2){
		$("#add_menu_item").hide();
		}else{
		$("#add_menu_item").show();
		}
}

CKEDITOR.replace("value_description_des");
CKEDITOR.replace("value_description");
document.querySelector("#file_input_block7").addEventListener("change",function(e){	debugger; var t=document.querySelector(".result_block7"),r=(document.querySelector(".img-result_block"),document.querySelector(".img-w_block7"),document.querySelector(".img-h_block7"),document.querySelector(".options_block7"));document.querySelector(".save_block7"),document.querySelector(".cropped_block7"),document.querySelector("#file_input_block7");if(e.target.files.length){var o=new FileReader;o.onload=function(e){e.target.result&&($(".save_block7").show(),$("#box-2_block7").show(),$(".cropped_block7").hide(),img=document.createElement("img"),img.id="image",img.src=e.target.result,t.innerHTML="",t.appendChild(img),r.classList.remove("hide"),cropper_block7 = new Cropper(img,{aspectRatio: 417 / 391}))},o.readAsDataURL(e.target.files[0])}}),document.querySelector(".save_block7").addEventListener("click",function(e){document.querySelector(".result_block7");var t=document.querySelector(".img-result_block7"),r=document.querySelector(".img-w_block7"),o=(document.querySelector(".img-h_block7"),document.querySelector(".options_block7"),document.querySelector(".save_block7"),document.querySelector(".cropped_block7"));document.querySelector("#file_input_block7");e.preventDefault();var c=cropper_block7.getCroppedCanvas({width:r.value}).toDataURL();o.classList.remove("hide"),t.classList.remove("hide"),o.src=c;document.querySelector("#block7_menu_file").value=c;document.getElementById("image_new_main").value=c;});

/*****************adding multiple values to chart*****************************/
$(document).on("click",".add_chart_val",function(e){
	e.preventDefault();
	var idd=$(this).attr("data-id");
	var cnnt=$("#count_chart_id_"+idd).val();
	nn=idd+""+cnnt;
	str='<tr id="remove_chart_val_'+nn+'" data-id="'+nn+'"><td><input type="text" class="chart_heading" name="chart_heading['+idd+'][]" value="" required="required" /></td><td><input type="text" class="chart_value" name="chart_value['+idd+'][]" value=""  required="required" /></td><td><input type="color" name="chart_bgcolor['+idd+'][]" class="chart_bgcolor"/></td><td><a data-id="'+nn+'" class="remove_chart_val" style="cursor:pointer">Remove</a></td></tr>';
	$("#addMoreVal"+idd).append(str);
	$("#count_chart_id_"+idd).val(parseInt(cnnt)+1);
});
$(document).on("click",".remove_chart_val",function(e){
	e.preventDefault();
	var idd=$(this).attr("data-id");
	$("#remove_chart_val_"+idd).remove();
	
});
$(document).on("click",".preview",function(e){
	var idd=$(this).attr("data-id");
	var id=$(this).attr("data-idd");
	var all_val=[];
		var heading=[];
		var values=[];
		var colors=[];
		$("#addMoreVal"+idd+" .chart_heading").each(function(val){
			heading.push($(this).val());
		});
		$("#addMoreVal"+idd+" .chart_value").each(function(val){
			values.push($(this).val());
		});
		$("#addMoreVal"+idd+" .chart_bgcolor").each(function(val){
			colors.push($(this).val());
		});
		all_val.push({id,heading,values,colors});
		
		google.charts.load("current", {packages:["corechart"]});
		 var str=[];
		 var i=0;
      //google.charts.setOnLoadCallback(drawchart);
	  google.charts.setOnLoadCallback(function() { drawchart(idd,heading,values,colors); });
     
		
	});

	function drawchart(idd,heading,values,colors)
	{
		var dt="['Task', 'Hours per Day'],";
		var value=[['Task', 'Hours per Day']]
		val=""+dt;
		for(var i=0;i<heading.length;i++)
		{
		var val=[];
			val.push(heading[i],parseInt(values[i]));
			value.push(val);
			//val.push(d);
			
		}
		val +="";
 var data = google.visualization.arrayToDataTable(value);		
		/* var data = google.visualization.arrayToDataTable(val);		 */
        var options = {
          pieHole: 0.4,
		  colors: colors,
        };

        var chart = new google.visualization.PieChart(document.getElementById('new_chart_'+idd));
        chart.draw(data, options);
      }


  
  
  (function($) {
  $.fn.inputFilter = function(inputFilter) {
    return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
      if (inputFilter(this.value)) {
        this.oldValue = this.value;
        this.oldSelectionStart = this.selectionStart;
        this.oldSelectionEnd = this.selectionEnd;
      } else if (this.hasOwnProperty("oldValue")) {
        this.value = this.oldValue;
        this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
      }
    });
  };
}(jQuery));
$(".chart_value").inputFilter(function(value) {
return /^\d*$/.test(value); });

<?php echo $script; ?>
</script>
