/*
* Author : Ali Aboussebaba
* Email : bewebdeveloper@gmail.com
* Website : http://www.bewebdeveloper.com
* Subject : Crop photo using PHP and jQuery
*/

// the target size
var TARGET_W = 300;
var TARGET_H = 300;
var jj = jQuery.noConflict();
// show loader while uploading photo
function submit_photo() {
	// display the loading texte
	jj('#loading_progress').html('<img src="images/loader.gif"> Uploading your photo...');
}

// show_popup : show the popup
function show_popup(id) {
	// show the popup
	jj('#'+id).show();
}

// close_popup : close the popup
function close_popup(id) {
	// hide the popup
	jj('#'+id).hide();
}

// show_popup_crop : show the crop popup
function show_popup_crop(url) {
	// change the photo source
	jj('#cropbox').attr('src', url);
	// destroy the Jcrop object to create a new one
	try {
		jcrop_api.destroy();
	} catch (e) {
		// object not defined
	}
	// Initialize the Jcrop using the TARGET_W and TARGET_H that initialized before
    jj('#cropbox').Jcrop({
      aspectRatio: TARGET_W / TARGET_H,
      setSelect:   [ 100, 100, TARGET_W, TARGET_H ],
      onSelect: updateCoords
    },function(){
        jcrop_api = this;
    });

    // store the current uploaded photo url in a hidden input to use it later
	jj('#photo_url').val(url);
	// hide and reset the upload popup
	jj('#popup_upload').hide();
	jj('#loading_progress').html('');
	jj('#photo').val('');

	// show the crop popup
	jj('#popup_crop').show();
}

// crop_photo : 
function crop_photo() {
	var x_ = jj('#x').val();
	var y_ = jj('#y').val();
	var w_ = jj('#w').val();
	var h_ = jj('#h').val();
	var photo_url_ = jj('#photo_url').val();

	// hide thecrop  popup
	jj('#popup_crop').hide();

	// display the loading texte
	jj('#photo_container').html('<img src="images/loader.gif"> Processing...');
	// crop photo with a php file using ajax call
	jj.ajax({
		url: 'crop_photo.php',
		type: 'POST',
		data: {x:x_, y:y_, w:w_, h:h_, photo_url:photo_url_, targ_w:TARGET_W, targ_h:TARGET_H},
		success:function(data){
			// display the croped photo
			jj('#photo_container').html(data);
		}
	});
}

// updateCoords : updates hidden input values after every crop selection
function updateCoords(c) {
	jj('#x').val(c.x);
	jj('#y').val(c.y);
	jj('#w').val(c.w);
	jj('#h').val(c.h);
}

