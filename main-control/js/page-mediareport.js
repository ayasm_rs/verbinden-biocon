$(document).ready(function(){
  

$("#art_title").click(function(){
	$("#art_title").removeClass("form-control errortxtbox");	
    $("#art_title").addClass("form-control");
	$("#art_titleerror").text("");
});
$("#art_description").click(function(){
	$("#art_description").removeClass("form-control errortxtbox");	
    $("#art_description").addClass("form-control");
	$("#art_descriptionerror").text("");
});
$("#art_url").click(function(){
	$("#art_url").removeClass("form-control errortxtbox");	
    $("#art_url").addClass("form-control");
	$("#art_urlerror").text("");
});
$("#art_meta_description").click(function(){
	$("#art_meta_description").removeClass("form-control errortxtbox");	
    $("#art_meta_description").addClass("form-control");
	$("#art_meta_descriptionerror").text("");
});
$("#art_page_title").click(function(){
	$("#art_page_title").removeClass("form-control errortxtbox");	
    $("#art_page_title").addClass("form-control");
	$("#art_page_titleerror").text("");
});
       
	$(document).on('click',"#sub_btn",function(){
		/* $('.order_no').each(function() {
			var id=$(this).attr("w_id");
			var order_no=$(this).val();
			var val_details=$("#order_details").val();
			val_details+=id+"-"+order_no+",";
			$("#order_details").attr('value',val_details);
		}); */
                
                var xxfrm        = $("#xxfrm");
                
                var art_title       = $("#art_title");
                var art_titleerror  = $("#art_titleerror");
				
                var art_url       = $("#art_url");
                var art_urlerror  = $("#art_urlerror");
                
                var art_description       = $("#art_description");
                var art_descriptionerror  = $("#art_descriptionerror");
                
                var art_meta_description       = $("#art_meta_description");
                var art_meta_descriptionerror  = $("#art_meta_descriptionerror");
                
                var art_page_title       = $("#art_page_title");
                var art_page_titleerror  = $("#art_page_titleerror");
                
                var news_description       = $("#news_description");
                var news_descriptionerror  = $("#news_descriptionerror");
                
                
                var fileerror            = $("#fileerror");
                
				
                 if(validateart_url() & validateart_title() & validateart_description())
                {
                    xxfrm.submit();
                } 
                
                
                function validateart_title()
                {
                  if(art_title.val().length<1)
                   {

                     art_title.removeClass("form-control");		
                     art_title.addClass("form-control errortxtbox");
                     art_titleerror.text("Please enter your media report heading");

                     return false;	
                   }
                   else
                   {
                     art_title.removeClass("form-control errortxtbox");	
                     art_title.addClass("form-control");		
                     art_titleerror.text("");	

                     return true;
                   }    
                   
                } ///////////////
                
                function validateart_url()
                {
                  if(art_url.val().length<1)
                   {

                     art_url.removeClass("form-control");		
                     art_url.addClass("form-control errortxtbox");
                     art_urlerror.text("Please enter your media report URL");

                     return false;	
                   }
                   else
                   {
                     art_url.removeClass("form-control errortxtbox");	
                     art_url.addClass("form-control");		
                     art_urlerror.text("");	

                     return true;
                   }    
                   
                } ///////////////
                
				function validateart_meta_description()
                {
                  if(art_meta_description.val().length<1)
                   {

                     art_meta_description.removeClass("form-control");		
                     art_meta_description.addClass("form-control errortxtbox");
                     art_meta_descriptionerror.text("Please enter your Media report meta Description");

                     return false;	
                   }
                   else
                   {
                     art_meta_description.removeClass("form-control errortxtbox");	
                     art_meta_description.addClass("form-control");		
                     art_meta_descriptionerror.text("");	

                     return true;
                   }    
                   
                } ///////////////
                
				function validateart_page_title()
                {
                  if(art_page_title.val().length<1)
                   {

                     art_page_title.removeClass("form-control");		
                     art_page_title.addClass("form-control errortxtbox");
                     art_page_titleerror.text("Please enter your Page title");

                     return false;	
                   }
                   else
                   {
                     art_page_title.removeClass("form-control errortxtbox");	
                     art_page_title.addClass("form-control");		
                     art_page_titleerror.text("");	

                     return true;
                   }    
                   
                } ///////////////
                
				function validateart_description()
                {
                  if(art_description.val().length<1)
                   {

                     art_description.removeClass("form-control");		
                     art_description.addClass("form-control errortxtbox");
                     art_descriptionerror.text("Please enter your short description");

                     return false;	
                   }
                   else
                   {
                     art_description.removeClass("form-control errortxtbox");	
                     art_description.addClass("form-control");		
                     art_descriptionerror.text("");	

                     return true;
                   }    
                   
                } ///////////////
                
                
                                
                return false;
	});
 
});

var fileCount=-1;
var output = document.getElementById("result");
