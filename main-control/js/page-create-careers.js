
$("#file").change(function(event) {
        
        var fileerror		= $("#fileerror");
        
        var names = [];
        var filenamestr = "";
        var fileExtension = ['jpeg', 'jpg', 'png', 'gif'];
        
        var errorstrvalid = "";
        
        for (var i = 0; i < $(this).get(0).files.length; ++i) {
            
            filenamestr = $(this).get(0).files[i].name;
            
            if(filenamestr.split('.').pop().toLowerCase()!="jpeg" & filenamestr.split('.').pop().toLowerCase()!="jpg" & filenamestr.split('.').pop().toLowerCase()!="gif" & filenamestr.split('.').pop().toLowerCase()!="png"){
                errorstrvalid = "error";
            }
            
        }
        
        if(errorstrvalid=="error"){
            
           fileerror.text("Only the following formats are allowed : jpeg, jpg, png, gif");
           return false;  
            
        }
        else
        {
            fileerror.text("");
            
            $("#uploadIcon").css("display","none");
            var fd = new FormData(); 
            var output = document.getElementById("result");
            fd.append('action', 'multipleImageUpload');
            jQuery.each(jQuery('#file')[0].files, function(i, file) {
                    fd.append('image[]', file);
            });	
            $.ajax({
                    url: "ajaxCalls.php", // Url to which the request is send
                    type: "POST",             // Type of request to be send, called as method
                    data: fd, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
                    contentType: false,       // The content type used when sending data to the server.
                    cache: false,             // To unable request pages to be cached
                    processData:false,        // To send DOMDocument or non processed data file it is set to false
                    success: function(data)   // A function to be called if request succeeds
                    {
                            console.log(data);
                            $("#result").append(data);
                    }
            });
            
            return true;
        } 
        
});

$(document).ready(function(){
   
        $('#xxfrm input').keypress(function (e) {
            if (e.which == 13) {
                $("#sub_btn1").click();
            }
        });
        
        $('#page_order_no').keypress(function (e) {
            if (e.which == 46 || (e.which>=48 & e.which<=57) ) {
                return true;
            }
            else
            {
                return false;
            }    
        });
        
        $(document).on('click',"#sub_btn1",function(){
            var xxfrm             = $("#xxfrm");
            
            var pagetitle         = $("#page_title");
            var pagetitleerror    = $("#pagetitleerror");
            
            var Category           = $("#Category");
            var categoryerror      = $("#categoryerror");
            
            var pageurl           = $("#page_url");
            var pageurlerror      = $("#pageurlerror");
            
            var locations       = $("#location");
            var locationerror  = $("#locationerror");
            
            var h1       = $("#h1");
            var jobtitleerror  = $("#jobtitleerror");
            
            var pageTypeerror     = $("#pageTypeerror");
            
            //alert("pageType :: "+$('input[name=pageType]:checked').val());
            
            if(validatejobtitleerror() & validateCategory() & validatepageurlfld() & validatepagenofld())
            {
               xxfrm.submit();
            }
            else
            {
                return false;
            }    
            
            
            
                function validatejobtitleerror()
                {

                   if(h1.val().length<1)
                   {

                     h1.removeClass("form-control");		
                     h1.addClass("form-control errortxtbox");
                     jobtitleerror.text("Please enter job title");

                     return false;	
                   }
                   else
                   {
                     h1.removeClass("form-control errortxtbox");	
                     h1.addClass("form-control");		
                     jobtitleerror.text("");	

                     return true;
                   }  

                } // function validatepagetitlefld(){
                 
				 function validateCategory()
                {

                   if(Category.val().length<1)
                   {

                     Category.removeClass("form-control");		
                     Category.addClass("form-control errortxtbox");
                     categoryerror.text("Please select category");

                     return false;	
                   }
                   else
                   {
                     Category.removeClass("form-control errortxtbox");	
                     Category.addClass("form-control");		
                     categoryerror.text("");	

                     return true;
                   }  

                } // function validatepagetitlefld(){
                
                function validatepagetitlefld()
                {

                   if(pagetitle.val().length<1)
                   {

                     pagetitle.removeClass("form-control");		
                     pagetitle.addClass("form-control errortxtbox");
                     pagetitleerror.text("Please enter page title");

                     return false;	
                   }
                   else
                   {
                     pagetitle.removeClass("form-control errortxtbox");	
                     pagetitle.addClass("form-control");		
                     pagetitleerror.text("");	

                     return true;
                   }  

                } // function validatepagetitlefld(){
                
                function validatepageurlfld()
                {
                  if(pageurl.val().length<1)
                   {

                     pageurl.removeClass("form-control");		
                     pageurl.addClass("form-control errortxtbox");
                     pageurlerror.text("Please enter page url");

                     return false;	
                   }
                   else
                   {
                     pageurl.removeClass("form-control errortxtbox");	
                     pageurl.addClass("form-control");		
                     pageurlerror.text("");	

                     return true;
                   }    
                   
                } // function validatepageurlfld()
                
                function validatepagecategoryfld()
                {
                  
                   if(pagecategory.val().length<1)
                   {

                     pagecategory.removeClass("form-control");		
                     pagecategory.addClass("form-control errortxtbox");
                     pagecategoryerror.text("Please enter page category");

                     return false;	
                   }
                   else
                   {
                     pagecategory.removeClass("form-control errortxtbox");	
                     pagecategory.addClass("form-control");		
                     pagecategoryerror.text("");	

                     return true;
                   }    
                  
                  
                } // function validatepagecategoryfld()
                
                function validatepagenofld()
                {
                
                   if(locations.val().length<1)
                   {

                     locations.removeClass("form-control");		
                     locations.addClass("form-control errortxtbox");
                     locationerror.text("Please enter location");

                     return false;	
                   }
                   else
                   {
                     locations.removeClass("form-control errortxtbox");	
                     locations.addClass("form-control");		
                     locationerror.text("");	

                     return true;
                   }     
                
                } // function validatepagenofld()
                
                function validatepagetype()
                {
                
                    var pageType = $('input[name=pageType]:checked').val();
                    
                    if(pageType==undefined)
                    {
                        pageTypeerror.text("Please select page type");
                        return false;	 
                    }
                    else
                    {
                        pageTypeerror.text("");
                        return true;	 
                    }    
                    
                
                } // function validatepagetype()
                
                function validateprofileimgfld()
                {
                
                    var fileerror		= $("#fileerror");
        
                    var names = [];
                    var filenamestr = "";
                    var fileExtension = ['jpeg', 'jpg', 'png', 'gif'];

                    var errorstrvalid = "";

                    for (var i = 0; i < $("#file").get(0).files.length; ++i) {

                        filenamestr = $("#file").get(0).files[i].name;

                        if(filenamestr.split('.').pop().toLowerCase()!="jpeg" & filenamestr.split('.').pop().toLowerCase()!="jpg" & filenamestr.split('.').pop().toLowerCase()!="gif" & filenamestr.split('.').pop().toLowerCase()!="png"){
                            errorstrvalid = "error";
                        }

                    }
                    
                    if(errorstrvalid=="error"){
            
                        fileerror.text("Only the following formats are allowed : jpeg, jpg, png, gif");
                        return false;  

                     }
                     else
                     {
                        fileerror.text("");
                        return true;
                     }   
                    
                    
                
                } // function validateprofileimgfld(){
                
                
           
        });
       
});