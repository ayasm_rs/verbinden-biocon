$(document).ready(function(){
  
        /* $('#xxfrm input').keypress(function (e) {
            if (e.which == 13) {
                $("#sub_btn").click();
            }
        }); */
$("#pro_category").click(function(){
	$("#pro_category").removeClass("form-control errortxtbox");	
    $("#pro_category").addClass("form-control");
	$("#pro_categoryerror").text("");
});
$("#pro_name").click(function(){
	$("#pro_name").removeClass("form-control errortxtbox");	
    $("#pro_name").addClass("form-control");
	$("#pro_nameerror").text("");
});
$("#pro_description").click(function(){
	$("#pro_description").removeClass("form-control errortxtbox");	
    $("#pro_description").addClass("form-control");
	$("#pro_descriptionerror").text("");
});
$("#pro_page_url").click(function(){
	$("#pro_page_url").removeClass("form-control errortxtbox");	
    $("#pro_page_url").addClass("form-control");
	$("#pro_page_urlerror").text("");
});

CKEDITOR.instances['pro_keybenefits'].on('contentDom', function() {
    this.document.on('click', function(event){
        $("#pro_keybenefitserror").text("");
     });
});
CKEDITOR.instances['pro_specification'].on('contentDom', function() {
    this.document.on('click', function(event){
        $("#pro_specificationerror").text("");
     });
});
CKEDITOR.instances['pro_overview'].on('contentDom', function() {
    this.document.on('click', function(event){
        $("#pro_overviewerror").text("");
     });
});        
	$(document).on('click',"#sub_btn",function(){
		/* $('.order_no').each(function() {
			var id=$(this).attr("w_id");
			var order_no=$(this).val();
			var val_details=$("#order_details").val();
			val_details+=id+"-"+order_no+",";
			$("#order_details").attr('value',val_details);
		}); */
                
                var xxfrm        = $("#xxfrm");
                
                var pro_category         = $("#pro_category");
                var pagetitleerror    = $("#pro_categoryerror");
                
                var pro_name       = $("#pro_name");
                var pro_nameerror  = $("#pro_nameerror");
				
                var pro_image       = $("#pro_image");
                var pro_imageerror  = $("#pro_imageerror");
                
                var pro_description       = $("#pro_description");
                var pro_descriptionerror  = $("#pro_descriptionerror");
                
                var pro_page_url       = $("#pro_page_url");
                var pro_page_urlerror  = $("#pro_page_urlerror");
                
                var pro_overview       = $("#pro_overview");
                var pro_overviewerror  = $("#pro_overviewerror");
                
                var pro_keybenefits       = $("#pro_keybenefits");
                var pro_keybenefitserror  = $("#pro_keybenefitserror");
                
                var pro_specification       = $("#pro_specification");
                var pro_specificationerror  = $("#pro_specificationerror");
                
                var fileerror            = $("#fileerror");
                
				
                 if(validatepro_category() & validatepro_name() & validatepro_page_url() & validatepro_description() & validatepro_image())
                {
                    xxfrm.submit();
                } 
                
				function validatepro_image()
                {
                   if(pro_image.val().length<1)
                   {

                     pro_image.removeClass("form-control");		
                     pro_image.addClass("form-control errortxtbox");
                     pro_imageerror.text("Please select the product image");

                     return false;	
                   }
                   else
                   {
                     pro_image.removeClass("form-control errortxtbox");	
                     pro_image.addClass("form-control");		
                     pro_imageerror.text("");	

                     return true;
                   }  

                } ///////////////
				
                function validatepro_category()
                {

                   if(pro_category.val().length<1)
                   {

                     pro_category.removeClass("form-control");		
                     pro_category.addClass("form-control errortxtbox");
                     pagetitleerror.text("Please enter or select the product category");

                     return false;	
                   }
                   else
                   {
                     pro_category.removeClass("form-control errortxtbox");	
                     pro_category.addClass("form-control");		
                     pagetitleerror.text("");	

                     return true;
                   }  

                } ///////////////
                
                function validatepro_name()
                {
                  if(pro_name.val().length<1)
                   {

                     pro_name.removeClass("form-control");		
                     pro_name.addClass("form-control errortxtbox");
                     pro_nameerror.text("Please enter your product name");

                     return false;	
                   }
                   else
                   {
                     pro_name.removeClass("form-control errortxtbox");	
                     pro_name.addClass("form-control");		
                     pro_nameerror.text("");	

                     return true;
                   }    
                   
                } ///////////////
                
				function validatepro_description()
                {
				//alert(pro_description.val().length<);
                  if(pro_description.val().length<1)
                   {

                     pro_description.removeClass("form-control");		
                     pro_description.addClass("form-control errortxtbox");
                     pro_descriptionerror.text("Please enter your product subtitle");

                     return false;	
                   }
                   else
                   {
                     pro_description.removeClass("form-control errortxtbox");	
                     pro_description.addClass("form-control");		
                     pro_descriptionerror.text("");	

                     return true;
                   }    
                   
                } ///////////////
                
				function validatepro_page_url()
                {
                  if(pro_page_url.val().length<1)
                   {

                     pro_page_url.removeClass("form-control");		
                     pro_page_url.addClass("form-control errortxtbox");
                     pro_page_urlerror.text("Please enter your page name");

                     return false;	
                   }
                   else
                   {
                     pro_page_url.removeClass("form-control errortxtbox");	
                     pro_page_url.addClass("form-control");		
                     pro_page_urlerror.text("");	

                     return true;
                   }    
                   
                } ///////////////
                
				/* function validatepro_page_url()
                {
                  if(pro_page_url.val().length<1)
                   {

                     pro_page_url.removeClass("form-control");		
                     pro_page_url.addClass("form-control errortxtbox");
                     pro_page_urlerror.text("Please enter your page name");

                     return false;	
                   }
                   else
                   {
                     pro_page_url.removeClass("form-control errortxtbox");	
                     pro_page_url.addClass("form-control");		
                     pro_page_urlerror.text("");	

                     return true;
                   }    
                   
                } /////////////// */
                
				function validatepro_overview()
                {
					//alert(CKEDITOR.instances.pro_overview.document.getBody().getChild(0).getText().length);
                  if(CKEDITOR.instances.pro_overview.document.getBody().getChild(0).getText().length<2)
                   {

                     pro_overview.removeClass("form-control");		
                     pro_overview.addClass("form-control errortxtbox");
                     pro_overviewerror.text("Please enter your product overview");

                     return false;	
                   }
                   else
                   {
                     pro_overview.removeClass("form-control errortxtbox");	
                     pro_overview.addClass("form-control");		
                     pro_overviewerror.text("");	

                     return true;
                   }    
                   
                } ///////////////
                
				function validatepro_keybenefits()
                {
					//alert(CKEDITOR.instances.pro_overview.document.getBody().getChild(0).getText().length);
                  if(CKEDITOR.instances.pro_keybenefits.document.getBody().getChild(0).getText().length<2)
                   {

                     pro_keybenefits.removeClass("form-control");		
                     pro_keybenefits.addClass("form-control errortxtbox");
                     pro_keybenefitserror.text("Please enter your product key benefits");

                     return false;	
                   }
                   else
                   {
                     pro_keybenefits.removeClass("form-control errortxtbox");	
                     pro_keybenefits.addClass("form-control");		
                     pro_keybenefitserror.text("");	

                     return true;
                   }    
                   
                } ///////////////
                
				function validatepro_specification()
                {
					//alert(CKEDITOR.instances.pro_overview.document.getBody().getChild(0).getText().length);
                  if(CKEDITOR.instances.pro_specification.document.getBody().getChild(0).getText().length<2)
                   {

                     pro_specification.removeClass("form-control");		
                     pro_specification.addClass("form-control errortxtbox");
                     pro_specificationerror.text("Please enter your product specification");

                     return false;	
                   }
                   else
                   {
                     pro_specification.removeClass("form-control errortxtbox");	
                     pro_specification.addClass("form-control");		
                     pro_specificationerror.text("");	

                     return true;
                   }    
                   
                } ///////////////
                
                return false;
	});
 
});

var fileCount=-1;
var output = document.getElementById("result");
/* $("#pro_image").change(function(event) {
        var fileerror		= $("#fileerror");
        
        var names = [];
        var filenamestr = "";
        var fileExtension = ['jpeg', 'jpg', 'png', 'gif'];
        
        var errorstrvalid = "";
        
        for (var i = 0; i < $(this).get(0).files.length; ++i) {
            
            filenamestr = $(this).get(0).files[i].name;
            
            if(filenamestr.split('.').pop().toLowerCase()!="jpeg" & filenamestr.split('.').pop().toLowerCase()!="jpg" & filenamestr.split('.').pop().toLowerCase()!="gif" & filenamestr.split('.').pop().toLowerCase()!="png"){
                errorstrvalid = "error";
            }
            
        }
        
        if(errorstrvalid=="error"){
            
           fileerror.text("Only the following formats are allowed : jpeg, jpg, png, gif");
           return false;  
            
        }
        else
        {
                fileerror.text("");
                
                
                $("#uploadIcon").css("display","none");
                var fd = new FormData(); 
                var output = document.getElementById("result");
                fd.append('action', 'multipleImageUpload');
                jQuery.each(jQuery('#file')[0].files, function(i, file) {
                        fd.append('image[]', file);
                });
                
                
                
                $.ajax({
                        url: "ajaxCalls.php", // Url to which the request is send
                        type: "POST",             // Type of request to be send, called as method
                        data: fd, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
                        contentType: false,       // The content type used when sending data to the server.
                        cache: false,             // To unable request pages to be cached
                        processData:false,        // To send DOMDocument or non processed data file it is set to false
                        success: function(data)   // A function to be called if request succeeds
                        {
                                //console.log(data);
                                $("#result").append(data);
                        }
                });
                
                return true;
                
        }  // if(errorstrvalid=="error"){  
        
        
        
        
              
}); */