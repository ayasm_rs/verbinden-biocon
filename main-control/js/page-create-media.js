$(document).ready(function(){
       
        $('#xxfrm input').keypress(function (e) {
            if (e.which == 13) {
                $("#sub_btn").click();
            }
        });
        
        $(document).on('click',"#sub_btn",function(){
           
            var xxfrm             = $("#xxfrm");
            
            var mediatitle         = $("#media_title");
            var mediatitleerror    = $("#mediatitleerror");
            
            
            

            
            if(validatepagetitlefld())
            {
               xxfrm.submit();
            }
            else
            {
                return false;
            }    
            
            
            
                function validatepagetitlefld()
                {

                   if(mediatitle.val().length<1)
                   {

                     mediatitle.removeClass("form-control");		
                     mediatitle.addClass("form-control errortxtbox");
                     mediatitleerror.text("Please enter title");

                     return false;	
                   }
                   else
                   {
                     mediatitle.removeClass("form-control errortxtbox");	
                     mediatitle.addClass("form-control");		
                     mediatitleerror.text("");	

                     return true;
                   }  

                } // function validatepagetitlefld(){
                
                
                
                
                
                
           
        });
       
});