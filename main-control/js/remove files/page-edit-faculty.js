$(document).ready(function(){
    
 $(document).on("focusout","#username",function(){
      
        var username          = $("#username");
        var usernameerror     = $("#usernameerror");
         
        if(validateusernamefld() & checkavaliableusernamefld())
        {
          return false;  
        }    
        
        return false;
         
        function validateusernamefld(){
              
              if(username.val().length<1)
              {
					
                username.removeClass("form-control");		
                username.addClass("form-control errortxtbox");
                usernameerror.text("Please enter your user name");
					
                return false;	
              }
              else
              {
                username.removeClass("form-control errortxtbox");	
                username.addClass("form-control");		
                usernameerror.text("");	
					
                return true;
              }  
              
           } //  function validateusernamefld(){ 
           
           function checkavaliableusernamefld()
           {
           
              var ajaxResponse = true;
              
              if(usernameerror.text()=="")
              {
                  $.ajax({
                        url: "ajaxcalls.php",
                        type: "POST",                        
                        data: {"username":username.val(),"action":"usernameCheckEdit","hidEditId":$("#hidEditId").val()} ,
                        async: false, 
                        success: function(data) {
                            
                            String.prototype.trim = function() {
                                return this.replace(/^\s+|\s+$/g, "");
                            };
                            data = data.trim();
                            if(data=="0")
                            {
                                username.removeClass("form-control error");	
                                username.addClass("form-control");		
                                usernameerror.text("");										
                                ajaxResponse = true;


                            }
                            else
                            {
                                username.removeClass("form-control");		
                                username.addClass("form-control error");
                                usernameerror.text("Username already Exist");										
                                ajaxResponse = false;	
                            }



                        }
                    });
                  
                  return ajaxResponse;
              }    
              else
              {
                  return ajaxResponse;
              }    
           
           } // function checkavaliableusernamefld()
           
           
  });
  
  $(document).on("focusout","#password",function(){
      var password          = $("#password");
      var passworderror     = $("#passworderror");
      
      if(validatepasswordfld())
      {
          return false;
      }
      
        function validatepasswordfld()
        {
           
            var a = password.val();
            
            if(a.length<1)
            {
                    password.removeClass("form-control errortxtbox");                	
                    password.addClass("form-control");		
                    passworderror.text("");
                    
                    return true;
            }
            else
            {
		var paswd=  /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[^\w\s]).{8,15}$/;  
                if(password.val().match(paswd))
                {
                    password.removeClass("form-control errortxtbox");                	
                    password.addClass("form-control");		
                    passworderror.text("");
                    
                    return true;
                }
                else
                {
                    password.removeClass("form-control");		
                    password.addClass("form-control errortxtbox");
                    passworderror.text("Password should contain (8 to 15 characters which should include at least one lowercase, one uppercase, one numeric digit and one special character)");
                    return false;				
                }    
            }   
           
        } // function validatepasswordfld() 
      
      return false;

  }); 
  
  $(document).on("focusout","#password1",function(){
      var password          = $("#password");
      var passworderror     = $("#passworderror");
           
      var confirmpassword      = $("#password1");
      var confirmpassworderror = $("#confirmpassworderror");
      var confirmpasswordhint  = $("#confirmpasswordhint");
      
        if(validateconfirmpasswordfld() & verifybothpassword()) 
        {
           return false; 
        }  
        
        return false;
        
           function validateconfirmpasswordfld()
           {
                var a = confirmpassword.val();
            
                if(a.length<1)
                {
                        confirmpassword.removeClass("form-control errortxtbox");                	
                        confirmpassword.addClass("form-control");		
                        confirmpassworderror.text("");
                        confirmpasswordhint.removeClass("password-error errorcls");
                        confirmpasswordhint.addClass("password-error");
                        return true;
                }
                else
                {
                    var paswd=  /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[^\w\s]).{8,15}$/;  
                    if(confirmpassword.val().match(paswd))
                    {
                        confirmpassword.removeClass("form-control errortxtbox");                	
                        confirmpassword.addClass("form-control");		
                        confirmpassworderror.text("");
                        confirmpasswordhint.removeClass("password-error errorcls");
                        confirmpasswordhint.addClass("password-error");
                        return true;
                    }
                    else
                    {
                        confirmpassword.removeClass("form-control");		
                        confirmpassword.addClass("form-control errortxtbox");
                        confirmpassworderror.text("");
                        
                        confirmpasswordhint.removeClass("password-error");
                        confirmpasswordhint.addClass("password-error errorcls");
                    
                        return false;				
                    }    
                }    
           } // function validateconfirmpasswordfld(){   
           
           function verifybothpassword()
           {
               var conpwdcls = confirmpasswordhint.attr('class');
               
               var a   = password.val();
               var aa  = confirmpassword.val();
               
               if(confirmpassworderror.text()=="" & passworderror.text()=="" & conpwdcls.indexOf('errorcls')=="-1")
               {
                    if(a!=aa)
                    {

                         password.removeClass("form-control");		
                         password.addClass("form-control errortxtbox");

                         confirmpassword.removeClass("form-control");		
                         confirmpassword.addClass("form-control errortxtbox");
                         confirmpassworderror.text("Password and confirm password mismatch");						
                         return false;

                    } 
                    else
                    {
                         password.removeClass("form-control errortxtbox");		
                         password.addClass("form-control");

                         confirmpassword.removeClass("form-control errortxtbox");		
                         confirmpassword.addClass("form-control");
                         confirmpassworderror.text("");						
                         return true;
                    }    
               }
               else
               {
                   return true;
               } 
               
           } // function verifybothpassword()
           
  });
  
	  $('#phone_number').keypress(function (e) {
				
		var charCode = (e.which) ? e.which : e.keyCode;
		
		if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)){
			return false;
		}
		else
		{
			return true;
		}
		
				
	  });
  
  $('#xxfrm input').keypress(function (e) {
            if (e.which == 13) {
                $("#sub_btn").click();
            }
  });
  
  $(document).on("click","#sub_btn",function(){
	   
           var xxfrm        = $("#xxfrm");
           
           var name         = $("#name");
           var nameerror    = $("#nameerror");
           
           var email        = $("#email");
           var emailerror   = $("#emailerror");
           
           var username          = $("#username");
           var usernameerror     = $("#usernameerror");
           
           var password          = $("#password");
           var passworderror     = $("#passworderror");
           
           var confirmpassword      = $("#password1");
           var confirmpassworderror = $("#confirmpassworderror");
           
           var confirmpasswordhint  = $("#confirmpasswordhint");
           
           var fileerror            = $("#fileerror");
		   
           var highDesignation			= $("#highDesignation");
		   var highDesignationerror		= $("#highDesignationerror");
           
           if(validatenamefld() & validateemailfld() & validateusernamefld() & validateotherrole() & checkavaliableusernamefld() & validatepasswordfld() & validateconfirmpasswordfld() & verifybothpassword() & validateprofileimgfld())
           {
			   
               
               xxfrm.submit();
           }
           else
           {
               
               return false;
           }    
           
		   function validateotherrole()
		    {
				var aa = $('.sumo_highDesignation > .CaptionCont').attr("title");
				var highcls = $('.sumo_highDesignation > p');
				
				if(aa=="Select Here"){
					highcls.removeClass("CaptionCont SelectBox");		
					highcls.addClass("CaptionCont SelectBox errortxtbox");
					highDesignationerror.text("Please select other roles");					
					return false;		
				}
				else
				{
					highcls.removeClass("CaptionCont SelectBox errortxtbox");		
					highcls.addClass("CaptionCont SelectBox");
					highDesignationerror.text("");
					return true;
				}		

				
				
		    }  
		   
           function validatenamefld()
           {
           
              if(name.val().length<1)
              {
					
                name.removeClass("form-control");		
                name.addClass("form-control errortxtbox");
                nameerror.text("Please enter your name");
					
                return false;	
              }
              else
              {
                name.removeClass("form-control errortxtbox");	
                name.addClass("form-control");		
                nameerror.text("");	
					
                return true;
              }  
           
           } // function validatenamefld() 
           
           function validateemailfld()
           {
                var a = email.val();
               
                if(a.length<1)
                {
                    email.removeClass("form-control errortxtbox");		
                    email.addClass("form-control");
                    emailerror.text("");
                    return true;				
		}
                else
                {
                    var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
					
                    var countdots	= 0;
					
					var n = a.indexOf("@");
					
					if(n!='-1')
					{
						var res 	= a.split("@");					
						
						if(res[1]!="")
						{
							var dotstr 	= res[1];						
							for(i=0;i<dotstr.length;i++)
							{
								if(dotstr.charAt(i)==".")
								{
									countdots++;			
								}
							}
						}
					}
					
                    if(countdots>3)			
                    {
                        email.removeClass("form-control");		
                        email.addClass("form-control errortxtbox");
                        emailerror.text("Please enter a valid email address");						
                        return false;							
                    }
                    else if(filter.test(a))
                    {
                        var ajaxResponse = true;
						
						$.ajax({
                            type: "POST",
							async: false, 
                            url: "ajaxCalls.php",
                            data: {"emailID":email.val(),"action":"emailidCheckEdit","hidEditId":$("#hidEditId").val()} ,
                            success: function(data) {
                                String.prototype.trim = function() {
                                return this.replace(/^\s+|\s+$/g, "");
                                };
                                data = data.trim();
                                  
                                  if(data=="0"){
                                        email.removeClass("form-control errortxtbox");	
                                        email.addClass("form-control");		
                                        emailerror.text("");
                                        ajaxResponse = true;
                                  }else{
									  
                                        email.removeClass("form-control");	
                                        email.addClass("form-control errortxtbox");		
                                        emailerror.text("Email-Id already exists. Please try with different Email Id.");						
                                        ajaxResponse = false;
                                  }
                            }
                        });
                        
                        
                        return ajaxResponse;
								
                    }
                    else
                    {
                        email.removeClass("form-control");		
                        email.addClass("form-control errortxtbox");
                        emailerror.text("Please enter a valid email address");
                        return false;						
                    } 
                }    
               
           } // function validateemailfld()   
           
           
           
           function validateusernamefld(){
              
              if(username.val().length<1)
              {
					
                username.removeClass("form-control");		
                username.addClass("form-control errortxtbox");
                usernameerror.text("Please enter your user name");
					
                return false;	
              }
              else
              {
                username.removeClass("form-control errortxtbox");	
                username.addClass("form-control");		
                usernameerror.text("");	
					
                return true;
              }  
              
           } //  function validateusernamefld(){    
           
           function checkavaliableusernamefld()
           {
           
              var ajaxResponse = true;
              
              if(usernameerror.text()=="")
              {
                  $.ajax({
                        url: "ajaxcalls.php",
                        type: "POST",                        
                        data: {"username":username.val(),"action":"usernameCheckEdit","hidEditId":$("#hidEditId").val()} ,
                        async: false, 
                        success: function(data) {
                            
                            String.prototype.trim = function() {
                                return this.replace(/^\s+|\s+$/g, "");
                            };
                            data = data.trim();
                            
                            if(data=="0")
                            {
                                username.removeClass("form-control error");	
                                username.addClass("form-control");		
                                usernameerror.text("");										
                                ajaxResponse = true;


                            }
                            else
                            {
                                username.removeClass("form-control");		
                                username.addClass("form-control error");
                                usernameerror.text("Username already Exist");										
                                ajaxResponse = false;	
                            }



                        }
                    });
                  
                  return ajaxResponse;
              }    
              else
              {
                  return ajaxResponse;
              }    
           
           } // function checkavaliableusernamefld()   
           
           function validatepasswordfld()
           {
           
            var a = password.val();
            
            if(a.length<1)
            {
                password.removeClass("form-control errortxtbox");                	
                password.addClass("form-control");		
                passworderror.text("");
                    
                return true;
            }
            else
            {
		var paswd=  /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[^\w\s]).{8,15}$/;  
                if(password.val().match(paswd))
                {
                    password.removeClass("form-control errortxtbox");                	
                    password.addClass("form-control");		
                    passworderror.text("");
                    
                    return true;
                }
                else
                {
                    password.removeClass("form-control");		
                    password.addClass("form-control errortxtbox");
                    passworderror.text("Password should contain (8 to 15 characters which should include at least one lowercase, one uppercase, one numeric digit and one special character)");
                    return false;				
                }    
            }   
           
           } // function validatepasswordfld() 
           
           function validateconfirmpasswordfld()
           {
                var a = confirmpassword.val();
            
                if(a.length<1)
                {
                    confirmpassword.removeClass("form-control errortxtbox");                	
                    confirmpassword.addClass("form-control");		
                    confirmpassworderror.text("");
                    confirmpasswordhint.removeClass("password-error errorcls");
                    confirmpasswordhint.addClass("password-error");
                    return true;
                }
                else
                {
                    var paswd=  /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[^\w\s]).{8,15}$/;  
                    if(confirmpassword.val().match(paswd))
                    {
                        confirmpassword.removeClass("form-control errortxtbox");                	
                        confirmpassword.addClass("form-control");		
                        confirmpassworderror.text("");
                        confirmpasswordhint.removeClass("password-error errorcls");
                        confirmpasswordhint.addClass("password-error");
                        return true;
                    }
                    else
                    {
                        confirmpassword.removeClass("form-control");		
                        confirmpassword.addClass("form-control errortxtbox");
                        confirmpassworderror.text("");
                        
                        confirmpasswordhint.removeClass("password-error");
                        confirmpasswordhint.addClass("password-error errorcls");
                    
                        return false;				
                    }    
                }    
           } // function validateconfirmpasswordfld(){   
           
           function verifybothpassword()
           {
               var conpwdcls = confirmpasswordhint.attr('class');
               
               var a   = password.val();
               var aa  = confirmpassword.val();
               
               if(confirmpassworderror.text()=="" & passworderror.text()=="" & conpwdcls.indexOf('errorcls')=="-1")
               {
                    if(a!=aa)
                    {

                         password.removeClass("form-control");		
                         password.addClass("form-control errortxtbox");

                         confirmpassword.removeClass("form-control");		
                         confirmpassword.addClass("form-control errortxtbox");
                         confirmpassworderror.text("Password and confirm password mismatch");						
                         return false;

                    } 
                    else
                    {
                         password.removeClass("form-control errortxtbox");		
                         password.addClass("form-control");

                         confirmpassword.removeClass("form-control errortxtbox");		
                         confirmpassword.addClass("form-control");
                         confirmpassworderror.text("");						
                         return true;
                    }    
               }
               else
               {
                   return true;
               } 
               
           } // function verifybothpassword()
           
           function validateprofileimgfld()
           {
           
                if($('#file').val()=="")
                {
                    fileerror.text("");
                    return true;
                }
                else 
                {
                    var fileExtension = ['jpeg', 'jpg', 'png', 'gif'];
                    if ($.inArray($('#file').val().split('.').pop().toLowerCase(), fileExtension) == -1) {				
                       fileerror.text("Only the following formats are allowed : jpeg, jpg, png, gif");
                       return false; 
                    }
                    else
                    {
                        fileerror.text("");
                        return true;
                    }
                }
           
           } // function validateprofileimgfld()   
           
           return false;
           
           
	});   
});