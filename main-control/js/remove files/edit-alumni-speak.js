$(document).ready(function(){
       
        $('#xxfrm input').keypress(function (e) {
            if (e.which == 13) {
                $("#sub_btn").click();				
				return false;
				
            }
        });        
        
        
        $(document).on('click',"#sub_btn",function(){
           
            var xxfrm             = $("#xxfrm");
            
            var pagetitle         = $("#calendar_title");
            var pagetitleerror    = $("#calendar_titleerror");
                       
            var fileerror            = $("#fileerror");
            
            if(validatepagetitlefld() & validateprofileimgfld())
            {
				
               xxfrm.submit();
			   
            }
            else
            {
                return false;
            }    
            
            
            
                function validatepagetitlefld()
                {

                   if(pagetitle.val().length<1)
                   {

                     pagetitle.removeClass("form-control");		
                     pagetitle.addClass("form-control errortxtbox");
                     pagetitleerror.text("Please enter title");

                     return false;	
                   }
                   else
                   {
                     pagetitle.removeClass("form-control errortxtbox");	
                     pagetitle.addClass("form-control");		
                     pagetitleerror.text("");	

                     return true;
                   }  

                } // function validatepagetitlefld(){
                
                               
                function validateprofileimgfld()
			   {
			   
					if($('#file').val()=="")
					{
						fileerror.text("");
						return true;
					}
					else 
					{
						if(fileerror.text()!="")
						{
							return false;
						}
						else
						{
							var fileExtension = ['jpeg', 'jpg', 'png', 'gif'];
							if ($.inArray($('#file').val().split('.').pop().toLowerCase(), fileExtension) == -1) {				
							   fileerror.text("Only the following formats are allowed : jpeg, jpg, png, gif");
							   return false; 
							}
							else
							{
								fileerror.text("");
								return true;
							}
						}	
					}
			   
			   } // function validateprofileimgfld()   
                
               return false; 
           
        });
		
		
       
});