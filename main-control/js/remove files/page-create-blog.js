$(document).ready(function(){
       
        $('#xxfrm input').keypress(function (e) {
            if (e.which == 13) {
                $("#sub_btn").click();
            }
        });
        
        $(document).on('click',"#sub_btn",function(){
           
            var xxfrm             = $("#xxfrm");
            
            var pagetitle         = $("#page_title");
            var pagetitleerror    = $("#pagetitleerror");
            
            var pageurl           = $("#page_url");
            var pageurlerror      = $("#pageurlerror");
            
			var pagecategory           = $("#Category");
            var pagecategoryerror      = $("#categoryerror");

            
            if(validatepagetitlefld() & validatepageurlfld())
            {
               xxfrm.submit();
            }
            else
            {
                return false;
            }    
            
            
            
                function validatepagetitlefld()
                {

                   if(pagetitle.val().length<1)
                   {

                     pagetitle.removeClass("form-control");		
                     pagetitle.addClass("form-control errortxtbox");
                     pagetitleerror.text("Please enter page title");

                     return false;	
                   }
                   else
                   {
                     pagetitle.removeClass("form-control errortxtbox");	
                     pagetitle.addClass("form-control");		
                     pagetitleerror.text("");	

                     return true;
                   }  

                } // function validatepagetitlefld(){
                
                function validatepageurlfld()
                {
                  if(pageurl.val().length<1)
                   {

                     pageurl.removeClass("form-control");		
                     pageurl.addClass("form-control errortxtbox");
                     pageurlerror.text("Please enter page url");

                     return false;	
                   }
                   else
                   {
                     pageurl.removeClass("form-control errortxtbox");	
                     pageurl.addClass("form-control");		
                     pageurlerror.text("");	

                     return true;
                   }    
                   
                } // function validatepageurlfld()
                
                function validatepagecategoryfld()
                {
                  
                   if(pagecategory.val().length<1)
                   {

                     pagecategory.removeClass("form-control tagit-hidden-field");		
                     pagecategory.addClass("form-control tagit-hidden-field errortxtbox");
                     pagecategoryerror.text("Please enter blog category");

                     return false;	
                   }
                   else
                   {
                     pagecategory.removeClass("form-control tagit-hidden-field errortxtbox");	
                     pagecategory.addClass("form-control tagit-hidden-field");		
                     pagecategoryerror.text("");	

                     return true;
                   }    
                  
                  
                } // function validatepagecategoryfld()
                
                function validatepagenofld()
                {
                
                   if(pageorderno.val().length<1)
                   {

                     pageorderno.removeClass("form-control");		
                     pageorderno.addClass("form-control errortxtbox");
                     pageordernoerror.text("Please enter page number");

                     return false;	
                   }
                   else
                   {
                     pageorderno.removeClass("form-control errortxtbox");	
                     pageorderno.addClass("form-control");		
                     pageordernoerror.text("");	

                     return true;
                   }     
                
                } // function validatepagenofld()
                
                function validatepagetype()
                {
                
                    var pageType = $('input[name=pageType]:checked').val();
                    
                    if(pageType==undefined)
                    {
                        pageTypeerror.text("Please select page type");
                        return false;	 
                    }
                    else
                    {
                        pageTypeerror.text("");
                        return true;	 
                    }    
                    
                
                } // function validatepagetype()
                
                function validateprofileimgfld()
                {
                
                    var fileerror		= $("#fileerror");
        
                    var names = [];
                    var filenamestr = "";
                    var fileExtension = ['jpeg', 'jpg', 'png', 'gif'];

                    var errorstrvalid = "";

                    for (var i = 0; i < $("#file").get(0).files.length; ++i) {

                        filenamestr = $("#file").get(0).files[i].name;

                        if(filenamestr.split('.').pop().toLowerCase()!="jpeg" & filenamestr.split('.').pop().toLowerCase()!="jpg" & filenamestr.split('.').pop().toLowerCase()!="gif" & filenamestr.split('.').pop().toLowerCase()!="png"){
                            errorstrvalid = "error";
                        }

                    }
                    
                    if(errorstrvalid=="error"){
            
                        fileerror.text("Only the following formats are allowed : jpeg, jpg, png, gif");
                        return false;  

                     }
                     else
                     {
                        fileerror.text("");
                        return true;
                     }   
                    
                    
                
                } // function validateprofileimgfld(){
                
                
           
        });
       
});