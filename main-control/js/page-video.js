$(document).ready(function(){
  

$("#video_title").click(function(){
	$("#video_title").removeClass("form-control errortxtbox");	
    $("#video_title").addClass("form-control");
	$("#video_titleerror").text("");
});
$("#video_iframe").click(function(){
	$("#video_iframe").removeClass("form-control errortxtbox");	
    $("#video_iframe").addClass("form-control");
	$("#video_iframeerror").text("");
});
$("#video_url").click(function(){
	$("#video_url").removeClass("form-control errortxtbox");	
    $("#video_url").addClass("form-control");
	$("#video_urlerror").text("");
});
$("#video_meta_description").click(function(){
	$("#video_meta_description").removeClass("form-control errortxtbox");	
    $("#video_meta_description").addClass("form-control");
	$("#video_meta_descriptionerror").text("");
});
       
	$(document).on('click',"#sub_btn",function(){
                
                var xxfrm        = $("#xxfrm");
                
                var video_title       = $("#video_title");
                var video_titleerror  = $("#video_titleerror");
				
                var video_url       = $("#video_url");
                var video_urlerror  = $("#video_urlerror");
                
                var video_iframe       = $("#video_iframe");
                var video_iframeerror  = $("#video_iframeerror");
                
                var video_meta_description       = $("#video_meta_description");
                var video_meta_descriptionerror  = $("#video_meta_descriptionerror");
                
                
                var fileerror            = $("#fileerror");
                
				
                 if(validatevideo_url() & validatevideo_title() & validatevideo_iframe())
                {
                    xxfrm.submit();
                } 
                
                
                function validatevideo_title()
                {
                  if(video_title.val().length<1)
                   {

                     video_title.removeClass("form-control");		
                     video_title.addClass("form-control errortxtbox");
                     video_titleerror.text("Please enter your video heading");

                     return false;	
                   }
                   else
                   {
                     video_title.removeClass("form-control errortxtbox");	
                     video_title.addClass("form-control");		
                     video_titleerror.text("");	

                     return true;
                   }    
                   
                } ///////////////
                
                function validatevideo_url()
                {
                  if(video_url.val().length<1)
                   {

                     video_url.removeClass("form-control");		
                     video_url.addClass("form-control errortxtbox");
                     video_urlerror.text("Please enter your video URL");

                     return false;	
                   }
                   else
                   {
                     video_url.removeClass("form-control errortxtbox");	
                     video_url.addClass("form-control");		
                     video_urlerror.text("");	

                     return true;
                   }    
                   
                } ///////////////
                
				function validatevideo_meta_description()
                {
                  if(video_meta_description.val().length<1)
                   {

                     video_meta_description.removeClass("form-control");		
                     video_meta_description.addClass("form-control errortxtbox");
                     video_meta_descriptionerror.text("Please enter your Media report meta Description");

                     return false;	
                   }
                   else
                   {
                     video_meta_description.removeClass("form-control errortxtbox");	
                     video_meta_description.addClass("form-control");		
                     video_meta_descriptionerror.text("");	

                     return true;
                   }    
                   
                } ///////////////
                                
				function validatevideo_iframe()
                { 
                  if(video_iframe.val().length<1)
                   {

                     video_iframe.removeClass("form-control");		
                     video_iframe.addClass("form-control errortxtbox");
                     video_iframeerror.text("Please enter your video iframe");

                     return false;	
                   }
                   else
                   {
                     video_iframe.removeClass("form-control errortxtbox");	
                     video_iframe.addClass("form-control");		
                     video_iframeerror.text("");	

                     return true;
                   }    
                   
                } ///////////////
                
                
                                
                return false;
	});
 
});

var fileCount=-1;
var output = document.getElementById("result");
