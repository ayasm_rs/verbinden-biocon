$(document).ready(function(){
  

$("#news_head").click(function(){
	$("#news_head").removeClass("form-control errortxtbox");	
    $("#news_head").addClass("form-control");
	$("#news_headerror").text("");
});
$("#news_metadescription").click(function(){
	$("#news_metadescription").removeClass("form-control errortxtbox");	
    $("#news_metadescription").addClass("form-control");
	$("#news_metadescriptionerror").text("");
});
$("#news_metakeyword").click(function(){
	$("#news_metakeyword").removeClass("form-control errortxtbox");	
    $("#news_metakeyword").addClass("form-control");
	$("#news_metakeyworderror").text("");
});

CKEDITOR.instances['news_description'].on('contentDom', function() {
    this.document.on('click', function(event){
        $("#news_descriptionerror").text("");
     });
});        
	$(document).on('click',"#sub_btn",function(){
		/* $('.order_no').each(function() {
			var id=$(this).attr("w_id");
			var order_no=$(this).val();
			var val_details=$("#order_details").val();
			val_details+=id+"-"+order_no+",";
			$("#order_details").attr('value',val_details);
		}); */
                
                var xxfrm        = $("#xxfrm");
                
                var news_head       = $("#news_head");
                var news_headerror  = $("#news_headerror");
                
                var news_metadescription       = $("#news_metadescription");
                var news_metadescriptionerror  = $("#news_metadescriptionerror");
                
                var news_metakeyword       = $("#news_metakeyword");
                var news_metakeyworderror  = $("#news_metakeyworderror");
                
                var news_description       = $("#news_description");
                var news_descriptionerror  = $("#news_descriptionerror");
                
                var news_pageurl       = $("#news_pageurl");
                var news_pageurlerror  = $("#news_pageurlerror");
                
                
                var fileerror            = $("#fileerror");
                
				
                 if(validatenews_head() & validatenews_description() & validatenews_pageurl())
                {
                    xxfrm.submit();
                } 
                
                
                function validatenews_pageurl()
                {
                  if(news_pageurl.val().length<1)
                   {

                     news_pageurl.removeClass("form-control");		
                     news_pageurl.addClass("form-control errortxtbox");
                     news_pageurlerror.text("Please enter page name");

                     return false;	
                   }
                   else
                   {
                     news_pageurl.removeClass("form-control errortxtbox");	
                     news_pageurl.addClass("form-control");		
                     news_pageurlerror.text("");	

                     return true;
                   }    
                   
                } ///////////////
                
                function validatenews_head()
                {
                  if(news_head.val().length<1)
                   {

                     news_head.removeClass("form-control");		
                     news_head.addClass("form-control errortxtbox");
                     news_headerror.text("Please enter your news heading");

                     return false;	
                   }
                   else
                   {
                     news_head.removeClass("form-control errortxtbox");	
                     news_head.addClass("form-control");		
                     news_headerror.text("");	

                     return true;
                   }    
                   
                } ///////////////
                
				function validatenews_metadescription()
                {
                  if(news_metadescription.val().length<1)
                   {

                     news_metadescription.removeClass("form-control");		
                     news_metadescription.addClass("form-control errortxtbox");
                     news_metadescriptionerror.text("Please enter your news small description");

                     return false;	
                   }
                   else
                   {
                     news_metadescription.removeClass("form-control errortxtbox");	
                     news_metadescription.addClass("form-control");		
                     news_metadescriptionerror.text("");	

                     return true;
                   }    
                   
                } ///////////////
                
				function validatenews_metakeyword()
                {
                  if(news_metakeyword.val().length<1)
                   {

                     news_metakeyword.removeClass("form-control");		
                     news_metakeyword.addClass("form-control errortxtbox");
                     news_metakeyworderror.text("Please enter your page title");

                     return false;	
                   }
                   else
                   {
                     news_metakeyword.removeClass("form-control errortxtbox");	
                     news_metakeyword.addClass("form-control");		
                     news_metakeyworderror.text("");	

                     return true;
                   }    
                   
                } ///////////////
                
                
				function validatenews_description()
                {
					//alert(CKEDITOR.instances.news_description.document.getBody().getChild(0).getText().length);
                  if(CKEDITOR.instances.news_description.document.getBody().getChild(0).getText().length<2)
                   {

                     news_description.removeClass("form-control");		
                     news_description.addClass("form-control errortxtbox");
                     news_descriptionerror.text("Please enter your news description");

                     return false;	
                   }
                   else
                   {
                     news_description.removeClass("form-control errortxtbox");	
                     news_description.addClass("form-control");		
                     news_descriptionerror.text("");	

                     return true;
                   }    
                   
                } ///////////////
                                
                return false;
	});
 
});

var fileCount=-1;
var output = document.getElementById("result");
