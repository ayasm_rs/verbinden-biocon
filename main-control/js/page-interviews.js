$(document).ready(function(){
  

$("#interview_title").click(function(){
	$("#interview_title").removeClass("form-control errortxtbox");	
    $("#interview_title").addClass("form-control");
	$("#interview_titleerror").text("");
});
$("#interview_description").click(function(){
	$("#interview_description").removeClass("form-control errortxtbox");	
    $("#interview_description").addClass("form-control");
	$("#interview_descriptionerror").text("");
});
$("#interview_url").click(function(){
	$("#interview_url").removeClass("form-control errortxtbox");	
    $("#interview_url").addClass("form-control");
	$("#interview_urlerror").text("");
});
$("#interview_meta_description").click(function(){
	$("#interview_meta_description").removeClass("form-control errortxtbox");	
    $("#interview_meta_description").addClass("form-control");
	$("#interview_meta_descriptionerror").text("");
});
$("#interview_page_title").click(function(){
	$("#interview_page_title").removeClass("form-control errortxtbox");	
    $("#interview_page_title").addClass("form-control");
	$("#interview_page_titleerror").text("");
});
       
	$(document).on('click',"#sub_btn",function(){
		/* $('.order_no').each(function() {
			var id=$(this).attr("w_id");
			var order_no=$(this).val();
			var val_details=$("#order_details").val();
			val_details+=id+"-"+order_no+",";
			$("#order_details").attr('value',val_details);
		}); */
                
                var xxfrm        = $("#xxfrm");
                
                var interview_title       = $("#interview_title");
                var interview_titleerror  = $("#interview_titleerror");
				
                var interview_url       = $("#interview_url");
                var interview_urlerror  = $("#interview_urlerror");
                
                var interview_description       = $("#interview_description");
                var interview_descriptionerror  = $("#interview_descriptionerror");
                
                var interview_meta_description       = $("#interview_meta_description");
                var interview_meta_descriptionerror  = $("#interview_meta_descriptionerror");
                
                var interview_page_title       = $("#interview_page_title");
                var interview_page_titleerror  = $("#interview_page_titleerror");
                
                var news_description       = $("#news_description");
                var news_descriptionerror  = $("#news_descriptionerror");
                
                
                var fileerror            = $("#fileerror");
                
				
                 if(validateinterview_url() & validateinterview_title() & validateinterview_description())
                {
                    xxfrm.submit();
                } 
                
                
                function validateinterview_title()
                {
                  if(interview_title.val().length<1)
                   {

                     interview_title.removeClass("form-control");		
                     interview_title.addClass("form-control errortxtbox");
                     interview_titleerror.text("Please enter your interviews heading");

                     return false;	
                   }
                   else
                   {
                     interview_title.removeClass("form-control errortxtbox");	
                     interview_title.addClass("form-control");		
                     interview_titleerror.text("");	

                     return true;
                   }    
                   
                } ///////////////
                
                function validateinterview_url()
                {
                  if(interview_url.val().length<1)
                   {

                     interview_url.removeClass("form-control");		
                     interview_url.addClass("form-control errortxtbox");
                     interview_urlerror.text("Please enter your interviews URL");

                     return false;	
                   }
                   else
                   {
                     interview_url.removeClass("form-control errortxtbox");	
                     interview_url.addClass("form-control");		
                     interview_urlerror.text("");	

                     return true;
                   }    
                   
                } ///////////////
                
				function validateinterview_meta_description()
                {
                  if(interview_meta_description.val().length<1)
                   {

                     interview_meta_description.removeClass("form-control");		
                     interview_meta_description.addClass("form-control errortxtbox");
                     interview_meta_descriptionerror.text("Please enter your Media report meta Description");

                     return false;	
                   }
                   else
                   {
                     interview_meta_description.removeClass("form-control errortxtbox");	
                     interview_meta_description.addClass("form-control");		
                     interview_meta_descriptionerror.text("");	

                     return true;
                   }    
                   
                } ///////////////
                
				function validateinterview_page_title()
                {
                  if(interview_page_title.val().length<1)
                   {

                     interview_page_title.removeClass("form-control");		
                     interview_page_title.addClass("form-control errortxtbox");
                     interview_page_titleerror.text("Please enter your Page title");

                     return false;	
                   }
                   else
                   {
                     interview_page_title.removeClass("form-control errortxtbox");	
                     interview_page_title.addClass("form-control");		
                     interview_page_titleerror.text("");	

                     return true;
                   }    
                   
                } ///////////////
                
				function validateinterview_description()
                {
                  if(interview_description.val().length<1)
                   {

                     interview_description.removeClass("form-control");		
                     interview_description.addClass("form-control errortxtbox");
                     interview_descriptionerror.text("Please enter your interviews short description");

                     return false;	
                   }
                   else
                   {
                     interview_description.removeClass("form-control errortxtbox");	
                     interview_description.addClass("form-control");		
                     interview_descriptionerror.text("");	

                     return true;
                   }    
                   
                } ///////////////
                
                
                                
                return false;
	});
 
});

var fileCount=-1;
var output = document.getElementById("result");
