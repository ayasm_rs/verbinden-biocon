<?PHP
require_once("./include/membersite_config.php");
include("./include/mysqli_connect.php");
include("./include/html_codes.php");

admin_way_top();

admin_top_bar();

admin_left_menu();

$id_user=$fgmembersite->idUser();
$admin = 0;

$page_access_urls ="";
$tejas_users = mysqli_query($connect,"SELECT * FROM tejas_users WHERE id_user=".$id_user);
while($row = mysqli_fetch_array($tejas_users)){
	$page_access_urls = $row['page_access_ids'];
}

$page_access_urls_Array = explode("~",$page_access_urls);

if($fgmembersite->UserRole()!="admin" && empty($page_access_urls)){
	if($id!=$fgmembersite->idUser()){
		$id_user=$fgmembersite->idUser();
		echo "<script>window.location.href='dashboard.php'</script>";
	}
}


if($fgmembersite->UserRole()=="admin"){
	$admin = 1;
}
?>
									
            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
<?php 

$sql = mysqli_query($connect,"SELECT * FROM tejas_users WHERE role='faculty' OR highDesignation LIKE '%faculty%'"); 
$row_count_users = mysqli_num_rows($sql);

$sql = mysqli_query($connect,"SELECT * FROM products "); 
$row_count_productes = mysqli_num_rows($sql);

$sql = mysqli_query($connect,"SELECT * FROM solutions "); 
$row_count_solution = mysqli_num_rows($sql);

$sql = mysqli_query($connect,"SELECT * FROM technologies "); 
$row_count_tech = mysqli_num_rows($sql);

$sql = mysqli_query($connect,"SELECT * FROM careers "); 
$row_count_careers = mysqli_num_rows($sql);

$sql = mysqli_query($connect,"SELECT * FROM job_categories "); 
$row_count_job_careers = mysqli_num_rows($sql);

$sql = mysqli_query($connect,"SELECT * FROM news_rooms "); 
$row_count_news_rooms = mysqli_num_rows($sql);

$sql = mysqli_query($connect,"SELECT * FROM news_interview "); 
$row_count_news_interview = mysqli_num_rows($sql);

$sql = mysqli_query($connect,"SELECT * FROM news_article "); 
$row_count_news_article = mysqli_num_rows($sql);

$sql = mysqli_query($connect,"SELECT * FROM news_video "); 
$row_count_news_video = mysqli_num_rows($sql);

?>
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
						
                        <div class="row">
							<div class="col-xs-12">
								<div class="page-title-box">
                                    <h4 class="page-title">Select Page Type</h4>
                                    <div class="clearfix"></div>
                                </div>
							</div>
						</div>
                        <!-- end row -->

						<div class="row">
						
							<div class="col-xs-12 col-md-6 col-lg-6 col-xl-3">
                                <div class="card-box tilebox-two">
                                    <i class="ion-ios7-paper-outline pull-xs-right text-muted"></i>
                                    <h5 class="text-success text-uppercase m-b-15 m-t-10">About Us</h5>
									<br><br>
									<div class="clearfix"></div>
									<a href="page-create-aboutus.php" class="btn btn-sm btn-custom waves-effect waves-light pull-xs-right">Create</a>
									<div class="clearfix"></div>
                                </div>
                            </div>
													
						</div>

                    </div> <!-- container -->

                </div> <!-- content -->



            </div>
            <!-- End content-page -->


            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->
<script>
	$("#page-create").attr("class","active");
</script>
<?php
admin_right_bar();

admin_footer();
?>