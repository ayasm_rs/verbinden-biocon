<?PHP
require_once("./include/membersite_config.php");
include("./include/mysqli_connect.php");
include("./include/html_codes.php");
include("./include/function.php");
$common_function = new common_function($connect);
$page_list= $common_function->page_list();
$option='';
for($i=0; $i < count($page_list); $i++)
{
$option .='<option value="'.$page_list[$i]["id"].'" >'.$page_list[$i]["page_name"].'</option>';
}

$temp_id=mysqli_real_escape_string($connect,$_GET['tid']);
$pid=mysqli_real_escape_string($connect,$_GET['pid']);
$tpos=mysqli_real_escape_string($connect,$_GET['tpos']);
$select_title="SELECT * FROM `template_title_desc` WHERE `temp_id`='$temp_id' AND page_id_td='$pid' AND temp_pos_id='$tpos' ";
$run_title=mysqli_query($connect,$select_title);


admin_way_top();

admin_top_bar();


admin_left_menu();

//After login check for page access
$id=8;
$id_user=$fgmembersite->idUser();
$admin = 0;



    $id_user=$fgmembersite->idUser();
	$approver = 0;$admin=0;
	if($fgmembersite->UserRole()=="admin"){
		$admin=1;
	}
	if($fgmembersite->UserRole()!="admin"){
		echo "<script>window.location.href='dashboard.php'</script>";
	}
	
	
if(isset($_POST['sub_btn']) && $_POST['create_page']=='submit')
{
$temp_id=htmlspecialchars(mysqli_real_escape_string($connect, $_POST['temp_id']),ENT_QUOTES, 'UTF-8');
$existing_count=count($_POST['tmp_det']);
$ex_id=$_POST['tmp_det'];
$tmp_title=$_POST['tmp_title'];
$bgcolor=$_POST['bgcolor'];
$block_menu_file_block=$_POST['block_menu_file_block'];
$new_links=array();
foreach($_POST['links'] as $k=>$v)
{
	$new_links[]=$v;
}

/****************existing section update****************/
for($i=0;$i<$existing_count;$i++)
{
	$tmid=htmlspecialchars(mysqli_real_escape_string($connect, $ex_id[$i]),ENT_QUOTES, 'UTF-8');
	$tmtitle=htmlspecialchars(mysqli_real_escape_string($connect, $tmp_title[$i]),ENT_QUOTES, 'UTF-8');
	$tmbgcolor=htmlspecialchars(mysqli_real_escape_string($connect, $bgcolor[$i]),ENT_QUOTES, 'UTF-8');
	$links=implode('~',$new_links[$i]);
	$file=htmlspecialchars(mysqli_real_escape_string($connect, $block_menu_file_block[$i]),ENT_QUOTES, 'UTF-8');
	/*************image***************/
	$str="";
	if($file!='')
	{
	$name='icon'.date('ymdis').rand(0,99999);
	$file_name = str_replace(" ","-",$name).".png";
	//echo $file;exit;
	list($type, $file) = explode(';', $file);
	list(, $file)      = explode(',', $file);
	file_put_contents("../images/icons/".$file_name , base64_decode($file));
	$str.="sub_icon='".$file_name."',";
	}
	/**************ends image***************/
	$query="Update template_details SET title='".$tmtitle."', ".$str." sub_link='".$links."', sub_bg_color='".$tmbgcolor."' WHERE id='".$tmid."'";
	mysqli_query($connect, $query);
}

/****************new section insertion****************/
if(count($tmp_title)>$existing_count)
{
	$maxnum=max($existing_count,count($tmp_title));
	$start=0;
	
	for($j=0;$j<$maxnum;$j++)
	{
		if(!(isset($tmp_title[$j]) && isset($ex_id[$j])))
		{	
		$start=$j;
		break;
		}
	}
	for($i=$start;$i<$maxnum;$i++)
	{
	$tmid=htmlspecialchars(mysqli_real_escape_string($connect, $ex_id[$i]),ENT_QUOTES, 'UTF-8');
	$tmtitle=htmlspecialchars(mysqli_real_escape_string($connect, $tmp_title[$i]),ENT_QUOTES, 'UTF-8');
	$tmbgcolor=htmlspecialchars(mysqli_real_escape_string($connect, $bgcolor[$i]),ENT_QUOTES, 'UTF-8');
	$links=implode('~',$new_links[$i]);
	$file=htmlspecialchars(mysqli_real_escape_string($connect, $block_menu_file_block[$i]),ENT_QUOTES, 'UTF-8');
	/*************image***************/
	$str=$str1="";
	if($file!='')
	{
	$name='icon'.date('ymdis').rand(0,99999);
	$file_name = str_replace(" ","-",$name).".png";
	list($type, $file) = explode(';', $file);
	list(, $file)      = explode(',', $file);
	file_put_contents("../images/icons/".$file_name , base64_decode($file));
	$str.=",sub_icon";
	$str1.=",'".$file_name."'";
	
	}
	
	/**************ends image***************/
	$query="INSERT INTO template_details (temp_id,title,sub_link,sub_bg_color".$str.", `page_id_td`, `temp_pos_id`) VALUES('".$temp_id."','".$tmtitle."','".$links."','".$tmbgcolor."'".$str1.",'".$pid."','".$tpos."')";
	mysqli_query($connect, $query);
	}
}

/********************removing the section*******************/
$deleted_ids=$_POST['deleted_ids'];
foreach($deleted_ids as $key=>$val)
{
	
	$delval=htmlspecialchars(mysqli_real_escape_string($connect, $val),ENT_QUOTES, 'UTF-8');
	$query="DELETE FROM template_details WHERE id='".$delval."'";
	mysqli_query($connect, $query);
}

}	
/*************************/
$select_event="SELECT * FROM `template_details` WHERE temp_id='$temp_id' AND page_id_td='$pid' AND temp_pos_id='$tpos' ";
$run_product=mysqli_query($connect,$select_event);
$tot_num_rws=mysqli_num_rows($run_product);
?>
<style>
.cropme_new
{
    float: left;
    background-color: #f1f1f1;
    margin-bottom: 5px;
    margin-right: 5px;
    background-image: url(simple_cropper/images/UploadLight.png);
    background-position: center center;
    background-repeat: no-repeat;
    cursor: pointer;
}
.page
{
	
	float:none !important;
}
.total_blk
{
border: 1px solid #a8a6a6;
}
</style>									
<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">
			
			<div class="row">
				<div class="col-xs-12">
					<div class="page-title-box">
						
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
			<!-- end row -->
                <form method="POST" name="xxfrm" id="xxfrm" action="" onsubmit="return valid();" >
				<div class="row">
					<div class="col-xs-12 col-lg-12 col-xl-12">
						<div class="card-box">
							<h4 class="header-title m-t-0 m-b-30">Widget Details</h4>
							<section>
							<input type="hidden" name="temp_id" value="<?php echo $id; ?>"/>
								<p class="text-muted m-b-20 font-13 pull-right">
									(* All fields are mandatory)
								</p>
								<div class="clearfix"></div>
										<?php
										$count=0;
										$script="";
										
										while($row_event=mysqli_fetch_array($run_product)){
										$title=$row_event["title"];
										$sub_icon=$row_event["sub_icon"];
										$sub_link=explode('~',$row_event["sub_link"]);
										$sub_bg_color=$row_event["sub_bg_color"];
										$num_rws=$row_event["id"];
										$option1='';
										for($i=0; $i < count($page_list); $i++)
										{
											$sel='';
											if(in_array(trim($page_list[$i]["id"]),$sub_link))
											{
												$sel='selected';
											}
											$option1 .='<option value="'.$page_list[$i]["id"].'" '.$sel.'>'.$page_list[$i]["page_name"].'</option>';
										}
										
										?>
										
								<div class="row each_box_<?php echo $num_rws ?> total_blk" >
								<input type="hidden" name="tmp_det[]" value="<?php echo $num_rws ?>" />
									<div class="col-lg-6 form-group">
										<div class="col-lg-12 form-group">
											<label class="control-label " for="tmp_title">Title*</label>
                                            <input id="tmp_title_<?php echo $num_rws; ?>" name="tmp_title[]" value="<?php echo $title; ?>"  type="text" class=" form-control" maxlength="20" required="required">
										</div>
										<div class="col-lg-12 form-group">
											<label class="control-label " for="cmp_metadescription">Background Color*</label>
											<input type="color" name="bgcolor[]" id="bgcolor" value="<?php echo $sub_bg_color; ?>" required="required">
										</div>
										<div class="col-lg-12 form-group">
											<label class="control-label " for="cmp_metadescription">Links*</label>
                                           <select id="dates-field2" class="form-control multiselect-ui" multiple="multiple" name="links[<?php echo $count ?>][]">
											<?php echo $option1; ?>
										</select>
										</div>
										
										</div>
										<div class="col-lg-6 form-group">
											<label class="control-label " for="cmp_metadescription">Icon</label>
                                            <div class="col-lg-12 form-group">
											<!-------------------------icon------------------->
										<div class="form-group text-center">
                                    <div class="col-md-6" style="width:100%;">
									<main class="page"><div class="box"><input type="file" id="file-input_block_<?php echo $num_rws ?>" class="file-input" accept="image/x-png, image/jpeg"><textarea class="hide" name="block_menu_file_block[]" id="block_menu_file_block_<?php echo $num_rws ?>" ></textarea></div><div class="box-2" id="box-2_block_<?php echo $num_rws ?>"><div class="result_block_<?php echo $num_rws ?>"></div></div><div class="box-2 img-result_block_<?php echo $num_rws ?>"><img class="cropped_block_<?php echo $num_rws ?>" src="<?php echo "../images/icons/".$sub_icon;?>" alt=""></div><div class="box"><div class="options_block_<?php echo $num_rws ?> "><input type="number" min="0" class="hide img-w_block_<?php echo $num_rws ?>" value="150" min="100" max="1200" /></div><a class="btn-success btn-sm btn save_block_<?php echo $num_rws ?> " onclick='$(".cropped_block_<?php echo $num_rws ?>").show();$(this).hide();$("#box-2_block_<?php echo $num_rws ?>").hide()' style="display:none;width: calc(100%/2 - 2em);padding: 0.5em;">Crop</a></div></main></div>
							<?php
							$script.='<script>document.querySelector("#file-input_block_'.$num_rws.'").addEventListener("change",function(e){if ( this.files[0].type.match(/^image\//) ) {var t=document.querySelector(".result_block_'.$num_rws.'"),r=(document.querySelector(".img-result_block_'.$num_rws.'"),document.querySelector(".img-w_block_'.$num_rws.'"),document.querySelector(".img-h_block_'.$num_rws.'"),document.querySelector(".options_block_'.$num_rws.'"));document.querySelector(".save_block_'.$num_rws.'"),document.querySelector(".cropped_block_'.$num_rws.'"),document.querySelector("#file-input_block_'.$num_rws.'");if(e.target.files.length){var o=new FileReader;o.onload=function(e){e.target.result&&($(".save_block_'.$num_rws.'").show(),$("#box-2_block_'.$num_rws.'").show(),$(".cropped_block_'.$num_rws.'").hide(),img=document.createElement("img"),img.id="image",img.src=e.target.result,t.innerHTML="",t.appendChild(img),r.classList.remove("hide"),cropper_block_'.$num_rws.' = new Cropper(img))},o.readAsDataURL(e.target.files[0])}}else{alert("Invalid Formate"); $(this).val("");}}),document.querySelector(".save_block_'.$num_rws.'").addEventListener("click",function(e){document.querySelector(".result_block_'.$num_rws.'");var t=document.querySelector(".img-result_block_'.$num_rws.'"),r=document.querySelector(".img-w_block_'.$num_rws.'"),o=(document.querySelector(".img-h_block_'.$num_rws.'"),document.querySelector(".options_block_'.$num_rws.'"),document.querySelector(".save_block_'.$num_rws.'"),document.querySelector(".cropped_block_'.$num_rws.'"));document.querySelector("#file-input_block_'.$num_rws.'");e.preventDefault();var c=cropper_block_'.$num_rws.'.getCroppedCanvas({width:r.value}).toDataURL();o.classList.remove("hide"),t.classList.remove("hide"),o.src=c;document.querySelector("#block_menu_file_block_'.$num_rws.'").value=c;});</script>';
							

							?>

                                </div>
										</div>
										<!-------------------------icon------------------->
										</div>
										
										
								<?php
								
									if($tot_num_rws>1 && $count!=0)
									{
										?>
									<button class="remove_field_exist btn btn-danger" data-id="<?php echo $num_rws; ?>">Remove</button>	
									<?php
									}
									
									?>		
								</div>
								
								
								
								<?php
									$count++;		
								}
								?>
								<div id="append_rows"></div>
								<button id="add_more_field" style="<?php echo ($tot_num_rws>=3)?"display:none;":""; ?>" class="btn btn-info">Add More Fields</button>
								<input type="hidden" id="field_limit" value="<?php echo $tot_num_rws ?>" />
								</div>
									
							</section>
						</div>
					</div>
					<div class="col-lg-12 form-group clearfix">
						<div class="card-box">
							<div class="col-lg-10 text-xs-center">
							<input type="hidden" name="create_page" id="create_page" value="submit" />
							<input class="btn btn-primary"  type="submit" name="sub_btn" id="sub_btn" value="Update Widget"/>
								
							</div>
						</div>
					</div>
					<div id="del_ids_div"></div>
				</div>
				<button class="btn btn-custom waves-effect waves-light btn-sm page_create_succcess" style="display:none" >Click me</button>
				</form>
			
		</div> <!-- container -->

	</div> <!-- content -->

</div>

<!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->
			
<!-- Custom box css -->
<link href="assets/css/custom.css" rel="stylesheet">

<link href="assets/plugins/custombox/css/custombox.min.css" rel="stylesheet">

 <!-- Modal-Effect -->
<script src="assets/plugins/custombox/js/custombox.min.js"></script>
<script src="assets/plugins/custombox/js/legacy.min.js"></script>
			
<!-- Sweet Alert css -->
<link href="assets/plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css" />
<!-- Sweet Alert js -->
<script src="assets/plugins/bootstrap-sweetalert/sweet-alert.min.js"></script>
<script src="assets/pages/jquery.sweet-alert.init.js"></script>

<!-- Editor -->
<script src="//code.jquery.com/jquery-1.12.0.min.js"></script>

<?php
admin_right_bar();
admin_footer();
transparent_cropper();
?>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/css/bootstrap-multiselect.css">
 <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.js"></script>
<script type="text/javascript" id="main_script">
function valid()
{
	var ct=0;
	$(".multiselect").each(function(e)
	{
		var val=$(this).text();
		console.log(val);
		if(val.trim()=='None selected')
		{
			$(this).css("border","1px solid red");
			ct=1;
			}
		
	});
	if(ct==1)
	return false;
	else
	return true;	
}
$(function() {
    $('.multiselect-ui').multiselect({
		numberDisplayed: 2,
        includeSelectAllOption: true
    });
});



var max_limit=2;
var n=<?php echo $count; ?>;
if(n>max_limit){
$("#add_more_field").hide();
}else{
$("#add_more_field").show();	
}



$(document).on("click","#add_more_field",function(e){
	e.preventDefault();
	var cnt=$("#field_limit").val();
	var total_blk=0;
	$(".total_blk").each(function(){
		total_blk++;
	});

	if(total_blk>max_limit)
	{
		$(this).hide();
	}
	else
	{
	cntt=parseInt(cnt)+1;
	$("#field_limit").val(cntt);
	var str='';
	var script='';
	str+='<div class="row each_box_'+cntt+' total_blk"> <div class="col-lg-6 form-group"><div class="col-lg-12 form-group"><label class="control-label " for="tmp_title">Title*</label><input id="tmp_title_'+cntt+'" name="tmp_title[]" value=""  type="text" class=" form-control" maxlength="20" required="required"></div><div class="col-lg-12 form-group"><label class="control-label " for="cmp_metadescription">Background Color*</label><input type="color" name="bgcolor[]" id="bgcolor'+cntt+'" required="required"></div> <div class="col-lg-12 form-group"><label class="control-label" for="cmp_metadescription">Links*</label><select id="dates-field2" class="multiselect-ui form-control mul_'+cntt+'" multiple="multiple" name="links['+cntt+'][]"><?php echo $option; ?></select></div></div><div class="col-lg-6 form-group"><label class="control-label">Icon</label><div class="col-lg-12 form-group"><main class="page"><div class="box"><input type="file" id="file-input_block_'+cntt+'" class="file-input" accept="image/x-png, image/jpeg"><textarea class="hide" name="block_menu_file_block[]" id="block_menu_file_block_'+cntt+'" ></textarea></div><div class="box-2" id="box-2_block_'+cntt+'"><div class="result_block_'+cntt+'"></div></div><div class="box-2 img-result_block_'+cntt+'"><img class="cropped_block_'+cntt+'" src="" alt=""></div><div class="box"><div class="options_block_'+cntt+' "><input type="number" min="0" class="hide img-w_block_'+cntt+'" value="150" min="100" max="1200" /></div><a class="btn-success btn-sm btn save_block_'+cntt+' " onclick=\'$(".cropped_block_'+cntt+'").show();$(this).hide();$("#box-2_block_'+cntt+'").hide()\' style="display:none;width: calc(100%/2 - 2em);padding: 0.5em;">Crop</a></div></main></div></div><div class="col-lg-12 form-group"><button class="remove_field btn btn-danger" data-id="'+cntt+'">Remove</button></div></div>';
	$("#append_rows").append(str);
	
script+='<script id="blk_'+cntt+'">document.querySelector("#file-input_block_'+cntt+'").addEventListener("change",function(e){var valu=document.querySelector("#file-input_block_'+cntt+'").value; var ext = valu.split(".").pop(); if(ext=="png" || ext=="PNG" || ext=="jpg" || ext=="JPG" || ext=="jpeg" || ext=="JPEG"){var t=document.querySelector(".result_block_'+cntt+'"),r=(document.querySelector(".img-result_block_'+cntt+'"),document.querySelector(".img-w_block_'+cntt+'"),document.querySelector(".img-h_block_'+cntt+'"),document.querySelector(".options_block_'+cntt+'"));document.querySelector(".save_block_'+cntt+'"),document.querySelector(".cropped_block_'+cntt+'"),document.querySelector("#file-input_block_'+cntt+'");if(e.target.files.length){var o=new FileReader;o.onload=function(e){e.target.result&&($(".save_block_'+cntt+'").show(),$("#box-2_block_'+cntt+'").show(),$(".cropped_block_'+cntt+'").hide(),img=document.createElement("img"),img.id="image",img.src=e.target.result,t.innerHTML="",t.appendChild(img),r.classList.remove("hide"),cropper_block_'+cntt+' = new Cropper(img))},o.readAsDataURL(e.target.files[0])}}else{alert("Invalid Formate");document.querySelector("#file-input_block_'+cntt+'").value="";}}),document.querySelector(".save_block_'+cntt+'").addEventListener("click",function(e){document.querySelector(".result_block_'+cntt+'");var t=document.querySelector(".img-result_block_'+cntt+'"),r=document.querySelector(".img-w_block_'+cntt+'"),o=(document.querySelector(".img-h_block_'+cntt+'"),document.querySelector(".options_block_'+cntt+'"),document.querySelector(".save_block_'+cntt+'"),document.querySelector(".cropped_block_'+cntt+'"));document.querySelector("#file-input_block_'+cntt+'");e.preventDefault();var c=cropper_block_'+cntt+'.getCroppedCanvas({width:r.value}).toDataURL();o.classList.remove("hide"),t.classList.remove("hide"),o.src=c;document.querySelector("#block_menu_file_block_'+cntt+'").value=c;});$(".mul_'+cntt+'").multiselect({numberDisplayed: 2,includeSelectAllOption: true});<\/script>';

$('body').append(script);

}
});
$(document).on("click",".remove_field",function(e){
	var data=$(this).attr("data-id");
	$(".each_box_"+data).remove();
	$("#blk_"+data).remove();
	$("#add_more_field").show();
});

$(document).on("click",".remove_field_exist",function(e){
	var data=$(this).attr("data-id");
	$(".each_box_"+data).remove();
	$("#blk_"+data).remove();
	$("#add_more_field").show();
	$('#del_ids_div').append('<input type="hidden" id="deleted_ids" name="deleted_ids[]"  value="'+data+'" />');
	
});
	$('.page_create_succcess' ).click(function () {
		swal({title:"Status!", text:"Widget updated successfully.", icon:"success"},
	function(){
		window.location="template-eight.php?pid=<?php echo $pid.'&tid='.$temp_id.'&tpos='.$tpos; ?>";
	});
		
	});	
	<?php  if(isset($_POST['create_page'])){ ?>
	$('.page_create_succcess').click();	
	<?php } ?>
/* $(document).on('click','.page_create_succcess',function () {
		swal("Status!", "New Key management created successfully.", "success");
	});	
/* 	swal({title:"Status!", text:"Widget updated successfully.", icon:"success"},
		function(){
		window.location="template-eight.php";
		}); * /
	<?php  if(isset($_POST['sub_btn']) && $_POST['create_page']=='submit'){ ?>
	alert("Widget updated successfully.");
	<?php } ?> */
</script>


<?php echo $script; ?>