<?PHP
require_once("./include/membersite_config.php");
include("./include/mysqli_connect.php");
include("./include/html_codes.php");

admin_way_top();

admin_top_bar();

admin_left_menu();

//After login check for page access

$id_user=$fgmembersite->idUser();
$admin = 0;

?>
<body class="fixed-left">

<!-- Begin page -->
<div id="wrapper">

<!-- DataTables -->
<link href="assets/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<link href="assets/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />		
<link href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
<link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
<style>
.fa-edit,.fa-trash,.fa-eye{ font-size:18px; }
.table th, .table td{
	width:10%;
}
.edit, .delete{ font-size: 22px;cursor: pointer;}
.img-responsive{display:block;max-width:100%;height:auto}
/* .page-title-box{ margin: 0px 0px 20px 0px; } */
.modal-demo .close {
    position: absolute;
    top: 8px;
    right: 8px;
}
.btn-error{
	color: #fff;
    background-color: #ff5d48;
    border-color: #ff5d48;
}
</style>
									
<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">
			<div class="row">
							<div class="col-xs-12">
								<div class="page-title-box">
                                    <h4 class="page-title">Category</h4>
                                    <div class="clearfix"></div>
									<button class="btn btn-primary" id="create_cate" style=" float: right; position: relative; top: -28px;">Create New</button>
                                </div>
							</div>
						</div>
                        <!-- end row -->
			
				
			
			 <div class="card-box table-responsive">
				<table id="datatable" class="table table-striped table-bordered table-responsive" style="width:100%">
					<thead>
						<tr>
							<th style="display:none">id</th>
							<th>Category Name</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<?php 
						$sql_job_applied = mysqli_query($connect,"SELECT * FROM category ORDER BY id DESC");
						while($row_job_applied = mysqli_fetch_array($sql_job_applied)){
							$id = $row_job_applied['id'];
							$name = $row_job_applied['cate_name'];
							
							echo '<tr id="articles_'.$id.'" >';
							echo '<td style="display:none">'.$id.'</td>';
							echo '<td>'.$name.'</td>';
							
							echo '<td><a href="javascript:void(0)" onclick="edit_cat('.$id.')">Edit</a>&nbsp;&nbsp;<a href="javascript:void(0)" onclick="delete_cat('.$id.')">Delete</a></td>';
							
							echo '</tr>';
						}?>
					</tbody>
				</table>
				<?php 
					if(!mysqli_num_rows($sql_job_applied)){
						echo "<script>$('.table-responsive').html('<center>No Data Found!</center>')</script>";
					} 
				?>
			</div>
		
			
	</div> <!-- content -->
</div>
<!-- End content-page -->


<!-- ============================================================== -->
<!-- End Right content here -->
<!-- ============================================================== -->


<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Create New Category</h4>
      </div>
	  <form name="create_cate" method="POST" action="include/api.php" >
      <div class="modal-body">
        <input type="text" name="category" id="category" value="" required />
      </div>
      <div class="modal-footer">
	  <input type="hidden" name="action" value="create_cate" >
	  <input type="hidden" name="id_user" value="<?php echo $id_user; ?>" >
		<input type="submit" class="btn btn-primary" name="create_cat" value="Create">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
	  </form>
    </div>

  </div>
</div>

<!-- Modal -->
<div id="myModal2" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Edit Category</h4>
      </div>
	  <form name="edit_cate" method="POST" action="include/api.php" >
      <div class="modal-body">
      </div>
      <div class="modal-footer">
	  <input type="hidden" name="action" value="edit_cate" >
	  <input type="hidden" name="id_user" value="<?php echo $id_user; ?>" >
		<input type="submit" class="btn btn-primary" name="create_cat" value="edit">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
	  </form>
    </div>

  </div>
</div>

<!-- Sweet Alert css -->
<link href="assets/plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css" />
<!-- Sweet Alert js -->
<script src="assets/plugins/bootstrap-sweetalert/sweet-alert.min.js"></script>
<script src="assets/pages/jquery.sweet-alert.init.js"></script>
		
<!-- Custom box css -->
<link href="assets/plugins/custombox/css/custombox.min.css" rel="stylesheet">

<!-- Modal-Effect -->
<script src="assets/plugins/custombox/js/custombox.min.js"></script>
<script src="assets/plugins/custombox/js/legacy.min.js"></script>

<!-- Editor -->
<script src="//code.jquery.com/jquery-1.12.0.min.js"></script>
<script src="js/jquery.syntaxhighlighter.js"></script>

<input type="text" id="id_article" value="" style="display:none" />
<script>
$(document).ready(function(){

	<?php if(isset($_GET["create-success"])){
		echo '	swal({
     title: "Success!",
     text: "New category created successfully.",
     type: "success",
	 showCancelButton: false,
	showConfirmButton: false,
     timer: 3000
     });';
	} 
	if(isset($_GET["edit-success"])){
		echo '	swal({
     title: "Success!",
     text: "Category updated successfully.",
     type: "success",
	 showCancelButton: false,
	showConfirmButton: false,
     timer: 3000
     });';
	}
	if(isset($_GET["delete-success"])){
		echo '	swal({
     title: "Success!",
     text: "Category deleted successfully.",
     type: "success",
	 showCancelButton: false,
	showConfirmButton: false,
     timer: 3000
     });';
	}
	?>
});
</script>
<script>
	$("#widget-existing").attr("class","active");
	function deletefn(id){ 
		$("#id_article").val(id);
		swal({  
			title: "Are you sure?",
			text: " ",
			type: "error",
			showCancelButton: true,
			cancelButtonClass: 'btn-secondary waves-effect',
			confirmButtonClass: 'btn-danger waves-effect waves-light'.id,
			confirmButtonId: id,
			confirmButtonText: 'Delete'
		});
	}
	function approve(id){
		$("#id_article").val(id);
		swal({  
			title: "Are you sure?",
			text: " ",
			type: "success",
			showCancelButton: true,
			cancelButtonClass: 'btn-secondary waves-effect',
			confirmButtonClass: 'btn-success waves-effect waves-light'.id,
			confirmButtonId: id,
			confirmButtonText: 'Approve'
		});
	}
	$(document).on('click','.btn-error',function () {
		id=$("#id_article").val();
		console.log(id);
		$.ajax({
			  type: "POST",
			  url: "ajaxCalls.php",
			  data: {"id":$("#id_article").val(),"action":"deleteArticle"} ,
			  success: function(data) {
				$("#articles_"+id).css("display","none");
			}
		});
	});
	$(document).on('click','.btn-success',function () {
		id=$("#id_article").val();
		console.log(id);
		$.ajax({
			  type: "POST",
			  url: "ajaxCalls.php",
			  data: {"id":$("#id_article").val(),"action":"approveArticle"} ,
			  success: function(data) {
				$("#check_"+id).css("display","none");
				$("#is_approev_"+id).text("Approved");
				$("#status_"+id).text("Published");
			}
		});
	});
	function edit_cat(id){
		$.ajax({
			  type: "POST",
			  url: "ajaxCalls.php",
			  data: {"id":id,"action":"EditCat"} ,
			  success: function(data) {
				$("#myModal2 .modal-body").html(data);
				$("#myModal2").modal("show");
			}
		});
	}

	function delete_cat(id){
		swal({
	title: "Are you sure?",
	text: "You will not be able to recover the data!",
	type: "error",
	showCancelButton: true,
	cancelButtonClass: 'btn-secondary waves-effect',
	confirmButtonClass: 'btn-danger waves-effect waves-light delete_cate',
	confirmButtonId: id,
	confirmButtonText: 'Ok!'
	},function(){
		$.ajax({
			  type: "POST",
			  url: "include/api.php",
			  data: {"id":id,"action":"DeleteCat"} ,
			  success: function(data) {
				window.location.href="category.php?delete-success";
			}
		});
	});

	}
/* 		$(document).on('click','.delete_cate',function () { 
	$.ajax({
			  type: "POST",
			  url: "include/api.php",
			  data: {"id":id,"action":"DeleteCat"} ,
			  success: function(data) {
				window.location.href="category.php?delete-success";
			}
		});
	}); */
</script>



<!-- Sweet Alert css -->
<link href="assets/plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css" />
<!-- Sweet Alert js -->
<script src="assets/plugins/bootstrap-sweetalert/sweet-alert.min.js"></script>
<script src="assets/pages/jquery.sweet-alert.init.js"></script>
<script src="//code.jquery.com/jquery-1.12.0.min.js"></script>
<script>

$(document).ready(function(){

	$(document).on("click", ".confirm",function(){
		$(".btn-primary").click();
	});

	$(document).on("click","#create_cate",function(){
		$("#myModal").modal("show");
	});
});

</script>
<!-- Required datatable js -->
<script src="assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="assets/plugins/datatables/dataTables.bootstrap4.min.js"></script>
<script src="//code.jquery.com/jquery-1.12.3.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="//cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
<script src="//cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
<script src="//cdn.datatables.net/plug-ins/1.10.12/sorting/numeric-comma.js"></script>
<?php 
$table_columns = rtrim($table_columns,',');
 ?>
<script type="text/javascript">
 
var j_dataTable = $.noConflict();
j_dataTable(document).ready(function() {
	var oTable = j_dataTable('#datatable').DataTable();
});

</script>

<?php
admin_right_bar();
?>

<?php
admin_footer();
?>