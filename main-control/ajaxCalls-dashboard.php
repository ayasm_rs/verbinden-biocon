<?php 
require_once("./include/membersite_config.php");
include("include/mysqli_connect.php");
include("include/html_codes.php");

/* ************** Logic Function *****************/

function get_temp_id_user($idparam,$connect){

  $result_data  = mysqli_query($connect," select * from biocon_users where id_user = ".$idparam);
  $row_cnt 	= $result_data->num_rows;
  
  if($row_cnt==0)
  {
    $ret_value = 0;
  }
  else
  {
      $row_data  = mysqli_fetch_assoc($result_data);
      $ret_value = $row_data["ref_id"];
  }
  
  return $ret_value;
  
} // function get_temp_id_user($id){

/* ************** Logic Function *****************/

$id =$fgmembersite->idUser();
if($fgmembersite->UserRole()!="admin"){
	$id =$fgmembersite->idUser();
	$role = $fgmembersite->role();
	$highDesignation = $fgmembersite->highDesignation();
	if($highDesignation == 'research' || $role == 'research'){
		$finid = get_temp_id_user($id,$connect);						
		//echo "<script>window.location.href='myprofile-research-people.php?id=".$finid."&profile=myprofile'</script>";
	}
	elseif($highDesignation == 'placement' || $role == 'placement'){
		
		//echo "<script>window.location.href='page-edit-placement-people.php?id=".$id."&profile=myprofile'</script>";
	}
	else{
                
                $finid = get_temp_id_user($id,$connect);

		//echo "<script>window.location.href='myprofile-faculty.php?id=".$finid."&profile=myprofile'</script>";
	}
							
}

$type = $_POST["type"];

switch($type){
	
	case "appenddataslatestupdate":
	
		$bindedtype = $_POST["bindedtype"];
	
		/********* Site Details ***************/
		
		$site_details = '';
		$query_site_details = mysqli_query($connect, "SELECT * FROM site_details");
		while($row = mysqli_fetch_array($query_site_details)){
			$site_details .= $row['value'].'~';
		}

		$site_details_exp = explode("~", $site_details);
		$site_name = $site_details_exp[0];
		$site_url = $site_details_exp[1];

		/********* Site Details ***************/
		
		$pagenum	= $_POST["pagenum"];
		$id			= $_POST["hiduserid"];
		
		$perPage 		= 4;
	
		$start = ($pagenum-1)*$perPage;
		if($start < 0) $start = 0;
		
		$final_str = '';
		
		$sql = " select lup.*,usr.name,usr.profile_pic from latest_updates lup , biocon_users usr WHERE lup.edited_by = usr.id_user ORDER BY lup.id DESC LIMIT $start,$perPage ";
		
		$query = mysqli_query($connect,$sql);
		
		while($row = mysqli_fetch_array($query)){
													
									
			$edited_type 	= $row["edited_type"];
			$updated_date 	= $row["updated_date_time"];
			$timestamp 		= strtotime($updated_date); 
			$table_name 	= $row['table_name'];
			$updated_date 	= date( 'd-m-Y h:i:s a', $timestamp );
			$updated_date1  = date( 'h:i:s a', $timestamp );
			$page_url		= '';
			$page_nav_name	= '';
			
			$approved_by    = $row["approved_by"];
			
			$url 			= "";
			
			$user 			= $row['name'];
			$profile_pic 	= $row['profile_pic'];
			
			$temp_id 		= $row['temp_page_id'];
			$page_id 		= $row['page_id'];
			$latest_id 		= $row['id'];			
			
			
			
			if($profile_pic==""){
				$profile_pic = "user-default-icon.jpg";
			}

			if(trim($user)==""){
				$user = "Admin";
			}
			
								
					$final_str .= '<div class="inbox-item">
                                                        <div class="inbox-item-img"><img src="../main-control/user_images/'.$profile_pic.'" class="img-circle" alt=""></div>
															<p class="inbox-item-author">'.$user.'</p>
															<p class="inbox-item-text" >'.$user.'&nbsp;has '. $edited_type .'&nbsp;'. $table_name .'&nbsp;On&nbsp;'. $updated_date.'</p>
															<p class="inbox-item-date">'.$updated_date1.'</p>
                                                </div>';
					
			} // while($row = mysqli_fetch_array($query)){
			
			
				
			echo $final_str; 	
	
	break;
	
} // 	
?>

