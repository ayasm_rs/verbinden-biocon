<?PHP
session_start();
require_once("./include/membersite_config.php");
include("./include/mysqli_connect.php");
include("./include/html_codes.php");

admin_way_top();

admin_top_bar();

admin_left_menu();


?>
<style>
.edit, .delete, .view{ font-size: 22px;cursor: pointer; color:black;}
.img-responsive{display:block;max-width:100%;height:auto}
.page-title-box{ margin: 0px 0px 20px 0px; }
h4{
	display: inline-block;
}

</style>	
<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
	<!-- Start content -->
	<div class="content">
	<div class="container">
	

	<?php 
  
	$staffs = mysqli_query($connect,"SELECT * FROM `page` WHERE page_name!='' ORDER BY id DESC ");
	
	$row_count_admin=mysqli_num_rows($staffs);
	if($row_count_admin){ 
	$i=0;
	while($staff=mysqli_fetch_array($staffs)){
	if($i==0){
	echo '<div class="card-box"><div class=""><div class="col-xs-12"><div class=""><div class="clearfix"></div></div><hr><br></div><div class="row">';
	echo '';
	$i++;
	}
	$id=$staff['id'];
	$page_name=$staff['page_name'];
	$page_heading=$staff['page_heading'];
	$page_title=$staff['page_title'];
	$id=$staff['id'];
	
	/* About Us */
	echo '<div class="col-xs-12 col-sm-6 col-lg-3 "  id="page_'.$id.'">
	<div class="card-box" style="background-color: white !important;   border-radius:0px !important;">
	<div class="text-xs-center m-b-20">
	</div>
	<div class="col-lg-12 text-xs-center">
	<h6>'.$page_title.'</h6>
	<p>'.stripslashes($page_heading).'</p>
	<span> <a href="../'.$page_name.'" target="_blank" ><i class="fa fa-eye view"></i></a> </span>&nbsp;&nbsp;
	<span> <a href="page-edit.php?pid='.$id.'" ><i class="fa fa-edit edit"></i></a> </span>&nbsp;&nbsp;
	<span> <i class="fa fa-trash delete waves-light danger-alert danger-page" id="'.$id.'"></i> </span>';
	echo '</div>
	</div>
	</div>';
	}
	echo '</div></div></div>';
	} 

	?>
	
	</div>
	</div>
	
	</div> <!-- container -->

	</div> <!-- content -->
</div>
<!-- End content-page -->


            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->

<!-- Sweet Alert css -->
<link href="assets/plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css" />
<!-- Sweet Alert js -->
<script src="assets/plugins/bootstrap-sweetalert/sweet-alert.min.js"></script>
<script src="assets/pages/jquery.sweet-alert.init.js"></script>

<!-- Editor -->
<script src="//code.jquery.com/jquery-1.12.0.min.js"></script>
<script src="js/jquery.syntaxhighlighter.js"></script>
<script src="assets/plugins/elrte/js/jquery-ui-1.8.13.custom.min.js" type="text/javascript" charset="utf-8"></script>
<link rel="stylesheet" href="assets/plugins/elrte/css/smoothness/jquery-ui-1.8.13.custom.css" type="text/css" media="screen" charset="utf-8">
<script src="assets/plugins/elrte/js/elrte.min.js" type="text/javascript" charset="utf-8"></script>
<link rel="stylesheet" href="assets/plugins/elrte/css/elrte.min.css" type="text/css" media="screen" charset="utf-8">
<script src="assets/plugins/elrte/js/i18n/elrte.ru.js" type="text/javascript" charset="utf-8"></script>

<script>
function imagePreview(input) {
	if (input.files && input.files[0]) {
	var reader = new FileReader();
	reader.onload = function (e) {
	$('#uploaded_image').attr('src', e.target.result);
	};
	reader.readAsDataURL(input.files[0]);
	}
}
$(document).ready(function(){
	$("#page-existing").attr("class","active");
	$("#email").on('change',function(){
	var emailID=$(this).val();
	$.ajax({
	  type: "POST",
	  url: "ajaxCalls.php",
	  data: {"emailID":emailID,"action":"emailidCheck"} ,
	  success: function(data) {
	if(data==0){
	$("#email_message").css("display","none");
	}else{ 
	$("#email_message").css("display","block");
	$("#email").val("");
	}
	  }
	});
	});
	$("#username").on('change',function(){
	var username=$(this).val();
	$.ajax({
	  type: "POST",
	  url: "ajaxCalls.php",
	  data: {"username":username,"action":"usernameCheck"} ,
	  success: function(data) {
	if(data==0){
	$("#userName_message").css("display","none");
	}else{  
	$("#userName_message").css("display","block");
	$("#username").val("");
	}
	  }
	});
	});
	<?php  if(isset($_POST['profile_create'])){ ?>
	$('#fadein').click();	
	<?php } ?>
	var id="";
	$('.danger-alert1').click(function () {
	id=$(this).attr("id");
	swal({
	title: "Are you sure?",
	text: "You will not be able to recover the data!",
	type: "error",
	showCancelButton: true,
	cancelButtonClass: 'btn-secondary waves-effect',
	confirmButtonClass: 'btn-danger waves-effect waves-light faculty_page',
	confirmButtonId: id,
	confirmButtonText: 'Ok!'
	});
	});
	$('.danger-alert2').click(function () {
	id=$(this).attr("id");
	swal({
	title: "Are you sure?",
	text: "You will not be able to recover the data!",
	type: "error",
	showCancelButton: true,
	cancelButtonClass: 'btn-secondary waves-effect',
	confirmButtonClass: 'btn-danger waves-effect waves-light solution_pages',
	confirmButtonId: id,
	confirmButtonText: 'Ok!'
	});
	});
	$('.danger-alert3').click(function () {
	id=$(this).attr("id");
	swal({
	title: "Are you sure?",
	text: "You will not be able to recover the data!",
	type: "error",
	showCancelButton: true,
	cancelButtonClass: 'btn-secondary waves-effect',
	confirmButtonClass: 'btn-danger waves-effect waves-light tech',
	confirmButtonId: id,
	confirmButtonText: 'Ok!'
	});
	});
	$('.danger-alert4').click(function () {
	id=$(this).attr("id");
	swal({
	title: "Are you sure?",
	text: "You will not be able to recover the data!",
	type: "error",
	showCancelButton: true,
	cancelButtonClass: 'btn-secondary waves-effect',
	confirmButtonClass: 'btn-danger waves-effect waves-light news_report',
	confirmButtonId: id,
	confirmButtonText: 'Ok!'
	});
	});
	$('.danger-alert5').click(function () {
	id=$(this).attr("id");
	swal({
	title: "Are you sure?",
	text: "You will not be able to recover the data!",
	type: "error",
	showCancelButton: true,
	cancelButtonClass: 'btn-secondary waves-effect',
	confirmButtonClass: 'btn-danger waves-effect waves-light news_media',
	confirmButtonId: id,
	confirmButtonText: 'Ok!'
	});
	});
	$('.danger-alert6').click(function () {
	id=$(this).attr("id");
	swal({
	title: "Are you sure?",
	text: "You will not be able to recover the data!",
	type: "error",
	showCancelButton: true,
	cancelButtonClass: 'btn-secondary waves-effect',
	confirmButtonClass: 'btn-danger waves-effect waves-light news_interview',
	confirmButtonId: id,
	confirmButtonText: 'Ok!'
	});
	});
	$('.danger-alert7').click(function () {
	id=$(this).attr("id");
	swal({
	title: "Are you sure?",
	text: "You will not be able to recover the data!",
	type: "error",
	showCancelButton: true,
	cancelButtonClass: 'btn-secondary waves-effect',
	confirmButtonClass: 'btn-danger waves-effect waves-light news_video',
	confirmButtonId: id,
	confirmButtonText: 'Ok!'
	});
	});
	$('.danger-alert8').click(function () {
	id=$(this).attr("id");
	swal({
	title: "Are you sure?",
	text: "You will not be able to recover the data!",
	type: "error",
	showCancelButton: true,
	cancelButtonClass: 'btn-secondary waves-effect',
	confirmButtonClass: 'btn-danger waves-effect waves-light contact_us',
	confirmButtonId: id,
	confirmButtonText: 'Ok!'
	});
	});
	$('.danger-alert9').click(function () {
	id=$(this).attr("id");
	swal({
	title: "Are you sure?",
	text: "You will not be able to recover the data!",
	type: "error",
	showCancelButton: true,
	cancelButtonClass: 'btn-secondary waves-effect',
	confirmButtonClass: 'btn-danger waves-effect waves-light events',
	confirmButtonId: id,
	confirmButtonText: 'Ok!'
	});
	});
	$('.danger-page').click(function () {
	id=$(this).attr("id");
	swal({
	title: "Are you sure?",
	text: "You will not be able to recover the data!",
	type: "error",
	showCancelButton: true,
	cancelButtonClass: 'btn-secondary waves-effect',
	confirmButtonClass: 'btn-danger waves-effect waves-light delete_pages',
	confirmButtonId: id,
	confirmButtonText: 'Ok!'
	});
	});
	$('.danger-job-category').click(function () {
	id=$(this).attr("id");
	swal({
	title: "Are you sure?",
	text: "You will not be able to recover the data!",
	type: "error",
	showCancelButton: true,
	cancelButtonClass: 'btn-secondary waves-effect',
	confirmButtonClass: 'btn-danger waves-effect waves-light job-category',
	confirmButtonId: id,
	confirmButtonText: 'Ok!'
	});
	});
	$(document).on('click','.news_report',function () { 
	$.ajax({
	  type: "POST",
	  url: "ajaxCalls.php",
	  data: {"id":id,"action":"deleteReport","deletedUserId":"<?php echo $fgmembersite->idUser(); ?>"} ,
	  success: function(data) {
	$("#news_"+id).css("display","none");
	}
	});
	});
	$(document).on('click','.news_media',function () { 
	$.ajax({
	  type: "POST",
	  url: "ajaxCalls.php",
	  data: {"id":id,"action":"deleteMedia","deletedUserId":"<?php echo $fgmembersite->idUser(); ?>"} ,
	  success: function(data) {
	$("#newsmedia_"+id).css("display","none");
	}
	});
	});
	$(document).on('click','.news_interview',function () { 
	$.ajax({
	  type: "POST",
	  url: "ajaxCalls.php",
	  data: {"id":id,"action":"deleteInterview","deletedUserId":"<?php echo $fgmembersite->idUser(); ?>"} ,
	  success: function(data) {
	$("#interview_"+id).css("display","none");
	}
	});
	});
	$(document).on('click','.news_video',function () { 
	$.ajax({
	  type: "POST",
	  url: "ajaxCalls.php",
	  data: {"id":id,"action":"deleteVideo","deletedUserId":"<?php echo $fgmembersite->idUser(); ?>"} ,
	  success: function(data) {
	$("#video_"+id).css("display","none");
	}
	});
	});
	$(document).on('click','.contact_us',function () { 
	$.ajax({
	  type: "POST",
	  url: "ajaxCalls.php",
	  data: {"id":id,"action":"deleteContactus","deletedUserId":"<?php echo $fgmembersite->idUser(); ?>"} ,
	  success: function(data) {
	$("#video_"+id).css("display","none");
	}
	});
	});
	$(document).on('click','.events',function () { 
	$.ajax({
	  type: "POST",
	  url: "ajaxCalls.php",
	  data: {"id":id,"action":"deleteEvents","deletedUserId":"<?php echo $fgmembersite->idUser(); ?>"} ,
	  success: function(data) {
	$("#event_"+id).css("display","none");
	}
	});
	});
	$(document).on('click','.faculty_page',function () { 
	$.ajax({
	  type: "POST",
	  url: "ajaxCalls.php",
	  data: {"id":id,"action":"deleteUser","deletedUserId":"<?php echo $fgmembersite->idUser(); ?>"} ,
	  success: function(data) {
	$("#product_"+id).css("display","none");
	}
	});
	});
	$(document).on('click','.solution_pages',function () { 
	$.ajax({
	  type: "POST",
	  url: "ajaxCalls.php",
	  data: {"id":id,"action":"deletesolution","deletedUserId":"<?php echo $fgmembersite->idUser(); ?>"} ,
	  success: function(data) {
	$("#solution_"+id).css("display","none");
	}
	});
	});
	$(document).on('click','.tech',function () {        
	$.ajax({
	  type: "POST",
	  url: "ajaxCalls.php",
	  data: {"id":id,"action":"deletetech" ,"deletedUserId":"<?php echo $fgmembersite->idUser(); ?>"} ,
	  success: function(data) {
	$("#tech_"+id).css("display","none");
	}
	});
	});
	$(document).on('click','.delete_pages',function () {        
	$.ajax({
	  type: "POST",
	  url: "ajaxCalls.php",
	  data: {"id":id,"action":"deletePage" } ,
	  success: function(data) {
	$("#page_"+id).css("display","none");
	}
	});
	});
	$(document).on('click','.job-category',function () {        
	$.ajax({
	  type: "POST",
	  url: "ajaxCalls.php",
	  data: {"id":id,"action":"deleteJobCategory" } ,
	  success: function(data) {
	$("#job_category_"+id).css("display","none");
	}
	});
	});
});
</script>

<script type="text/javascript" charset="utf-8">
var j_editor = $.noConflict();
j_editor(document).ready(function() {
	var opts = {
	cssClass : "el-rte",
	// lang     : "ru",
	height   : 100,
	toolbar  : "complete",
	cssfiles : ["assets/plugins/elrte/css/elrte-inner.css"]
	}
	j_editor("#description").elrte(opts);
	j_editor("#selected_publications").elrte(opts);
});
</script>

<?php
/*$sql = mysqli_query($connect,"SELECT * FROM tejas_users");
while($row = mysqli_fetch_array($sql)){
	$id_user = $row['id_user'];
	$page_url = preg_replace("/[^a-zA-Z0-9]+/", "", $row['name']) ;
	$sql1 = mysqli_query($connect,"SELECT * FROM tejas_users WHERE page_url='".$page_url."'");
	if(mysqli_num_rows($sql1)){
		$page_url .= "_".$id_user;
	}
	//echo "UPDATE tejas_users set page_url ='".$page_url."' where id_user=".$id_user;
	mysqli_query($connect,"UPDATE tejas_users set page_url ='".$page_url."' where id_user=".$id_user);
}*/
?>

<?php
admin_right_bar();

admin_footer();
?>