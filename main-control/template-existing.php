<?PHP
session_start();
require_once("./include/membersite_config.php");
include("./include/mysqli_connect.php");
include("./include/html_codes.php");

admin_way_top();

admin_top_bar();

admin_left_menu();


?>
<style>
.edit, .delete, .view{ font-size: 22px;cursor: pointer; color:black;}
.img-responsive{display:block;max-width:100%;height:auto}
.page-title-box{ margin: 0px 0px 20px 0px; }
h4{
	display: inline-block;
}

</style>	
<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
	<!-- Start content -->
	<div class="content">
	<div class="container">

	<?php 
  
	$staffs = mysqli_query($connect,"SELECT * FROM `templates`");
	
	$row_count_admin=mysqli_num_rows($staffs);
	if($row_count_admin){ 
	$i=0;
	while($staff=mysqli_fetch_array($staffs)){
	if($i==0){
	echo '<div class="card-box"><div class=""><div class="col-xs-12"><div class=""><div class="clearfix"></div></div><hr><br></div><div class="row">';
	echo '';
	$i++;
	}
	$id=$staff['id'];
	$template_name=$staff['template_name'];
	$template_icon=$staff['template_icon'];
	$template_link=$staff['template_link'];
	$link='';
	if($template_link!=""){
	$link='<span> <a href="'.$template_link.'?pid=0&tid='.$id.'&tpos=0" ><i class="fa fa-edit edit"></i></a> </span>';
	}
	/* About Us */
	echo '<div class="col-xs-12 col-sm-6 col-lg-2 "  id="tmp_'.$id.'">
	<div class="card-box" style="background-color: white !important;   border-radius:0px !important;">
	<div class="text-xs-center m-b-20">
	</div>
	<div class="col-lg-12 text-xs-center">
	<p>'.stripslashes($template_name).'</p>
	<img src="images/'.$template_icon.'" style="width: 400px;height: 100px;">
	'.$link.'&nbsp;&nbsp;';
	echo '</div>
	</div>
	</div>';
	}
	echo '</div></div></div>';
	} 

	?>
	
	
	
	</div>
	</div>
	
	</div> <!-- container -->

	</div> <!-- content -->
</div>
<!-- End content-page -->


            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->

<!-- Sweet Alert css -->
<link href="assets/plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css" />
<!-- Sweet Alert js -->
<script src="assets/plugins/bootstrap-sweetalert/sweet-alert.min.js"></script>
<script src="assets/pages/jquery.sweet-alert.init.js"></script>

<!-- Editor -->
<script src="//code.jquery.com/jquery-1.12.0.min.js"></script>
<script src="js/jquery.syntaxhighlighter.js"></script>
<script src="assets/plugins/elrte/js/jquery-ui-1.8.13.custom.min.js" type="text/javascript" charset="utf-8"></script>
<link rel="stylesheet" href="assets/plugins/elrte/css/smoothness/jquery-ui-1.8.13.custom.css" type="text/css" media="screen" charset="utf-8">
<script src="assets/plugins/elrte/js/elrte.min.js" type="text/javascript" charset="utf-8"></script>
<link rel="stylesheet" href="assets/plugins/elrte/css/elrte.min.css" type="text/css" media="screen" charset="utf-8">
<script src="assets/plugins/elrte/js/i18n/elrte.ru.js" type="text/javascript" charset="utf-8"></script>



<?php
admin_right_bar();

admin_footer();
?>