<?php include('include/html-codes.php');
include("main-control/include/mysqli_connect.php");
/*************************/
$select_event="SELECT * FROM `page_title_des` WHERE page_name='about us' ORDER BY id DESC LIMIT 1";
$run_product=mysqli_query($connect,$select_event);
while($row_event=mysqli_fetch_array($run_product)){
	$metadescription=$row_event["metadescription"];
	$page_title=$row_event["page_title"];
	$page_description=$row_event["page_description"];
	$eid=$row_event["id"];
	$imgFile=$row_event["image_path"];
	$page_short_description=$row_event["page_short_description"];
}
/*************************/
wayTop();
 ?>
        <!--title-->
        <title>About Us | Biocon</title>    
        <?php css();?>

    </head>
    <body class="about_us">
        <?php nav(); ?>	         
        <!--BANNER SECTION-->
        <section class="main_banner inner-banner" style="background:url(images/banner-images/<?php echo $imgFile; ?>);">
        	
                <div class="banner_content float-right d-flex flex-wrap align-content-center inner-content">
                    <h1 class="blue-text"><?php echo $page_title; ?></h1>
                    <p><?php echo $page_short_description; ?></p>
                </div> 
                <nav aria-label="breadcrumb" class="mt-5 position-absolute pl-md-5 bottom-5">
                    <ol class="breadcrumb">
                    <li class="breadcrumb-item frist-one"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">About Us</li>
                    </ol>
                </nav> 
                 <div class="clearfix"></div>
        </section>
        <div class="clearfix"></div>
        <section class="pt-4 pb-4 pt-md-5 pb-md-5">
            <div class="container-fluid pl-md-5 pr-md-5">
                 <h2 class="border_head blue-text"><span>Leveraging Innovation for Affordable Access</span></h2>
                 <p><?php echo html_entity_decode($page_description); ?></p>
                <div class="row">
                    <div class="col-12 col-lg-6 white-text mt-4">
                        <div class=" col-md-6 col-lg-6 float-left p-0">
                            <div class="box turquoise pt-5 min-hgt-300 d-flex flex-wrap align-content-center">
                                <div class="boxHead text-center no-border m-auto">
                                    <img src="images/icons/vission-mission.png" width="80" class="img-fluid">
                                    <h3 class="h-80">Our Vision, Mission & Values</h3>
                                     <a href="#" class="text-white text-bold">Know More</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-6 float-left p-0 min-hgt-300 d-none d-md-block" style="background:url(images/vission-right-img.jpg) no-repeat;">
                        	
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="col-12 col-lg-6 white-text mt-4">
                        <div class=" col-md-6 col-lg-6 float-left p-0">
                            <div class="box dark-orange pt-5 min-hgt-300 d-flex flex-wrap align-content-center">
                                <div class="boxHead text-center no-border  m-auto">
                                    <img src="images/icons/fact-sheet.png" width="80" class="img-fluid">
                                    <h3 class="h-80">Fact Sheet</h3>
                                    <a href="#" class="text-white text-bold">Know More</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-6 float-left p-0 min-hgt-300 d-none d-md-block" style="background:url(images/fact-sheet-img.jpg) no-repeat;">
                        	
                        </div>
                        <div class="clearfix"></div>
                    </div>
                     <div class="clearfix"></div>
                    <div class="col-12 col-md-6 col-lg-4 white-text mt-4">
                        <div class="box blue-bg">
                            <div class="boxHead">
                                <div class="homeBxicon d-md-none d-lg-flex">
                                	<img src="images/icons/network.png" width="60" class="img-fluid">
                                </div>
                                <h3>Our Leadership Team</h3>
                            </div>
                            <div class="min-hgt-200 d-flex flex-wrap align-content-center">
                                <ul class="custom-list">
                                    <li><a href="#">Board of Directors</a></li>
                                    <li><a href="#">Key Management Team</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-4 white-text mt-4">
                        <div class="box dodger-blue">
                            <div class="boxHead">
                                <div class="homeBxicon d-md-none d-lg-flex">
                                	<img src="images/icons/destination.png" width="60" class="img-fluid">
                                </div>
                                <h3>Our Journey</h3>
                            </div>
                            <div class="min-hgt-200 d-flex flex-wrap align-content-center">
                                <ul class="custom-list">
                                    <li><a href="#">Our Value Drivers</a></li>
                                    <li><a href="#">Milestones</a></li>
                                    <li><a href="#">History</a></li>
                                    <li><a href="#">Awards</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 offset-md-3 offset-lg-0 col-lg-4 white-text mt-4">
                        <div class="box meduim-blue">
                            <div class="boxHead">
                                <div class="homeBxicon d-md-none d-lg-flex">
                                	<img src="images/icons/map.png" width="60" class="img-fluid">
                                </div>
                                <h3>Our Expertise</h3>
                            </div>
                            <div class="min-hgt-200 d-flex flex-wrap align-content-center">
                                <ul class="custom-list">
                                    <li><a href="#">Research & Development</a></li>
                                    <li><a href="#">Manufacturing</a></li>
                                    <li><a href="#">Quality & Compliance</a></li>
                                </ul>
                            </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </section>
        <div class="clearfix"></div>
        <?php press_release();
              life_at_biocon();
        ?>
        
        <section class="pb-4 pb-md-5">
            <div class="container-fluid pl-md-5 pr-md-5">
                 <h2 class="border_head blue-text"><span>Kiran mazumdar-Shaw</span></h2>
                 <div class="turquoise">
                    <div class="row">
                        <div class="col-12 col-md-5 col-lg-4 text-center">
                            <img src="images/kiran-mazumdar-shaw.jpg" class="img-fluid min-hgt-150">
                        </div>
                        <div class="col-12  col-md-7 col-lg-8">
                            <div class="pt-3 pl-3 pr-3 pl-md-0 pr-md-0 pb-3 img-with-text">
                                <p class="text-white mb-2 mb-md-1">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. </p>
                                <a href="#" class="text-white text-bold">Know More</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php
			biocon_live();
		?>
        <section class="pb-4 pb-md-5 heading-with-content">
            <div class="container-fluid pl-md-5 pr-md-5">
                 <h2 class="border_head blue-text"><span> Compassion and Care are a Part of Our DNA</span></h2>
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 white-text mt-4">
                        <div class=" col-md-6 col-lg-6 float-left p-0">
                            <div class="box royal-blue min-hgt-300 d-flex flex-wrap align-content-center">
                                <div class="boxHead">
                                    <h3>Biocon Foundation </h3>
                                </div>
                                <div class="boxHead no-border m-auto">
                                   <strong>Lake Revival</strong>
                                    <p>Biocon Foundation aims at ensuring environmental sustainaibility by undertaking the ambitious initiative of contributing to the lake revival mission of Bengaluru.
                                    </p>
                                </div>
                                <a href="#" class="read_more right-10">View All</a>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-6 float-left p-0 min-hgt-300 d-none d-md-block" style="background:url(images/compassion_1.jpg) no-repeat;background-size:cover;">
                            
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 white-text mt-4">
                        <div class=" col-md-6 col-lg-6 float-left p-0">
                            <div class="box turquoise min-hgt-300 d-flex flex-wrap align-content-center">
                                <div class="boxHead">
                                    <h3>Biocon Academy </h3>
                                </div>
                                <div class="boxHead no-border m-auto">
                                    <p>Biocon Academy is committed to create a globally competitive biotech ecosystem in India through skill development programs at its Centre of Excellence for Advanced Learning in Applied Biosciences.</p>
                                </div>
                                <a href="#" class="read_more right-10">View All</a>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-6 float-left p-0 min-hgt-300 d-none d-md-block" style="background:url(images/compassion_2.jpg) no-repeat;background-size:cover;">
                            
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </section>
        <?php footer(); ?>
    
        <?php script(); ?>  
    </body>
</html>  
 