<?php include('include/html-codes.php');
wayTop();
 ?>
    <!--title-->
    <title>Awards | Biocon</title>
    <?php css();?>

        </head>

        <body class="awards">
            <?php nav(); ?>
                <!--BANNER SECTION-->
                <section class="main_banner inner-banner mobile_banner" style="background:url(images/banner-images/awards_bg.jpg);">

                    <div class="banner_content mobile_content float-right d-flex flex-wrap align-content-center inner-content">
                        <h1 class="blue-text text-right-absolute mobile_h1">Awards</h1>

                    </div>
                    <nav aria-label="breadcrumb" class="mt-5 position-absolute pl-md-5 bottom-5">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item frist-one"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Awards</li>
                        </ol>
                    </nav>
                    <div class="clearfix"></div>
                </section>
                <div class="clearfix"></div>
                <section class="pt-4 pb-4 pt-md-5 pb-md-5">
                    <div class="container-fluid pl-md-5 pr-md-5">
                        <h2 class="border_head blue-text"><span>Overview</span></h2>
                        <p>Biocon’s strong brand equity, commitment to address unmet patient needs, steadfast pursuit of affordable innovation and a keen sense of responsibility towards the society and the environment are reflected in the awards and accolades conferred upon the Company. Our efforts have been recognized in areas ranging from business management to talent engagement to technology implementation as well as CSR. These awards continue to motivate team Biocon to pursue new challenges every day. </p>

                        <h1 class="text-center blue-text mt-5">Awards & Recognitions</h1>

                        <div class="awardsTab">
                            <form class="mt-2 mb-2">
                                <select id='mySelect' class="color-navy-blue">
                                    <option value='0'>2018</option>
                                    <option value='1'>2017</option>

                                </select>
                            </form>
                            <ul class="nav nav-tabs" id="myTab" style="display: none;">
                                <li class="active"><a href="#tab2018">2018</a></li>
                                <li><a href="#tab2017">2017</a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane fade show active" id="tab2018" role="tabpanel" aria-labelledby="awards_Tab">
                                    <div class="p-2 p-sm-4">

                                        <div class="media d-block d-sm-flex p-2 p-sm-2 p-md-0">
                                            <img class="d-flex align-self-center mr-3" src="images/awards/awards_01.jpg" alt="" width="250">
                                            <div class="align-self-center media-body p-1 p-sm-0">
                                                <h4 class="mt-0 mb-2 color-navy-blue">Biocon felicitated at World HRD Congress 2018 for ‘Excellence in Training & Development’.</h4>
                                                <p class="mb-0">The excellent L&D initiatives at Biocon has contributed to the company being ranked amongst the Global Top 10 Biotech Employers. </p>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="p-2 p-sm-4">

                                        <div class="media d-block d-sm-flex p-2 p-sm-2 p-md-0">
                                            <img class="d-flex align-self-center mr-3" src="images/awards/awards_02.jpg" alt="" width="250">
                                            <div class="align-self-center media-body p-1 p-sm-0">
                                                <h4 class="mt-0 mb-2 color-navy-blue">Biocon Recognized for its SFE Digital Initiative, in BFI Conferred with 
DigiPharmaX 2017 Award.</h4>
                                                <p class="mb-0">Biocon received the award under the category of ‘Leveraging technology for use of both digital and onsite promotion for integrated digital marketing’.</p>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="p-2 p-sm-4">

                                        <div class="media d-block d-sm-flex p-2 p-sm-2 p-md-0">
                                            <img class="d-flex align-self-center mr-3" src="images/awards/awards_03.jpg" alt="" width="250">
                                            <div class="align-self-center media-body p-1 p-sm-0">
                                                <h4 class="mt-0 mb-2 color-navy-blue">Biocon conferred with ‘The IDMA Margi Memorial Best Biotech Patents award for 2016-17’</h4>
                                                <p class="mb-0">by the Indian Drug Manufacturers’ Association. Biocon was recognized for 21 patent families (34 patents) granted in the year 2016-17.</p>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="p-2 p-sm-4">

                                        <div class="media d-block d-sm-flex p-2 p-sm-2 p-md-0">
                                            <img class="d-flex align-self-center mr-3" src="images/awards/awards_04.jpg" alt="" width="250">
                                            <div class="align-self-center media-body p-1 p-sm-0">
                                                <h4 class="mt-0 mb-2 color-navy-blue">Biocon was honoured with the prestigious IDMA Corporate Citizen Award 2017 at the 56th Annual Day celebrations of the Indian Drug Manufacturers' Association.</h4>
                                                <p class="mb-0">Biocon won the coveted award under the category of turnover above INR 500 Crores.</p>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="tab-pane fade" id="tab2017" role="tabpanel" aria-labelledby="awards_Tab">
                                    <div class="p-2 p-sm-4">

                                        <div class="media d-block d-sm-flex p-2 p-sm-2 p-md-0">
                                            <img class="d-flex align-self-center mr-3" src="images/awards/awards_02.jpg" alt="" width="250">
                                            <div class="align-self-center media-body p-1 p-sm-0">
                                                <h4 class="mt-0 mb-2 color-navy-blue">Biocon Recognized for its SFE Digital Initiative, in BFI Conferred with 
DigiPharmaX 2017 Award.</h4>
                                                <p class="mb-0">Biocon received the award under the category of ‘Leveraging technology for use of both digital and onsite promotion for integrated digital marketing’.</p>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="p-2 p-sm-4">

                                        <div class="media d-block d-sm-flex p-2 p-sm-2 p-md-0">
                                            <img class="d-flex align-self-center mr-3" src="images/awards/awards_03.jpg" alt="" width="250">
                                            <div class="align-self-center media-body p-1 p-sm-0">
                                                <h4 class="mt-0 mb-2 color-navy-blue">Biocon conferred with ‘The IDMA Margi Memorial Best Biotech Patents award for 2016-17’</h4>
                                                <p class="mb-0">by the Indian Drug Manufacturers’ Association. Biocon was recognized for 21 patent families (34 patents) granted in the year 2016-17.</p>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="p-2 p-sm-4">
                                <a href="#" class="btn archive_btn">
                                    Archive <img src="images/icons/next-arrow-dark-blue.png">
                                </a>
                            </div>
                            
                        </div>
                    </div>

                </section>
                <div class="clearfix"></div>

                <!-- <div class="tab-content">
    <div class="tab-pane active" id="home">Home content</div>
    <div class="tab-pane" id="profile">Profile content</div>
    <div class="tab-pane" id="messages">Messages content</div>
    <div class="tab-pane" id="settings">Settings content</div>
</div> -->

                <?php footer(); ?>

                    <?php script(); ?>

                        <script type="text/javascript">
                            $('#mySelect').on('change', function(e) {
                                $('#myTab li a').eq($(this).val()).tab('show');
                            });
                        </script>
        </body>

        </html>