<?php include('include/html-codes.php');
wayTop();
?>
   <!--title-->
   <title>404 Page not found | Biocon</title>
   <?php css();?>
<style>
.fnt-100{font-size:150px;}
</style>
</head>

<body class="four-0-4">
<?php nav(); ?>	 
   <section class="pt-5 pb-5">
       <div class="container text-center">
            <h1 class="fnt-100 mt-5">404</h1>       
           <h2>THE PAGE NOT FOUND</h2>
           <button class="btn btn-default royal-blue white-text">Back To Homepage</button>
       </div>
   </section>    
<?php footer(); ?>
<?php script(); ?>

</body>
</html>