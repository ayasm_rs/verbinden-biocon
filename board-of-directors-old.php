<?php include('include/html-codes.php');
wayTop();
 ?>
        <!--title-->
        <title>Board of Directors | Biocon</title>    
        <?php css();?>

    </head>
    <body class="board_of_directors">
        <?php nav(); ?>	         
        <!--BANNER SECTION-->
        <section class="main_banner inner-banner" style="background:url(images/banner-images/board-of-directors.jpg);">
        	
                <div class="banner_content float-right d-flex flex-wrap align-content-center inner-content">
                    <h1 class="blue-text">Board of Directors</h1>
                   
                </div> 
                <nav aria-label="breadcrumb" class="mt-5 position-absolute pl-md-5 bottom-5">
                    <ol class="breadcrumb">
                    <li class="breadcrumb-item frist-one"><a href="/">Home</a></li>
                    <li class="breadcrumb-item frist-one"><a href="about-us.php">About Us</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Board of Directors</li>
                    </ol>
                </nav> 
                 <div class="clearfix"></div>
        </section>
        <div class="clearfix"></div>
        <section class="pt-4 pb-4 pt-md-5 pb-md-5">
            <div class="container-fluid pl-md-5 pr-md-5">
                <h2 class="border_head blue-text"><span>Overview</span></h2>                 
                <p>The composition of Biocon’s Board of Directors reflects the vision of bringing together a diverse and multidisciplinary group of erudite and experienced professionals who can contribute towards providing strategic direction to the Company’s management to pursue its stated mission of enhancing global healthcare whilst upholding the highest standards of Corporate Governance.</p>
                <p>Our board’s diversity, in terms of gender, age, experience, ethnicity, geography, and industry expertise, contributes significantly to enriching the quality of the Company’s decision-making process. Our directors have vast insights and experience in various fields such as Research & Innovation, Corporate & Financial Management, Regulatory & Compliance, Global Healthcare and International Marketing.</p>
                <div class="row">                    
                    <div class="col-12 col-lg-6 white-text mt-4">
                        <div class="col-sm-6 col-sm-6 col-md-6 col-lg-6 float-left p-0">
                            <div class="box royal-blue pt-5 min-hgt-300 d-flex flex-wrap align-content-center">
                                <div class="boxHead text-center no-border m-auto">
                                    <h4 class="mb-0 text-uppercase">Ms. Kiran Mazumdar Shaw</h4>
                                    <p class="pl-md-4 pr-md-4">Chairperson &  Managing Director </p>
                                    <a href="#" class="text-white text-bold" data-toggle="modal" data-target="#myModal">Know More</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-6 col-lg-6 float-left p-0 min-hgt-300  bg-cover" style="background:url(images/member/kiran-mazumdar-shaw.jpg) no-repeat;">
                            
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="col-12 col-lg-6 white-text mt-4">
                        <div class=" col-sm-6 col-md-6 col-lg-6 float-left p-0">
                            <div class="box violet pt-5 min-hgt-300 d-flex flex-wrap align-content-center">
                                <div class="boxHead text-center no-border m-auto">
                                    <h4 class="mb-0 text-uppercase">John Shaw</h4>
                                    <p class="pl-md-4 pr-md-4">Vice Chairman & Non-Executive Director</p>
                                    <a href="#" class="text-white text-bold">Know More</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-6 col-lg-6 float-left p-0 min-hgt-300  bg-cover" style="background:url(images/member/john-shaw.jpg) no-repeat;">
                            
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="col-12 col-lg-6 white-text mt-4">
                        <div class=" col-sm-6 col-md-6 col-lg-6 float-left p-0">
                            <div class="box turquoise pt-5 min-hgt-300 d-flex flex-wrap align-content-center">
                                <div class="boxHead text-center no-border m-auto">
                                    <h4 class="mb-0 text-uppercase">Dr. Arun Chandavarkar</h4>
                                    <p class="pl-md-4 pr-md-4">Chief Executive Officer & 
Joint Managing Director</p>
                                    <a href="#" class="text-white text-bold">Know More</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-6 col-lg-6 float-left p-0 min-hgt-300  bg-cover" style="background:url(images/member/arun-chandavarkar.jpg) no-repeat;">
                            
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="col-12 col-lg-6 white-text mt-4">
                        <div class=" col-sm-6 col-md-6 col-lg-6 float-left p-0">
                            <div class="box dark-orange pt-5 min-hgt-300 d-flex flex-wrap align-content-center">
                                <div class="boxHead text-center no-border m-auto">
                                    <h4 class="mb-0 text-uppercase">Prof. Ravi Mazumdar</h4>
                                    <p class="pl-md-4 pr-md-4">Non-Executive Director</p>
                                    <a href="#" class="text-white text-bold">Know More</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-6 col-lg-6 float-left p-0 min-hgt-300  bg-cover" style="background:url(images/member/ravi.jpg) no-repeat;">
                            
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="col-12 col-lg-6 white-text mt-4">
                        <div class=" col-sm-6 col-md-6 col-lg-6 float-left p-0">
                            <div class="box meduim-blue pt-5 min-hgt-300 d-flex flex-wrap align-content-center">
                                <div class="boxHead text-center no-border m-auto">
                                    <h4 class="mb-0 text-uppercase">Russell Walls</h4>
                                    <p class="pl-md-4 pr-md-4">Independent Director
</p>
                                    <a href="#" class="text-white text-bold">Know More</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-6 col-lg-6 float-left p-0 min-hgt-300  bg-cover" style="background:url(images/member/russell-walls.jpg) no-repeat;">
                            
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="col-12 col-lg-6 white-text mt-4">
                        <div class=" col-sm-6 col-md-6 col-lg-6 float-left p-0">
                            <div class="box sky-blue pt-5 min-hgt-300 d-flex flex-wrap align-content-center">
                                <div class="boxHead text-center no-border m-auto">
                                    <h4 class="mb-0 text-uppercase">Mary Harney</h4>
                                    <p class="pl-md-4 pr-md-4">Independent Director</p>
                                    <a href="#" class="text-white text-bold">Know More</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-6 col-lg-6 float-left p-0 min-hgt-300  bg-cover" style="background:url(images/member/mary-harney.jpg) no-repeat;">
                            
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="col-12 col-lg-6 white-text mt-4">
                        <div class=" col-sm-6 col-md-6 col-lg-6 float-left p-0">
                            <div class="box dodger-blue pt-5 min-hgt-300 d-flex flex-wrap align-content-center">
                                <div class="boxHead text-center no-border m-auto">
                                    <h4 class="mb-0 text-uppercase">Daniel M. Bradbury</h4>
                                    <p class="pl-md-4 pr-md-4">Independent Director
</p>
                                    <a href="#" class="text-white text-bold">Know More</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-6 col-lg-6 float-left p-0 min-hgt-300  bg-cover" style="background:url(images/member/daniel-m-bradbury.jpg) no-repeat;">
                            
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="col-12 col-lg-6 white-text mt-4">
                        <div class=" col-sm-6 col-md-6 col-lg-6 float-left p-0">
                            <div class="box dark-orange pt-5 min-hgt-300 d-flex flex-wrap align-content-center">
                                <div class="boxHead text-center no-border m-auto">
                                    <h4 class="mb-0 text-uppercase">Dr. Jeremy Levin</h4>
                                    <p class="pl-md-4 pr-md-4">Independent Director</p>
                                    <a href="#" class="text-white text-bold">Know More</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-6 col-lg-6 float-left p-0 min-hgt-300  bg-cover" style="background:url(images/member/jeremy-levin.jpg) no-repeat;">
                            
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="col-12 col-lg-6 white-text mt-4">
                        <div class=" col-sm-6 col-md-6 col-lg-6 float-left p-0">
                            <div class="box royal-blue pt-5 min-hgt-300 d-flex flex-wrap align-content-center">
                                <div class="boxHead text-center no-border m-auto">
                                    <h4 class="mb-0 text-uppercase">Dr. Vijay Kuchroo</h4>
                                    <p class="pl-md-4 pr-md-4">Independent Director
</p>
                                    <a href="#" class="text-white text-bold">Know More</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-6 col-lg-6 float-left p-0 min-hgt-300  bg-cover" style="background:url(images/member/vijay-kuchroo.jpg) no-repeat;">
                            
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="col-12 col-lg-6 white-text mt-4">
                        <div class=" col-sm-6 col-md-6 col-lg-6 float-left p-0">
                            <div class="box violet pt-5 min-hgt-300 d-flex flex-wrap align-content-center">
                                <div class="boxHead text-center no-border m-auto">
                                    <h4 class="mb-0 text-uppercase">M. Damodaran</h4>
                                    <p class="pl-md-4 pr-md-4">Independent Director</p>
                                    <a href="#" class="text-white text-bold">Know More</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-6 col-lg-6 float-left p-0 min-hgt-300  bg-cover" style="background:url(images/member/damodaran.jpg) no-repeat;">
                            
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="col-12 col-lg-6 white-text mt-4">
                        <div class=" col-sm-6 col-md-6 col-lg-6 float-left p-0">
                            <div class="box turquoise pt-5 min-hgt-300 d-flex flex-wrap align-content-center">
                                <div class="boxHead text-center no-border m-auto">
                                    <h4 class="mb-0 text-uppercase">Bobby Kanubhai Parikh</h4>
                                    <p class="pl-md-4 pr-md-4">Independent Director</p>
                                    <a href="#" class="text-white text-bold">Know More</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-6 col-lg-6 float-left p-0 min-hgt-300  bg-cover" style="background:url(images/member/bobby-kanubhai-parikh.jpg) no-repeat;">
                            
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                </div>
        </section>
         <?php our_values();?>
        <section class="pb-4 pb-md-5 sec_02 without-color">
       <div class="container-fluid">
          <h2 class="border_head blue-text mb-3"><span>BIOCON LIVE</span></h2>
          <div class="row">
             
             <div class="col-sm col-md-6 col-lg-3 mt-4">
                <div class="box">
                   <div class="boxHead"><h3 class="text-uppercase color-royal-blue">PRESS RELEASES </h3>
                                           
                   </div>
                   <div  style="min-height: 220px">
                      <h4 class="mb-1">Biocon’s Sterile Drug Product facility Receives EU...</h4>
                      <p><small><em>Bengaluru, India, July 4, 2018</em></small></p>
                   
                      <h4 class="mb-1">Biocon retains economic interest in Etanercept...</h4>
                      <p><small><em>Bengaluru, India, July 28, 2018</em></small></p>
                   </div>
                   <a href="#" class="read_more right-10 color-dark-gray">View All</a>
                </div>
             </div>
             <div class="col-sm-6 col-md-6 col-lg-3 mt-4">
                <div class="box ">
                   <div class="boxHead">
                      <h3 class="text-uppercase color-royal-blue">Biocon in News</h3>
                   </div>
                   <div style="min-height: 220px">
                      <img src="images/news_01.jpg" class="img-fluid">
                      <h4 class="mb-1 mt-2">Biocon Q1 PAT seen up 70.8% YoY to Rs. 138.9 cr: Kotak</h4>
                      <p><small><em>18 May, 2018 | Source: PTI</em></small></p>
                   </div>
                   <a href="#" class="read_more right-10 color-dark-gray">View All</a>
                </div>
             </div>
             <div class="col-sm col-md-6 col-lg-3 mt-4">
                <div class="box ">
                   <div class="boxHead">
                      <h3 class="text-uppercase color-royal-blue">Biocon on Twitter</h3>
                   </div>
                   <div id="twitter_content" style="margin-bottom: 25px;border-radius: 0px;background: #fff;min-height: 220px">
                      <a class="twitter-timeline" width="100%" href="https://twitter.com/Bioconlimited?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor" data-tabs="timeline"  data-width="300" data-height="200" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"></a>
                      <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
                   </div>
                   <a href="#" class="read_more right-10 color-dark-gray">View on Twitter</a>
                </div>
             </div>
             <div class="col-sm-6 col-md-6 col-lg-3 mt-4">
                <div class="box">
                   <div class="boxHead">
                      <h3 class="text-uppercase color-royal-blue">Media Stories</h3>
                   </div>
                   <div style="min-height: 220px">
                      <img src="images/news_02.jpg" class="img-fluid">
                      <h4 class="mb-1 mt-2">U.S. FDA approves
Biocon-Mylan’s first biosimilar of cancer...</h4>
                      <p><small><em>06 June, 2018 | Source: CNBC TV18</em></small></p>
                   </div>
                   <a href="#" class="read_more right-10 color-dark-gray">View All</a>
                </div>
             </div>
          </div>
       </div>
    </section>
    <!-- Modal Ms. Kiran Mazumdar-Shaw-->
    <div id="myModal" class="modal fade popup" role="dialog">
        <div class="modal-dialog">        
            <!-- Modal content-->
            <div class="modal-content p-4">
                <div class="text-right position-absolute right-top-0"><button type="button" class="close" data-dismiss="modal">&times;</button> </div>
                <div class="col-12"> 
                     
                    <div class="row">            
                        <div class="col-12 col-md-5 "><img src="images/member/kiran-mazumdar-shaw.jpg"  class="img-fluid" /></div>
                        <div class="col-12 col-md-7 ">
                            <h3 class="color-royal-blue">Ms. Kiran Mazumdar-Shaw </h3>
                            <p>First generation entrepreneur with over four decades of experience in biotechnology + Global business leader + Board member, Infosys, Narayana Health + Full-time member, Board of Trustees, MIT, U.S. + Fellow of the Australian Academy of Technology and Engineering + Recipient of Indian civilian honors Padma Shri & Padma Bhushan + Highest French civilian honor Chevalier de l’Ordre National de la Légion d’Honneur + AWSM Award for Excellence by Feinstein Institute for Medical Research, U.S. + Othmer Gold Medal by Chemical Heritage Foundation, U.S.+ Forbes ‘World’s Most Powerful Women’ + Forbes ‘World's Self-Made Women Billionaires’.</p>
                        <a href="#" class="color-royal-blue col-12 col-md-9 col-lg-8">Know more about Kiran Mazumdar shaw</a>
                        </div>
                    </div>
                </div>
            </div>        
        </div>
    </div>
        <?php footer(); ?>
    
        <?php script(); ?>  
    </body>
</html>  
 