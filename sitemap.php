<?php 
header("Content-Type: text/xml");
$xmlBody = '<?xml version="1.0" encoding="UTF-8"?>';
$xmlBody .= "<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">";
	include("includes/mysqli_connect.php");	
	
	function clean($string) { 
		$string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.                 
		return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
	}
	
	$site_url = "";
	
	$site_details = mysqli_query($connect,"SELECT * FROM site_details WHERE parameter = 'site_url' LIMIT 0,1 ");
	while($row_site_detail = mysqli_fetch_array($site_details)){
		$site_url = $row_site_detail["value"]; 
	}	

			$finaldate = "";
			$sql_latestupdate = "SELECT * FROM latest_updates ORDER BY updated_date_time DESC LIMIT 0,1 ";
			$datas_latestupdate = mysqli_query($connect,$sql_latestupdate);
			while($row_latestupdate = mysqli_fetch_array($datas_latestupdate)){
				$finaldate 		= $row_latestupdate['updated_date_time'];
				$finaldate 		= date('Y-m-d',strtotime($finaldate));
			}		
			
	$xmlBody .= '
		<url>
		  <loc>'. $site_url.'</loc>
		  <lastmod>'. $finaldate .'T18:43:37+00:00</lastmod>
		  <changefreq>weekly</changefreq>
		  <priority>1</priority>
		</url>';
		
		$xmlBody .= '
		<url>
		  <loc>'. $site_url.'investor-relations.php'.'</loc>
		  <lastmod>'. $finaldate.'T18:43:37+00:00</lastmod>
		  <changefreq>weekly</changefreq>
		  <priority>0.80</priority>
		</url>';
		
		$xmlBody .= '
		<url>
		  <loc>'. $site_url.'financial-information-quarterly-results.php'.'</loc>
		  <lastmod>'. $finaldate.'T18:43:37+00:00</lastmod>
		  <changefreq>weekly</changefreq>
		  <priority>0.80</priority>
		</url>';
		
		$xmlBody .= '
		<url>
		  <loc>'. $site_url.'financial-information-subsidiaries.php'.'</loc>
		  <lastmod>'. $finaldate.'T18:43:37+00:00</lastmod>
		  <changefreq>weekly</changefreq>
		  <priority>0.80</priority>
		</url>';
		
		$xmlBody .= '
		<url>
		  <loc>'. $site_url.'annual-reports.php'.'</loc>
		  <lastmod>'. $finaldate.'T18:43:37+00:00</lastmod>
		  <changefreq>weekly</changefreq>
		  <priority>0.80</priority>
		</url>';
		
		$xmlBody .= '
		<url>
		  <loc>'. $site_url.'shareholders_agm.php'.'</loc>
		  <lastmod>'. $finaldate.'T18:43:37+00:00</lastmod>
		  <changefreq>weekly</changefreq>
		  <priority>0.80</priority>
		</url>';
		
		$xmlBody .= '
		<url>
		  <loc>'. $site_url.'shareholder-chairman-speech.php'.'</loc>
		  <lastmod>'. $finaldate.'T18:43:37+00:00</lastmod>
		  <changefreq>weekly</changefreq>
		  <priority>0.80</priority>
		</url>';
		
		$xmlBody .= '
		<url>
		  <loc>'. $site_url.'investor_contact.php'.'</loc>
		  <lastmod>'. $finaldate.'T18:43:37+00:00</lastmod>
		  <changefreq>weekly</changefreq>
		  <priority>0.80</priority>
		</url>';
		
		$xmlBody .= '
		<url>
		  <loc>'. $site_url.'stock-exchange-filing-shareholding-patterns.php'.'</loc>
		  <lastmod>'. $finaldate.'T18:43:37+00:00</lastmod>
		  <changefreq>weekly</changefreq>
		  <priority>0.80</priority>
		</url>';
		
		$xmlBody .= '
		<url>
		  <loc>'. $site_url.'stock-exchange-filing-corporate-governance.php'.'</loc>
		  <lastmod>'. $finaldate.'T18:43:37+00:00</lastmod>
		  <changefreq>weekly</changefreq>
		  <priority>0.80</priority>
		</url>';
		
		$xmlBody .= '
		<url>
		  <loc>'. $site_url.'stock-exchange-filing-investor-complaints.php'.'</loc>
		  <lastmod>'. $finaldate.'T18:43:37+00:00</lastmod>
		  <changefreq>weekly</changefreq>
		  <priority>0.80</priority>
		</url>';
		
		$xmlBody .= '
		<url>
		  <loc>'. $site_url.'stock-exchange-filing-other-filing.php'.'</loc>
		  <lastmod>'. $finaldate.'T18:43:37+00:00</lastmod>
		  <changefreq>weekly</changefreq>
		  <priority>0.80</priority>
		</url>';
		
		$xmlBody .= '
		<url>
		  <loc>'. $site_url.'policies-codes.php'.'</loc>
		  <lastmod>'. $finaldate.'T18:43:37+00:00</lastmod>
		  <changefreq>weekly</changefreq>
		  <priority>0.80</priority>
		</url>';
		
		$xmlBody .= '
		<url>
		  <loc>'. $site_url.'other-documents.php'.'</loc>
		  <lastmod>'. $finaldate.'T18:43:37+00:00</lastmod>
		  <changefreq>weekly</changefreq>
		  <priority>0.80</priority>
		</url>';
		
		$xmlBody .= '
		<url>
		  <loc>'. $site_url.'analyst-coverage.php'.'</loc>
		  <lastmod>'. $finaldate.'T18:43:37+00:00</lastmod>
		  <changefreq>weekly</changefreq>
		  <priority>0.80</priority>
		</url>';
		
		$xmlBody .= '
		<url>
		  <loc>'. $site_url.'investor-presentation.php'.'</loc>
		  <lastmod>'. $finaldate.'T18:43:37+00:00</lastmod>
		  <changefreq>weekly</changefreq>
		  <priority>0.80</priority>
		</url>';
		
		$xmlBody .= '
		<url>
		  <loc>'. $site_url.'customer-services.php'.'</loc>
		  <lastmod>'. $finaldate.'T18:43:37+00:00</lastmod>
		  <changefreq>weekly</changefreq>
		  <priority>0.80</priority>
		</url>';
		
		$xmlBody .= '
		<url>
		  <loc>'. $site_url.'company-history.php'.'</loc>
		  <lastmod>'. $finaldate.'T18:43:37+00:00</lastmod>
		  <changefreq>weekly</changefreq>
		  <priority>0.80</priority>
		</url>';
		
		$xmlBody .= '
		<url>
		  <loc>'. $site_url.'board-of-directors.php'.'</loc>
		  <lastmod>'. $finaldate.'T18:43:37+00:00</lastmod>
		  <changefreq>weekly</changefreq>
		  <priority>0.80</priority>
		</url>';
		
		$xmlBody .= '
		<url>
		  <loc>'. $site_url.'key-management.php'.'</loc>
		  <lastmod>'. $finaldate.'T18:43:37+00:00</lastmod>
		  <changefreq>weekly</changefreq>
		  <priority>0.80</priority>
		</url>';
		
		$xmlBody .= '
		<url>
		  <loc>'. $site_url.'awards-and-recogniton.php'.'</loc>
		  <lastmod>'. $finaldate.'T18:43:37+00:00</lastmod>
		  <changefreq>weekly</changefreq>
		  <priority>0.80</priority>
		</url>';
		
		$xmlBody .= '
		<url>
		  <loc>'. $site_url.'corporate-social-responsibility.php'.'</loc>
		  <lastmod>'. $finaldate.'T18:43:37+00:00</lastmod>
		  <changefreq>weekly</changefreq>
		  <priority>0.80</priority>
		</url>';
		
		$xmlBody .= '
		<url>
		  <loc>'. $site_url.'contact.php'.'</loc>
		  <lastmod>'. $finaldate.'T18:43:37+00:00</lastmod>
		  <changefreq>weekly</changefreq>
		  <priority>0.80</priority>
		</url>';
		
		$xmlBody .= '
		<url>
		  <loc>'. $site_url.'careers/'.'</loc>
		  <lastmod>'. $finaldate.'T18:43:37+00:00</lastmod>
		  <changefreq>weekly</changefreq>
		  <priority>0.80</priority>
		</url>';
	
	$select_product=mysqli_query($connect,"SELECT a.* FROM category a,products b WHERE b.category=a.cate_name GROUP BY id");
	while($row_head=mysqli_fetch_array($select_product)){
		$cate_name=$row_head["cate_name"];
			$pro_name=mysqli_query($connect,"SELECT * FROM `products` WHERE category='$cate_name'");
			while($row_pro=mysqli_fetch_array($pro_name)){
                $xmlBody .= '
			<url>
			  <loc>'. $site_url.'products/'.strtolower($row_pro["pro_page_url"]).'</loc>
			  <lastmod>'. $finaldate .'T18:43:37+00:00</lastmod>
			  <changefreq>weekly</changefreq>
			  <priority>0.80</priority>
			</url>';
			}
		}
		
	
	$solution_name=mysqli_query($connect,"SELECT * FROM `solutions`");
		while($row_solution=mysqli_fetch_array($solution_name)){
                $xmlBody .= '
			<url>
			  <loc>'. $site_url.'solution/'.str_replace(" ","-",strtolower($row_solution['solu_name'])).'</loc>
			  <lastmod>'. $finaldate .'T18:43:37+00:00</lastmod>
			  <changefreq>weekly</changefreq>
			  <priority>0.80</priority>
			</url>';
			}	
	
	$technologies=mysqli_query($connect,"SELECT * FROM `technologies`");
		while($row_technologies=mysqli_fetch_array($technologies)){
                $xmlBody .= '
			<url>
			  <loc>'. $site_url.'technologies/'.str_replace(" ","-",str_replace("-","-",strtolower($row_technologies['tech_metakeyword']))).'</loc>
			  <lastmod>'. $finaldate .'T18:43:37+00:00</lastmod>
			  <changefreq>weekly</changefreq>
			  <priority>0.80</priority>
			</url>';
			}	
	
	$news_rooms=mysqli_query($connect,"SELECT * FROM `news_rooms`  ORDER BY id DESC");
		while($row_news_rooms=mysqli_fetch_array($news_rooms)){
                $xmlBody .= '
			<url>
			  <loc>'. $site_url.'newsroom-details/'.strtolower($row_news_rooms['news_url']).'</loc>
			  <lastmod>'. $finaldate .'T18:43:37+00:00</lastmod>
			  <changefreq>weekly</changefreq>
			  <priority>0.80</priority>
			</url>';
			}	
			
	$run_total=mysqli_query($connect,"SELECT * FROM `news_rooms`");
	$total_pages=mysqli_num_rows($run_total);
	$limit = 5;
	$first_page = 1;
	$lastpage = ceil($total_pages/$limit);
		while($first_page<=$lastpage){
			
                $xmlBody .= '
			<url>
			  <loc>'. $site_url.'newsroom-press-releases/'.$first_page++.'</loc>
			  <lastmod>'. $finaldate .'T18:43:37+00:00</lastmod>
			  <changefreq>weekly</changefreq>
			  <priority>0.80</priority>
			</url>';
			
			}
			
	$run_total=mysqli_query($connect,"SELECT * FROM `news_interview`");
	$total_pages=mysqli_num_rows($run_total);
	$limit = 5;
	$first_page = 1;
	$lastpage = ceil($total_pages/$limit);
		while($first_page<=$lastpage){
			
                $xmlBody .= '
			<url>
			  <loc>'. $site_url.'newsroom-interviews/'.$first_page++.'</loc>
			  <lastmod>'. $finaldate .'T18:43:37+00:00</lastmod>
			  <changefreq>weekly</changefreq>
			  <priority>0.80</priority>
			</url>';
			
			}
			
	$run_total=mysqli_query($connect,"SELECT * FROM `news_article`");
	$total_pages=mysqli_num_rows($run_total);
	$limit = 5;
	$first_page = 1;
	$lastpage = ceil($total_pages/$limit);
		while($first_page<=$lastpage){
			
                $xmlBody .= '
			<url>
			  <loc>'. $site_url.'newsroom-media-relations/'.$first_page++.'</loc>
			  <lastmod>'. $finaldate .'T18:43:37+00:00</lastmod>
			  <changefreq>weekly</changefreq>
			  <priority>0.80</priority>
			</url>';
			
			}
			
	$run_total=mysqli_query($connect,"SELECT * FROM `news_video`");
	$total_pages=mysqli_num_rows($run_total);
	$limit = 5;
	$first_page = 1;
	$lastpage = ceil($total_pages/$limit);
		while($first_page<=$lastpage){
			
                $xmlBody .= '
			<url>
			  <loc>'. $site_url.'newsroom-videos/'.$first_page++.'</loc>
			  <lastmod>'. $finaldate .'T18:43:37+00:00</lastmod>
			  <changefreq>weekly</changefreq>
			  <priority>0.80</priority>
			</url>';
			
			}
		
	
	$xmlBody .= "</urlset>";
	echo $xmlBody;
	
	
?>	