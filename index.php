<?php include('include/html-codes.php');
wayTop();
 ?>
	 	<!--title-->
	 	<title>Biocon - India's largest biopharmaceutical company</title>
   		<?php css();?>

  	</head>
	<body class="home">
  		<?php nav(); ?>
<!--Banner-->
		<section class="main_banner">
			<div class="container-fluid">
				<div class="banner_content pt-5 pb-5 float-right">
			         <h1 class="blue-text pt-5">Access & Affordability</h1>
			         <p class="gray-text mb-0 pb-5">Biocon is on a quest to enhance access to high quality biopharmaceuticals by making them affordable for patients worldwide.</p>
		   		</div>
		   
				</div>
		</section>
		<div class="clearfix"></div>
    	<?php our_values();?>
	<div class="clearfix"></div>
		<?php our_growth_drivers();?>
	<section class="pt-3 pb-5 sec_03">
	   <div class="container-fluid">
	      <h2 class="border_head blue-text mb-3"><span>OUR INNOVATION PATH</span></h2>
	      <p>Biocon is consistently focused on value creation through innovation and differentiation with significant investments in cutting edge R&D. Leveraging its expertise in drug discovery and development processes and best-in-class manufacturing capabilities, Biocon has built a strong portfolio of Biosimilars and Novel Biologics to address unmet medical needs globally.</p>
	      <div class="row">
	         <div class="col-sm col-md-4 white-text mt-4">
	            <div class="box indigo-bg mr-0 ml-0 mr-xl-4 ml-xl-4 pt-5">
	               <div class="boxHead text-center no-border mb-0">
	                  <img src="images/icons/innovation_path_icon_01.png" width="80" class="img-fluid">
	                  <h3 class="h-80">Biosimilars</h3>
	               </div>
	               <div class="text-center">
	                  <a href="#" class="btn white-brdr-btn m-1">Overview</a>
	                  <a href="#" class="btn white-brdr-btn m-1">Pipeline</a>
	               </div>
	            </div>
	         </div>
	         <div class="col-sm col-md-4 white-text mt-4">
	            <div class="box orange-bg mr-0 ml-0 mr-xl-4 ml-xl-4 pt-5">
	               <div class="boxHead text-center no-border mb-0">
	                  <img src="images/icons/groth_drivers_icon_03.png" width="80" class="img-fluid">
	                  <h3 class="h-80">Novel Biologics</h3>
	               </div>
	               <div class="text-center">
	                  <a href="#" class="btn white-brdr-btn m-1">Overview</a>
	                  <a href="#" class="btn white-brdr-btn m-1">Pipeline</a>
	               </div>
	            </div>
	         </div>
	         <div class="col-sm col-md-4 white-text mt-4">
	            <div class="box green-bg mr-0 ml-0 mr-xl-4 ml-xl-4 pt-5">
	               <div class="boxHead text-center no-border mb-0">
	                  <img src="images/icons/innovation_path_icon_02.png" width="80" class="img-fluid">
	                  <h3 class="h-80">Our Science</h3>
	               </div>
	               <div class="text-center">
	                  <a href="#" class="btn white-brdr-btn m-1">Overview</a>
	               </div>
	            </div>
	         </div>
	      </div>
	   </div>
	</section>
	<?php press_release();
		life_at_biocon();
		biocon_live();
	?>
	
	<section class="pt-5 pb-5 sec_02 compassion">
	   <div class="container-fluid">
	      <h2 class="border_head blue-text mb-3"><span>Compassion and Care are a Part of Our DNA</span></h2>
	      <div class="row">
	         <div class="col-sm col-md-3 white-text mt-4">
	            <div class="box blue-bg">
	               <div class="boxHead">
	                  <h3>eLAJ Smart Clinics</h3>
	               </div>
	               <div style="min-height: 200px">
	                  <img src="images/eLAJ.jpg" class="img-fluid">
	                  <h4 class="mb-3 mt-3">Improving the public healthcare system through innovation.</h4>
	               </div>
	               <a href="#" class="read_more right-10">Know more</a>
	            </div>
	         </div>
	         <div class="col-sm- col-md-3 white-text mt-4">
	            <div class="box skyblue-bg">
	               <div class="boxHead">
	                  <h3>Lake Revival Mission</h3>
	               </div>
	               <div style="min-height: 200px">
	                  <img src="images/LRM.jpg" class="img-fluid">
	                  <h4 class="mb-3 mt-3">A commitment to revive Bengaluru’s dying lakes.</h4>
	               </div>
	               <a href="#" class="read_more right-10">Know more</a>
	            </div>
	         </div>
	         <div class="col-sm-6 col-md-3 white-text mt-4">
	            <div class="box green-bg">
	               <div class="boxHead">
	                  <h3>Biocon Academy</h3>
	               </div>
	               <div style="min-height: 200px">
	                  <img src="images/biocon-academy.jpg" class="img-fluid">
	                  <h4 class="mb-3 mt-3">Centre of Excellence for Advanced Learning in Biosciences.</h4>
	               </div>
	               <a href="#" class="read_more right-10">Know more</a>
	            </div>
	         </div>
	         <div class="col-sm-6 col-md-3 white-text mt-4">
	            <div class="box indigo-bg">
	               <div class="boxHead">
	                  <h3>latest on Biocon Academy</h3>
	               </div>
	               <div style="min-height: 200px">
	                  <img src="images/eLAJ.jpg" class="img-fluid">
	                  <h4 class="mb-3 mt-3">Improving the public healthcare system through innovation.</h4>
	               </div>
	               <a href="#" class="read_more right-10">Know more</a>
	            </div>
	         </div>
	      </div>
	   </div>
	</section>
  	<?php footer(); ?>
  	
	<?php script(); ?>    
  </body>
</html>