<?PHP

date_default_timezone_set('Asia/Kolkata');
class common_function
{
    var $connection;
	 function __construct(&$connection) {
    $this->db=$connection;
	}

	function template1($id,$temp_pos_id){
	$return_tmp='';
		$select="SELECT * FROM templates a LEFT JOIN template_title_desc b ON a.id=b.temp_id WHERE a.id='1' AND b.temp_pos_id='$temp_pos_id' AND b.page_id_td=".$id;
		$query=mysqli_query($this->db,$select);
            while($row = mysqli_fetch_array($query)){
                $template_name=$row["template_name"];
				$template_link=$row["template_link"];
				$template_icon=$row["template_icon"];
				$is_deleted=$row["is_deleted"];
				$updated_by=$row["updated_by"];
				$updated_date=$row["updated_date"];
				$temp_id=$row["temp_id"];
				$title=$row["title"];
				$description=$row["description"];
            }
    $return_tmp .='<section class="pt-4 pb-4 pt-md-5 pb-md-5 our_values">
	        <div class="container-fluid">
	            <h2 class="border_head blue-text"><span>'.$title.'</span></h2>
	            <p>'.$description.'</p>
	            <div class="row">';
				$select_item="SELECT * FROM template_details a LEFT JOIN page b ON a.sub_link=b.id WHERE a.temp_id='1' AND a.temp_pos_id='$temp_pos_id' AND a.page_id_td='".$id."' ORDER BY a.id";
		$query_item=mysqli_query($this->db,$select_item);
		            
            while($row_item = mysqli_fetch_array($query_item)){
	               $return_tmp .='<div class="col-12 col-md-6 col-lg-3 white-text mt-4">
	                    <div class="box " style="background-color: '.$row_item["sub_bg_color"].';">
	                        <div class="boxHead">';
							if($row_item["sub_icon"]!=""){
	                           $return_tmp .='<div class="homeBxicon">
	                            	<img src="images/icons/'.$row_item["sub_icon"].'" width="60" class="img-fluid">
	                            </div>';
							}
	                       $return_tmp .='<h3>'.$row_item["sub_title"].'</h3>
	                        </div>
	                    	<p>'.$row_item["sub_description"].'</p>
	                    	<a href="'.$row_item["page_name"].'" class="read_more">Know More</a>
	                    </div>                
	                </div> ';           
			}        
	     $return_tmp .='      </div>
	        </div>
    </section>'	;
      return $return_tmp;
	}
	
	function template2($id,$temp_pos_id){
	$return_tmp='';
		$select="SELECT * FROM templates a LEFT JOIN template_title_desc b ON a.id=b.temp_id WHERE a.id='2'  AND b.temp_pos_id='$temp_pos_id' AND b.page_id_td=".$id;
		$query=mysqli_query($this->db,$select);
		$row_count=mysqli_num_rows($query);
            while($row = mysqli_fetch_array($query)){
                $template_name=$row["template_name"];
				$template_link=$row["template_link"];
				$template_icon=$row["template_icon"];
				$is_deleted=$row["is_deleted"];
				$updated_by=$row["updated_by"];
				$updated_date=$row["updated_date"];
				$temp_id=$row["temp_id"];
				$title=$row["title"];
				$description=$row["description"];
            }
	 $return_tmp .='<section class="pt-2 pb-5 pb-md-5 sec_02 our_growth_drivers">
	   <div class="container-fluid">';
	   if($row_count==1){
		   
	    $return_tmp .= '<h2 class="border_head blue-text mb-3"><span>'.$title.'</span></h2>
	      <p>'.$description.'</p>';
	   }
	     $return_tmp .='<div class="row">';
		  $select_item="SELECT * FROM template_details a LEFT JOIN page b ON a.sub_link=b.id WHERE a.temp_id='2'  AND a.temp_pos_id='$temp_pos_id' AND a.page_id_td='".$id."'  ORDER BY a.id";
		$query_item=mysqli_query($this->db,$select_item);
		            
            while($row_item = mysqli_fetch_array($query_item)){
	     $return_tmp .=  '<div class="col-sm-6 col-md-6 col-lg-3 col-xl-3 white-text mt-4">
	            <div class="box " style="background-color: '.$row_item["sub_bg_color"].';">
	               <div class="boxHead">';
							if($row_item["sub_icon"]!=""){
	                           $return_tmp .='<div class="homeBxicon">
	                            	<img src="images/icons/'.$row_item["sub_icon"].'" width="60" class="img-fluid">
	                            </div>';
							}
				 $return_tmp .='<h3>'.$row_item["sub_title"].'</h3>
	               </div>
	               <p>'.$row_item["sub_description"].'</p>
	               <a href="'.$row_item["page_name"].'" class="read_more">Know More</a>
	            </div>
	            <img src="images/'.$row_item["sub_image"].'" class="img-fluid">
	         </div>';
			}
	     $return_tmp .='</div>
	   </div>
	</section>';
	
      return $return_tmp;
	}
	
	function template76($id,$temp_pos_id){
	$return_tmp='';
		$select="SELECT * FROM templates a LEFT JOIN template_title_desc b ON a.id=b.temp_id WHERE a.id='76' AND b.temp_pos_id='$temp_pos_id' AND b.page_id_td=".$id;
		$query=mysqli_query($this->db,$select);
            while($row = mysqli_fetch_array($query)){
                $template_name=$row["template_name"];
				$template_link=$row["template_link"];
				$template_icon=$row["template_icon"];
				$is_deleted=$row["is_deleted"];
				$updated_by=$row["updated_by"];
				$updated_date=$row["updated_date"];
				$temp_id=$row["temp_id"];
				$title=$row["title"];
				$description=$row["description"];
            }
		 $return_tmp .='<section class="pt-3 pb-5 sec_03">
	   <div class="container-fluid">
	      <h2 class="border_head blue-text mb-3"><span>'.$title.'</span></h2>
	      <p>'.$description.'</p>
	      <div class="row">';
		  $select_item="SELECT * FROM template_details a LEFT JOIN page b ON a.sub_link=b.id WHERE a.temp_id='76'  AND a.temp_pos_id='$temp_pos_id' AND a.page_id_td='".$id."'  ORDER BY a.id";
		$query_item=mysqli_query($this->db,$select_item);
		            
            while($row_item = mysqli_fetch_array($query_item)){
	        $return_tmp .='<div class="col-sm col-md-4 white-text mt-4">
	            <div class="box mr-0 ml-0 mr-xl-4 ml-xl-4 pt-5" style="background-color: '.$row_item["sub_bg_color"].';">
	               <div class="boxHead text-center no-border mb-0">
	                  <img src="images/icons/'.$row_item["sub_icon"].'" width="80" class="img-fluid">
	                  <h3 class="h-80">'.$row_item["sub_title"].'</h3>
	               </div>
	               <div class="text-center">
	                  <a href="'.$row_item["page_name"].'" class="btn white-brdr-btn m-1">Overview</a>
	                  <a href="#" class="btn white-brdr-btn m-1">Pipeline</a>
	               </div>
	            </div>
	         </div>';
			}
	     $return_tmp .='</div>
	   </div>
	</section>';
      return $return_tmp;
	}
	function template4($id,$temp_pos_id){
	$return_tmp='';
	 $return_tmp .='<section class="pt-3 pb-4 sec_04 press_release">
	   <div class="container-fluid">
	      <div class="row">
	         <div class="col-12 col-lg-6">
	            <h2 class="border_head blue-text mb-3"><span>PRESS RELEASES </span></h2>
	            <div class="white-text skyblue-bg p-4 relative">
	               <div class="row">
	                  <div class="col-sm border_right">
	                     <h4 class="mb-0">Biocon’s Sterile Drug Product facility Receives EU GMP Certification</h4>
	                     <p class="mb-0"><small>Bengaluru, India, July 4, 2018</small></p>
	                  </div>
	                  <div class="col-sm">
	                     <h4 class="mb-0">Biocon retains economic interest in Etanercept collaboration with Mylan</h4>
	                     <p class="mb-0"><small>Bengaluru, India, June 28, 2018</small></p>
	                  </div>
	                  <div class="w-100 pb-4">
	                     <a href="#" class="read_more">Know More</a>
	                  </div>
	               </div>
	            </div>
	         </div>
	         <div class="col-12 col-lg-6 pt-4 pt-md-4 pt-lg-0">
	            <h2 class="border_head blue-text mb-3"><span>STOCK UPDATE</span></h2>
	            <div class="white-text blue-bg p-4 relative">
	               <div class="row text-center">
	                  <div class="col-sm border_right pt-2">
	                     <p class="mb-0">BSE</p>
	                     <h2 class="mb-0">606.60</h2>
	                  </div>
	                  <div class="col-sm pt-2">
	                     <p class="mb-0">NSE</p>
	                     <h2 class="mb-0">606.75</h2>
	                  </div>
	                  <div class="w-100 text-center pt-3">
	                     <a href="#" class="btn white-brdr-btn m-1">Overview</a>
	                  </div>
	               </div>
	            </div>
	            <div class="w-100 text-right">
	               <p><small><em><b>Disclaimer:</b> The stock updates may be delayed by up to 5 minutes. </em></small></p>
	            </div>
	         </div>
	      </div>
	   </div>
	   </div>
	</section>';
      return $return_tmp;	
	}	
	function template5($id,$temp_pos_id){
	$return_tmp='';
		$select_item="SELECT * FROM template_details a LEFT JOIN page b ON a.sub_link=b.id WHERE a.temp_id='5'  AND a.temp_pos_id='$temp_pos_id' AND a.page_id_td='".$id."'  ORDER BY a.id";
		$query_item=mysqli_query($this->db,$select_item);
		            
            while($row_item = mysqli_fetch_array($query_item)){
				$sub_image=$row_item['sub_image'];
				$title=$row_item['title'];
				$description=$row_item['description'];
				$sub_icon=$row_item['sub_icon'];
				$sub_title=$row_item['sub_title'];
				$sub_description=$row_item['sub_description'];
				$sub_link=$row_item['sub_link'];
				$sub_link2=$row_item['sub_link2'];
				$sub_bg_color=$row_item['sub_bg_color'];
				$sub_year=$row_item['sub_year'];
				$location_id=$row_item['location_id'];
				$resource=$row_item['resource'];
			}
		 $return_tmp .='<section class="pt-4 pb-4 sec_04 orange-bg white-text mb-4 mb-md-1 mb-lg-4">
           <div class="container-fluid">
              <div class="row">
                 <div class="col-sm col-lg-6 pr-md-5">
                    <img src="images/icons/'.$sub_icon.'" class="img-fluid" width="80">
                    <h2 class="mb-2 mt-2 head">'.$title.'</h2>
                    <p class="mb-1">'.$description.'</p>
                    <div class="w-100 pt-3">
                       <a href="#" class="btn white-brdr-btn m-1 text-center">Overview</a>
                    </div>
                 </div>
                 <div class="col-sm col-lg-6 ">
                    <!-- <div class="d-flex align-items-stretch">
                       <div class="p-2"><img src="images/home/life_at_biocon_01.jpg" class="img-fluid float-left"></div>
                       </div> -->
                    <div class="row pt-2 life_at_biocon">
                       <div class="col-md-5 p-2 ">
                          <img src="images/life_at_biocon_01.jpg" class="img-fluid float-left ">
                       </div>
                       <div class="col-sm-12 col-md-12 col-lg-7 mx-auto">
                          <div class="row">
                             <div class="col-sm p-2">
                                <img src="images/life_at_biocon_02.jpg" class="img-fluid float-left">
                             </div>
                             <div class="col-sm p-2">
                                <img src="images/life_at_biocon_03.jpg" class="img-fluid float-left">
                             </div>
                          </div>
                          <div class="row">
                             <div class="col-sm p-2">
                                <img src="images/life_at_biocon_04.jpg" class="img-fluid float-left">
                             </div>
                             <div class="col-sm p-2">
                                <img src="images/life_at_biocon_05.jpg" class="img-fluid float-left">
                             </div>
                          </div>
                       </div>
                    </div>
                 </div>
              </div>
           </div>
        </section>';
      return $return_tmp;
	}
	function template6($id,$temp_pos_id){
	$return_tmp='';
		 $return_tmp .='<section class="pt-4 pb-4 pt-md-4 pb-md-4 sec_02 bio_live">
	   <div class="container-fluid">
	      <h2 class="border_head blue-text mb-3"><span>BIOCON LIVE</span></h2>
	      <div class="row">
	         <div class="col-sm-12 col-md-6 col-lg-6 col-xl-3 white-text mt-4">
	            <div class="box skyblue-bg">
	               <div class="boxHead">
	                  <h3>Biocon on Twitter</h3>
	               </div>
	               <div id="twitter_content" style="margin-bottom: 25px;border-radius: 0px;background: #fff;min-height: 220px">
	                  <a class="twitter-timeline" width="100%" href="https://twitter.com/Bioconlimited?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor" data-tabs="timeline"  data-width="300" data-height="200" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"></a>
	                  <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
	               </div>
	               <a href="#" class="read_more right-10">View on Twitter</a>
	            </div>
	         </div>
	         <div class="col-sm-12 col-md-6 col-lg-6 col-xl-3 white-text mt-4">
	            <div class="box green-bg">
	               <div class="boxHead">
	                  <div class="homeBxicon">
	                     <img src="images/biocon-conversation.png" class="img-fluid">
	                  </div>
	                  <h3></h3>
	               </div>
	               <div style="min-height: 220px">
	                  <img src="images/fda.png" class="img-fluid">
	                  <h4 class="mb-1 mt-2">First biosimilar from India to be approved by U.S. FDA</h4>
	                  <p><small><em>Posted by Biocon on August 13, 2018</em></small></p>
	               </div>
	               <a href="#" class="read_more right-10">View All</a>
	            </div>
	         </div>
	         <div class="col-sm-12 col-md-6 col-lg-6 col-xl-3 white-text mt-4">
	            <div class="box indigo-bg">
	               <div class="boxHead">
	                  <h3>Biocon in News</h3>
	               </div>
	               <div style="min-height: 220px">
	                  <img src="images/news_01.jpg" class="img-fluid">
	                  <h4 class="mb-1 mt-2">Biocon Q1 PAT seen up 70.8% YoY to Rs. 138.9 cr: Kotak</h4>
	                  <p><small><em>18 May, 2018 | Source: PTI</em></small></p>
	               </div>
	               <a href="#" class="read_more right-10">View All</a>
	            </div>
	         </div>
	         <div class="col-sm-12 col-md-6 col-lg-6 col-xl-3 white-text mt-4">
	            <div class="box blue-bg">
	               <div class="boxHead">
	                  <h3>Engaged in Affordable Innovation for Global Impact</h3>
	               </div>
	               <div style="min-height: 220px">
	                  <iframe width="100%" height="223" style="margin-bottom: 15px;" src="https://www.youtube.com/embed/EicKpqKKEyg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
	               </div>
	               <a href="#" class="read_more right-10">View All</a>
	            </div>
	         </div>
	      </div>
	   </div>
	</section>';
      return $return_tmp;
	}
	function template77($id,$temp_pos_id){
	$return_tmp='';
		$select="SELECT * FROM templates a LEFT JOIN template_title_desc b ON a.id=b.temp_id WHERE a.id='77' AND b.temp_pos_id='$temp_pos_id' AND b.page_id_td=".$id;
		$query=mysqli_query($this->db,$select);
            while($row = mysqli_fetch_array($query)){
                $template_name=$row["template_name"];
				$template_link=$row["template_link"];
				$template_icon=$row["template_icon"];
				$is_deleted=$row["is_deleted"];
				$updated_by=$row["updated_by"];
				$updated_date=$row["updated_date"];
				$temp_id=$row["temp_id"];
				$title=$row["title"];
				$description=$row["description"];
            }
		 $return_tmp .='<section class="pt-3 pb-3 pt-md-0 pb-md-4 pt-lg-4 pb-lg-5 sec_02 compassion">
	   <div class="container-fluid">
	      <h2 class="border_head blue-text mb-3"><span>'.$title.'</span></h2>
	      <div class="row">';
		  $select_item="SELECT * FROM template_details a LEFT JOIN page b ON a.sub_link=b.id WHERE a.temp_id='77'  AND a.temp_pos_id='$temp_pos_id' AND a.page_id_td='".$id."'  ORDER BY a.id";
		$query_item=mysqli_query($this->db,$select_item);
		            
            while($row_item = mysqli_fetch_array($query_item)){
	      $return_tmp .='<div class="col-sm-6 col-md-6 col-lg-6 col-xl-3 white-text mt-4">
	            <div class="box " style="background-color: '.$row_item["sub_bg_color"].';">
	               <div class="boxHead">
	                  <h3>'.$row_item["sub_title"].'</h3>
	               </div>
	               <div style="min-height: 200px">
	                  <img src="images/icons/'.$row_item["sub_icon"].'" class="img-fluid w-100">
	                  <h4 class="mb-3 mt-3">'.$row_item["sub_description"].'</h4>
	               </div>
	               <a href="'.$row_item["page_name"].'" class="read_more right-10">Know more</a>
	            </div>
	         </div>';
			}
	     $return_tmp .='</div>
	   </div>
	</section>';
      return $return_tmp;
	}
	
		function template9($id,$temp_pos_id){
	$return_tmp='';
		 $return_tmp .='<section class="pt-md-3 pb-4 pb-md-0 pb-lg-4">
				<div class="container-fluid pl-md-5 pr-md-5">
					 <h2 class="border_head blue-text"><span>Kiran mazumdar-Shaw</span></h2>
					 <div class="turquoise">
						<div class="row">
							<div class="col-12 col-md-5 col-lg-4 text-center">
								<img src="images/kiran-mazumdar-shaw.jpg" class="img-fluid min-hgt-150">
							</div>
							<div class="col-12  col-md-7 col-lg-8">
								<div class="pt-3 pl-3 pr-3 pl-md-0 pr-md-0 pb-3 img-with-text">
									<p class="text-white mb-2 mb-md-1">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. </p>
									<a href="#" class="text-white text-bold">Know More</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>	
			';
      return $return_tmp;
	}
	
	function template8($id,$temp_pos_id){
	$return_tmp='';
		$select="SELECT * FROM templates a LEFT JOIN template_title_desc b ON a.id=b.temp_id WHERE a.id='8' AND b.temp_pos_id='$temp_pos_id' AND b.page_id_td=".$id;
		$query=mysqli_query($this->db,$select);
            while($row = mysqli_fetch_array($query)){
				 $temp_id=$row["id"];
				$main_title=$row["title"];
				$main_desc=$row["description"];
            }
		 $return_tmp .='<section class="pt-4 pb-4 pt-md-5 pb-md-5" style="padding-top: 0 !important;">
            <div class="container-fluid pl-md-5 pr-md-5">
               <div class="row">';
		 $select_item="SELECT * FROM template_details a LEFT JOIN page b ON a.sub_link=b.id WHERE a.temp_id='8'  AND a.temp_pos_id='$temp_pos_id' AND a.page_id_td='".$id."'  ORDER BY a.id";
		$query_item=mysqli_query($this->db,$select_item);
		            
            while($row_item = mysqli_fetch_array($query_item)){
				$links=$row_item['sub_link'];
				
				if(!empty($links))
				{
					$all_links=explode('~',$links);
				}
				
	        $return_tmp .='<div class="col-12 col-md-6 col-lg-4 white-text mt-4" >
                        <div class="box blue-bg" style="background-color: '.$row_item["sub_bg_color"].';">
                            <div class="boxHead">
                                <div class="homeBxicon d-md-none d-lg-flex">
                                	<img src="images/icons/'.$row_item["sub_icon"].'" width="60" class="img-fluid">
                                </div>
                                <h3>'.$row_item["title"].'</h3>
                            </div>
                            <div class="min-hgt-200 d-flex flex-wrap align-content-center">
                                <ul class="custom-list">';
				if(count($all_links)>0)
				{
					foreach($all_links as $key=>$val)
					{
						
						$query="SELECT * FROM page_links WHERE id=".$val;
						$query_pages=mysqli_query($this->db,$query);
						 while($row_pages = mysqli_fetch_array($query_pages))
						 {
							 $return_tmp .='<li><a href="'.$row_pages['page_link'].'">'.$row_pages['page_name'].'</a></li>';
						 }
					}
				}
			  
             $return_tmp .='</ul>
                            </div>
                        </div>
                    </div>';
			}
	     $return_tmp .='</div>
            </div>
</section>';
      return $return_tmp;
	}
	
	
	function template78($id,$temp_pos_id){
	$return_tmp='';
		$select="SELECT * FROM templates a LEFT JOIN template_title_desc b ON a.id=b.temp_id WHERE a.id='78' AND b.temp_pos_id='$temp_pos_id' AND b.page_id_td=".$id;
		$query=mysqli_query($this->db,$select);
            while($row = mysqli_fetch_array($query)){
                $template_name=$row["template_name"];
				$template_link=$row["template_link"];
				$template_icon=$row["template_icon"];
				$is_deleted=$row["is_deleted"];
				$updated_by=$row["updated_by"];
				$updated_date=$row["updated_date"];
				$temp_id=$row["temp_id"];
				$title=$row["title"];
				$description=$row["description"];
            }
		 $return_tmp .='<section class="pt-3 pt-md-3 pb-4 pb-md-5 heading-with-content">
            <div class="container-fluid pl-md-5 pr-md-5">
                 <h2 class="border_head blue-text"><span> '.$title.'</span></h2>
                <div class="row">';
		 $select_item="SELECT * FROM template_details a LEFT JOIN page b ON a.sub_link=b.id WHERE a.temp_id='78'  AND a.temp_pos_id='$temp_pos_id' AND a.page_id_td='".$id."'  ORDER BY a.id";
		$query_item=mysqli_query($this->db,$select_item);
		            
            while($row_item = mysqli_fetch_array($query_item)){
				
	      $return_tmp .='<div class="col-12 col-sm-12 col-md-12 col-lg-6 white-text mt-4" >
                        <div class=" col-md-6 col-lg-6 float-left p-0">
                            <div class="box royal-blue min-hgt-300 d-flex flex-wrap align-content-center" style="background-color:'.$row_item['sub_bg_color'].';">
                                <div class="boxHead pt-3 pt-sm-0">
                                    <h3>'.$row_item['sub_title'].'</h3>
                                </div>
                                <div class="boxHead no-border m-auto">
                                   '.$row_item['sub_description'].'
                                </div>
                                <a href="#" class="read_more right-10">View All</a>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-6 float-left p-0 min-hgt-300 d-none d-md-block" style="background:url(images/icons/'.$row_item['sub_icon'].') no-repeat;background-size:cover;">
                            
                        </div>
                        <div class="clearfix"></div>
                    </div>';
			}
	     $return_tmp .='</div>
           </div>
</section>';
      return $return_tmp;
	}	
	
	function template80($id,$temp_pos_id){
	$return_tmp='';
		$select="SELECT * FROM templates a LEFT JOIN template_title_desc b ON a.id=b.temp_id WHERE a.id='80' AND b.temp_pos_id='$temp_pos_id' AND b.page_id_td=".$id;
		$query=mysqli_query($this->db,$select);
            while($row = mysqli_fetch_array($query)){
                $template_name=$row["template_name"];
				$template_link=$row["template_link"];
				$template_icon=$row["template_icon"];
				$is_deleted=$row["is_deleted"];
				$updated_by=$row["updated_by"];
				$updated_date=$row["updated_date"];
				$temp_id=$row["temp_id"];
				$title=$row["title"];
				$description=$row["description"];
            }
		 $return_tmp .='<section class="pt-4 pb-4 pt-md-4 pb-md-3 pb-lg-4 heading-with-content" style="padding-bottom: 0 !important;">
            <div class="container-fluid pl-md-5 pr-md-5">
                 <h2 class="border_head blue-text"><span>'.$title.'</span></h2>
				 '.htmlspecialchars_decode($description).'
                <div class="row">';
		 $select_item="SELECT * FROM template_details a LEFT JOIN page b ON a.sub_link=b.id LEFT JOIN page_links c ON(a.sub_link=c.id)WHERE a.temp_id='80'  AND a.temp_pos_id='$temp_pos_id' AND a.page_id_td='".$id."'  ORDER BY a.id";
		$query_item=mysqli_query($this->db,$select_item);
		            
            while($row_item = mysqli_fetch_assoc($query_item)){
				
			 $return_tmp .='<div class="col-12 col-lg-6 white-text mt-4" >
                        <div class=" col-md-6 col-lg-6 float-left p-0" >
                            <div class="box turquoise pt-5 min-hgt-300 d-flex flex-wrap align-content-center" style="background-color:'.$row_item['sub_bg_color'].';">
                                <div class="boxHead text-center no-border m-auto">
                                    <img src="images/icons/'.$row_item['sub_icon'].'" width="80" class="img-fluid">
                                    <h3 class="h-80">'.$row_item['sub_title'].'</h3>
                                     <a href="'.$row_item['page_link'].'" class="text-white text-bold">Know More</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-6 float-left p-0 min-hgt-300 d-none d-md-block" style="background:url(images/'.$row_item['sub_image'].') no-repeat;">
                        	
                        </div>
                        <div class="clearfix"></div>
                    </div>';
			}
	     $return_tmp .='</div>
           </div>
</section>';
      return $return_tmp;
	}
	
	function template79($id,$temp_pos_id){
	$return_tmp='';
		$select="SELECT * FROM templates a LEFT JOIN template_title_desc b ON a.id=b.temp_id WHERE a.id='79' AND b.temp_pos_id='$temp_pos_id' AND b.page_id_td=".$id;
		$query=mysqli_query($this->db,$select);
            while($row = mysqli_fetch_array($query)){
                $template_name=$row["template_name"];
				$template_link=$row["template_link"];
				$template_icon=$row["template_icon"];
				$is_deleted=$row["is_deleted"];
				$updated_by=$row["updated_by"];
				$updated_date=$row["updated_date"];
				$temp_id=$row["temp_id"];
				$title=$row["title"];
				$description=$row["description"];
				$bimg_file=$row["bimg_file"];
				$bcolor=$row["bcolor"];
			}
		 $return_tmp .='<section class="pt-4 pb-4 pt-md-5 pb-md-5">
            <div class="container-fluid pl-md-5 pr-md-5">
                <h2 class="border_head blue-text"><span>'.$title.'</span></h2>
                <div class="row">
                    <div class="col-12  white-text mt-4">
                        <div class="col-lg-8 float-left p-0">                            
                            <div class="box turquoise pt-4 min-hgt-300 d-flex flex-wrap align-content-center" style="background-color:'.$bcolor.'"> ';
							
		 $select_item="SELECT * FROM template_details a LEFT JOIN page b ON a.sub_link=b.id LEFT JOIN page_links c ON(a.sub_link=c.id)WHERE a.temp_id='79'  AND a.temp_pos_id='$temp_pos_id' AND a.page_id_td='".$id."'  ORDER BY a.id";
		$query_item=mysqli_query($this->db,$select_item);
		           
					$str='<div class="col-sm-6"><ul class="flags">';
					$str1='<div class="col-sm-6"><ul class="flags">';
					$count=mysqli_num_rows($query_item);
					$i=0;
            while($row_item = mysqli_fetch_assoc($query_item)){
				 
				 $title=$row_item['title'];
				 $sub_image=$row_item['sub_image'];
					if($i%2==0)
					{
						
						$str.=' <li><img src="main-control/images/temp_img/'.$sub_image.'">'.$title.'</li>';
					}
					else
					{
						$str1.='<li><img src="main-control/images/temp_img/'.$sub_image.'">'.$title.'</li>';
					}
					$i++;
			} 
			$ss=$str.'</ul></div>'.$str1.'</ul></div>';
	     $return_tmp .=$ss.' <div class="clearfix"></div></div>
                        </div>
                        <div class="col-lg-4 float-left p-0 min-hgt-300 d-none d-lg-block" style="background:url(main-control/images/'.$bimg_file.') no-repeat;">                        	
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
            </div></div></section>';
      return $return_tmp;
	}
		function template11($id,$temp_pos_id){
	$return_tmp='';
		$select="SELECT * FROM templates a LEFT JOIN template_title_desc b ON a.id=b.temp_id WHERE a.id='11' AND b.temp_pos_id='$temp_pos_id' AND b.page_id_td=".$id;
		$query=mysqli_query($this->db,$select);
            while($row = mysqli_fetch_array($query)){
                $template_name=$row["template_name"];
				$template_link=$row["template_link"];
				$template_icon=$row["template_icon"];
				$is_deleted=$row["is_deleted"];
				$updated_by=$row["updated_by"];
				$updated_date=$row["updated_date"];
				$temp_id=$row["temp_id"];
				$title=$row["title"];
				$description=$row["description"];
            }
		 $return_tmp .='<section class="pb-4 pb-md-5">
            <div class="container-fluid pl-md-5 pr-md-5">
                <h2 class="border_head blue-text"><span>'.$title.'</span></h2>
				<div class="table-responsive">
					<table class="table table-striped table-bordered mt-4 custom-table text-center">
						<thead class="royal-blue white-text">
							<tr>
							  <th scope="col" width="20%">FACILITY LOCATIONS</th>
							  <th scope="col" width="40%">PRODUCTS  MANUFACTURED</th>
							  <th scope="col" width="20%">IMAGE</th>
							  <th scope="col" width="20%">VIDEO</th>
							</tr>
						</thead><tbody>';
		 $select_item="SELECT * FROM template_details a LEFT JOIN page b ON a.sub_link=b.id LEFT JOIN page_links c ON(a.sub_link=c.id) LEFT JOIN location loc ON(a.location_id=loc.id)WHERE a.temp_id='11' AND a.temp_pos_id='$temp_pos_id' AND a.page_id_td='".$id."'  ORDER BY a.id";
		$query_item=mysqli_query($this->db,$select_item);
			
			
			 $str='';
			  while($row_item = mysqli_fetch_assoc($query_item)){
				  $resources=array();
				  $resource=array();
				$loc_name=$row_item['loc_name'];
				$description=$row_item['description'];
				$sub_image=explode('~',$row_item['sub_image']);
				$resource=explode('https://www.youtube.com/watch?v=', $row_item['resource']);
				array_shift($resource);
				//$resources=array_shift($resource);
			$str.= '<tr>
							  <th scope="row" class="align-middle royal-blue white-text">'.$loc_name.'</th>
							  <td class="text-left color-royal-blue">
								'.htmlspecialchars_decode($description).'
							</td>
							  <td>';
							  foreach($sub_image as $key=>$val)
							  {
						$str.= '<img src="main-control/images/temp_img/'.$val.'" class="img-fluid mb-4">';
						
							  }
							  
						$str.= '</td>';
						$str.= '<td>';
						foreach($resource as $k=>$v)
				  {
					 $str.= '<iframe width="240" height="180" src="https://www.youtube.com/embed/'.$v.'"></iframe>';
				  }
						$str.= '</td>
							</tr>
							';
			  }
	     $return_tmp .=$str.'</tbody></table>
				</div>	
            </div>    
        </section>';
      return $return_tmp;
	}
		function template81($id,$temp_pos_id){
	$return_tmp='';
		$select="SELECT * FROM templates a LEFT JOIN template_title_desc b ON a.id=b.temp_id WHERE a.id='81' AND b.temp_pos_id='$temp_pos_id' AND b.page_id_td=".$id;
		$query=mysqli_query($this->db,$select);
            while($row = mysqli_fetch_array($query)){
                $template_name=$row["template_name"];
				$template_link=$row["template_link"];
				$template_icon=$row["template_icon"];
				$is_deleted=$row["is_deleted"];
				$updated_by=$row["updated_by"];
				$updated_date=$row["updated_date"];
				$temp_id=$row["temp_id"];
				$title=$row["title"];
				$description=$row["description"];
				$bimg_file=$row["bimg_file"];
				$bcolor=$row["bcolor"];
			}
		 $return_tmp .='<section class="pb-4 pb-md-5 heading-with-content">
            <div class="container-fluid pl-md-5 pr-md-5">
                 <h2 class="border_head blue-text"><span>'.$title.'</span></h2> ';
							
		 $select_item="SELECT * FROM template_details a LEFT JOIN page b ON a.sub_link=b.id LEFT JOIN page_links c ON(a.sub_link=c.id)WHERE a.temp_id='81' AND a.temp_pos_id='$temp_pos_id' AND a.page_id_td='".$id."'  ORDER BY a.id";
		$query_item=mysqli_query($this->db,$select_item);
			$i=1;
            while($row_item = mysqli_fetch_assoc($query_item)){
				 
				 $title=$row_item['title'];
				 $sub_image=$row_item['sub_image'];
				 $sub_description=$row_item['sub_description'];
				 $description=$row_item['description'];
				 $sub_bg_color=$row_item['sub_bg_color'];
				 $str='float-left';
				 if($i%2)
				 $str='float-left';
				 else
				 $str='float-right';
				 
				$return_tmp .='<div class="row">
                    <div class="col-12 white-text mt-4 mb-4">
                        <div class="col-lg-8 '.$str.' p-0">
                            <div class="box dark-orange min-hgt-300 d-flex flex-wrap align-content-center pb-0 " style="background-color:'.$sub_bg_color.';">
                                <div class="boxHead">
                                    <h3>'.$i.'. '.$title.'</h3>
                                </div>
                                <div class="boxHead no-border m-auto">
                                   '.htmlspecialchars_decode($sub_description).'
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 float-left p-0 min-hgt-300 d-none d-lg-block" style="background:url(images/'.$sub_image.') no-repeat;">
                        </div>
                        <div class="clearfix"></div>
                    </div>                     
            </div>
            <div class="mb-4">
                '.htmlspecialchars_decode($description).'
            </div>';
			$i++;
			} 
			
	     $return_tmp .='</div></section>';
      return $return_tmp;
	}
	
	function template17($id,$temp_pos_id){
	$return_tmp='';
		$select="SELECT * FROM templates a LEFT JOIN template_title_desc b ON a.id=b.temp_id WHERE a.id='17' AND b.temp_pos_id='$temp_pos_id' AND b.page_id_td=".$id;
		$query=mysqli_query($this->db,$select);
            while($row = mysqli_fetch_array($query)){
                $template_name=$row["template_name"];
				$template_link=$row["template_link"];
				$template_icon=$row["template_icon"];
				$is_deleted=$row["is_deleted"];
				$updated_by=$row["updated_by"];
				$updated_date=$row["updated_date"];
				$temp_id=$row["temp_id"];
				$title=$row["title"];
				$description=$row["description"];
				$bimg_file=$row["bimg_file"];
				$bcolor=$row["bcolor"];
			}
		 $return_tmp .='<section class="pt-4 pb-4 pt-md-5 pb-md-5">
            <div class="container-fluid pl-md-5 pr-md-5">
                 <h2 class="border_head blue-text"><span>'.$title.'</span></h2> <p>'.htmlspecialchars_decode($description).'</p> <div class="row">  ';
							
		 $select_item="SELECT * FROM template_details a LEFT JOIN page b ON a.sub_link=b.id LEFT JOIN page_links c ON(a.sub_link=c.id)WHERE a.temp_id='17'  AND a.temp_pos_id='$temp_pos_id' AND a.page_id_td='".$id."'  ORDER BY a.id";
		$query_item=mysqli_query($this->db,$select_item);
			$i=1;
            while($row_item = mysqli_fetch_assoc($query_item)){
				 
				 $title=$row_item['title'];
				 $sub_image=$row_item['sub_image'];
				 $sub_description=$row_item['sub_description'];
				 $description=$row_item['description'];
				 $sub_bg_color=$row_item['sub_bg_color'];
				 $sub_title=$row_item['sub_title'];
				 $sub_icon=$row_item['sub_icon'];
				 
				 
				$return_tmp .='<div class="col-12 col-lg-6 white-text mt-4">
                        <div class="col-sm-6 col-sm-6 col-md-6 col-lg-6 float-left p-0">
                            <div class="box royal-blue pt-5 min-hgt-300 d-flex flex-wrap align-content-center" style="background-color:'.$sub_bg_color.';">
                                <div class="boxHead text-center no-border m-auto">
                                    <h4 class="mb-0 text-uppercase">'.$title.'</h4>
                                    <p class="pl-md-4 pr-md-4">'.$sub_title.'</p>
									<a href="#" class="text-white text-bold" data-toggle="modal" data-target="#myModal'.$i.'">Know More</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-6 col-lg-6 float-left p-0 min-hgt-300  bg-cover" style="background:url(images/icons/'.$sub_icon.') no-repeat;">
                            
                        </div>
                        <div class="clearfix"></div>
                    </div>
					
					';
		$return_tmp .='<div id="myModal'.$i.'" class="modal fade popup" role="dialog">
        <div class="modal-dialog">        
            <!-- Modal content-->
            <div class="modal-content p-4">
                <div class="text-right position-absolute right-top-0"><button type="button" class="close" data-dismiss="modal">&times;</button> </div>
                <div class="col-12"> 
                     
                    <div class="row">            
                        <div class="col-12 col-md-5 "><img src="images/icons/'.$sub_icon.'"  class="img-fluid" /></div>
                        <div class="col-12 col-md-7 ">
                            <h3 class="color-royal-blue">'.$title.'</h3>
                            <p>'.htmlspecialchars_decode($description).'</p>
                        <a href="#" class="color-royal-blue col-12 col-md-9 col-lg-8">Know more</a>
                        </div>
                    </div>
                </div>
            </div>        
        </div>
    </div>';
			$i++;
			} 
			
	     $return_tmp .='</div></div></section>';
      return $return_tmp;
		 
	}
	function template83($id,$temp_pos_id){
	$return_tmp='';
		$select="SELECT * FROM templates a LEFT JOIN template_title_desc b ON a.id=b.temp_id WHERE a.id='83' AND b.temp_pos_id='$temp_pos_id' AND b.page_id_td=".$id;
		$query=mysqli_query($this->db,$select);
            while($row = mysqli_fetch_array($query)){
                $template_name=$row["template_name"];
				$template_link=$row["template_link"];
				$template_icon=$row["template_icon"];
				$is_deleted=$row["is_deleted"];
				$updated_by=$row["updated_by"];
				$updated_date=$row["updated_date"];
				$temp_id=$row["temp_id"];
				$title=$row["title"];
				$description=$row["description"];
				$bimg_file=$row["bimg_file"];
				$bcolor=$row["bcolor"];
			}
		 $return_tmp .='<section class="pt-3 pt-md-3 mt-3 mt-md-3 mt-lg-5 heading-with-content">
            <div class="container-fluid pl-md-5 pr-md-5">
                 <h2 class="border_head blue-text"><span>'.$title.'</span></h2>
                  <p>'.htmlspecialchars_decode($description).'</p>
			</div>
		</section>';
							
		 $select_item="SELECT * FROM template_details a LEFT JOIN page b ON a.sub_link=b.id LEFT JOIN page_links c ON(a.sub_link=c.id)WHERE a.temp_id='83'  AND a.temp_pos_id='$temp_pos_id' AND a.page_id_td='".$id."'  ORDER BY a.id";
		$query_item=mysqli_query($this->db,$select_item);
			$i=1;
            while($row_item = mysqli_fetch_assoc($query_item)){
				 
				 $title=$row_item['title'];
				 $sub_image=$row_item['sub_image'];
				 $sub_description=$row_item['sub_description'];
				 $description=$row_item['description'];
				 $sub_bg_color=$row_item['sub_bg_color'];
				 $str='float-left';
				 if($i%2)
				 $str='float-left';
				 else
				 $str='float-right';
				 
				$return_tmp .='<section class="pt-3 pb-4 pt-md-0 pb-md-4 pt-lg-3 pb-lg-5 heading-with-content">
            <div class="container-fluid pl-md-5 pr-md-5">
				<div class="row">
                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 white-text mt-1 mt-md-4">
						<div class="col-md-6 col-lg-4 float-left  p-0 min-hgt-300 '.$str.'" style="background:url(images/'.$sub_image.') no-repeat;background-size:cover; ">
                            
                        </div>
						<div class=" col-md-6 col-lg-8  float-right p-0">
                            <div class="box navy-blue min-hgt-300 d-flex flex-wrap align-content-center pt-3 pb-3 pt-md-0 pb-md-0" style="background-color:'.$sub_bg_color.';">
                            <div class="boxHead">
                                    <h3>'.$title.'</h3>
                                </div>
								<p>'.htmlspecialchars_decode($sub_description).'</p>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </section>';
			$i++;
			} 
			
	     $return_tmp .='</div></section>';
      return $return_tmp;
	}
	
		function template84($id,$temp_pos_id){
	$return_tmp='';
		$select="SELECT * FROM templates a LEFT JOIN template_title_desc b ON a.id=b.temp_id WHERE a.id='84' AND b.temp_pos_id='$temp_pos_id' AND b.page_id_td=".$id;
		$query=mysqli_query($this->db,$select);
            while($row = mysqli_fetch_array($query)){
                $template_name=$row["template_name"];
				$template_link=$row["template_link"];
				$template_icon=$row["template_icon"];
				$is_deleted=$row["is_deleted"];
				$updated_by=$row["updated_by"];
				$updated_date=$row["updated_date"];
				$temp_id=$row["temp_id"];
				$title=$row["title"];
				$description=$row["description"];
				$bimg_file=$row["bimg_file"];
				$bcolor=$row["bcolor"];
			}
		 $return_tmp .='<section class="pt-4 pb-4 pt-md-5 pb-md-5">
            <div class="container-fluid pl-md-5 pr-md-5">
                <h2 class="border_head blue-text"><span>'.$title.'</span></h2><div class="row">
                    <div class="col-12 p-0 color-dark-gray mt-4">                            
                            <div class="box pt-4 min-hgt-300 d-flex flex-wrap align-content-center">';
							
		 $select_item="SELECT * FROM template_details a LEFT JOIN page b ON a.sub_link=b.id LEFT JOIN page_links c ON(a.sub_link=c.id)WHERE a.temp_id='84'  AND a.temp_pos_id='$temp_pos_id' AND a.page_id_td='".$id."'  ORDER BY a.id";
		$query_item=mysqli_query($this->db,$select_item);
		           
					$str='<div class="col-sm-6"><ul class="flags">';
					$str1='<div class="col-sm-6"><ul class="flags">';
					$count=mysqli_num_rows($query_item);
					$i=0;
            while($row_item = mysqli_fetch_assoc($query_item)){
				 
				 $title=$row_item['title'];
				 $sub_image=$row_item['sub_image'];
					if($i%2==0)
					{
						
						$str.=' <li><img src="main-control/images/temp_img/'.$sub_image.'">'.$title.'</li>';
					}
					else
					{
						$str1.='<li><img src="main-control/images/temp_img/'.$sub_image.'">'.$title.'</li>';
					}
					$i++;
			} 
			$ss=$str.' </ul></div>'.$str1.'</ul></div>';
	     $return_tmp .=$ss.' </div>                         
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
            </div>
        </div></section>';
      return $return_tmp;
	}
	
		function template85($id,$temp_pos_id){
	$return_tmp='';
		$select="SELECT * FROM templates a LEFT JOIN template_title_desc b ON a.id=b.temp_id WHERE a.id='85' AND b.temp_pos_id='$temp_pos_id' AND b.page_id_td=".$id;
		$query=mysqli_query($this->db,$select);
            while($row = mysqli_fetch_array($query)){
                $template_name=$row["template_name"];
				$template_link=$row["template_link"];
				$template_icon=$row["template_icon"];
				$is_deleted=$row["is_deleted"];
				$updated_by=$row["updated_by"];
				$updated_date=$row["updated_date"];
				$temp_id=$row["temp_id"];
				$title=$row["title"];
				$description=$row["description"];
            }
    $return_tmp .='<section class="pt-4 pb-4 pt-md-4 pb-md-4 our_values">
	        <div class="container-fluid">
	            <h2 class="border_head blue-text"><span>'.$title.'</span></h2>
	            <p>
					'.html_entity_decode($description).'
				</p>
	            <div class="row">
				';
				$select_item="SELECT * FROM template_details a LEFT JOIN page b ON a.sub_link=b.id WHERE a.temp_id='85'  AND a.page_id_td='".$id."'  AND a.temp_pos_id='$temp_pos_id' ORDER BY a.id";
		$query_item=mysqli_query($this->db,$select_item);
		            
            while($row_item = mysqli_fetch_array($query_item)){
				
					 $return_tmp .='<div class="col-12 col-md-6 col-lg-6 white-text mt-4 mt-md-4">
	                    <div class="box orange-bg" style="background-color: '.$row_item["sub_bg_color"].';">
	                        <div class="boxHead">
	                            <div class="homeBxicon">';
								if($row_item["sub_icon"]!=""){
									 $return_tmp .='<div class="homeBxicon">
										<img src="images/icons/'.$row_item["sub_icon"].'" class="img-fluid" width="60">
									</div>';
								}
	                           $return_tmp .='</div>
	                        	<h3>'.$row_item["sub_title"].'</h3>
	                        </div>
	                    	<p>'.$row_item["sub_description"].'</p>
	                    </div>                
	                </div>  ';
			}        
	     $return_tmp .='      </div>
	        </div>
    </section>'	;
        
      return $return_tmp;
	}
	
	function template82($id,$temp_pos_id){
	$return_tmp='';
		
		$select="SELECT * FROM templates a LEFT JOIN template_title_desc b ON a.id=b.temp_id WHERE a.id='82' AND b.temp_pos_id='$temp_pos_id' AND b.page_id_td=".$id;
		$query=mysqli_query($this->db,$select);
            while($row = mysqli_fetch_array($query)){
                $template_name=$row["template_name"];
				$template_link=$row["template_link"];
				$template_icon=$row["template_icon"];
				$is_deleted=$row["is_deleted"];
				$updated_by=$row["updated_by"];
				$updated_date=$row["updated_date"];
				$temp_id=$row["temp_id"];
				$title=$row["title"];
				$description=$row["description"];
            }
    $return_tmp .='<section class="pt-4 pb-4 pt-md-5">
            <div class="container-fluid pl-md-5 pr-md-5">
			 <h2 class="border_head blue-text"><span>'.$title.'</span></h2>
				';
				$select_item="SELECT * FROM template_details a LEFT JOIN page b ON a.sub_link=b.id WHERE a.temp_id='82'  AND a.temp_pos_id='$temp_pos_id' AND a.page_id_td='".$id."'  ORDER BY a.id";
		$query_item=mysqli_query($this->db,$select_item);
		            
            while($row_item = mysqli_fetch_array($query_item)){
				
					 $return_tmp .=' <img src="images/icons/'.$row_item['sub_icon'].'" class="img-fluid">';
			}        
	     $return_tmp .='
	        </div>
    </section>'	;
      return $return_tmp;
	}
	
	function template86($id,$temp_pos_id){
		 $return_tmp="";
		$select="SELECT * FROM `template_details` WHERE temp_id='86' AND temp_pos_id='$temp_pos_id' AND  page_id_td='".$id."' ";
		$query=mysqli_query($this->db,$select);
           $all_data=array();
							while($rows=mysqli_fetch_array($query)){ 
							$all_data[]=$rows;
							}
		
		 $return_tmp .='<section class="pt-3 pt-md-3 pb-4 pb-md-5 mt-3 mt-md-3 mt-lg-5 heading-with-content">
            <div class="container-fluid pl-md-5 pr-md-5">
                 <h2 class="border_head blue-text"><span>'.$all_data[0]["title"].'</span></h2>
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 white-text mt-1 mt-md-4">
						<div class="col-md-6 col-lg-8 float-sm-right p-0 min-hgt-300" style="background:url(images/icons/'.$all_data[0]["sub_icon"].') no-repeat;background-size:cover;">
                            
                        </div>
                        <div class=" col-md-6 col-lg-4 float-left float-sm-right p-0">
                            <div class="box min-hgt-300 d-flex flex-wrap align-content-center border-turquoise border pt-3 pb-3 pt-md-0 pb-md-0">
                                <div class="boxHead no-border m-auto" style="color:'.$all_data[0]['sub_bg_color'].';">
                                    
										'.html_entity_decode($all_data[0]['description']).'
                                  
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
				</div>
			</div>
		</section>
		
		
		
		<section class="pt-3 pb-4 pt-md-0 pb-md-4 pt-lg-3 pb-lg-5 heading-with-content">
            <div class="container-fluid pl-md-5 pr-md-5">
				<h2 class="border_head blue-text"><span>'.$all_data[1]["title"].'</span></h2>
				<div class="row">
                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 white-text mt-1 mt-md-4">
						<div class="col-md-12 col-lg-4 float-left p-0 min-hgt-300" style="background:url(images/icons/'.$all_data[1]["sub_icon"].') no-repeat;background-size:cover;">
                            
                        </div>
                        <div class="col-md-12 col-lg-8 float-left p-0">
                            <div class="box orange-bg min-hgt-300 d-flex flex-wrap align-content-center pt-3 pb-3 pt-md-0 pb-md-0" style="background-color:'.$all_data[1]['sub_bg_color'].';">
								'.html_entity_decode($all_data[1]['description']).'
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </section>
		<section class="pt-3 pt-md-3 pb-4 pb-md-5 heading-with-content">
            <div class="container-fluid pl-md-5 pr-md-5">
				<h2 class="border_head blue-text"><span>'.$all_data[2]['title'].'</span></h2>
				<div class="row">
                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 white-text mt-1 mt-md-4">
						<div class="col-md-6 col-lg-4 float-left float-sm-right p-0 min-hgt-300" style="background:url(images/icons/'.$all_data[2]['sub_icon'].') no-repeat;background-size:cover;">
                            
                        </div>
						<div class=" col-md-6 col-lg-8 float-sm-right p-0">
                            <div class="box meduim-blue min-hgt-300 d-flex flex-wrap align-content-center pt-3 pb-3 pt-md-0 pb-md-0" style="background-color:'.$all_data[2]['sub_bg_color'].';">
								'.html_entity_decode($all_data[2]['description']).'
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </section>';
      return $return_tmp;
	}
	
	
	function template87($id,$temp_pos_id){
	$return_tmp='';
	$return_tmp_script='';
		
		$select="SELECT * FROM templates a LEFT JOIN template_title_desc b ON a.id=b.temp_id WHERE a.id='87' AND b.temp_pos_id='$temp_pos_id' AND b.page_id_td=".$id;
		$query=mysqli_query($this->db,$select);
            while($row = mysqli_fetch_array($query)){
                $template_name=$row["template_name"];
				$template_link=$row["template_link"];
				$template_icon=$row["template_icon"];
				$is_deleted=$row["is_deleted"];
				$updated_by=$row["updated_by"];
				$updated_date=$row["updated_date"];
				$temp_id=$row["temp_id"];
				$title=$row["title"];
				$description=$row["description"];
				$shortdescription=$row["shortdescription"];
				$bimg_file=$row["bimg_file"];
            }
	
    $return_tmp .='<section class="pt-4 pb-4 pt-md-5">
            <div class="container-fluid pl-md-5 pr-md-5">
                 <h2 class="border_head blue-text"><span>'.$title.'</span></h2>'.html_entity_decode($shortdescription).'</div></section>';
    $return_tmp .='		<section class="pt-3 pb-4 pt-md-0 pb-md-4 pb-lg-5 heading-with-content">
            <div class="container-fluid pl-md-5 pr-md-5">
				<div class="row">
                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 white-text mt-1 mt-md-4">
						<div class="col-md-12 col-lg-4 float-left p-0 min-hgt-300" style="background:url(images/icons/'.$bimg_file.') no-repeat;background-size:cover;">
                            
                        </div>
                        <div class="col-md-12 col-lg-8 float-left p-0">
                            <div class="box meduim-blue min-hgt-300 d-flex flex-wrap align-content-center pt-3 pb-3 pt-md-0 pb-md-0">'.html_entity_decode($description).'</div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </section>';
				$select_item="SELECT * FROM template_details a LEFT JOIN page b ON a.sub_link=b.id WHERE a.temp_id='87'  AND a.temp_pos_id='$temp_pos_id' AND a.page_id_td='".$id."'  ORDER BY a.id";
		$query_item=mysqli_query($this->db,$select_item);
		           $return_tmp .='<section class="pt-3 pt-md-3 pb-4 pb-md-5 heading-with-content">
            <div class="container-fluid pl-md-5 pr-md-5">            	
				<div class="row">';
				
				$j=1;
            while($row_item = mysqli_fetch_array($query_item)){
				    $resource=explode('~',$row_item['resource']);
										
					$heading=explode("*",$resource[0]);
					$values=explode("*",$resource[1]);
					$bgcolor=explode("*",$resource[2]);
					$php_data_array=array_combine($heading,$values);
					 $return_tmp .='<div class="col-lg-4 col-sm-4 bdr-right-chart text-center">';
					 $return_tmp .='<div class="min-heght">';
					 $return_tmp .='<h3 class="color-royal-blue">'.$row_item['title'].'</h3>';
					 $return_tmp .= '</div>';
					
					
					$j++;
			 
		 $return_tmp .=' <div id="donutchart'.$j.'"></div>';	
		 $return_tmp .= '</div>';
	      
		 $return_tmp_script .="	    <script type=\"text/javascript\">
	   google.charts.load(\"current\", {packages:[\"corechart\"]});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([";
		 $return_tmp_script .="['Task', 'Hours per Day'],";
		$all=array();
		foreach($php_data_array as $key=>$val)
		{
			$return_tmp_script .="['".$key."',".(int)$val."],";
		}
		 $return_tmp_script .="]);
        var options = {
             pieHole: 0.4,
		   colors: ["; 
		   foreach($bgcolor as $key=>$val)
			{
				$return_tmp_script .="'".$val."',";
			}
		 $return_tmp_script .="],};
		var chart = new google.visualization.PieChart(document.getElementById('donutchart".$j."'));
        chart.draw(data, options);
      }
    </script>";
	}
	 $return_tmp .='
	        </div>
            </div>
			
        </section>'	;
      return $return_tmp."~~".$return_tmp_script;
	}
	function template88($id,$temp_pos_id){
	$return_tmp='';
		$select="SELECT * FROM templates a LEFT JOIN template_title_desc b ON a.id=b.temp_id WHERE a.id='88' AND b.temp_pos_id='$temp_pos_id' AND b.page_id_td=".$id;
		$query=mysqli_query($this->db,$select);
            while($row = mysqli_fetch_array($query)){
                $template_name=$row["template_name"];
				$template_link=$row["template_link"];
				$template_icon=$row["template_icon"];
				$is_deleted=$row["is_deleted"];
				$updated_by=$row["updated_by"];
				$updated_date=$row["updated_date"];
				$temp_id=$row["temp_id"];
				$title=$row["title"];
				$description=$row["description"];
            }
    $return_tmp .='<section class="pt-4 pb-4 pt-md-5">
            <div class="container-fluid pl-md-5 pr-md-5">
                 <h2 class="border_head blue-text"><span>'.$title.'</span></h2>
				 <div class="row">
				';
				$select_item="SELECT * FROM template_details a LEFT JOIN page b ON a.sub_link=b.id WHERE a.temp_id='88'  AND a.temp_pos_id='$temp_pos_id' AND a.page_id_td='".$id."'  ORDER BY a.id";
		$query_item=mysqli_query($this->db,$select_item);
		            $j=1;
					$count=mysqli_num_rows($query_item);
					
            while($row_item = mysqli_fetch_array($query_item)){
				if($j%2==0)
				{
				 $return_tmp .='<div class="col-lg-6 col-sm-6">';
				}
				else
				{
					if($count==$j)
						 $return_tmp .='<div class="col-lg-12 col-sm-6">';
					else
						 $return_tmp .='<div class="col-lg-6 col-sm-6">';
				}
               $return_tmp .='<div class="turquoise text-white min-hgt-200 pt-4 pb-4  mb-4" style="background-color:'.$row_item['sub_bg_color'].';">
                            '.html_entity_decode($row_item["description"]).'
                        </div>
						</div>
                     
                     ';
					 $j++;
			}        
	     $return_tmp .='</div></div>
        </section>'	;
      return $return_tmp;
        
	} 
	
	function template89($id,$temp_pos_id){
	$return_tmp='';
		$select="SELECT * FROM templates a LEFT JOIN template_title_desc b ON a.id=b.temp_id WHERE a.id='89' AND b.temp_pos_id='$temp_pos_id' AND b.page_id_td=".$id;
		$query=mysqli_query($this->db,$select);
            while($row = mysqli_fetch_array($query)){
                $template_name=$row["template_name"];
				$template_link=$row["template_link"];
				$template_icon=$row["template_icon"];
				$is_deleted=$row["is_deleted"];
				$updated_by=$row["updated_by"];
				$updated_date=$row["updated_date"];
				$temp_id=$row["temp_id"];
				$title=$row["title"];
				$description=$row["description"];
            }
    $return_tmp .='<section class="pt-4 pb-4 pt-md-5 pb-md-3 pb-lg-5">
            <div class="container-fluid pl-md-5 pr-md-5">
                 <h2 class="border_head blue-text"><span>'.$title.'</span></h2>
				 '.html_entity_decode($description).'
                  <div class="row">
				';
				$select_item="SELECT * FROM template_details a LEFT JOIN page b ON a.sub_link=b.id WHERE a.temp_id='89'  AND a.temp_pos_id='$temp_pos_id' AND a.page_id_td='".$id."'  ORDER BY a.id";
		$query_item=mysqli_query($this->db,$select_item);
		            $j=1;
					$count=mysqli_num_rows($query_item);
					
            while($row_item = mysqli_fetch_array($query_item)){
				$sub_title=$row_item['sub_title'];
				$page_name=$row_item['page_name'];
				$page_img=$row_item['page_img'];
				$sub_bg_color=$row_item['sub_bg_color'];
				
				 $return_tmp .='<div class="col-12 col-lg-6 white-text mt-4">
                        <div class=" col-md-6 col-lg-6 float-left p-0">
                            <div class="box turquoise pt-4 pt-sm-5 min-hgt-300 d-flex flex-wrap align-content-center" style="background-color:'.$sub_bg_color.';">
                                <div class="boxHead text-center no-border m-auto">
                                    <h3 class="h-80">'.$sub_title.'</h3>
                                     <a href="#" class="text-white text-bold"><img src="images/next-arrow.png"></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-6 float-left p-0 min-hgt-300 d-none d-md-block" style="background:url(images/'.$page_img.') no-repeat;">
                        	
                        </div>
                        <div class="clearfix"></div>
                    </div>';
					 $j++;
			}        
	     $return_tmp .='</div>
        </div></section>'	;
      return $return_tmp;
        
	} 
	
 	function template10($id,$temp_pos_id){
	$return_tmp='';
	$return_tmp_script="";
	$select_option="";
	$list_year="";
	$awards="";
	$year="";
		$select="SELECT * FROM templates a LEFT JOIN template_title_desc b ON a.id=b.temp_id WHERE a.id='10' AND b.temp_pos_id='$temp_pos_id' AND b.page_id_td=".$id;
		$query=mysqli_query($this->db,$select);
            while($row = mysqli_fetch_array($query)){
                $template_name=$row["template_name"];
				$template_link=$row["template_link"];
				$template_icon=$row["template_icon"];
				$is_deleted=$row["is_deleted"];
				$updated_by=$row["updated_by"];
				$updated_date=$row["updated_date"];
				$temp_id=$row["temp_id"];
				$title=$row["title"];
				$description=$row["description"];
            }
    $return_tmp .='<section class="pt-4 pb-4 pt-md-5 pb-md-5">
                    <div class="container-fluid pl-md-5 pr-md-5">
                        <h2 class="border_head blue-text"><span>'.$title.'</span></h2>
                        <p>'.$description.'</p>
                        <h1 class="text-center blue-text mt-5">Awards & Recognitions</h1>
                  <div class="awardsTab">
                            <form class="mt-2 mb-2">
                                <select id="mySelect" class="color-navy-blue">
				';
				
				$select_item="SELECT * FROM template_details a LEFT JOIN page b ON a.sub_link=b.id WHERE a.temp_id='10'  AND a.temp_pos_id='$temp_pos_id' AND a.page_id_td='".$id."'  ORDER BY a.sub_year DESC";
		$query_item=mysqli_query($this->db,$select_item);
		            $j=1;
					$count=mysqli_num_rows($query_item);
				$i=0;	
            while($row_item = mysqli_fetch_array($query_item)){
				$sub_title=$row_item['sub_title'];
				$page_name=$row_item['page_name'];
				$page_img=$row_item['page_img'];
				$sub_bg_color=$row_item['sub_bg_color'];
				$sub_year=$row_item['sub_year'];
				$sub_icon=$row_item['sub_icon'];
				if($year==""){
					$year=$sub_year;
					$select_option .="<option value='".$i."' selected>".$year."</option>";
					$list_year .='</select>
                            </form>
                            <ul class="nav nav-tabs" id="myTab" style="display: none;">
                                <li class="active"><a href="#tab'.$year.'">'.$year.'</a></li>';
					$awards .='</ul>
                            <div class="tab-content">
                                <div class="tab-pane fade show active" id="tab'.$year.'" role="tabpanel" aria-labelledby="awards_Tab">
								<div class="p-2 p-sm-4">

                                        <div class="media d-block d-sm-flex p-2 p-sm-2 p-md-0">
                                            <img class="d-flex align-self-center mr-3" src="images/icons/'.$sub_icon.'" alt="" width="250">
                                            <div class="align-self-center media-body p-1 p-sm-0">
                                                <h4 class="mt-0 mb-2 color-navy-blue">Biocon felicitated at World HRD Congress 2018 for ‘Excellence in Training & Development’.</h4>
                                                <p class="mb-0">The excellent L&D initiatives at Biocon has contributed to the company being ranked amongst the Global Top 10 Biotech Employers. </p>
                                            </div>
                                        </div>

                                    </div>
									';
				}elseif($year==$sub_year){
					$awards .='
								<div class="p-2 p-sm-4">

                                        <div class="media d-block d-sm-flex p-2 p-sm-2 p-md-0">
                                            <img class="d-flex align-self-center mr-3" src="images/icons/'.$sub_icon.'" alt="" width="250">
                                            <div class="align-self-center media-body p-1 p-sm-0">
                                                <h4 class="mt-0 mb-2 color-navy-blue">Biocon felicitated at World HRD Congress 2018 for ‘Excellence in Training & Development’.</h4>
                                                <p class="mb-0">The excellent L&D initiatives at Biocon has contributed to the company being ranked amongst the Global Top 10 Biotech Employers. </p>
                                            </div>
                                        </div>

                                    </div>
									';
				}else{
					 $i++;
					$year=$sub_year;
					$select_option .="<option value='".$i."' >".$year."</option>";
					$list_year .='<li><a href="#tab'.$year.'">'.$year.'</a></li>';
					$awards .='
								</div>
                                <div class="tab-pane fade" id="tab'.$year.'" role="tabpanel" aria-labelledby="awards_Tab">
								<div class="p-2 p-sm-4">

                                        <div class="media d-block d-sm-flex p-2 p-sm-2 p-md-0">
                                            <img class="d-flex align-self-center mr-3" src="images/icons/'.$sub_icon.'" alt="" width="250">
                                            <div class="align-self-center media-body p-1 p-sm-0">
                                                <h4 class="mt-0 mb-2 color-navy-blue">Biocon felicitated at World HRD Congress 2018 for ‘Excellence in Training & Development’.</h4>
                                                <p class="mb-0">The excellent L&D initiatives at Biocon has contributed to the company being ranked amongst the Global Top 10 Biotech Employers. </p>
                                            </div>
                                        </div>

                                    </div>
									';
					
				}
				
					 $j++;
			}  
			 $awards .=' </div>
                            </div>
                            <div class="p-2 p-sm-4">
                                <a href="#" class="btn archive_btn">
                                    Archive <img src="images/icons/next-arrow-dark-blue.png">
                                </a>
                            </div>
                            
                        </div>
                    </div>

                </section>';
			$return_tmp_script="<script type=\"text/javascript\">
                            $('#mySelect').on('change', function(e) {
                                $('#myTab li a').eq($(this).val()).tab('show');
                            });
                        </script>";	
      return $return_tmp.$select_option.$list_year.$awards."~~".$return_tmp_script;
	} 
	
	function template90($id,$temp_pos_id){
	$return_tmp='';
		$select="SELECT * FROM templates a LEFT JOIN template_title_desc b ON a.id=b.temp_id WHERE a.id='90' AND b.temp_pos_id='$temp_pos_id' AND b.page_id_td=".$id;
		$query=mysqli_query($this->db,$select);
            while($row = mysqli_fetch_array($query)){
                $template_name=$row["template_name"];
				$template_link=$row["template_link"];
				$template_icon=$row["template_icon"];
				$is_deleted=$row["is_deleted"];
				$updated_by=$row["updated_by"];
				$updated_date=$row["updated_date"];
				$temp_id=$row["temp_id"];
				$title=$row["title"];
				$description=$row["description"];
				$bimg_file=$row["bimg_file"];
				$bcolor=$row["bcolor"];
			}
		 $return_tmp .='<section class="pt-4 pb-4 pt-md-5 pb-md-5">
            <div class="container-fluid pl-md-5 pr-md-5">
                 <h2 class="border_head blue-text"><span>'.$title.'</span></h2>
                  '.htmlspecialchars_decode($description).'
			';
							
		 $select_item="SELECT * FROM template_details a LEFT JOIN page b ON a.sub_link=b.id LEFT JOIN page_links c ON(a.sub_link=c.id)WHERE a.temp_id='90'  AND a.temp_pos_id='$temp_pos_id' AND a.page_id_td='".$id."'  ORDER BY a.id";
		$query_item=mysqli_query($this->db,$select_item);
			$i=1;
            while($row_item = mysqli_fetch_assoc($query_item)){
				 
				 $title=$row_item['title'];
				 $sub_image=$row_item['sub_image'];
				 $sub_description=$row_item['sub_description'];
				 $description=$row_item['description'];
				 $sub_bg_color=$row_item['sub_bg_color'];
				 $str='float-left';
				 if($i%2)
				 $str='float-left';
				 else
				 $str='float-right';
				 
				$return_tmp .='<div class="row">
                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 white-text mt-1 mt-md-4">
						<div class="col-md-12 col-lg-4 float-left p-0 min-hgt-300" style="background:url(images/'.$sub_image.') no-repeat;background-size:cover;">
                            
                        </div>
                        <div class="col-md-12 col-lg-8 float-left p-0">
                            <div class="box min-hgt-300 d-flex flex-wrap align-content-center pt-3 pb-3 pt-md-0 pb-md-0" style="background:'.$sub_bg_color.'">
                    '.htmlspecialchars_decode($sub_description).'
					</div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>';
			$i++;
			} 
			
	     $return_tmp .='</div>
		</section>';
      return $return_tmp;
	} 
	
	function template91($id,$temp_pos_id){
	$return_tmp='';
		$select="SELECT * FROM templates a LEFT JOIN template_title_desc b ON a.id=b.temp_id WHERE a.id='91' AND b.temp_pos_id='$temp_pos_id' AND b.page_id_td=".$id;
		$query=mysqli_query($this->db,$select);
            while($row = mysqli_fetch_array($query)){
                $template_name=$row["template_name"];
				$template_link=$row["template_link"];
				$template_icon=$row["template_icon"];
				$is_deleted=$row["is_deleted"];
				$updated_by=$row["updated_by"];
				$updated_date=$row["updated_date"];
				$temp_id=$row["temp_id"];
				$title=$row["title"];
				$description=$row["description"];
				$bimg_file=$row["bimg_file"];
				$bcolor=$row["bcolor"];
			}
		 $return_tmp .='
<section class="pt-4 pb-4 pt-md-0 pb-md-0">
            <div class="container-fluid pl-md-5 pr-md-5">
				<h2 class="border_head blue-text"><span>'.$title.'</span></h2>
                  '.htmlspecialchars_decode($description).'
			';
					
	     $return_tmp .='</div>
		</section>';
      return $return_tmp;
	} 
	
	function template92($id,$temp_pos_id){
	$return_tmp='';
		$select="SELECT * FROM templates a LEFT JOIN template_title_desc b ON a.id=b.temp_id WHERE a.id='92' AND b.temp_pos_id='$temp_pos_id' AND b.page_id_td=".$id;
		$query=mysqli_query($this->db,$select);
            while($row = mysqli_fetch_array($query)){
                $template_name=$row["template_name"];
				$template_link=$row["template_link"];
				$template_icon=$row["template_icon"];
				$is_deleted=$row["is_deleted"];
				$updated_by=$row["updated_by"];
				$updated_date=$row["updated_date"];
				$temp_id=$row["temp_id"];
				$title=$row["title"];
				$description=$row["description"];
            }
		 $return_tmp .='<section class="pt-2 pb-3 pb-md-5 sec_02 similars">
		   <div class="container-fluid">
			  <div class="row">
                 ';
		 $select_item="SELECT * FROM template_details a LEFT JOIN page b ON a.sub_link=b.id LEFT JOIN page_links c ON(a.sub_link=c.id)WHERE a.temp_id='92'  AND a.temp_pos_id='$temp_pos_id' AND a.page_id_td='".$id."'  ORDER BY a.id";
		$query_item=mysqli_query($this->db,$select_item);
		            
            while($row_item = mysqli_fetch_assoc($query_item)){
				
			 $return_tmp .='
			 <div class="col-sm-6 col-md-6 col-lg-4 col-xl-4 white-text mt-0 mt-md-4">
					<img src="images/'.$row_item['sub_image'].'" class="img-fluid">   
					<div class="box orange-bg d-flex flex-wrap align-content-center justify-content-center">
						<h3>'.$row_item['sub_title'].'</h3>
					   <a href="'.$row_item['page_link'].'" class="read_more">Know More</a>
					</div>
				 </div>
			 ';
			}
	     $return_tmp .='</div>
           </div>
</section>';
      return $return_tmp;
	}
	
	function template94($id,$temp_pos_id){
	$return_tmp='';
		$select="SELECT * FROM templates a LEFT JOIN template_title_desc b ON a.id=b.temp_id WHERE a.id='94' AND b.temp_pos_id='$temp_pos_id' AND b.page_id_td=".$id;
		$query=mysqli_query($this->db,$select);
            while($row = mysqli_fetch_array($query)){
                $template_name=$row["template_name"];
				$template_link=$row["template_link"];
				$template_icon=$row["template_icon"];
				$is_deleted=$row["is_deleted"];
				$updated_by=$row["updated_by"];
				$updated_date=$row["updated_date"];
				$temp_id=$row["temp_id"];
				$title=$row["title"];
				$description=$row["description"];
            }
    $return_tmp .='<section class="pt-3 pt-md-3 mt-3 mt-md-3 mt-lg-5 heading-with-content">
            <div class="container-fluid pl-md-5 pr-md-5">
            	<div class="col-12 col-md-8 offset-md-2">
                    <table class="table table-striped table-bordered">
                        <thead class="royal-blue white-text fnt-bold">
                          <tr>';
				$select_item="SELECT * FROM template_details a LEFT JOIN page b ON a.sub_link=b.id WHERE a.temp_id='94' AND a.temp_pos_id='$temp_pos_id' AND a.page_id_td='".$id."' ORDER BY a.id";
		$query_item=mysqli_query($this->db,$select_item);
		            
            while($row_item = mysqli_fetch_array($query_item)){
				$titles=explode("~",$row_item["sub_title"]);
				$sub_description=explode("~",$row_item["sub_description"]);
			}
			$i=1;
			foreach($titles as $key => $value){
				if($i==1){
					$return_tmp .='<th width="50%">'.$titles[$key].'</th>
                            <th width="50%">'.$sub_description[$key].'</th>
                          </tr>
                        </thead>
                        <tbody>';
				}else{
					$return_tmp .='<tr>
                            <td>'.$titles[$key].'</td>
                            <td>'.$sub_description[$key].'</td>
                          </tr>';
				}
			$i++;				
			}        
	     $return_tmp .=' </tbody>
                      </table>
                  </div>
			</div>
		</section>'	;
      return $return_tmp;
	}
	
	function template96($id,$temp_pos_id){
	$return_tmp='';
		$select="SELECT * FROM templates a LEFT JOIN template_title_desc b ON a.id=b.temp_id WHERE a.id='96' AND b.temp_pos_id='$temp_pos_id' AND b.page_id_td=".$id;
		$query=mysqli_query($this->db,$select);
            while($row = mysqli_fetch_array($query)){
                $template_name=$row["template_name"];
				$template_link=$row["template_link"];
				$template_icon=$row["template_icon"];
				$is_deleted=$row["is_deleted"];
				$updated_by=$row["updated_by"];
				$updated_date=$row["updated_date"];
				$temp_id=$row["temp_id"];
				$title=$row["title"];
				$description=$row["description"];
            }
    $return_tmp .='<section class="pt-3 pt-md-3 mt-3 mt-md-3 mt-lg-5 heading-with-content">
            <div class="container-fluid pl-md-5 pr-md-5">
              <div class="row">';
				$select_item="SELECT * FROM template_details a LEFT JOIN page b ON a.sub_link=b.id WHERE a.temp_id='96' AND a.temp_pos_id='$temp_pos_id' AND a.page_id_td='".$id."' ORDER BY a.id";
		$query_item=mysqli_query($this->db,$select_item);
		         $j=1;   
            while($row_item = mysqli_fetch_array($query_item)){
				if($j==1){
											$block7_menu_title_1=$row_item["title"];
											$block7_menu_video=explode('https://www.youtube.com/watch?v=', $row_item["description"]);
											$sub_link=$row_item["sub_link"];
											$color_1=$row_item["sub_bg_color"];
										}
										else
										{
											$block7_menu_title_2=$row_item["title"];
											$block7_menu_video_2=$row_item["description"];
											$sub_link_2=$row_item["sub_link"];
											$color_2=$row_item["sub_bg_color"];
										}
										$j++;
			}
			$return_tmp .= '<div class="col-12 col-md-6 mb-4">
               <div class="border border-turquoise min-hgt-300" style="border-color:'.$color_1.'">
                  <div class="turquoise white-text p-4">
                    <h3>'.$block7_menu_title_1.'</h3>
                  </div>
                  <div class="text-center pt-4 pb-4">
                    <iframe width="240" height="180" src="https://www.youtube.com/embed/'.$block7_menu_video[1].'"></iframe>
                    <div class="clearfix"></div>
                    <a href="'.$sub_link.'" class="border border-turquoise p-2 pl-4 pr-4 view-icons color-turquoise d-inline-block">View All <img src="images/icons/next-arrowturquoise.png" width="25px" class="img-fluid ml-4"></a>
                  </div>
              </div>
            </div>
            <div class="col-12 col-md-6 mb-4">
               <div class="border border-dodger-blue min-hgt-300" style="border-color:'.$color_2.'">
                  <div class="dodger-blue white-text p-4">
                    <h3>'.$block7_menu_title_2.'</h3>
                  </div>
                  <div class="pt-4 pb-4">
                    <ul class="pdf-list">';
					$titles=explode("~",$block7_menu_video_2);
					$files=explode("~",$sub_link_2);
				foreach($titles as $key => $value){
                     $return_tmp .= '<li><a href="uploads/'.$files[$key].'" target="_blank">'.$titles[$key].'</a></li>';
					}
                    $return_tmp .= '</ul>
                  </div>
              </div>
            </div>';
			       
	     $return_tmp .=' </div>
      			</div>
      		</section>'	;
      return $return_tmp;
	}
	function template60($id,$temp_pos_id){
	$return_tmp='';
		$select="SELECT * FROM templates a LEFT JOIN template_title_desc b ON a.id=b.temp_id WHERE a.id='60' AND b.temp_pos_id='$temp_pos_id' AND b.page_id_td=".$id;
		$query=mysqli_query($this->db,$select);
            while($row = mysqli_fetch_array($query)){
                $template_name=$row["template_name"];
				$template_link=$row["template_link"];
				$template_icon=$row["template_icon"];
				$is_deleted=$row["is_deleted"];
				$updated_by=$row["updated_by"];
				$updated_date=$row["updated_date"];
				$temp_id=$row["temp_id"];
				$title=$row["title"];
				$description=$row["description"];
            }
    $return_tmp .='<section class="mb-4">
        <div class="container-fluid pl-md-5 pr-md-5">
            ';
				$select_item="SELECT * FROM template_details a LEFT JOIN page b ON a.sub_link=b.id WHERE a.temp_id='60' AND a.temp_pos_id='$temp_pos_id' AND a.page_id_td='".$id."' ORDER BY a.id";
		$query_item=mysqli_query($this->db,$select_item);
		          
            while($row_item = mysqli_fetch_array($query_item)){
				
				$block7_menu_title_2=$row_item["title"];
				$block7_menu_video_2=$row_item["description"];
				$sub_link_2=$row_item["sub_link"];
				$color_2=$row_item["sub_bg_color"];
										
			}
			    $return_tmp .='
            <div class=" pt-4 pl-4 pr-4" style="background:'.$color_2.'">
              <ul class="row white-pdf">';
					$titles=explode("~",$block7_menu_video_2);
					$files=explode("~",$sub_link_2);
				foreach($titles as $key => $value){
                     $return_tmp .= '<li class="col-12 col-md-3"><a href="uploads/'.$files[$key].'" target="_blank">'.$titles[$key].'</a></li>';
					}
			       
	     $return_tmp .='</ul>
            </div>


        </div>
      </section>'	;
      return $return_tmp;
	}
		
/*============================= this method is for widget no: 93===========================*/
function template93($id,$temp_pos_id){
	$return_tmp='';
	$select="SELECT * FROM templates a LEFT JOIN template_title_desc b ON a.id=b.temp_id WHERE a.id='93' AND b.temp_pos_id='$temp_pos_id' AND b.page_id_td=".$id;
	$query=mysqli_query($this->db,$select);
		while($row = mysqli_fetch_array($query)){
			$template_name=$row["template_name"];
			$template_link=$row["template_link"];
			$template_icon=$row["template_icon"];
			$is_deleted=$row["is_deleted"];
			$updated_by=$row["updated_by"];
			$updated_date=$row["updated_date"];
			$temp_id=$row["temp_id"];
			$title=$row["title"];
			$description=$row["description"];
		}
		$return_tmp .='
		<section class="pt-1 pb-4 pt-md-0 pb-md-5 mt-5 at_biocon">
			<div class="container-fluid pl-md-5 pr-md-5">
				<h2 class="border_head blue-text"><span>'.$title.'</span></h2>
				<div class="row mt-3">
			  ';
		 $select_item="SELECT * FROM template_details a LEFT JOIN page b ON a.sub_link=b.id LEFT JOIN page_links c ON(a.sub_link=c.id)WHERE a.temp_id='93'  AND a.temp_pos_id='$temp_pos_id' AND a.page_id_td='".$id."'  ORDER BY a.id";
		$query_item=mysqli_query($this->db,$select_item);
					 
			 while($row_item = mysqli_fetch_assoc($query_item)){
			
			  $return_tmp .='
				  <div class="col-sm-6 col-md-6 col-lg-4 col-xl pl-xl-0 pr-xl-0 pb-4 pb-xl-0 mr-2" >
					<div class="box pt-3 pb-0 d-flex flex-wrap align-content-center" style=" background-color: '.$row_item['sub_bg_color'].' !important;">
						<div class="boxHead text-center no-border m-auto">
							<img src="images/icons/'.$row_item['sub_icon'].'" class="img-fluid mb-3">
							<h3 class="h-80 text-white pb-4">'.$row_item['sub_title'].'</h3>
							<a href="'.$row_item['page_link'].'" class="text-white text-bold">Know More</a>	
						</div>
					</div>
				</div>
			  ';
			 }				
		  $return_tmp .='</div>
			</div>
 </section>';
	   return $return_tmp;
	 }


	 /* ==================================This method is for template 25 ======================================= */

	 function template25($id,$temp_pos_id){
		$return_tmp='';
		$select="SELECT * FROM templates a LEFT JOIN template_title_desc b ON a.id=b.temp_id WHERE a.id='25' AND b.temp_pos_id='$temp_pos_id' AND b.page_id_td=".$id;
		$query=mysqli_query($this->db,$select);
			while($row = mysqli_fetch_array($query)){
				$template_name=$row["template_name"];
				$template_link=$row["template_link"];
				$template_icon=$row["template_icon"];
				$is_deleted=$row["is_deleted"];
				$updated_by=$row["updated_by"];
				$updated_date=$row["updated_date"];
				$temp_id=$row["temp_id"];
				$title=$row["title"];
				$bimg_file=$row["bimg_file"];
				$description=$row["description"];
			}
			$return_tmp .='						
						<section class="pt-4 pb-4 pt-md-5 mb-4 mb-md-0 pb-md-5 hr_policies">
							<div class="container-fluid pl-md-5 pr-md-5">
								<h2 class="border_head blue-text"><span>'.$title.'</span></h2>
								<div style="background-image: url(\'main-control/images/'.$bimg_file.'\') ;background-size:cover;" class="pt-4 pb-4 mt-3">
									<div class="row"> 
				  ';
			$select_item="SELECT * FROM template_details a LEFT JOIN page b ON a.sub_link=b.id LEFT JOIN page_links c ON(a.sub_link=c.id)WHERE a.temp_id='25'  AND a.temp_pos_id='$temp_pos_id' AND a.page_id_td='".$id."'  ORDER BY a.id";
			$query_item=mysqli_query($this->db,$select_item);
						 
				 while($row_item = mysqli_fetch_assoc($query_item)){
					
				  $return_tmp .='
					  <div class="col-sm-6 col-md-6 col-lg-4 col-xl mb-sm-4 mb-xl-0 border-gray-right">
						<div class="box min-hgt-300 d-flex flex-wrap align-content-end">
							<div class="boxHead text-center no-border m-auto">
									<img src="images/icons/'.$row_item['sub_icon'].'" class="img-fluid mb-3">
									<h3 class="h-80 text-white pb-4">'.$row_item['sub_title'].'</h3>
								<a href="#" class="text-white text-bold" data-toggle="modal" data-target="#myModal">Know More</a>
							</div>
						</div>
					</div>
				  ';
				 }
				//  /<a href="'.$row_item['page_link'].'" class="text-white text-bold">Know More</a>
			  $return_tmp .='</div>
					  </div>
				</div>
	 </section>';
		   return $return_tmp;
		 }


		 /* ==================================This method is for template 31 ======================================= */

 function template31($id,$temp_pos_id){
	$return_tmp='';
	$select="SELECT * FROM templates a LEFT JOIN template_title_desc b ON a.id=b.temp_id WHERE a.id='31' AND b.temp_pos_id='$temp_pos_id' AND b.page_id_td=".$id;
	$query=mysqli_query($this->db,$select);
		while($row = mysqli_fetch_array($query)){
			$template_name=$row["template_name"];
			$template_link=$row["template_link"];
			$template_icon=$row["template_icon"];
			$is_deleted=$row["is_deleted"];
			$updated_by=$row["updated_by"];
			$updated_date=$row["updated_date"];
			$temp_id=$row["temp_id"];
			$title=$row["title"];				
			$description=$row["description"];
			$bimg_file = $row["bimg_file"];
			$bg_color = $row["bcolor"];
		}
		$return_tmp .='						
				<section class="pt-4 pb-4 pt-md-2 pb-md-4 roots">
					<div class="container-fluid pl-md-5 pr-md-5">
						<div class="row">
							<div class="col-12 col-md-12 col-lg-12 col-xl-5 col-xl white-text mt-0 mt-md-4">
								<div class="box pt-4 d-flex align-items-center justify-content-center flex-column" style="background:'.$bg_color.'">
									<img src="main-control/images/'.$bimg_file.'" class="img-fluid">
									<h4 class="text-white text-center pt-3">'.$title.'</h4>
								</div>                
							</div>   
							<div class="col-12 col-md-12 col-lg-12 col-xl-7 white-text mt-0 mt-md-4">
								<div class="box light-gray d-flex align-items-center justify-content-center flex-column pt-5">
			  ';
		$select_item="SELECT * FROM template_details a LEFT JOIN page b ON a.sub_link=b.id LEFT JOIN page_links c ON(a.sub_link=c.id)WHERE a.temp_id='31'  AND a.temp_pos_id='$temp_pos_id' AND a.page_id_td='".$id."'  ORDER BY a.id";
		$query_item=mysqli_query($this->db,$select_item);

		 
			 while($row_item = mysqli_fetch_assoc($query_item)){
				
			  $return_tmp .=' <div class="row">
								<div class="col-md-4 text-center">
									<img src="images/icons/'.$row_item['sub_image'].'" class="img-fluid">
								</div> 
								<div class="col-md-8 color-dark-gray">									
									'.htmlspecialchars_decode($row_item['sub_description']).'
									<h4 class="color-dark-gray fnt-light text-mobile-center">'.$row_item['sub_title'].'</h4>
								</div>
							</div>';
			 }
			
		  $return_tmp .='            </div>                
								</div>  
							</div>
						</div>
					</section>';
	   return $return_tmp;
	 }
		


		  /* ==================================This method is for template 32 ======================================= */

	 function template32($id,$temp_pos_id){
		$return_tmp='';
		$select="SELECT * FROM templates a LEFT JOIN template_title_desc b ON a.id=b.temp_id WHERE a.id='32' AND b.temp_pos_id='$temp_pos_id' AND b.page_id_td=".$id;
		$query=mysqli_query($this->db,$select);
			while($row = mysqli_fetch_array($query)){
				$template_name=$row["template_name"];
				$template_link=$row["template_link"];
				$template_icon=$row["template_icon"];
				$is_deleted=$row["is_deleted"];
				$updated_by=$row["updated_by"];
				$updated_date=$row["updated_date"];
				$temp_id=$row["temp_id"];
				$title=$row["title"];				
				$description=$row["description"];
			}
			$return_tmp .='						
			<section class="pt-4 pb-0 pt-md-5 pb-md-3 pb-lg-1">
				<div class="container-fluid pl-md-5 pr-md-5">
					<h2 class="border_head blue-text"><span>'.$title.' </span></h2>
					'.htmlspecialchars_decode($description).'
				</div>
			</section>
				  ';
			$select_item="SELECT * FROM template_details a LEFT JOIN page b ON a.sub_link=b.id LEFT JOIN page_links c ON(a.sub_link=c.id)WHERE a.temp_id='32'  AND a.temp_pos_id='$temp_pos_id' AND a.page_id_td='".$id."'  ORDER BY a.id";
			$query_item=mysqli_query($this->db,$select_item);

			$return_tmp .='<section class="pt-0 pb-4 pt-md-0 pt-lg-0 pt-xl-4 pb-md-4 getter">
								<div class="container-fluid pl-md-5 pr-md-5">
									<div class="row">';		 
				 while($row_item = mysqli_fetch_assoc($query_item)){
					
				  $return_tmp .='
								<div class="col-12 col-sm-6 col-md-6 col-lg-4 col-xl white-text mt-4 mt-md-4">
									<div class="box sky-blue"  style="background-color: '.$row_item['sub_bg_color'].'" >
										<h2 class="text-center color-sky-blue d-none d-xl-block">'.$row_item['sub_icon'].'</h2>
										<div class="boxHead">
											<h3 class="text-mobile-center">'.$row_item['sub_title'].'</h3>
										</div>
										<p>	'.$row_item['sub_description'].'	</p>
									</div>                
								</div> ';
				 }
				
			  $return_tmp .='
					  </div>
				</div>
	 </section>';
		   return $return_tmp;
		 }

 /*=============================This is for template number : 95============================ */

 function template95($id,$temp_pos_id){
	$return_tmp='';
	$select="SELECT * FROM templates a LEFT JOIN template_title_desc b ON a.id=b.temp_id WHERE a.id='95' AND b.temp_pos_id='$temp_pos_id' AND b.page_id_td=".$id;
	$query=mysqli_query($this->db,$select);
		while($row = mysqli_fetch_array($query)){
			$template_name=$row["template_name"];
			$template_link=$row["template_link"];
			$template_icon=$row["template_icon"];
			$is_deleted=$row["is_deleted"];
			$updated_by=$row["updated_by"];
			$updated_date=$row["updated_date"];
			$temp_id=$row["temp_id"];
			$title=$row["title"];
			$bimg_file=$row["bimg_file"];
			$description=$row["description"];
		}
		$return_tmp .='						
					<section class="pt-3 pt-md-3 mt-3 mt-md-3 mt-lg-5 heading-with-content">
					  <div class="container-fluid pl-md-5 pr-md-5">
						<h2 class="text-center color-navy-blue">'.$title.'</h2>
			  ';
				$select_item="SELECT * FROM template_details a LEFT JOIN page b ON a.sub_link=b.id LEFT JOIN page_links c ON(a.sub_link=c.id)WHERE a.temp_id='95'  AND a.temp_pos_id='$temp_pos_id' AND a.page_id_td='".$id."'  ORDER BY a.id";
				$query_item=mysqli_query($this->db,$select_item);
					 
			 while($row_item = mysqli_fetch_assoc($query_item)){
				
			  $return_tmp .='
			  <form class="mt-2 mb-2 mt-md-4 mb-md-4 text-center">
					<select id="annual-report-year" class="color-navy-blue custom-select1 border-navy-blue">
						<option value="0">FY 2018-19</option>
						<option value="1">FY 2017-18</option>
					</select>
				</form>
				<div class="row">';
				for($i=0; $i<4; $i++){
					$title = explode( "~", $row_item['title']);
					$description = explode( "~", $row_item['description']);
					$bg_color = explode("~", $row_item['sub_bg_color']);
					$file = explode( "~", $row_item['sub_title']);
					$file_description = explode( "~", $row_item['sub_description']);


				$return_tmp .= '<div class="col-12 col-md-2 col-sm-3 offset-md-1">
									<a href="#" onclick="func('.$i.');">
										<div class="text-white text-white p-4 text-center mb-4" style="background-color:'.$bg_color[$i].' ">
											<h2 class="font-50">'.$title[$i].'</h2>
											<p>'.$description[$i].'</p>
										</div>
                       				</a>
								</div>
								';
								$return_tmp .= '';
					}
						
					$x = '<span id="doc"></span>';
				if($i == $x){
					$return_tmp .= '</div><div class="text-center mt-4 mb-4 mt-md-5 mb-md-5 ">
									<h5>
										<a href="downloads/'.$file[$i].'" class="color-dark-gray">
											<img src="images/icons/pdf-blue.png" class="img-fluid mr-3" />
											'.$file_description[$i].'
										</a>
									</h5>
								</div>';
				}				
							
				
			 }			
		  $return_tmp .=' </div>
						</div>
					</section>';

			$return_script = '<script>
			 				function func(x = ""){
								$("#doc").text(x);
								alert(x);
							}
							</script>';
	   return $return_tmp."~~".$return_script;
	 }

	  /*=============================This is for template number : 110============================ */


	function template110($id,$temp_pos_id){
		$return_tmp='';
			$select="SELECT * FROM templates a LEFT JOIN template_title_desc b ON a.id=b.temp_id WHERE a.id='110'  AND b.temp_pos_id='$temp_pos_id' AND b.page_id_td=".$id;
			$query=mysqli_query($this->db,$select);
			$row_count=mysqli_num_rows($query);
				while($row = mysqli_fetch_array($query)){
					$template_name=$row["template_name"];
					$template_link=$row["template_link"];
					$template_icon=$row["template_icon"];
					$is_deleted=$row["is_deleted"];
					$updated_by=$row["updated_by"];
					$updated_date=$row["updated_date"];
					$temp_id=$row["temp_id"];
					$title=$row["title"];
					$description=$row["description"];
				}
		 $return_tmp .='<section class="pt-2 pb-3 pb-md-5 sec_02 talent">
		 <div class="container-fluid">';
		   if($row_count==1){
			   
			$return_tmp .= '<h2 class="border_head blue-text mb-3"><span>'.$title.'</span></h2>
			  <p>'.$description.'</p>';
		   }
			 $return_tmp .='<div class="row">';
			  $select_item="SELECT * FROM template_details a LEFT JOIN page b ON a.sub_link=b.id WHERE a.temp_id='110'  AND a.temp_pos_id='$temp_pos_id' AND a.page_id_td='".$id."'  ORDER BY a.id";
			$query_item=mysqli_query($this->db,$select_item);
					$i = 1;	
				while($row_item = mysqli_fetch_array($query_item)){
					if(($i % 2) != 0){
						// echo "true";
						$return_tmp .= '
							<div class="col-sm-12 col-md-6 col-lg-6 col-xl-4 white-text mt-0 mt-md-4">
								<div class="box box01 dodger-blue">
									<div class="boxHead">
									<div class="homeBxicon">';
									
									if(!empty($row_item['sub_icon'])){
										$return_tmp .= '<img src="images/icons/'.$row_item['sub_icon'].'" width="60" class="img-fluid"></div>
									<h3>'.$row_item['sub_title'].'</h3>';
									}else {
										$return_tmp .= '</div><h3>'.$row_item['sub_title'].'</h3>';
									}
									$return_tmp .= '</div>
									<p>'.$row_item['sub_description'].'</p>
								</div>
								<div class="box02" style="background:url(images/'.$row_item['sub_image'].') no-repeat;background-size:cover;"></div>	
							</div>
						'; 
					}else{
						// echo "false"
						$return_tmp .= '<div class="col-sm-12 col-md-6 col-lg-6 col-xl-4 white-text mt-4 mt-md-4">
						<div class="box01 blue-bg d-flex flex-wrap justify-content-center align-content-center pt-4 pb-4">
							<img src="images/'.$row_item['sub_image'].'" class="img-fluid">
						</div>
						<div class="box box02 dark-orange">
							<div class="boxHead">
								<h3>'.$row_item['sub_title'].'</h3>
							</div>
							<p>'.$row_item['sub_description'].'</p>
						</div>
					</div>';
					}

			

				$i++;
				}
			 $return_tmp .='</div>
		   </div>
		</section>';
		
		  return $return_tmp;
		}
		



}
?>