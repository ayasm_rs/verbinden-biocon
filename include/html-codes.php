<?php
include("main-control/include/mysqli_connect.php");
date_default_timezone_set('Asia/Kolkata');
//Site Details 
$site_details = '';

$query_site_details = mysqli_query($connect, "SELECT * FROM site_details");
while($row = mysqli_fetch_array($query_site_details)){
	$site_details .= $row['value'].'~';
}

$site_details_exp = explode("~", $site_details);
$site_name = $site_details_exp[0];
$site_url = $site_details_exp[1];
$logo = $site_details_exp[2];
$logo_small = $site_details_exp[3];
$favicon = $site_details_exp[4];
function wayTop(){
	echo '<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">';
}
function css(){
	$favicon = $GLOBALS['favicon'];
		echo'<!-- Bootstrap CSS -->
		
		<link rel="shortcut icon" href="'.$favicon.'">
		
		<link rel="stylesheet" href="css/bootstrap.min.css" integrity="" crossorigin="anonymous">
		<!--Font Awesome-->
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" >
		<!--Custom Css Styles-->
		<link rel="stylesheet" type="text/css" href="css/style.css">
		<link rel="stylesheet" type="text/css" href="css/custom.css">		
		<link rel="stylesheet" type="text/css" href="css/responsive.css">
		<link rel="stylesheet" type="text/css" href="css/color.css">
		<link rel="stylesheet" type="text/css" href="css/navbar.css">

		
		
		
		
		';	
		
}
function nav(){
		echo'<div class="serach-overlay"></div>
		<header class="cd-header ">
			<div class="ml-md-5 mr-md-5 header_container">
				<div class="cd-logo">
					<a href="https://www.biocon.com/"><img src="images/logo.png" alt="Biocon"></a>
				</div>
				<div class="searchContainer">
					<form class="searchbox">
						<input type="search" placeholder="Search.." name="search" class="searchbox-input" onkeyup="buttonUp();" required>
						<button type="submit" class="searchbox-submit"><img src="images/icons/search_icon.png" alt="Biocon"></button>
					</form>
	
				</div>
				<nav>
					<ul class="searchBtn">
						<li>
							<a href="#" class="searchbox-icon"><img src="images/icons/search_icon.png" alt="Biocon" style="margin-left: -42px;">
							</a>
						</li>
					</ul>
					<ul class="cd-top-nav d-none d-lg-block">
								<li><a href="#">Investors</a></li>
								<li><a href="#">Media</a></li>
								<li><a href="#">Partners</a></li>
								<li><a href="#">Careers</a></li>
								<li><a href="#">Health  Professionals</a></li>
								<li><a href="#" class="share_arrow">For Patients</a></li>
					</ul>
					<ul class="cd-secondary-nav">
						<li class="primeryNav">
							<ul>
								<li><a href="#">OUR COMPANY</a></li>
								<li><a href="#">OUR BUSINESS</a></li>
								<li><a href="#">OUR SCIENCE</a></li>
								<li><a href="#">OUR PRODUCTS</a></li>
								<li><a href="#">WE CARE</a></li>
							</ul>
						</li>
					</ul>
				</nav>
				<!-- cd-nav -->
				<a class="cd-primary-nav-trigger" href="#0">
					<p style="float: left; width: 35px; position: relative;"><span class="cd-menu-text"></span><span class="cd-menu-icon"></span></p></a>
	
			</div>
			<nav>
				<div>
					<div class="cd-primary-nav">
						<div class="container-fluid pl-5 pr-5">
							<div class="row">
							<div class="col-sm">
								<ul>
									<li><a href="about-us.php"> About Us</a>
									<li><a href="mission-vision-values.php">Mission,Vision,Values</a></li>
									<li><a href="#">Fact Sheet</a></li>
									<li><a href="key-management-team.php">Key Management Team</a></li>
									<li><a href="#">Kiran Mazumdar Shaw</a></li>
									<li><a href="milestone.php">Milestones</a></li>
									<li><a href="achievements-and-awards.php">Awards</a></li>
									<li><a href="company-history.php">History</a></li>
									
								</ul>
							</div>
							<div class="col-sm">
								<ul>
									<li><a href="#"> Businesses</a></li>
									<li><a href="#">Biologics</a></li>
									<li><a href="#">Small Molecules</a></li>
									<li><a href="#">Key Management Team</a></li>
									<li><a href="#">Branded Formulations - India</a></li>
									<!-- <li><a href="#" class="view_all">View All <i class="fa fa-angle-right" aria-hidden="true"></i></a></li> -->
								</ul>
							</div>
							<div class="col-sm">
								<ul>
									<li><a href="#">R&amp;D</a></li>
									<li><a href="#">Areas of Focus</a></li>
									<li><a href="#">Capability</a></li>
									<li><a href="#">Pipeline</a></li>
									<li><a href="#">Biosimilars</a></li>
									<li><a href="#">Mabs Recombinant Proteins</a></li>
									<li><a href="#">Clinical Research</a></li>
									<li><a href="#">Publications & Presentations</a></li>
									<li><a href="#">Partnerships & Collaborations</a></li>
									
								</ul>
							</div>
							<div class="col-sm">
								<ul>
									<li><a href="#">Investors</a></li>
									<li><a href="#">Corporate Structure</a></li>
									<li><a href="#">Company History</a></li>
									<li><a href="#">Corporate Governance</a></li>
									<li><a href="#">Policies</a></li>
									<li><a href="#">Stock Info</a></li>
									<li><a href="#">Disclosure to Stock Exchange</a></li>
									<li><a href="#">Annual General Meeting</a></li>
									<li><a href="#">Secretarial Contact</a></li>
									<li><a href="#">Financial Results</a></li>
									<li><a href="#">Subsidiary Financials</a></li>
									<li><a href="#">Annual Report</a></li>
								   
								</ul>
							</div>
							<div class="col-sm">
								<ul>
									<li><a href="#">Newsroom</a>
									<li><a href="#">Press Releases</a></li>
									<li><a href="#">Media Stories</a></li>
									<li><a href="#">Biocon Blog</a></li>
									<li><a href="#">Fact Sheet</a></li>
									<li><a href="#">Biocon Profile</a></li>
									<li><a href="#">Kiran Mazumdar-Shaw\'s Profile</a></li>
									<li><a href="#">Presentations</a></li>
									<li><a href="#">Video Gallery</a></li>
									<li><a href="#">Photo Gallery</a></li>
									<li><a href="#">Media FAQs</a></li>
									<li><a href="#">Media Events</a></li>
									<li><a href="#">Media Conference Calls</a></li>
									<li><a href="#">Media Contacts</a></li>
								</ul>
							</div>
						</div>
						</div>
					</div>
				</div>
			</nav>
			<!-- cd-primary-nav-trigger -->
		</header>
		';	
}
function footer(){
	echo'<footer class="pt-4 pb-4 pt-md-5 pb-md-5 pl-3 pr-3">
	   <div class="container-fluid">
		  <div class="">
			 <div class="col-sm-12 col-md-3 col-lg-3 footer-about mb-4 float-left">
				<div class="">
				   <img src="images/logo.png" class="img-fluid mt-2 mb-2" width="120">
				   <p>Information and resources 
					  useful for prescribing, 
					  dispensing and taking drugs 
					  manufactured by us.
				   </p>
				   <ul>
					  <li><a href="#">For Patients</a></li>
					  <li><a href="#">For Healthcare Professionals</a></li>
					  <li><a href="#" class="blue">Report Adverse Events</a></li>
				   </ul>
				</div>
			 </div>
			 <div class="col-sm-12 col-md-9 col-lg-9  float-left">
				<div class="row">
				   <div class="col-sm-4 col-md-6 col-lg-3 pl-4">
					  <h4>Get To Know Us</h4>
					  <ul class="footer-ul">
						 <li><a href="#">About Us</a></li>
						 <li><a href="#">Investors</a></li>
						 <li><a href="#">News room</a></li>
						 <li><a href="#">R&D</a></li>
						 <li><a href="#">Businesses</a></li>
						 <li><a href="#">Sustainability</a></li>
						 <li><a href="#">Careers</a></li>
					  </ul>
				   </div>
				   <div class="col-sm-4 col-md-6 col-lg-3">
					  <h4>Downloads</h4>
					  <ul class="footer-ul">
						 <li><a href="#">Annual Report</a></li>
						 <li><a href="#">Press Releases</a></li>
						 <li><a href="#">Presentations</a></li>
						 <li><a href="#">Video Gallery</a></li>
						 <li><a href="#">Photo Gallery</a></li>
					  </ul>
				   </div>
				   <div class="col-sm-4 col-md-6 col-lg-3">
					  <h4>Quick Links</h4>
					  <ul class="footer-ul">
						 <li><a href="#">Quality & Compliance</a></li>
						 <li><a href="#">Manufacturing</a></li>
						 <li><a href="#">Biocon Academy</a></li>
						 <li><a href="#">Biocon Foundation</a></li>
						 <li><a href="#">Syngene</a></li>
					  </ul>
				   </div>
				   <div class="col-md-6 col-lg-3">
					  <h4>Reach Us</h4>
					  <p>
						 Biocon Campus<br>
						 20th KM, Hosur Road,
						 Electronic City,
						 Bengaluru, Karnataka
						 560100
					  </p>
					  <ul class="footer-ul">
						 <li><a href="#" class="phone">080 2808 2808 </a></li>
						 <li><a href="#" class="email">contact.us@biocon.com</a></li>
					  </ul>
				   </div>
				</div>
				<!-- Copy right & Social -->
				<div class="row">
				   <div class="col-sm">
					  <div>
						 <a href="#"  class="mr-2">
						 <img src="images/icons/facebook.png" width="28">
						 </a>
						 <a href="#"  class="mr-2">
						 <img src="images/icons/twitter.png" width="28">
						 </a>
						 <a href="#" class="mr-2">
						 <img src="images/icons/linkedin.png" width="28">
						 </a>
						 <a href="#"  class="mr-2">
						 <img src="images/icons/wordpress.png" width="28">
						 </a>
						 <a href="#"  class="mr-2">
						 <img src="images/icons/youtube.png" width="28">
						 </a>
					  </div>
					  <div class="footer-bottom-menu">
						 <a href="#">Safe Harbour</a>|
						 <a href="#">Terms of Use</a>|
						 <a href="#">Privacy Policy</a>|
						 <a href="#">Site Map</a>
					  </div>
				   </div>
				   <div class="col-sm align-self-end">
					  <p class="float-right mb-0" style="color: #016acf;">Copyright © '.date("Y").' Biocon Ltd. All rights reserved</p>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</footer>';	
}
function script(){
	echo '<!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
     <script src="js/nav.js"></script>';	
}
function our_values(){
	echo'<section class="pt-4 pb-4 pt-md-5 pb-md-5 our_values">
	        <div class="container-fluid">
	            <h2 class="border_head blue-text"><span>Our Value Drivers</span></h2>
	            <p>Our philosophy of affordable innovation, which goes to the core of ensuring a global right to healthcare,
	            has helped us discover, develop and produce life-saving biotherapeutics that address unmet needs.</p>
	            <div class="row">
	                <div class="col-12 col-md-6 col-lg-3 white-text mt-4">
	                    <div class="box orange-bg">
	                        <div class="boxHead">
	                            <div class="homeBxicon">
	                            	<img src="images/icons/value_drivers_icon.png" width="60" class="img-fluid">
	                            </div>
	                        	<h3>Vision, Mission, Values</h3>
	                        </div>
	                    	<p>Our Vision is to enhance global healthcare through innovative and affordable biopharmaceuticals for patients, partners and healthcare systems across the globe.</p>
	                    	<a href="#" class="read_more">Know More</a>
	                    </div>                
	                </div>            
	                <div class="col-12 col-md-6 col-lg-3 white-text mt-4">
	                    <div class="box green-bg">
	                        <div class="boxHead">
	                        	<h3>Profitably Smart & Socially Good</h3>
	                        </div>
	                        <p>We are in a humanitarian business of making medicines to save human lives.</p>
	                        <a href="#" class="read_more">Know More</a>
	                    </div>
	                </div>
	            
	                <div class="col-sm-6 col-md-6 col-lg-3 white-text mt-4">
	                    <div class="box indigo-bg ">
	                        <div class="boxHead">
	                        	<h3>Differentiation & Innovation</h3>
	                        </div>
	                        <p>The pursuit of innovation and differentiation has given us a distinct competitive edge in the global marketplace.</p>
	                        <a href="#" class="read_more">Know More</a>
	                    </div>      
	                </div>
	            
	                <div class="col-sm-6 col-md-6 col-lg-3 white-text mt-4">
	                    <div class="box blue-bg">
	                        <div class="boxHead">
	                        	<h3>Quality & Reliability</h3>
	                        </div>
	                        <p>The pursuit of innovation and differentiation has given us a distinct competitive edge in the global marketplace.</p>
	                        <a href="#" class="read_more">Know More</a>
	                    </div>
	                </div>            
	            </div>
	        </div>
    </section>'	;
}
function press_release(){
	echo'<section class="pt-3 pb-4 sec_04 press_release">
	   <div class="container-fluid">
	      <div class="row">
	         <div class="col-12 col-lg-6">
	            <h2 class="border_head blue-text mb-3"><span>PRESS RELEASES </span></h2>
	            <div class="white-text skyblue-bg p-4 relative">
	               <div class="row">
	                  <div class="col-sm border_right">
	                     <h4 class="mb-0">Biocon’s Sterile Drug Product facility Receives EU GMP Certification</h4>
	                     <p class="mb-0"><small>Bengaluru, India, July 4, 2018</small></p>
	                  </div>
	                  <div class="col-sm">
	                     <h4 class="mb-0">Biocon retains economic interest in Etanercept collaboration with Mylan</h4>
	                     <p class="mb-0"><small>Bengaluru, India, June 28, 2018</small></p>
	                  </div>
	                  <div class="w-100 pb-4">
	                     <a href="#" class="read_more">Know More</a>
	                  </div>
	               </div>
	            </div>
	         </div>
	         <div class="col-12 col-lg-6 pt-4 pt-md-4 pt-lg-0">
	            <h2 class="border_head blue-text mb-3"><span>STOCK UPDATE</span></h2>
	            <div class="white-text blue-bg p-4 relative">
	               <div class="row text-center">
	                  <div class="col-sm border_right pt-2">
	                     <p class="mb-0">BSE</p>
	                     <h2 class="mb-0">606.60</h2>
	                  </div>
	                  <div class="col-sm pt-2">
	                     <p class="mb-0">NSE</p>
	                     <h2 class="mb-0">606.75</h2>
	                  </div>
	                  <div class="w-100 text-center pt-3">
	                     <a href="#" class="btn white-brdr-btn m-1">Overview</a>
	                  </div>
	               </div>
	            </div>
	            <div class="w-100 text-right">
	               <p><small><em><b>Disclaimer:</b> The stock updates may be delayed by up to 5 minutes. </em></small></p>
	            </div>
	         </div>
	      </div>
	   </div>
	   </div>
	</section>';	
}
function life_at_biocon(){
	echo'<section class="pt-5 pb-5 sec_04 orange-bg white-text mb-4 mb-md-5">
           <div class="container-fluid">
              <div class="row">
                 <div class="col-sm col-lg-6 pr-md-5">
                    <img src="images/icons/Home-Life-icon.png" class="img-fluid" width="80">
                    <h2 class="mb-2 mt-2 head">Life at Biocon</h2>
                    <p class="mb-1">We attract the best talent and provide them the opportunity to build an exciting career in an employee-friendly environment that is comparable to the best in the world. As a highly innovative and socially responsible company with a clear vision for the future, Biocon is the only Asian company to feature among the Top 10 Global Pharma & Biotech Employers in the Science Careers Top Employers list.</p>
                    <div class="w-100 pt-3">
                       <a href="#" class="btn white-brdr-btn m-1 text-center">Overview</a>
                    </div>
                 </div>
                 <div class="col-sm col-lg-6 ">
                    <!-- <div class="d-flex align-items-stretch">
                       <div class="p-2"><img src="images/home/life_at_biocon_01.jpg" class="img-fluid float-left"></div>
                       </div> -->
                    <div class="row pt-2 life_at_biocon">
                       <div class="col-md-5 p-2 ">
                          <img src="images/life_at_biocon_01.jpg" class="img-fluid float-left ">
                       </div>
                       <div class="col-md-7">
                          <div class="row">
                             <div class="col-sm p-2">
                                <img src="images/life_at_biocon_02.jpg" class="img-fluid float-left">
                             </div>
                             <div class="col-sm p-2">
                                <img src="images/life_at_biocon_03.jpg" class="img-fluid float-left">
                             </div>
                          </div>
                          <div class="row">
                             <div class="col-sm p-2">
                                <img src="images/life_at_biocon_04.jpg" class="img-fluid float-left">
                             </div>
                             <div class="col-sm p-2">
                                <img src="images/life_at_biocon_05.jpg" class="img-fluid float-left">
                             </div>
                          </div>
                       </div>
                    </div>
                 </div>
              </div>
           </div>
        </section>';
}
function biocon_live(){
	echo'<section class="pt-5 pb-5 sec_02 bio_live">
	   <div class="container-fluid">
	      <h2 class="border_head blue-text mb-3"><span>BIOCON LIVE</span></h2>
	      <div class="row">
	         <div class="col-sm-12 col-md-6 col-lg-6 col-xl-3 white-text mt-4">
	            <div class="box skyblue-bg">
	               <div class="boxHead">
	                  <h3>Biocon on Twitter</h3>
	               </div>
	               <div id="twitter_content" style="margin-bottom: 25px;border-radius: 0px;background: #fff;min-height: 220px">
	                  <a class="twitter-timeline" width="100%" href="https://twitter.com/Bioconlimited?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor" data-tabs="timeline"  data-width="300" data-height="200" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"></a>
	                  <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
	               </div>
	               <a href="#" class="read_more right-10">View on Twitter</a>
	            </div>
	         </div>
	         <div class="col-sm-12 col-md-6 col-lg-6 col-xl-3 white-text mt-4">
	            <div class="box green-bg">
	               <div class="boxHead">
	                  <div class="homeBxicon">
	                     <img src="images/biocon-conversation.png" class="img-fluid">
	                  </div>
	                  <h3></h3>
	               </div>
	               <div style="min-height: 220px">
	                  <img src="images/fda.png" class="img-fluid">
	                  <h4 class="mb-1 mt-2">First biosimilar from India to be approved by U.S. FDA</h4>
	                  <p><small><em>Posted by Biocon on August 13, 2018</em></small></p>
	               </div>
	               <a href="#" class="read_more right-10">View All</a>
	            </div>
	         </div>
	         <div class="col-sm-12 col-md-6 col-lg-6 col-xl-3 white-text mt-4">
	            <div class="box indigo-bg">
	               <div class="boxHead">
	                  <h3>Biocon in News</h3>
	               </div>
	               <div style="min-height: 220px">
	                  <img src="images/news_01.jpg" class="img-fluid">
	                  <h4 class="mb-1 mt-2">Biocon Q1 PAT seen up 70.8% YoY to Rs. 138.9 cr: Kotak</h4>
	                  <p><small><em>18 May, 2018 | Source: PTI</em></small></p>
	               </div>
	               <a href="#" class="read_more right-10">View All</a>
	            </div>
	         </div>
	         <div class="col-sm-12 col-md-6 col-lg-6 col-xl-3 white-text mt-4">
	            <div class="box blue-bg">
	               <div class="boxHead">
	                  <h3>Engaged in Affordable Innovation for Global Impact</h3>
	               </div>
	               <div style="min-height: 220px">
	                  <iframe width="100%" height="223" style="margin-bottom: 15px;" src="https://www.youtube.com/embed/EicKpqKKEyg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
	               </div>
	               <a href="#" class="read_more right-10">View All</a>
	            </div>
	         </div>
	      </div>
	   </div>
	</section>';
}
function our_growth_drivers(){
	echo'<section class="pt-2 pb-5 sec_02 our_growth_drivers">
	   <div class="container-fluid">
	      <h2 class="border_head blue-text mb-3"><span>OUR GROWTH DRIVERS</span></h2>
	      <p>Biocon’s four key growth verticals aim to deliver sustainable, long term value for patients, partners and healthcare systems across the globe. The fully integrated business model spans the complete drug value chain, from pre-clinical discovery to clinical development and to commercialisation of small molecule APIs, 
	         generic formulations, branded formulations and biologics including both novels & biosimilars.
	      </p>
	      <div class="row">
	         <div class="col-sm-6 col-md-6 col-lg-3 col-xl-3 white-text mt-4">
	            <div class="box blue-bg">
	               <div class="boxHead">
	                  <div class="homeBxicon">
	                     <img src="images/icons/groth_drivers_icon_01.png" width="60" class="img-fluid">
	                  </div>
	                  <h3>Biosimilars</h3>
	               </div>
	               <p>We have taken a range of novel biologics and biosimilars from ‘lab to market.’</p>
	               <a href="#" class="read_more">Know More</a>
	            </div>
	            <img src="images/home_sec_one_img_01.jpg" class="img-fluid">
	         </div>
	         <div class="col-sm-6 col-md-6 col-lg-3 col-xl-3 white-text mt-4">
	            <div class="box skyblue-bg">
	               <div class="boxHead">
	                  <div class="homeBxicon">
	                     <img src="images/icons/groth_drivers_icon_02.png" width="60" class="img-fluid">
	                  </div>
	                  <h3>Branded Formulations</h3>
	               </div>
	               <p>We are in a humanitarian business of making medicines to save human lives.</p>
	               <a href="#" class="read_more">Know More</a>
	            </div>
	            <img src="images/home_sec_one_img_02.jpg" class="img-fluid">
	         </div>
	         <div class="col-sm-6 col-md-6 col-lg-3 col-xl-3 white-text mt-4">
	            <div class="box green-bg ">
	               <div class="boxHead">
	                  <div class="homeBxicon">
	                     <img src="images/icons/groth_drivers_icon_03.png" width="60" class="img-fluid">
	                  </div>
	                  <h3>Small Molecules</h3>
	               </div>
	               <p>The pursuit of innovation and differentiation has given us a distinct competitive edge in the global marketplace.</p>
	               <a href="#" class="read_more">Know More</a>
	            </div>
	            <img src="images/home_sec_one_img_03.jpg" class="img-fluid">    
	         </div>
	         <div class="col-sm-6 col-md-6 col-lg-3 col-xl-3 white-text mt-4">
	            <div class="box indigo-bg">
	               <div class="boxHead">
	                  <div class="homeBxicon">
	                     <img src="images/icons/groth_drivers_icon_04.png" width="60" class="img-fluid">
	                  </div>
	                  <h3>Quality & Reliability</h3>
	               </div>
	               <p>The pursuit of innovation and differentiation has given us a distinct competitive edge in the global marketplace.</p>
	               <a href="#" class="read_more">Know More</a>
	            </div>
	            <img src="images/home_sec_one_img_04.jpg" class="img-fluid">  
	         </div>
	      </div>
	   </div>
	</section>';
}
?>