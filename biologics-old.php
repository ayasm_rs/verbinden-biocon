<?php include('include/html-codes.php');
wayTop();
 ?>
        <!--title-->
        <title>Biologics | Biocon</title>    
        <?php css();?>

    </head>
    <body>
        <?php nav(); ?>	         
        <!--BANNER SECTION-->
        <section class="main_banner inner-banner" style="background:url(images/banner-images/biologics.jpg);">
        	
                <div class="banner_content float-right d-flex flex-wrap align-content-center inner-content">
                    <h1 class="blue-text">Biologics</h1>
                   
                </div> 
                <nav aria-label="breadcrumb" class="mt-5 position-absolute pl-md-5 bottom-5">
                    <ol class="breadcrumb">
                    <li class="breadcrumb-item frist-one"><a href="/">Home</a></li>
                    <li class="breadcrumb-item frist-one"><a href="about-us.php">About Us</a></li>
                    <li class="breadcrumb-item frist-one"><a href="#">Manufacturing</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Biologics</li>
                    </ol>
                </nav> 
                 <div class="clearfix"></div>
        </section>
        <div class="clearfix"></div>
        <section class="pt-4 pb-4 pt-md-5 pb-md-5">
            <div class="container-fluid pl-md-5 pr-md-5">
                <h2 class="border_head blue-text"><span>A. GMP certifications from</span></h2>                 
                <div class="row">
                    <div class="col-12  white-text mt-4">
                        <div class="col-lg-8 float-left p-0">                            
                            <div class="box turquoise pt-4 min-hgt-300 d-flex flex-wrap align-content-center">
                                <div class="col-sm-6">
                                    <ul class="flags">
                                        <li><img src="images/flags/us.jpg">U.S. FDA</li>
                                        <li><img src="images/flags/ema.jpg">EMA</li>
                                        <li><img src="images/flags/tga.jpg">TGA, Australia</li>
                                        <li><img src="images/flags/pbda.jpg">PMDA, Japan</li>
                                        <li><img src="images/flags/mexico.jpg">COFEPRIS, Mexico</li>
                                        <li><img src="images/flags/ind.jpg">Central Drugs Standard</li>
                                        <li><img src="images/flags/ind.jpg">Control Organization (CDSCO), India</li>
                                    </ul>
                                </div>
                                <div class="col-sm-6">
                                    <ul class="flags">
                                        <li><img src="images/flags/food.jpg">Ministry of Food and 
Drug Safety (MFDS), South Korea </li>
                                        <li><img src="images/flags/brazil.jpg">ANVISA, Brazil  </li>
                                        <li><img src="images/flags/russia.jpg">Ministry of Health, Russia</li>
                                        <li><img src="images/flags/malaysia.jpg">NPRA, Ministry of Health, Malaysia</li>
                                        <li><img src="images/flags/turkey.jpg">Ministry of Health, Turkey </li>
                                        <li><img src="images/flags/africa.jpg">MCC, South Africa</li>
                                    </ul>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="col-lg-4 float-left p-0 min-hgt-300 d-none d-lg-block" style="background:url(images/gmp-certification.jpg) no-repeat;">                        	
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
            </div>
        </section>
        <section class="pb-4 pb-md-5">
            <div class="container-fluid pl-md-5 pr-md-5">
                <h2 class="border_head blue-text"><span>B. Facilities</span></h2>
				<div class="table-responsive">
					<table class="table table-striped table-bordered mt-4 custom-table text-center" >
						<thead class="royal-blue white-text">
							<tr>
							  <th scope="col" width="20%">FACILITY LOCATIONS</th>
							  <th scope="col" width="40%">PRODUCTS  MANUFACTURED</th>
							  <th scope="col" width="20%">IMAGE</th>
							  <th scope="col" width="20%">VIDEO</th>
							</tr>
						</thead>
						<tbody>
							<tr>
							  <th scope="row" class="align-middle royal-blue white-text">Bangalore</th>
							  <td class="text-left color-royal-blue">
								<p><strong>Drug Substances:</strong> Insulins, Monoclonal Antibodies & Other Recombinant Proteins.</p>
								<strong>Drug Products:</strong>
								<p>1. Sterile Injectables</p>
								- Vials<br/>
								- Lypholized Vials<br/>
								- Cartridges <br/>
								- Pre-filled Syringes<br/>
								<p>2. Devices</p>
								- Reusable pens<br/>
								- Disposable pens 
							</td>
							  <td>
								<img src="images/facilities.jpg" class="img-fluid mb-4" />
								<img src="images/facilities1.jpg" class="img-fluid" />
							  </td>
							  <td><img src="images/video1.jpg" class="img-fluid mb-4" />
								<img src="images/video1.jpg" class="img-fluid" /></td>
							</tr>
							<tr>
							  <th scope="row" class="align-middle  royal-blue white-text">Hyderabad</th>
							  <td class="text-left color-royal-blue">
								<p><strong>Drug Substances:</strong> rh-Insulin and insulin analogs.</p>
								 <strong>Drug Products:</strong>
								<p>1. Sterile Injectables</p>
								- Vials<br/>
								- Lypholized Vials<br/>
								- Cartridges <br/>
								- Pre-filled Syringes<br/>
								<p>2. Devices</p>
								- Disposable pens 
							  </td>
							  <td><img src="images/facilities2.jpg" class="img-fluid mb-4" />
								<img src="images/facilities1.jpg" class="img-fluid" /></td>
							  <td><img src="images/video1.jpg" class="img-fluid mb-4" />
								<img src="images/video1.jpg" class="img-fluid" /></td>
							</tr>
						</tbody>
					</table>
				</div>	
            </div>    
        </section>
         
         
        <section class="pb-4 pb-md-5 heading-with-content">
            <div class="container-fluid pl-md-5 pr-md-5">
                 <h2 class="border_head blue-text"><span> C. Technology Platforms</span></h2>
                <div class="row">
                    <div class="col-12 white-text mt-4 mb-4">
                        <div class="col-lg-8 float-left p-0">
                            <div class="box dark-orange min-hgt-300 d-flex flex-wrap align-content-center pb-0 ">
                                <div class="boxHead">
                                    <h3>1. Microbial Fermentation</h3>
                                </div>
                                <div class="boxHead no-border m-auto">
                                    <p>Biocon’s Pichia pastoris platform for expression of recombinant protein is our proprietary technology, which is applied in the recombinant human insulin (rh-insulin) and insulin analog product lines. We were the first company in the world to have successfully commercialized the large-scale manufacture of human insulin, using this proprietary technology. This resulted in the launch of our insulin brand, Insugen®, in 2004.</p>

                                    <p class="mb-0">Biocon's human insulin facility employs several sophisticated technologies and equipment used for the first time in India. The human insulin manufacturing process begins by fermenting a methylotrophic yeast wherein...
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 float-left p-0 min-hgt-300 d-none d-lg-block" style="background:url(images/technology.jpg) no-repeat;">
                            
                        </div>
                        <div class="clearfix"></div>
                    </div>                     
            </div>
            <div class="mb-4">
                <p>The final processing steps are carried out in high-quality, electropolished, steam sterilizable equipment in a controlled environment with restricted entry through multiple airlocks. The entire process is largely automated and monitored continuously in a central control room by trained personnel.</p>
                <p>Investments made in the human insulin facility can support other products such as in Biocon’s insulin analogs. The expertise developed in protein processing, formulation and aseptic fill-finish is also leveraged for commercializing therapeutic peptides.</p>
            </div>
            <div class="col-12 white-text mt-4 mb-4">
                <div class="row">
                <div class=" col-lg-4 float-left p-0 min-hgt-300 d-none d-lg-block" style="background:url(images/cell-culture.jpg) no-repeat;">
                            
                        </div>
                        <div class="col-lg-8 float-left p-0">
                            <div class="box turquoise min-hgt-300 d-flex flex-wrap align-content-center pb-0 ">
                                <div class="boxHead">
                                    <h3>2. Mammalian Cell Culture</h3>
                                </div>
                                <div class="boxHead no-border m-auto">
                                    <p>Biocon has established a large-scale, multi-product, cell culture facility which is the core technology in the manufacture of several important biologicals such as monoclonal antibodies. Our consistent and scalable mammalian CHO and NSO cell-based expression platforms are helping us deliver novel and biosimilar monoclonal antibodies. Mammalian cells, being extremely sensitive to growth conditions such as shear, require equipment and processes that are quite different from microbial fermentation. Much of the nutrient media needs to be filter sterilized instead of steam sterilized to retain its nutritive value. The design of agitation...</p>                                   
                                </div>
                            </div>
                        </div>
                        
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="mb-4">
                <p>TOur advanced analytical capability, which is anchored in cutting-edge tools and latest orthogonal approaches, guarantees the high-quality and consistency of our products. All the operations are undertaken in clean rooms with strictly controlled environment to provide a high-level of assurance in consistently achieving the best product quality.</p>
                <p>This sophisticated manufacturing facility has been instrumental in supporting our ‘lab to market’ journey for biologics started with two novel monoclonal antibodies, Nimotuzumab for head and neck cancer and Itolizumab for chronic psoriasis.
</p>
            </div>
            <div class="col-12 white-text mt-4 mb-4">
                <div class="row">
                
                        <div class="col-lg-8 float-left p-0">
                            <div class="box meduim-blue min-hgt-300 d-flex flex-wrap align-content-center pb-0 ">
                                <div class="boxHead">
                                    <h3>3. Formulations and Fill-Finish</h3>
                                </div>
                                <div class="boxHead no-border m-auto">
                                    <p>Biocon's expertise in Formulation & Product Science enables it to convert drug substances into formulations for transfer into vials, cartridges and pre-filled syringes at our biologics drug product facilities.</p>
                                    <p>The launch of recombinant human insulin in 2004 represented Biocon’s foray into the aseptic formulations space. In order to facilitate the complex formulation requirements for insulin with different pharmacokinetic profiles, and also to support the extensive pipeline of protein, peptide and antibody products, Biocon commissioned its first aseptic formulation and fill-finish facility. This facility is equipped to deliver liquid and...</p>                              
                                </div>
                            </div>
                        </div>
                        <div class=" col-lg-4 float-left p-0 min-hgt-300 d-none d-lg-block" style="background:url(images/fill-finish.jpg) no-repeat;">
                            
                        </div>
                        <div class="clearfix"></div>

                    </div>
                </div>
                 <div class="mb-4">
                <p>This facility has been approved by several national regulatory agencies around the world and fulfills the global demand for Biocon’s sterile injectable products. Our generic insulin business is complemented by a comprehensive delivery device basket. Biocon offers reusable injector devices branded INSUPEN® and INSUPEN® EZ, developed along with Haselmeier, to patients.</p>
                <p>Our portfolio also includes Basalog One™, our state-of-the-art Insulin Glargine disposable pen, which we launched in India in 2015 and in Japan in 2016. The pen has been designed in collaboration with Becton Dickinson & Co., a leading global medical devices company and is specially customised for Biocon. We commissioned our new 100,000 square-feet, state-of-the-art insulin delivery devices assembly facility in Bangalore in 2015. It is equipped with the latest Swiss technology and approved by local and international regulators. The facility leverages India’s manufacturing expertise to produce new generation, patient-friendly medical devices that meet the highest quality and safety standards.</p>
            </div>
        </section>
        <?php footer(); ?>
    
        <?php script(); ?>  
    </body>
</html>  
 