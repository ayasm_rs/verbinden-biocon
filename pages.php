<?php
include('include/html-codes.php');
include('main-control/include/mysqli_connect.php');
include('include/function.php');
$function = new common_function($connect);
wayTop();

$pid = mysqli_real_escape_string($connect, $_GET['pid']);
$pre = mysqli_real_escape_string($connect, @$_GET['cat']);
$page = mysqli_real_escape_string($connect, @$_GET['page']);

$select_page = "SELECT *,p.id as pid FROM page p LEFT JOIN category c ON p.cat_id=c.id WHERE page_name='" . $pid . "'";
$run = mysqli_query($connect, $select_page);
if (mysqli_num_rows($run) == 0) {
    header("Location: 404.php");
}
while ($row = mysqli_fetch_array($run)) {
    $id = $row["pid"];
    $page_title = $row["page_title"];
    $meta_desc = $row["meta_desc"];
    $page_name = $row["page_name"];
    $page_img = $row["page_img"];
    $page_heading = $row["page_heading"];
    $page_description = $row["page_description"];
    $temp_ids = $row["temp_ids"];
    $created_by = $row["created_by"];
    $cate_name = $row["cate_name"];
    $img_pos = $row["img_pos"];
    $page_img = $row["page_img"];
}

//echo $pid;exit;
?>
<!--title-->
<title>Biocon - <?php echo $page_title; ?></title>
<?php css(); ?>
<script type='text/javascript' src='https://www.gstatic.com/charts/loader.js'></script>

<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.0/dist/jquery.validate.min.js"></script>
</head>
<body class="<?php echo str_replace("-", "_", $pid); ?>">
<?php nav(); ?>
<!--Banner-->
<section class="main_banner inner-banner mobile_banner"
         style="background: #a1aba6 url(images/<?php echo $page_img; ?>);">

    <div class="banner_content mobile_content float-right d-flex flex-wrap align-content-center inner-content">
        <h1 class="blue-text pt-0 pt-md-5 mobile_h1"><?php echo $page_heading; ?></h1>

        <p class="gray-text mb-0 pb-0 pb-md-5 mobile_p"><?php echo $page_description; ?></p>
    </div>
    <nav aria-label="breadcrumb" class="mt-5 position-absolute pl-md-5 bottom-5">
        <ol class="breadcrumb">
            <li class="breadcrumb-item frist-one"><a href="#">Home</a></li>
            <?php if ($cate_name != "") echo '<li class="breadcrumb-item frist-one"><a href="#">' . $cate_name . '</a></li>'; ?>
            <li class="breadcrumb-item active" aria-current="page"><?php echo $page_title; ?></li>
        </ol>
    </nav>

</section>
<div class="clearfix"></div>
<?php
$temp_script = "";
$temp_data = "";
$tmp_array = explode("~", $temp_ids);
$i = 1;
foreach ($tmp_array as $key) {
    $tmp_function = 'template' . $key;
    try {
        if (method_exists($function, $tmp_function)) {
            $temp_datas = $function->$tmp_function($id, $i, $pre, $page, $pid);

        } else {

            //$temp_datas = "Widget ID:" . $key . " not found";
            $temp_datas = $function->getFromWidgetFactory($key, $id, $i);

        }

    } catch (Exception $e) {
        //var_dump($e);
        $temp_datas = "Widget" . ($key?' ID:$key':'') . " not found";
        var_dump($e);
    }
    if (strpos($temp_datas, '~~') !== false) {
        $temp_datas = explode("~~", $temp_datas);
        $temp_data .= $temp_datas[0];
        $temp_script .= $temp_datas[1];
    } else {
        $temp_data .= $temp_datas;
    }
    $i++;
}
echo $temp_data;

?>


<?php footer(); ?>

<?php script();
echo $temp_script;
?>
</body>
</html>